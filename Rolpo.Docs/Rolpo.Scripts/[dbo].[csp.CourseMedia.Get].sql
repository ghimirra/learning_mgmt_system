

/****** Object:  StoredProcedure [dbo].[csp.CourseMedia.Get]    Script Date: 6/26/2018 4:42:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*
Created by: Prashant 
Created on: 6/26/2018 2:01:59 PM 
DESC: GET DATA FROM TABLE [dbo].[CourseMedia]


EXEC dbo.[csp.CourseMedia.Get]
@CourseMediaId = 11 
,@CourseId = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.CourseMedia.Get]
(
	@CourseMediaId 		INT = NULL 
	,@CourseId 				INT = NULL 
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber			INT = 1 
	,@PageSize 				INT = 20 
	,@ShowAll				INT = 0 

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TC.CourseMediaId)) AS RowNumber 
				,TC.[CourseMediaId] AS [CourseMediaId]
				,TC.[OrganizationId] AS [OrganizationId]
				,TC.[CourseId] AS [CourseId]
				,ISNULL(TC.[TopicId],0) AS [TopicId]
				,TC.[MediaURL] AS [MediaURL]
				,TC.[MediaType] AS [MediaType]
				,ISNULL(TC.[Remarks],'') AS [Remarks],
				'Module' AS Module,
				'Media' AS MediaTypeName
				--,ISNULL(TM.ModuleTitle,'') AS Module
				--,ISNULL(TMD.[MediaName],'') AS MediaTypeName			
				,COUNT(TC.[CourseMediaId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_CourseMedia] TC WITH(NOLOCK) 			
			--INNER  JOIN
			--	[dbo].[TBL_Modules] TM WITH(NOLOCK) ON TM.AssociatedCourseId=TC.CourseId
			--INNER JOIN
			--	[dbo].[TBL_Media] TMD WITH(NOLOCK) ON TMD.MediaId=TC.MediaType
			WHERE 
				(ISNULL(@CourseMediaId,0) = 0 OR TC.[CourseMediaId] = @CourseMediaId )
				AND (ISNULL(@CourseId,0) = 0 OR TC.[CourseId] = @CourseId )
				AND TC.DeletedDate IS NULL 
		) 


		SELECT 
			RowNumber 
			,[CourseMediaId]
			,[OrganizationId]
			,[CourseId]
			,[TopicId]
			,[MediaURL]
			,[MediaType]
			,[Remarks]	
			,[Module]
			,[MediaTypeName]		
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 

	
GO



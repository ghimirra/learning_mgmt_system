USE [NEA_Trainning]
GO
/****** Object:  StoredProcedure [dbo].[csp.BatchTrainee.GetForReport]    Script Date: 7/19/2018 5:35:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 6/24/2018 12:08:36 PM 
DESC: GET DATA FROM TABLE [dbo].[BatchTrainee]


EXEC dbo.[csp.BatchTrainee.GetForReport]
@BatchTraineeId = NULL 
,@BatchId = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

ALTER PROCEDURE [dbo].[csp.BatchTrainee.GetForReport]
(
	@BatchTraineeId		INT = NULL 
	,@BatchId  			INT = NULL 
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 
	BEGIN 
		DECLARE @CurrentDate DATETIME  = GETUTCDATE();

		;WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TB.BatchTraineeId)) AS RowNumber 
				,TB.[BatchTraineeId] AS [BatchTraineeId] 
				,TB.[BatchId] AS [BatchId]
				,TB.[MemberId] AS [MemberId] 
				,COUNT(TB.[BatchTraineeId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_BatchTrainee] TB WITH(NOLOCK)  
			
			WHERE 
				(ISNULL(@BatchTraineeId,0) = 0 OR TB.[BatchTraineeId] = @BatchTraineeId )
				AND (ISNULL(@BatchId,0) = 0 OR TB.[BatchId] = @BatchId )
				AND TB.DeletedDate IS NULL 
		) 


		SELECT 
			RowNumber 
			,[BatchTraineeId] 
			,T.[BatchId]
			,B.BatchCode
			,B.BatchName 
			,TM.MemberId
			,ISNULL(TM.[ProfileImageUrl],'') AS [ProfileImageUrl] 
			,TM.FirstName + ' ' + TM.LastName AS TraineeName
			,ISNULL(TL.TraineeLevelName,'') AS TraineeLevelName
			,TM.Position
			,TM.CompanyName
			,TM.Department
			,B.[BatchStartDate] AS [BatchStartDate]
			,B.[BatchEndDate]  AS [BatchEndDate]
			,CONVERT(VARCHAR,DATEADD(MINUTE,345,B.BatchStartDate),107) AS BatchStartDateLOCAL
			,CONVERT(VARCHAR,DATEADD(MINUTE,345,B.BatchEndDate),107) AS BatchEndDateLOCAL 
			,CAST((DATEDIFF(DAY, ISNULL(B.BatchStartDate,@CurrentDate), ISNULL(B.BatchEndDate,@CurrentDate))+1) AS VARCHAR(3)) + ' DAYS' AS Duration
			,[TotalCount]
		FROM 
			TMP_TBL T
		INNER JOIN 
			[dbo].[TBL_Batch] B WITH(NOLOCK) ON (B.BatchId = T.BatchId)
		INNER JOIN
			[dbo].[TBL_Member] TM WITH(NOLOCK) ON (TM.MemberId = T.MemberId)
		LEFT JOIN 
			[dbo].[TBL_TraineeLevel] TL WITH(NOLOCK) ON (TL.TraineeLevelId = TM.LevelId)
	 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
		AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
		AND B.DeletedDate IS NULL 
		AND TM.DeletedDate IS NULL  
	END 

 
/*
Created by: Prashant 
Created on: 17/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[Member]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.Member.Update]
	@ActionType = 'ADD' 
	,@MemberId = NULL 
	,@OrganizationId = NULL 
	,@FirstName = NULL 
	,@LastName = NULL 
	,@SystemRoleId = NULL 
	,@Designation = NULL 
	,@Level = NULL 
	,@Department = NULL 
	,@Address = NULL 
	,@PrimaryContactEmail = NULL 
	,@Phone = NULL 
	,@Mobile = NULL 
	,@ProfileImageUrl = NULL 
	,@Education = NULL 
	,@Notes = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_Member] 

	
  ALTER TABLE [dbo].[TBL_Member]
  ADD LevelId INT NULL


*/

ALTER PROCEDURE [dbo].[csp.Member.Update]
(
	@ActionType			VARCHAR(10) 
	,@MemberId 			INT = NULL 
	,@OrganizationId	UNIQUEIDENTIFIER = NULL 
	,@FirstName			NVARCHAR(150) = NULL 
	,@LastName 			NVARCHAR(150) = NULL 
	,@SystemRoleId  	INT = NULL 
	,@Designation   	NVARCHAR(250) = NULL 
	,@LevelId    		INT = NULL 
	,@Department    	NVARCHAR(250) = NULL 
	,@Address  			NVARCHAR(250) = NULL 
	,@PrimaryContactEmail	VARCHAR(150) = NULL 
	,@Phone    				VARCHAR(25) = NULL 
	,@Mobile   				VARCHAR(25) = NULL 
	,@ProfileImageUrl    	NVARCHAR(500) = NULL 
	,@Education			NVARCHAR(200) = NULL 
	,@Notes    			NVARCHAR(250) = NULL 
	,@Position			NVARCHAR(200)=NULL
	,@Company			NVARCHAR(200)=NULL
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnMemberId 	INT =	0 OUTPUT 
)
AS 

SET @ReturnMemberId = @MemberId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_Member] WHERE MemberId = @MemberId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_Member] 
				( 
					[OrganizationId]  
					,[FirstName]  
					,[LastName]  
					,[SystemRoleId]  
					,[Designation]  
					,[LevelId]  
					,[Department]  
					,[Address]  
					,[PrimaryContactEmail]  
					,[Phone]  
					,[Mobile]  
					,[ProfileImageUrl]  
					,[Education]  
					,[Notes]  
					,[Position]
					,[CompanyName]
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@FirstName  
					,@LastName  
					,@SystemRoleId  
					,@Designation  
					,@LevelId  
					,@Department  
					,@Address  
					,@PrimaryContactEmail  
					,@Phone  
					,@Mobile  
					,@ProfileImageUrl  
					,@Education  
					,@Notes  
					,@Position
					,@Company
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnMemberId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_Member] 
				SET 
					[OrganizationId]		 = @OrganizationId  
					,[FirstName]			 = @FirstName  
					,[LastName] 			 = @LastName  
					,[SystemRoleId]  		 = @SystemRoleId  
					,[Designation]   		 = @Designation  
					,[LevelId]    			 = @LevelId
					,[Department]    		 = @Department  
					,[Address]  			 = @Address  
					,[PrimaryContactEmail]	 = @PrimaryContactEmail  
					,[Phone]    			 = @Phone  
					,[Mobile]   			 = @Mobile  
					,[ProfileImageUrl]    	 = @ProfileImageUrl  
					,[Education]			 = @Education  
					,[Notes]    			 = @Notes  
					,[Position]				 =@Position
					,[CompanyName]			 =@Company
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE MemberId = @MemberId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_Member] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE MemberId = @MemberId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 


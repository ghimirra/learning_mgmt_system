 ALTER TABLE [dbo].[UsersInRoles] DROP CONSTRAINT [UsersInRole_User]
GO
ALTER TABLE [dbo].[UsersInRoles] DROP CONSTRAINT [UsersInRole_Role]
GO
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [User_Application]
GO
ALTER TABLE [dbo].[Roles] DROP CONSTRAINT [RoleEntity_Application]
GO
ALTER TABLE [dbo].[Profiles] DROP CONSTRAINT [ProfileEntity_User]
GO
ALTER TABLE [dbo].[Memberships] DROP CONSTRAINT [MembershipEntity_User]
GO
ALTER TABLE [dbo].[Memberships] DROP CONSTRAINT [MembershipEntity_Application]
GO
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserLogins] DROP CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserClaims] DROP CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
/****** Object:  Table [dbo].[UsersInRoles]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[UsersInRoles]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[Users]
GO
/****** Object:  Table [dbo].[TBL_TraineeLevel]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_TraineeLevel]
GO
/****** Object:  Table [dbo].[TBL_SystemRole]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_SystemRole]
GO
/****** Object:  Table [dbo].[TBL_Status]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_Status]
GO
/****** Object:  Table [dbo].[TBL_QueuedEmail]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_QueuedEmail]
GO
/****** Object:  Table [dbo].[TBL_Organization]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_Organization]
GO
/****** Object:  Table [dbo].[TBL_Modules]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_Modules]
GO
/****** Object:  Table [dbo].[TBL_Member]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_Member]
GO
/****** Object:  Table [dbo].[TBL_Media]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_Media]
GO
/****** Object:  Table [dbo].[TBL_EmailFormat]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_EmailFormat]
GO
/****** Object:  Table [dbo].[TBL_CourseTraineeLevel]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_CourseTraineeLevel]
GO
/****** Object:  Table [dbo].[TBL_CoursePeriodType]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_CoursePeriodType]
GO
/****** Object:  Table [dbo].[TBL_CourseMedia]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_CourseMedia]
GO
/****** Object:  Table [dbo].[TBL_CourseLevel]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_CourseLevel]
GO
/****** Object:  Table [dbo].[TBL_CourseCategory]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_CourseCategory]
GO
/****** Object:  Table [dbo].[TBL_Course]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_Course]
GO
/****** Object:  Table [dbo].[TBL_BatchTrainer]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_BatchTrainer]
GO
/****** Object:  Table [dbo].[TBL_BatchTrainee]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_BatchTrainee]
GO
/****** Object:  Table [dbo].[TBL_BatchSummary]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_BatchSummary]
GO
/****** Object:  Table [dbo].[TBL_BatchCourse]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_BatchCourse]
GO
/****** Object:  Table [dbo].[TBL_Batch]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[TBL_Batch]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[Roles]
GO
/****** Object:  Table [dbo].[Profiles]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[Profiles]
GO
/****** Object:  Table [dbo].[NLogEntries]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[NLogEntries]
GO
/****** Object:  Table [dbo].[Memberships]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[Memberships]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[AspNetUsers]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[AspNetUserRoles]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[AspNetUserLogins]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[AspNetUserClaims]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[AspNetRoles]
GO
/****** Object:  Table [dbo].[Applications]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP TABLE [dbo].[Applications]
GO
/****** Object:  StoredProcedure [dbo].[csp.TraineeLevel.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.TraineeLevel.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.TraineeLevel.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.TraineeLevel.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.Trainee.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.Trainee.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.TableColumns.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.TableColumns.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.Status.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.Status.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.Status.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.Status.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.QueuedEmail.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.QueuedEmail.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.QueuedEmail.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.QueuedEmail.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.Modules.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.Modules.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.Modules.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.Modules.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.Member.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.Member.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.Member.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.Member.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.Media.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.Media.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.Media.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.Media.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.EmailFormat.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.EmailFormat.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.EmailFormat.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.EmailFormat.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.DDLItems.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.DDLItems.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.CoursePeriodType.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.CoursePeriodType.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.CoursePeriodType.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.CoursePeriodType.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.CourseMedia.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.CourseMedia.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.CourseMedia.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.CourseMedia.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.CourseLevel.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.CourseLevel.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.CourseLevel.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.CourseLevel.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.CourseCategory.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.CourseCategory.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.CourseCategory.GetParents]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.CourseCategory.GetParents]
GO
/****** Object:  StoredProcedure [dbo].[csp.CourseCategory.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.CourseCategory.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.Course.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.Course.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.Course.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.Course.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.BatchTrainer.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.BatchTrainer.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.BatchTrainer.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.BatchTrainer.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.BatchTrainee.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.BatchTrainee.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.BatchTrainee.GetNotInList]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.BatchTrainee.GetNotInList]
GO
/****** Object:  StoredProcedure [dbo].[csp.BatchTrainee.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.BatchTrainee.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.BatchSummary.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.BatchSummary.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.BatchSummary.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.BatchSummary.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.BatchCourse.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.BatchCourse.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.BatchCourse.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.BatchCourse.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.Batch.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.Batch.Update]
GO
/****** Object:  StoredProcedure [dbo].[csp.Batch.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.Batch.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.ASPNETUsers.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
DROP PROCEDURE [dbo].[csp.ASPNETUsers.Get]
GO
/****** Object:  StoredProcedure [dbo].[csp.ASPNETUsers.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Created by: Prashant 
Created on: 6/1/2016 12:59:11 PM 
DESC: GET DATA FROM TABLE [dbo].[ASPNETUsers]


EXEC dbo.[csp.ASPNETUsers.Get]
@Id = NULL
,@FirstName = '' 
,@LastName = '' 
,@Email = ''
--,@UserId = '85b25794-5974-4b2b-bd6b-04b9fd44059d' 
,@PageNumber = 1 
,@PageSize	= 50 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.ASPNETUsers.Get]
(
	@Id  					NVARCHAR(256) = NULL 
	,@FirstName				VARCHAR(100) = NULL 
	,@LastName 				VARCHAR(100) = NULL 
	,@Email    				VARCHAR(100) = NULL 
	,@SearchText			NVARCHAR(100) = NULL
	,@UserId				NVARCHAR(256) = NULL 
	,@PageNumber			INT = 1 
	,@PageSize 				INT = 20 
	,@ShowAll				INT = 0 

)
AS 
	BEGIN
	
		-- Check if current user is in role Admin/SuperAdmin
		DECLARE @IsSuperAdmin BIT, @IsManager BIT
		
		SELECT 
			@IsManager = 1 
		FROM 
			[dbo].[AspNetUserRoles] UR WITH(NOLOCK)
		LEFT JOIN 
			[dbo].[AspNetRoles] AR WITH(NOLOCK) ON AR.Id = UR.RoleId
		WHERE 
			UR.UserId = @UserId 
			AND AR.Name IN ('ADMIN')

		SELECT 
			@IsSuperAdmin = 1 
		FROM 
			[dbo].[AspNetUserRoles] UR WITH(NOLOCK)
		LEFT JOIN 
			[dbo].[AspNetRoles] AR WITH(NOLOCK) ON AR.Id = UR.RoleId
		WHERE 
			UR.UserId = @UserId 
			AND AR.Name IN ('SUPERADMIN' )
		 
		 SET @IsSuperAdmin = ISNULL(@IsSuperAdmin,0)

		  
		 SET @IsManager = ISNULL(@IsManager, @IsSuperAdmin )

		-- Create Roles TABLE
		SELECT 
			R.RoleGroup
			,R.RoleId
			,R.RoleName
			,R.DisplayName
			,R.RolePriority
			,RO.RoleOrderId
			,'false' AS Value
		INTO #Roles
		FROM 
		(
			SELECT 
			RoleGroup AS RoleGroup
			,Id AS RoleId
			,Name AS RoleName
			,SUBSTRING(Name, CASE CHARINDEX('_', Name) WHEN 0 THEN 0 ELSE CHARINDEX('_', Name)+1 END, 1000) AS DisplayName
			,CASE WHEN RoleGroup = 'ALL' THEN 0 ELSE 1 END AS RolePriority
			FROM [dbo].[AspNetRoles]
			)R
			INNER JOIN 
			(
			SELECT  RoleOrderId, RoleName
			FROM    (VALUES(1, 'SUPERADMIN'), 
					(2, 'ADMIN'),
					(4, 'COORDINATOR')) AS RoleOrder(RoleOrderId, RoleName)
			) RO ON RO.RoleName =  R.DisplayName
		WHERE R.RoleName NOT IN ('SUPERADMIN')
		AND (@IsSuperAdmin = 1 OR R.RoleName NOT IN ('ADMIN'))
		


		-- Create Users TABLE
		SELECT  
			AU.Id
			,AU.UserName
		INTO #Users
		FROM [dbo].[AspNetUsers] AU WITH(NOLOCK)
		WHERE  AU.Id NOT IN (
				SELECT 
					AU.Id
				FROM [dbo].[AspNetUsers] AU WITH(NOLOCK)
				INNER JOIN 
					[dbo].[AspNetUserRoles] UR WITH(NOLOCK) ON UR.UserId = AU.Id
				INNER JOIN 
					(
					SELECT Id, Name FROM [AspNetRoles] WITH(NOLOCK) WHERE Name NOT IN (SELECT RoleName FROM #Roles)
					) AR ON AR.Id = UR.RoleId
			)
		GROUP BY AU.Id, AU.UserName

	
		;WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TA.Id)) AS RowNumber 
				,TA.[Id] AS [Id]
				,ISNULL(TA.[FirstName],'') AS [FirstName]
				,ISNULL(TA.[LastName],'') AS [LastName]
				,ISNULL(TA.[Email],'') AS [Email]
				,ISNULL(TA.[UserName],'') AS [UserName]
				,ISNULL(TA.[StaffId],0) AS [StaffId]
				,COUNT(TA.[Id]) OVER () AS TotalCount 
			FROM 
				[dbo].[AspNetUsers] TA WITH(NOLOCK) 
			INNER JOIN 
				#Users U ON (U.Id = TA.Id)
				
			WHERE 
				((ISNULL(@Id,'') = '' AND TA.[Id] <> @UserId) OR TA.[Id] = @Id )
				AND (ISNULL(@FirstName,'') = '' OR TA.[FirstName] LIKE '%'+@FirstName+'%'  )
				AND (ISNULL(@LastName,'') = '' OR TA.[LastName]  LIKE '%'+@LastName+'%'  )
				AND (ISNULL(@Email,'') = '' OR TA.[Email]  LIKE '%'+@Email+'%'  )
				
		) 


		SELECT 
			RowNumber 
			,[Id]
			,[UserName]
			,[FirstName]
			,[LastName]
			,[Email]
			,[StaffId]
			,'[' + ISNULL( SUBSTRING(
			(
				SELECT ','
						+'{' 
						+ '"RoleGroup": "'+ R.RoleGroup + '"'
						+ ',"RoleId": "'+ R.RoleId + '"'
						+ ',"RoleName": "'+ R.RoleName + '"'
						+ ',"DisplayName": "'+ R.DisplayName + '"'
						+ ',"Value": '+ CASE WHEN UR.UserId IS NULL THEN 'false' ELSE 'true' END 
						+ '}' 
						AS [text()]
				From 
					#Roles R WITH(NOLOCK)
				LEFT JOIN 
					[dbo].[AspNetUserRoles] UR WITH(NOLOCK) ON UR.[UserId] = T.Id AND UR.RoleId = R.RoleId
					ORDER BY R.RolePriority, R.RoleOrderId
				For XML PATH ('')
				), 2, 10000
			),'') + ']' AS RolesJSON
			,[TotalCount]
		FROM TMP_TBL T
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
			AND (ISNULL(@SearchText,'') = '' OR 
								(([FirstName] LIKE '%'+@SearchText+'%') 
								OR ([LastName] LIKE '%'+@SearchText+'%') 
								OR ([Email] LIKE '%'+@SearchText+'%') 
								)
				)


		SELECT * FROM #Roles ORDER BY RolePriority, RoleOrderId


	END 

















GO
/****** Object:  StoredProcedure [dbo].[csp.Batch.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 6/21/2018 4:07:53 PM 
DESC: GET DATA FROM TABLE [dbo].[Batch]


EXEC dbo.[csp.Batch.Get]
@BatchId = NULL 
,@BatchCode = NULL 
,@Coordinator = NULL 
,@BatchStartDate = NULL 
,@BatchEndDate = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.Batch.Get]
(
	@BatchId  			INT = NULL 
	,@BatchCode			VARCHAR(100) = NULL 
	,@Coordinator   	NVARCHAR(200) = NULL 
	,@BatchStartDate	DATETIME = NULL 
	,@BatchEndDate  	DATETIME = NULL 
	,@SearchText		NVARCHAR(100) = NULL
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 
	BEGIN 

		DECLARE @FromDate DATE
		DECLARE @Todate DATE
		DECLARE @CurrentDate DATE = GETDATE()
		DECLARE @TimeOffSet  INT=NULL
		SET  @TimeOffSet=(SELECT TimeOffsetInMins    FROM [dbo].[TBL_Organization] WHERE OrganizationId IN (SELECT OrganizationId FROM [dbo].[AspNetUsers] WHERE Id=@UserId))					
		IF ISDATE(@BatchStartDate)=1
			SET @FromDate = CAST(@BatchStartDate AS DATE)
		ELSE
			SET @FromDate = '2000-01-01'
		
		IF ISDATE(@BatchEndDate)=1
			SET @Todate = CAST(@BatchEndDate as DATE)
		ELSE
			SET @Todate = '2100-01-01'


		;WITH TMP_TBL AS 
		( 
			SELECT  
				TB.[BatchId] AS [BatchId] 
			FROM 
				[dbo].[TBL_Batch] TB WITH(NOLOCK) 
			LEFT JOIN 
				[dbo].[TBL_BatchCourse] TBC WITH(NOLOCK) ON (TBC.BatchId  = TB.BatchId)
			LEFT JOIN 
				[dbo].[TBL_Course] C WITH(NOLOCK) ON (C.CourseId = TBC.CourseId)
			LEFT JOIN 
				[dbo].[TBL_CourseLevel] CL WITH(NOLOCK) ON (CL.CourseLevelId = C.CourseLevelId)
			WHERE 
				(ISNULL(@BatchId,0) = 0 OR TB.[BatchId] = @BatchId )
				AND (ISNULL(@BatchCode,'') = '' OR TB.[BatchCode] = @BatchCode )
				AND (ISNULL(@Coordinator,'') = '' OR TB.[Coordinator] = @Coordinator )
				AND (CAST(ISNULL(TB.[BatchStartDate],@CurrentDate) AS DATE) BETWEEN @FromDate AND @Todate)
				AND (CAST(ISNULL(TB.[BatchEndDate],@CurrentDate) AS DATE) BETWEEN @FromDate AND @Todate) 
				AND (
						(ISNULL(@SearchText,'' ) = '')
						OR (TB.BatchCode LIKE '%'+@SearchText+'%')
						OR (TB.BatchName LIKE '%'+@SearchText+'%')
						OR (C.CourseCode LIKE '%'+@SearchText+'%')
						OR (C.CourseFullName LIKE '%'+@SearchText+'%')
						OR (CL.LevelName LIKE '%'+@SearchText+'%')
					)
				AND TB.DeletedDate IS NULL 
				AND C.DeletedDate IS NULL
				AND CL.DeletedDate IS NULL
				AND TBC.DeletedDate IS NULL
		) 

		SELECT 
			(ROW_NUMBER() OVER (ORDER BY BatchId)) AS RowNumber 
			,BatchId
		INTO #TMP_Batch
		FROM TMP_TBL
		GROUP BY BatchId



		SELECT 
				T.RowNumber
				,T.BatchId
				,TB.[BatchCode] AS [BatchCode]
				,ISNULL(TB.[BatchName],'') AS [BatchName]
				,ISNULL(TB.[BatchLevel],'') AS [BatchLevel]
				,ISNULL(TB.[BatchCategory],'') AS [BatchCategory]
				,ISNULL(TB.[Coordinator],'') AS [Coordinator]
				,ISNULL(TB.[ASSCoordinator],'') AS [ASSCoordinator]
				,TB.[BatchStartDate] AS [BatchStartDate]
				,TB.[BatchEndDate]  AS [BatchEndDate]
				,TB.[BatchSummary]  AS [BatchSummary] 
				,COUNT(T.[BatchId]) OVER () AS TotalCount  
		FROM #TMP_Batch T
		INNER JOIN 
			 [dbo].[TBL_Batch] TB WITH(NOLOCK) ON (TB.BatchId = T.BatchId)
		WHERE 
			(@ShowAll = 1 OR (T.RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR T.RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 


GO
/****** Object:  StoredProcedure [dbo].[csp.Batch.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 21/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[Batch]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.Batch.Update]
	@ActionType = 'ADD' 
	,@BatchId = NULL 
	,@OrganizationId = NULL 
	,@BatchCode = NULL 
	,@BatchName = NULL 
	,@BatchLevel = NULL 
	,@BatchCategory = NULL 
	,@Coordinator = NULL 
	,@ASSCoordinator = NULL 
	,@BatchStartDate = NULL 
	,@BatchEndDate = NULL 
	,@BatchSummary = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_Batch] 


*/

CREATE PROCEDURE [dbo].[csp.Batch.Update]
(
	@ActionType			VARCHAR(10) 
	,@BatchId  			INT = NULL 
	,@OrganizationId	UNIQUEIDENTIFIER = NULL 
	,@BatchCode			VARCHAR(100) = NULL 
	,@BatchName			VARCHAR(500) = NULL 
	,@BatchLevel    	NVARCHAR(100) = NULL 
	,@BatchCategory 	NVARCHAR(100) = NULL 
	,@Coordinator   	NVARCHAR(200) = NULL 
	,@ASSCoordinator	NVARCHAR(200) = NULL 
	,@BatchStartDate	DATETIME = NULL 
	,@BatchEndDate  	DATETIME = NULL 
	,@BatchSummary  	NVARCHAR(MAX) = NULL 
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnBatchId 	INT =	0 OUTPUT 
)
AS 

SET @ReturnBatchId = @BatchId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_Batch] WHERE BatchId = @BatchId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 

IF (
	@ActionType <> 'DELETE'  
	AND (@BatchEndDate< @BatchStartDate ))

BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Batch end date must be greater than start date.' 
	RETURN; 

END 

DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_Batch] 
				( 
					[OrganizationId]  
					,[BatchCode]  
					,[BatchName]  
					,[BatchLevel]  
					,[BatchCategory]  
					,[Coordinator]  
					,[ASSCoordinator]  
					,[BatchStartDate]  
					,[BatchEndDate]  
					,[BatchSummary]  
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@BatchCode  
					,@BatchName  
					,@BatchLevel  
					,@BatchCategory  
					,@Coordinator  
					,@ASSCoordinator  
					,@BatchStartDate  
					,@BatchEndDate  
					,@BatchSummary  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnBatchId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_Batch] 
				SET 
					[OrganizationId]		 = @OrganizationId  
					,[BatchCode]			 = @BatchCode  
					,[BatchName]			 = @BatchName  
					,[BatchLevel]    		 = @BatchLevel  
					,[BatchCategory] 		 = @BatchCategory  
					,[Coordinator]   		 = @Coordinator  
					,[ASSCoordinator]		 = @ASSCoordinator  
					,[BatchStartDate]		 = @BatchStartDate  
					,[BatchEndDate]  		 = @BatchEndDate  
					,[BatchSummary]  		 = @BatchSummary  
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE BatchId = @BatchId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_Batch] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE BatchId = @BatchId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 


GO
/****** Object:  StoredProcedure [dbo].[csp.BatchCourse.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 6/24/2018 2:58:26 PM 
DESC: GET DATA FROM TABLE [dbo].[BatchCourse]


EXEC dbo.[csp.BatchCourse.Get]
@BatchCourseId = NULL 
,@CourseId = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.BatchCourse.Get]
(
	@BatchCourseId 		INT = NULL 
	,@BatchId  			INT = NULL 
	,@CourseId 			INT = NULL 
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TB.BatchCourseId)) AS RowNumber 
				,TB.[BatchCourseId] AS [BatchCourseId] 
				,TB.[BatchId] AS [BatchId]
				,TB.[CourseId] AS [CourseId]
				,C.CourseFullName
				,C.CourseCode
				,ISNULL(TB.[CourseStartDate],'') AS [CourseStartDate]
				,ISNULL(TB.[Remarks],'') AS [Remarks] 
				,COUNT(TB.[BatchCourseId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_BatchCourse] TB WITH(NOLOCK) 
			INNER JOIN 
				[dbo].[TBL_Course] C WITH(NOLOCK) ON (C.CourseId = TB.CourseId)
			WHERE 
				(ISNULL(@BatchCourseId,0) = 0 OR TB.[BatchCourseId] = @BatchCourseId )
				AND (ISNULL(@CourseId,0) = 0 OR TB.[CourseId] = @CourseId )
				AND (ISNULL(@BatchId,0) = 0 OR TB.[BatchId] = @BatchId )
				AND TB.DeletedDate IS NULL 
				AND C.DeletedDate IS NULL
		) 


		SELECT 
			RowNumber 
			,[BatchCourseId] 
			,[BatchId]
			,[CourseId]
			,CourseFullName
			,CourseCode
			,[CourseStartDate]
			,[Remarks] 
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.BatchCourse.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 24/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[BatchCourse]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.BatchCourse.Update]
	@ActionType = 'ADD' 
	,@BatchCourseId = NULL 
	,@OrganizationId = NULL 
	,@BatchId = NULL 
	,@CourseId = NULL 
	,@CourseStartDate = NULL 
	,@Remarks = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_BatchCourse] 


*/

CREATE PROCEDURE [dbo].[csp.BatchCourse.Update]
(
	@ActionType			VARCHAR(10) 
	,@BatchCourseId 	INT = NULL 
	,@OrganizationId	UNIQUEIDENTIFIER = NULL 
	,@BatchId  			INT = NULL 
	,@CourseId 			INT = NULL 
	,@CourseStartDate   DATETIME = NULL 
	,@Remarks  			NVARCHAR(MAX) = NULL 
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnBatchCourseId 		INT =	0 OUTPUT 
)
AS 

SET @ReturnBatchCourseId = @BatchCourseId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_BatchCourse] WHERE BatchCourseId = @BatchCourseId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 


IF (
	@ActionType <> 'DELETE'  
	AND EXISTS (SELECT 1 FROM [dbo].[TBL_BatchCourse] WHERE BatchId = @BatchId AND (@CourseId = CourseId AND BatchCourseId <> @ReturnBatchCourseId) AND DeletedDate IS NULL) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Course already on the list. ' 
	RETURN; 

END 



DECLARE @BatchStartDate DATETIME = NULL;
SELECT @BatchStartDate  = BatchStartDate FROM [dbo].[TBL_Batch] WHERE BatchId = @BatchId;

IF (
	@ActionType <> 'DELETE'  
	AND (@CourseStartDate < @BatchStartDate)
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Course start date must be after Batch start date.' 
	RETURN; 

END 


DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_BatchCourse] 
				( 
					[OrganizationId]  
					,[BatchId]  
					,[CourseId]  
					,[CourseStartDate]  
					,[Remarks]  
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@BatchId  
					,@CourseId  
					,@CourseStartDate  
					,@Remarks  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnBatchCourseId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_BatchCourse] 
				SET 
					[OrganizationId]	= @OrganizationId  
					,[BatchId]  		= @BatchId  
					,[CourseId] 		= @CourseId  
					,[CourseStartDate]  = @CourseStartDate  
					,[Remarks]  		= @Remarks  
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE BatchCourseId = @BatchCourseId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_BatchCourse] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE BatchCourseId = @BatchCourseId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.BatchSummary.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 6/24/2018 3:55:06 PM 
DESC: GET DATA FROM TABLE [dbo].[BatchSummary]


EXEC dbo.[csp.BatchSummary.Get]
@BatchSummaryId = NULL 
,@StatusId = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.BatchSummary.Get]
(
	@BatchSummaryId		INT = NULL 
	,@StatusId 			INT = NULL 
	,@BatchId  			INT = NULL 
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TB.BatchSummaryId)) AS RowNumber 
				,TB.[BatchSummaryId] AS [BatchSummaryId]
				,TB.[BatchId] AS [BatchId]
				,TB.[StatusId] AS [StatusId]
				,BS.StatusName
				,ISNULL(TB.[Summary],'') AS [Summary] 
				,TB.CreatedDate AS CreatedDate
				,A.UserName AS UserName
				,COUNT(TB.[BatchSummaryId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_BatchSummary] TB WITH(NOLOCK) 
			INNER JOIN 
				[dbo].[TBL_Status] BS WITH(NOLOCK) ON (BS.StatusId = TB.StatusId)
			LEFT JOIN 
				[dbo].[AspNetUsers] A WITH(NOLOCK) ON (A.Id = TB.CreatedBy)
			WHERE 
				(ISNULL(@BatchSummaryId,0) = 0 OR TB.[BatchSummaryId] = @BatchSummaryId )
				AND (ISNULL(@StatusId,0) = 0 OR TB.[StatusId] = @StatusId )
				AND (ISNULL(@BatchId,0) = 0 OR TB.[BatchId] = @BatchId )
				AND TB.DeletedDate IS NULL 
				AND BS.DeletedDate IS NULL
				
		) 


		SELECT 
			RowNumber 
			,[BatchSummaryId] 
			,[BatchId]
			,[StatusId]
			,StatusName
			,[Summary] 
			,CreatedDate
			,UserName
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.BatchSummary.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 24/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[BatchSummary]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.BatchSummary.Update]
	@ActionType = 'ADD' 
	,@BatchSummaryId = NULL 
	,@OrganizationId = NULL 
	,@BatchId = NULL 
	,@StatusId = NULL 
	,@Summary = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_BatchSummary] 


*/

CREATE PROCEDURE [dbo].[csp.BatchSummary.Update]
(
	@ActionType			VARCHAR(10) 
	,@BatchSummaryId	INT = NULL 
	,@OrganizationId	UNIQUEIDENTIFIER = NULL 
	,@BatchId  			INT = NULL 
	,@StatusId 			INT = NULL 
	,@Summary  			NVARCHAR(MAX) = NULL 
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnBatchSummaryId 		INT =	0 OUTPUT 
)
AS 

SET @ReturnBatchSummaryId = @BatchSummaryId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_BatchSummary] WHERE BatchSummaryId = @BatchSummaryId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_BatchSummary] 
				( 
					[OrganizationId]  
					,[BatchId]  
					,[StatusId]  
					,[Summary]  
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@BatchId  
					,@StatusId  
					,@Summary  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnBatchSummaryId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_BatchSummary] 
				SET 
					[OrganizationId]	= @OrganizationId  
					,[BatchId]  		= @BatchId  
					,[StatusId] 		= @StatusId  
					,[Summary]  		= @Summary  
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE BatchSummaryId = @BatchSummaryId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_BatchSummary] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE BatchSummaryId = @BatchSummaryId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.BatchTrainee.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 6/24/2018 12:08:36 PM 
DESC: GET DATA FROM TABLE [dbo].[BatchTrainee]


EXEC dbo.[csp.BatchTrainee.Get]
@BatchTraineeId = NULL 
,@BatchId = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.BatchTrainee.Get]
(
	@BatchTraineeId		INT = NULL 
	,@BatchId  			INT = NULL 
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TB.BatchTraineeId)) AS RowNumber 
				,TB.[BatchTraineeId] AS [BatchTraineeId]
				,TB.[OrganizationId] AS [OrganizationId]
				,TB.[BatchId] AS [BatchId]
				,TB.[MemberId] AS [MemberId]
				,ISNULL(TM.[ProfileImageUrl],'') AS [ProfileImageUrl] 
				,TM.FirstName + ' ' + TM.LastName AS TraineeName
				,TB.[JoinedDate] AS [JoinedDate] 
				,COUNT(TB.[BatchTraineeId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_BatchTrainee] TB WITH(NOLOCK) 
			INNER JOIN
				[dbo].[TBL_Member] TM WITH(NOLOCK) ON (TM.MemberId = TB.MemberId)
			WHERE 
				(ISNULL(@BatchTraineeId,0) = 0 OR TB.[BatchTraineeId] = @BatchTraineeId )
				AND (ISNULL(@BatchId,0) = 0 OR TB.[BatchId] = @BatchId )
				AND TB.DeletedDate IS NULL 
		) 


		SELECT 
			RowNumber 
			,[BatchTraineeId]
			,[OrganizationId]
			,[BatchId]
			,[MemberId]
			,TraineeName
			,[ProfileImageUrl]
			,[JoinedDate] 
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.BatchTrainee.GetNotInList]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
/* 
EXEC [dbo].[csp.BatchTrainee.GetNotInList]
@SearchText = NULL 
,@BatchId = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.BatchTrainee.GetNotInList]
(
	@SearchText			VARCHAR(100) 
	,@BatchId  			INT 
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 
	BEGIN 

		IF(ISNULL(@SearchText,'') = '')
			BEGIN
				SET @ShowAll = 0; 
				Set @PageSize  = 10;
			END

		;WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TM.MemberId)) AS RowNumber 
				,TM.MemberId
				 ,TM.[FirstName] AS [FirstName]
				,TM.[LastName] AS [LastName]
				,TM.[SystemRoleId] AS [SystemRoleId]
				,ISNULL(TM.[Designation],'') AS [Designation]
				,ISNULL(TM.[Level],'') AS [Level]
				,ISNULL(TM.[Department],'') AS [Department]
				,ISNULL(TM.[Address],'') AS [Address]
				,ISNULL(TM.[PrimaryContactEmail],'') AS [PrimaryContactEmail]
				,ISNULL(TM.[Phone],'') AS [Phone]
				,ISNULL(TM.[Mobile],'') AS [Mobile]
				,ISNULL(TM.[ProfileImageUrl],'') AS [ProfileImageUrl] 
				,ISNULL(TM.[Education],'') AS [Education]
				,ISNULL(TM.[Notes],'') AS [Notes]
				,ISNULL(TM.Position,'') AS Position
				,ISNULL(TM.CompanyName,'') AS CompanyName
				,COUNT(TM.[MemberId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_Member] TM WITH(NOLOCK) 
			WHERE TM.DeletedDate IS NULL
			 AND (TM.[Designation] = 'TRAINEE' )
			 AND (
				  (TM.FirstName LIKE '%'+@SearchText+'%')
				OR (TM.LastName LIKE '%'+@SearchText+'%')
				OR (TM.FirstName + ' ' + TM.LastName LIKE '%'+@SearchText+'%')
				OR (TM.CompanyName LIKE '%'+@SearchText+'%')
				OR (TM.Department LIKE '%'+@SearchText+'%')
				OR (TM.Position LIKE '%'+@SearchText+'%')
			 )
			AND TM.MemberId NOT IN (SELECT MemberId FROM [dbo].[TBL_BatchTrainee] WHERE BatchId = @BatchId AND DeletedDate IS NULL)
			 
		) 


		SELECT 
			[MemberId] 
			,[FirstName]
			,[LastName]
			,[SystemRoleId]
			,[Designation]
			,[Level]
			,[Department]
			,[Address]
			,[PrimaryContactEmail]
			,[Phone]
			,[Mobile]
			,[ProfileImageUrl] 
			,[Education]
			,[Notes]
			,[Position]
			,[CompanyName]
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.BatchTrainee.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 24/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[BatchTrainee]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.BatchTrainee.Update]
	@ActionType = 'ADD' 
	,@BatchTraineeId = NULL 
	,@OrganizationId = NULL 
	,@BatchId = NULL 
	,@MemberId = NULL 
	,@JoinedDate = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_BatchTrainee] 


*/

CREATE PROCEDURE [dbo].[csp.BatchTrainee.Update]
(
	@ActionType			VARCHAR(10) 
	,@BatchTraineeId	INT = NULL 
	,@OrganizationId	UNIQUEIDENTIFIER = NULL 
	,@BatchId  			INT = NULL 
	,@MemberId 			INT = NULL 
	,@JoinedDate    	DATETIME = NULL 
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnBatchTraineeId 		INT =	0 OUTPUT 
)
AS 

SET @ReturnBatchTraineeId = @BatchTraineeId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_BatchTrainee] WHERE BatchTraineeId = @BatchTraineeId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_BatchTrainee] 
				( 
					[OrganizationId]  
					,[BatchId]  
					,[MemberId]  
					,[JoinedDate]  
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@BatchId  
					,@MemberId  
					,@JoinedDate  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnBatchTraineeId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_BatchTrainee] 
				SET 
					[OrganizationId]	= @OrganizationId  
					,[BatchId]  		= @BatchId  
					,[MemberId] 		= @MemberId  
					,[JoinedDate]    	= @JoinedDate  
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE BatchTraineeId = @BatchTraineeId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_BatchTrainee] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE BatchTraineeId = @BatchTraineeId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.BatchTrainer.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 6/24/2018 10:44:10 AM 
DESC: GET DATA FROM TABLE [dbo].[BatchTrainer]


EXEC dbo.[csp.BatchTrainer.Get]
@BatchTrainerId = NULL 
,@MemberId = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.BatchTrainer.Get]
(
	@BatchTrainerId		INT = NULL 
	,@MemberId 			INT = NULL 
	,@BatchId			INT = NULL
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TB.BatchTrainerId)) AS RowNumber 
				,TB.[BatchTrainerId] AS [BatchTrainerId] 
				,TB.[MemberId] AS [MemberId]
				,TM.FirstName + ' ' + TM.LastName AS TrainerName
				,TB.[BatchId] AS [BatchId]
				,ISNULL(TB.[ModuleId],0) AS [ModuleId]
				,ISNULL(ML.[ModuleTitle],'') AS [ModuleTitle]
				,ISNULL(TM.[ProfileImageUrl],'') AS [ProfileImageUrl] 
				,TB.[TrainingStartDate] AS [TrainingStartDate] 
				,COUNT(TB.[BatchTrainerId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_BatchTrainer] TB WITH(NOLOCK) 
			INNER JOIN
				[dbo].[TBL_Member] TM WITH(NOLOCK) ON (TM.MemberId = TB.MemberId)
			LEFT JOIN
				[dbo].[TBL_Modules] ML WITH(NOLOCK) ON (ML.ModuleId = TB.ModuleId)
			WHERE 
				(ISNULL(@BatchTrainerId,0) = 0 OR TB.[BatchTrainerId] = @BatchTrainerId )
				AND (ISNULL(@MemberId,0) = 0 OR TB.[MemberId] = @MemberId )
				AND (ISNULL(@BatchId,0) = 0 OR TB.BatchId = @BatchId )
				AND TB.DeletedDate IS NULL 
				AND TM.DeletedDate IS NULL
		) 


		SELECT 
			RowNumber 
			,[BatchTrainerId] 
			,[MemberId]
			,TrainerName
			,[BatchId]
			,[ModuleId]
			,[ModuleTitle]
			,[ProfileImageUrl]
			,[TrainingStartDate] 
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.BatchTrainer.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 24/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[BatchTrainer]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.BatchTrainer.Update]
	@ActionType = 'ADD' 
	,@BatchTrainerId = NULL 
	,@OrganizationId = NULL 
	,@MemberId = NULL 
	,@BatchId = NULL 
	,@ModuleId = NULL 
	,@TrainingStartDate = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_BatchTrainer] 


*/

CREATE PROCEDURE [dbo].[csp.BatchTrainer.Update]
(
	@ActionType			VARCHAR(10) 
	,@BatchTrainerId	INT = NULL 
	,@OrganizationId	UNIQUEIDENTIFIER = NULL 
	,@MemberId 			INT = NULL 
	,@BatchId  			INT = NULL 
	,@ModuleId  			INT = NULL 
	,@TrainingStartDate DATETIME = NULL 
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnBatchTrainerId 		INT =	0 OUTPUT 
)
AS 

SET @ReturnBatchTrainerId = @BatchTrainerId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_BatchTrainer] WHERE BatchTrainerId = @BatchTrainerId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_BatchTrainer] 
				( 
					[OrganizationId]  
					,[MemberId]  
					,[BatchId]  
					,[ModuleId]  
					,[TrainingStartDate]  
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@MemberId  
					,@BatchId  
					,@ModuleId  
					,@TrainingStartDate  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnBatchTrainerId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_BatchTrainer] 
				SET 
					[OrganizationId]		 = @OrganizationId  
					,[MemberId] 			 = @MemberId  
					,[BatchId]  			 = @BatchId  
					,[ModuleId]  			 = @ModuleId  
					,[TrainingStartDate]  	 = @TrainingStartDate  
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE BatchTrainerId = @BatchTrainerId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_BatchTrainer] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE BatchTrainerId = @BatchTrainerId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.Course.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
/*
Created by: Prashant 
Created on: 6/22/2018 1:12:03 PM 
DESC: GET DATA FROM TABLE [dbo].[Course]


EXEC dbo.[csp.Course.Get]
@CourseId = 1 
,@CourseFullName = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.Course.Get]
(
	@CourseId 			INT = NULL 
	,@CourseFullName	NVARCHAR(500) = NULL 
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 


	BEGIN 

	--SELECT 
	--			(ROW_NUMBER() OVER (ORDER BY TC.CourseId)) AS RowNumber 
	--			,TC.[CourseId] AS [CourseId]
	--			,TC.[OrganizationId] AS [OrganizationId]
	--			,TC.[CourseFullName] AS [CourseFullName]
	--			,TC.[CourseCode] AS [CourseCode]
	--			,TC.[CourseShortName] AS [CourseShortName]
	--			,ISNULL(TC.[CourseDescription],'') AS [CourseDescription]
	--			,ISNULL(TC.[CourseCategoryId],0) AS [CourseCategoryId]
	--			,ISNULL(TC_MAIN.[CategoryName],'') AS CourseCategory
	--			,ISNULL(TC_CHILD.[CategoryName],'') AS CourseSubCategory
	--			,ISNULL(TC.[CourseSubCategoryId],0) AS [CourseSubCategoryId]
	--			,ISNULL(TC.[CourseLevelId],0) AS [CourseLevelId]
	--			,ISNULL(CL.[LevelName],'') AS [LevelName]
	--			,ISNULL(TC.[CourseContent],'') AS [CourseContent]
	--			,ISNULL(TC.[CoursePeriod],0) AS [CoursePeriod]
	--			,ISNULL(TC.[CoursePeriodTypeId],0) AS [CoursePeriodTypeId]
	--			,ISNULL(CPT.[PeriodTypeName],'') AS [PeriodTypeName]
	--			,ISNULL(TC.[TraineeLevelDesc],'') AS [TraineeLevelDesc] 
	--			,COUNT(TC.[CourseId]) OVER () AS TotalCount 
	--		FROM 
	--			[dbo].[TBL_Course] TC WITH(NOLOCK) 
	--		LEFT JOIN 
	--			[dbo].[TBL_CourseCategory] TC_MAIN WITH(NOLOCK) ON (TC_MAIN.[CourseCategoryId] = TC.[CourseCategoryId] AND TC_Main.DeletedDate IS NULL)
	--		LEFT JOIN 
	--			[dbo].[TBL_CourseCategory] TC_CHILD WITH(NOLOCK) ON (TC_CHILD.[CourseCategoryId] = TC.[CourseSubCategoryId] AND TC_CHILD.DeletedDate IS NULL)
	--		LEFT JOIN 
	--			[dbo].[TBL_CourseLevel] CL WITH(NOLOCK) ON (CL.[CourseLevelId] = TC.[CourseLevelId] AND CL.DeletedDate IS NULL)
	--		LEFT JOIN 
	--			[dbo].[TBL_CoursePeriodType] CPT WITH(NOLOCK) ON (CPT.[CoursePeriodTypeId] = TC.[CoursePeriodTypeId] AND CPT.DeletedDate IS NULL)
	--		WHERE 
	--			(ISNULL(@CourseId,0) = 0 OR TC.[CourseId] = @CourseId )
	--			AND (ISNULL(@CourseFullName,'') = '' OR TC.[CourseFullName] = @CourseFullName )
	--			AND TC.DeletedDate IS NULL 
	--			--AND TC_MAIN.DeletedDate IS NULL
	--			--AND TC_CHILD.DeletedDate IS NULL
	--			--AND CL.DeletedDate IS NULL
	--			--AND CPT.DeletedDate IS NULL
		;WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TC.CourseId)) AS RowNumber 
				,TC.[CourseId] AS [CourseId]
				,TC.[OrganizationId] AS [OrganizationId]
				,TC.[CourseFullName] AS [CourseFullName]
				,TC.[CourseCode] AS [CourseCode]
				,TC.[CourseShortName] AS [CourseShortName]
				,ISNULL(TC.[CourseDescription],'') AS [CourseDescription]
				,ISNULL(TC.[CourseCategoryId],0) AS [CourseCategoryId]
				,ISNULL(TC_MAIN.[CategoryName],'') AS CourseCategory
				,ISNULL(TC_CHILD.[CategoryName],'') AS CourseSubCategory
				,ISNULL(TC.[CourseSubCategoryId],0) AS [CourseSubCategoryId]
				,ISNULL(TC.[CourseLevelId],0) AS [CourseLevelId]
				,ISNULL(CL.[LevelName],'') AS [LevelName]
				,ISNULL(TC.[CourseContent],'') AS [CourseContent]
				,ISNULL(TC.[CoursePeriod],0) AS [CoursePeriod]
				,ISNULL(TC.[CoursePeriodTypeId],0) AS [CoursePeriodTypeId]
				,ISNULL(CPT.[PeriodTypeName],'') AS [PeriodTypeName]
				,ISNULL(TC.[TraineeLevelDesc],'') AS [TraineeLevelDesc] 
				,COUNT(TC.[CourseId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_Course] TC WITH(NOLOCK) 
			LEFT JOIN 
				[dbo].[TBL_CourseCategory] TC_MAIN WITH(NOLOCK) ON (TC_MAIN.[CourseCategoryId] = TC.[CourseCategoryId] AND TC_Main.DeletedDate IS NULL)
			LEFT JOIN 
				[dbo].[TBL_CourseCategory] TC_CHILD WITH(NOLOCK) ON (TC_CHILD.[CourseCategoryId] = TC.[CourseSubCategoryId] AND TC_CHILD.DeletedDate IS NULL)
			LEFT JOIN 
				[dbo].[TBL_CourseLevel] CL WITH(NOLOCK) ON (CL.[CourseLevelId] = TC.[CourseLevelId] AND CL.DeletedDate IS NULL)
			LEFT JOIN 
				[dbo].[TBL_CoursePeriodType] CPT WITH(NOLOCK) ON (CPT.[CoursePeriodTypeId] = TC.[CoursePeriodTypeId] AND CPT.DeletedDate IS NULL)
			WHERE 
				(ISNULL(@CourseId,0) = 0 OR TC.[CourseId] = @CourseId )
				AND (ISNULL(@CourseFullName,'') = '' OR TC.[CourseFullName] = @CourseFullName )
				AND TC.DeletedDate IS NULL 
				AND TC_MAIN.DeletedDate IS NULL
				AND TC_CHILD.DeletedDate IS NULL
				AND CL.DeletedDate IS NULL
				AND CPT.DeletedDate IS NULL

		) 


		SELECT 
			RowNumber 
			,[CourseId]
			,[OrganizationId]
			,[CourseFullName]
			,[CourseCode]
			,[CourseShortName]
			,[CourseDescription]
			,[CourseCategoryId]
			,CourseCategory
			,[CourseSubCategoryId]
			,CourseSubCategory
			,[CourseLevelId]
			,[LevelName]
			,[CourseContent]
			,[CoursePeriod]
			,[CoursePeriodTypeId]
			,[PeriodTypeName]
			,[TraineeLevelDesc] 
			, '[' + ISNULL( SUBSTRING(
			   (
				SELECT ','
				  +'{' 
				   + '"Name": "'+ ISNULL(TL.[TraineeLevelName],'')  + '"'
				   +',"Value":"'+ISNULL(CAST(TL.[TraineeLevelId] AS VARCHAR),'0') +'"'
				  + '}' 
				  AS [text()]
				From 
				 [dbo].[TBL_CourseTraineeLevel] CTL WITH(NOLOCK)
				LEFT JOIN 
				 [dbo].[TBL_TraineeLevel] TL ON (CTL.[TraineeLevelId] = TL.[TraineeLevelId])	
				 WHERE CTL.[CourseId] = T.CourseId	 
				 ORDER BY TL.[TraineeLevelId] DESC
				For XML PATH ('')
				), 2, 5000
			   ),'') + ']'  AS  TraineeLevelsJSON
			,[TotalCount]
		FROM TMP_TBL T
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 



GO
/****** Object:  StoredProcedure [dbo].[csp.Course.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 22/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[Course]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.Course.Update]
	@ActionType = 'ADD' 
	,@CourseId = NULL 
	,@OrganizationId = NULL 
	,@CourseFullName = NULL 
	,@CourseCode = NULL 
	,@CourseShortName = NULL 
	,@CourseDescription = NULL 
	,@CourseCategoryId = NULL 
	,@CourseSubCategoryId = NULL 
	,@CourseLevelId = NULL 
	,@CourseContent = NULL 
	,@CoursePeriod = NULL 
	,@CoursePeriodTypeId = NULL 
	,@TraineeLevelDesc = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_Course] 


*/

CREATE PROCEDURE [dbo].[csp.Course.Update]
(
	@ActionType			VARCHAR(10) 
	,@CourseId 			INT = NULL 
	,@OrganizationId	UNIQUEIDENTIFIER = NULL 
	,@CourseFullName	NVARCHAR(500) = NULL 
	,@CourseCode    	NVARCHAR(100) = NULL 
	,@CourseShortName   NVARCHAR(200) = NULL 
	,@CourseDescription NVARCHAR(1000) = NULL 
	,@CourseCategoryId  INT = NULL 
	,@CourseSubCategoryId	INT = NULL 
	,@CourseLevelId 	INT = NULL 
	,@CourseContent 	NTEXT = NULL 
	,@CoursePeriod  	INT = NULL 
	,@CoursePeriodTypeId 	INT = NULL 
	,@TraineeLevelDesc  NVARCHAR(MAX) = NULL 
	,@UserId	 		NVARCHAR(256) = NULL 
	,@TraineeLevelsXML	NVARCHAR(MAX) = NULL

	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnCourseId 		INT =	0 OUTPUT 
)
AS 

SET @ReturnCourseId = @CourseId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_Course] WHERE CourseId = @CourseId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_Course] 
				( 
					[OrganizationId]  
					,[CourseFullName]  
					,[CourseCode]  
					,[CourseShortName]  
					,[CourseDescription]  
					,[CourseCategoryId]  
					,[CourseSubCategoryId]  
					,[CourseLevelId]  
					,[CourseContent]  
					,[CoursePeriod]  
					,[CoursePeriodTypeId]  
					,[TraineeLevelDesc]  
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@CourseFullName  
					,@CourseCode  
					,@CourseShortName  
					,@CourseDescription  
					,@CourseCategoryId  
					,@CourseSubCategoryId  
					,@CourseLevelId  
					,@CourseContent  
					,@CoursePeriod  
					,@CoursePeriodTypeId  
					,@TraineeLevelDesc  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnCourseId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_Course] 
				SET 
					[OrganizationId]		 = @OrganizationId  
					,[CourseFullName]		 = @CourseFullName  
					,[CourseCode]    		 = @CourseCode  
					,[CourseShortName]    	 = @CourseShortName  
					,[CourseDescription]  	 = @CourseDescription  
					,[CourseCategoryId]   	 = @CourseCategoryId  
					,[CourseSubCategoryId]	 = @CourseSubCategoryId  
					,[CourseLevelId] 		 = @CourseLevelId  
					,[CourseContent] 		 = @CourseContent  
					,[CoursePeriod]  		 = @CoursePeriod  
					,[CoursePeriodTypeId] 	 = @CoursePeriodTypeId  
					,[TraineeLevelDesc]   	 = @TraineeLevelDesc  
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE CourseId = @CourseId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 

		--Add Course Trainee Levels
		
		DELETE FROM [TBL_CourseTraineeLevel] WHERE CourseId = @ReturnCourseId;

		DECLARE @TraineeXML AS XML = CAST(@TraineeLevelsXML AS XML)

			SELECT
				ISNULL(_root._data.value('(Name)[1]', 'VARCHAR(200)'),0) AS 'TraineeLevelName'
				,ISNULL(_root._data.value('(Value)[1]',  'INT'),0) AS 'TraineeLevelId' 
			INTO #TMP_TRAIN_XML
			FROM
				@TraineeXML.nodes('/root/Data') as _root(_data)

  
			INSERT INTO [dbo].[TBL_CourseTraineeLevel]
			(
				[OrganizationId]
				,[TraineeLevelId]
				,[CourseId]
			)
     
			SELECT 
				@OrganizationId
				,T.TraineeLevelId  
				,@ReturnCourseId
			FROM #TMP_TRAIN_XML T
			 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_Course] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE CourseId = @CourseId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 


GO
/****** Object:  StoredProcedure [dbo].[csp.CourseCategory.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 7/1/2018 8:55:05 PM 
DESC: GET DATA FROM TABLE [dbo].[CourseCategory]


EXEC dbo.[csp.CourseCategory.Get]
@CourseCategoryId = NULL 
,@CategoryName = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.CourseCategory.Get]
(
	@CourseCategoryId   INT = NULL 
	,@CategoryName  	NVARCHAR(100) = NULL 
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TC.CourseCategoryId)) AS RowNumber 
				,TC.[CourseCategoryId] AS [CourseCategoryId] 
				,TC.[CategoryName] AS [CategoryName]
				,ISNULL(TC.[Remarks],'') AS [Remarks]
				,ISNULL(TC.[ParentCategoryId],0) AS [ParentCategoryId] 
				 
				,COUNT(TC.[CourseCategoryId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_CourseCategory] TC WITH(NOLOCK) 
			--LEFT JOIN 
			--	[dbo].[TBL_CourseCategory] TCC WITH(NOLOCK) ON (TCC.ParentCategoryId = TC.CourseCategoryId)
			WHERE 
				(ISNULL(@CourseCategoryId,0) = 0 OR TC.[CourseCategoryId] = @CourseCategoryId )
				AND (ISNULL(@CategoryName,'') = '' OR TC.[CategoryName] = @CategoryName )
				AND TC.DeletedDate IS NULL 
		) 


		SELECT 
			RowNumber 
			,[CourseCategoryId] 
			,[CategoryName]
			,[Remarks]
			,[ParentCategoryId] 
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.CourseCategory.GetParents]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 6/26/2018 3:29:19 PM 
DESC: GET DATA FROM TABLE [dbo].[CourseCategory]


EXEC  [dbo].[csp.CourseCategory.GetParents]
@CourseCategoryId = NULL 
,@CategoryName = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.CourseCategory.GetParents]
(
	@CourseCategoryId   INT = NULL 
	,@CategoryName  	NVARCHAR(100) = NULL 
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 

	-- GET PARENT CATEGORY
	SELECT 
		 TC.[CourseCategoryId] AS [CourseCategoryId] 
		,TC.[CategoryName] AS [CategoryName]
		,ISNULL(TC.[Remarks],'') AS [Remarks]
		,ISNULL(TC.[ParentCategoryId],0) AS [ParentCategoryId] 
		
	INTO #TMP_ALL
	FROM 
		[dbo].[TBL_CourseCategory] TC WITH(NOLOCK) 
	LEFT JOIN 
		[dbo].[TBL_CourseCategory] TC_P WITH(NOLOCK)  ON (TC_P.CourseCategoryId = TC.ParentCategoryId)
	WHERE 
		(ISNULL(@CourseCategoryId,0) = 0 OR TC.[CourseCategoryId] = @CourseCategoryId )
		AND (ISNULL(@CategoryName,'') = '' OR (TC.[CategoryName] LIKE '%'+@CategoryName+'%') OR TC_P.CategoryName LIKE '%'+@CategoryName+'%' )
		AND TC.DeletedDate IS NULL 
	 
	SELECT
		(ROW_NUMBER() OVER (ORDER BY TC.CourseCategoryId)) AS RowNumber 
		, TC.CourseCategoryId
		,COUNT(TC.[CourseCategoryId]) OVER () AS TotalCount 
	INTO #TMP_PARENTS
	FROM #TMP_ALL TC
	WHERE ISNULL(TC.ParentCategoryId,0) = 0

	SELECT
		TP.RowNumber
		,TP.CourseCategoryId
		,A.CategoryName
		,A.Remarks
		, '[' + ISNULL( SUBSTRING(
			   (
				SELECT ','
				  +'{' 
				    + '"CourseCategoryId": '+ CAST(CCC.CourseCategoryId AS VARCHAR(5)) 
				   + ',"CategoryName": "'+ CCC.CategoryName  + '"'
				   +',"Remarks":"'+ ISNULL(CCC.Remarks,'')  + '"'
				  + '}' 
				  AS [text()]
				From 
				 [dbo].[TBL_CourseCategory] CCC WITH(NOLOCK)
				 WHERE CCC.ParentCategoryId= TP.CourseCategoryId 
				 AND CCC.DeletedDate IS NULL
				 ORDER BY CCC.ParentCategoryId DESC
				For XML PATH ('')
				), 2, 15000
			   ),'') + ']'  AS  ChildCategoryJSON
		,TP.TotalCount
	FROM #TMP_PARENTS TP
	INNER JOIN 
		#TMP_ALL A ON (A.CourseCategoryId  = TP.CourseCategoryId)
	WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	 	
	  
GO
/****** Object:  StoredProcedure [dbo].[csp.CourseCategory.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 26/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[CourseCategory]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.CourseCategory.Update]
	@ActionType = 'ADD' 
	,@CourseCategoryId = NULL 
	,@OrganizationId = NULL 
	,@CategoryName = NULL 
	,@Remarks = NULL 
	,@ParentCategoryId = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_CourseCategory] 


*/

CREATE PROCEDURE [dbo].[csp.CourseCategory.Update]
(
	@ActionType			VARCHAR(10) 
	,@CourseCategoryId  INT = NULL 
	,@OrganizationId	UNIQUEIDENTIFIER = NULL 
	,@CategoryName  	NVARCHAR(100) = NULL 
	,@Remarks  			NVARCHAR(100) = NULL 
	,@ParentCategoryId  INT = NULL 
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnCourseCategoryId 		INT =	0 OUTPUT 
)
AS 

SET @ReturnCourseCategoryId = @CourseCategoryId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_CourseCategory] WHERE CourseCategoryId = @CourseCategoryId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_CourseCategory] 
				( 
					[OrganizationId]  
					,[CategoryName]  
					,[Remarks]  
					,[ParentCategoryId]  
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@CategoryName  
					,@Remarks  
					,@ParentCategoryId  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnCourseCategoryId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_CourseCategory] 
				SET 
					[OrganizationId]	= @OrganizationId  
					,[CategoryName]  	= @CategoryName  
					,[Remarks]  		= @Remarks  
					,[ParentCategoryId] = @ParentCategoryId  
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE CourseCategoryId = @CourseCategoryId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_CourseCategory] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE CourseCategoryId = @CourseCategoryId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.CourseLevel.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 6/27/2018 8:14:02 PM 
DESC: GET DATA FROM TABLE [dbo].[CourseLevel]


EXEC dbo.[csp.CourseLevel.Get]
@CourseLevelId = NULL 
,@LevelName = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.CourseLevel.Get]
(
	@CourseLevelId 		INT = NULL 
	,@LevelName			NVARCHAR(100) = NULL 
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TC.CourseLevelId)) AS RowNumber 
				,TC.[CourseLevelId] AS [CourseLevelId] 
				,TC.[LevelName] AS [LevelName]
				,ISNULL(TC.[Remarks],'') AS [Remarks] 
				,COUNT(TC.[CourseLevelId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_CourseLevel] TC WITH(NOLOCK) 
			WHERE 
				(ISNULL(@CourseLevelId,0) = 0 OR TC.[CourseLevelId] = @CourseLevelId )
				AND (ISNULL(@LevelName,'') = '' OR TC.[LevelName] = @LevelName )
				AND TC.DeletedDate IS NULL 
		) 


		SELECT 
			RowNumber 
			,[CourseLevelId] 
			,[LevelName]
			,[Remarks]
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.CourseLevel.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 27/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[CourseLevel]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.CourseLevel.Update]
	@ActionType = 'ADD' 
	,@CourseLevelId = NULL 
	,@OrganizationId = NULL 
	,@LevelName = NULL 
	,@Remarks = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_CourseLevel] 


*/

CREATE PROCEDURE [dbo].[csp.CourseLevel.Update]
(
	@ActionType			VARCHAR(10) 
	,@CourseLevelId 	INT = NULL 
	,@OrganizationId	UNIQUEIDENTIFIER = NULL 
	,@LevelName			NVARCHAR(100) = NULL 
	,@Remarks  			NVARCHAR(100) = NULL 
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnCourseLevelId 		INT =	0 OUTPUT 
)
AS 

SET @ReturnCourseLevelId = @CourseLevelId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_CourseLevel] WHERE CourseLevelId = @CourseLevelId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_CourseLevel] 
				( 
					[OrganizationId]  
					,[LevelName]  
					,[Remarks]  
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@LevelName  
					,@Remarks  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnCourseLevelId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_CourseLevel] 
				SET 
					 [LevelName]		 = @LevelName  
					,[Remarks]  		 = @Remarks  
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE CourseLevelId = @CourseLevelId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_CourseLevel] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE CourseLevelId = @CourseLevelId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.CourseMedia.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Created by: Prashant 
Created on: 6/26/2018 2:01:59 PM 
DESC: GET DATA FROM TABLE [dbo].[CourseMedia]


EXEC dbo.[csp.CourseMedia.Get]
@CourseMediaId = 11 
,@CourseId = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.CourseMedia.Get]
(
	@CourseMediaId 		INT = NULL 
	,@CourseId 				INT = NULL 
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber			INT = 1 
	,@PageSize 				INT = 20 
	,@ShowAll				INT = 0 

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TC.CourseMediaId)) AS RowNumber 
				,TC.[CourseMediaId] AS [CourseMediaId]
				,TC.[OrganizationId] AS [OrganizationId]
				,TC.[CourseId] AS [CourseId]
				,ISNULL(TC.[TopicId],0) AS [TopicId]
				,TC.[MediaURL] AS [MediaURL]
				,TC.[MediaType] AS [MediaType]
				,ISNULL(TC.[Remarks],'') AS [Remarks]
				,TM.[ModuleTitle] AS Module,
				TMD.[MediaName] AS MediaTypeName
				--,ISNULL(TM.ModuleTitle,'') AS Module
				--,ISNULL(TMD.[MediaName],'') AS MediaTypeName			
				,COUNT(TC.[CourseMediaId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_CourseMedia] TC WITH(NOLOCK) 			
			LEFT JOIN
				[dbo].[TBL_Modules] TM WITH(NOLOCK) ON TM.ModuleId=TC.TopicId
			INNER JOIN
			[dbo].[TBL_Media] TMD WITH(NOLOCK) ON TMD.MediaId=TC.MediaType
			WHERE 
				(ISNULL(@CourseMediaId,0) = 0 OR TC.[CourseMediaId] = @CourseMediaId )
				AND (ISNULL(@CourseId,0) = 0 OR TC.[CourseId] = @CourseId )
				AND TC.DeletedDate IS NULL 
				AND TM.DeletedDate IS NULL
				AND TMD.DeletedDate IS NULL
		) 


		SELECT 
			RowNumber 
			,[CourseMediaId]
			,[OrganizationId]
			,[CourseId]
			,[TopicId]
			,[MediaURL]
			,[MediaType]
			,[Remarks]	
			,[Module]
			,[MediaTypeName]		
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 

	

GO
/****** Object:  StoredProcedure [dbo].[csp.CourseMedia.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Created by: Prashant 
Created on: 26/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[CourseMedia]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.CourseMedia.Update]
	@ActionType = 'ADD' 
	,@CourseMediaId = NULL 
	,@OrganizationId = NULL 
	,@CourseId = NULL 
	,@TopicId = NULL 
	,@MediaURL = NULL 
	,@MediaType = NULL 
	,@Remarks = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_CourseMedia] 


*/

CREATE PROCEDURE [dbo].[csp.CourseMedia.Update]
(
	@ActionType			VARCHAR(10) 
	,@CourseMediaId 		INT = NULL 
	,@OrganizationId		INT = 0 
	,@CourseId 				INT = NULL 
	,@TopicId  				INT = NULL 
	,@MediaURL 				NVARCHAR(500) = NULL 
	,@MediaType			INT = NULL 
	,@Remarks  				NVARCHAR(500) = NULL 
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnCourseMediaId 		INT =	0 OUTPUT 
)
AS 

SET @ReturnCourseMediaId = @CourseMediaId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_CourseMedia] WHERE CourseMediaId = @CourseMediaId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_CourseMedia] 
				( 
					[OrganizationId]  
					,[CourseId]  
					,[TopicId]  
					,[MediaURL]  
					,[MediaType]  
					,[Remarks]  
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@CourseId  
					,@TopicId  
					,@MediaURL  
					,@MediaType  
					,@Remarks  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnCourseMediaId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_CourseMedia] 
				SET 
					 [TopicId]  	= @TopicId  
					,[MediaURL] 	= @MediaURL  
					,[MediaType]	= @MediaType  
					,[Remarks]  	= @Remarks  
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE CourseMediaId = @CourseMediaId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_CourseMedia] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE CourseMediaId = @CourseMediaId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 


GO
/****** Object:  StoredProcedure [dbo].[csp.CoursePeriodType.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 6/27/2018 8:26:14 PM 
DESC: GET DATA FROM TABLE [dbo].[CoursePeriodType]


EXEC dbo.[csp.CoursePeriodType.Get]
@CoursePeriodTypeId = NULL 
,@PeriodTypeName = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.CoursePeriodType.Get]
(
	@CoursePeriodTypeId INT = NULL 
	,@PeriodTypeName	NVARCHAR(100) = NULL 
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TC.CoursePeriodTypeId)) AS RowNumber 
				,TC.[CoursePeriodTypeId] AS [CoursePeriodTypeId] 
				,TC.[PeriodTypeName] AS [PeriodTypeName]
				,ISNULL(TC.[Remarks],'') AS [Remarks] 
				,COUNT(TC.[CoursePeriodTypeId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_CoursePeriodType] TC WITH(NOLOCK) 
			WHERE 
				(ISNULL(@CoursePeriodTypeId,0) = 0 OR TC.[CoursePeriodTypeId] = @CoursePeriodTypeId )
				AND (ISNULL(@PeriodTypeName,'') = '' OR TC.[PeriodTypeName] = @PeriodTypeName )
				AND TC.DeletedDate IS NULL 
		) 


		SELECT 
			RowNumber 
			,[CoursePeriodTypeId] 
			,[PeriodTypeName]
			,[Remarks] 
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.CoursePeriodType.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 27/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[CoursePeriodType]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.CoursePeriodType.Update]
	@ActionType = 'ADD' 
	,@CoursePeriodTypeId = NULL 
	,@OrganizationId = NULL 
	,@PeriodTypeName = NULL 
	,@Remarks = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_CoursePeriodType] 


*/

CREATE PROCEDURE [dbo].[csp.CoursePeriodType.Update]
(
	@ActionType				VARCHAR(10) 
	,@CoursePeriodTypeId 	INT = NULL 
	,@OrganizationId		UNIQUEIDENTIFIER = NULL 
	,@PeriodTypeName		NVARCHAR(100) = NULL 
	,@Remarks  				NVARCHAR(100) = NULL 
	,@UserId	 			NVARCHAR(256) = NULL 
	,@MsgType				VARCHAR(10) =	NULL OUTPUT 
	,@MsgText				VARCHAR(100) =	NULL OUTPUT 
	,@ReturnCoursePeriodTypeId 		INT =	0 OUTPUT 
)
AS 

SET @ReturnCoursePeriodTypeId = @CoursePeriodTypeId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_CoursePeriodType] WHERE CoursePeriodTypeId = @CoursePeriodTypeId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_CoursePeriodType] 
				( 
					[OrganizationId]  
					,[PeriodTypeName]  
					,[Remarks]  
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@PeriodTypeName  
					,@Remarks  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnCoursePeriodTypeId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_CoursePeriodType] 
				SET 
					 [PeriodTypeName]	 = @PeriodTypeName  
					,[Remarks]  		 = @Remarks  
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE CoursePeriodTypeId = @CoursePeriodTypeId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_CoursePeriodType] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE CoursePeriodTypeId = @CoursePeriodTypeId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.DDLItems.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

 
/*
//// CURRENTLY IMPLEMENTED DDL JSONS /////
 
------------------------------------------------------------------------
NAME: TABLE_NAMES
PARAM1: '' = NOTHING
PARAM2: ''  = NOTHING
------------------------------------------------------------------------
NAME: COURSECATEGORY
PARAM1: 'ALL' = ALL: Show all categories including sub categories
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: SUBCATEGORY
PARAM1: 'Id' = Id: Show Sub categories for given Id
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: COURSELEVEL 
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: PERIODTYPE 
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: TRAINEELEVELS 
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: MEMBERLEVELS 
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------ 

NAME: TRAINEE
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: TRAINER
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: MODULES
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: COURSES
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: BATCHCOURSE
PARAM1: =	BATCHID NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: BATCH_STATUS
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: BATCHES
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------


*/
 

/*
Created by: Prashant 
Created on: 5/7/2016
DESC: GET DDL VAlues in Terms of JSON

 

EXEC [dbo].[csp.DDLItems.Get]

@OrganizationId = ''
,@DDLListXML = '<root>
  <Data>
    <DDLName>BATCHES</DDLName>
    <Param1>NULL</Param1>
    <Param2>HIDE_DEFAULT</Param2>
  </Data>
</root>'


*/

CREATE PROCEDURE [dbo].[csp.DDLItems.Get]
(
	@PageName VARCHAR(100) = ''
	,@DDLListXML	NVARCHAR(MAX) = NULL
	,@OrganizationId	NVARCHAR(50) = NULL
	,@UserId NVARCHAR(256)  = NULL
)
AS 
	BEGIN 
		
		-- Get OranizationId
		DECLARE @OrgId UNIQUEIDENTIFIER = NULL;
		SELECT @OrgId = TRY_CONVERT(UNIQUEIDENTIFIER, @OrganizationId)

		DECLARE @DDLXML AS XML = CAST(@DDLListXML AS XML)
		DECLARE @TableJSON VARCHAR(MAX) = ''
		DECLARE @TMPJSON AS VARCHAR(MAX) = ''

		DECLARE @Param1 VARCHAR(150)
		DECLARE @Param2 VARCHAR(150)
		DECLARE @Id INT 

   	CREATE TABLE #TBLDDL(Name VARCHAR(100) , VALUE VARCHAR(100),ROW_PRIORITY SMALLINT NULL)

		SELECT
			ISNULL(_root._data.value('(DDLName)[1]', 'varchar(50)'),0) AS 'DDLName',
			_root._data.value('(Param1)[1]', 'varchar(50)') AS 'Param1',
			_root._data.value('(Param2)[1]', 'varchar(50)') AS 'Param2'
			INTO #TMP_DDLListWithParams
			FROM
				@DDLXML.nodes('/root/Data') as _root(_data)

		-- FUNCTION FOR DDL ------
		-- LOAD TABLES NAME ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'TABLE_NAMES')
			BEGIN
				-- Clean TEMP VARIABLES  --
				DELETE FROM #TBLDDL
				SET @TMPJSON = NULL;
				SET @Param1 = '';
				SET @Param2 = '';

				-- Fill Table with New Value --
				INSERT INTO  #TBLDDL(Name, Value,ROW_PRIORITY)
				SELECT 
					NAME AS Name
					,NAME  AS Value
					,(ROW_NUMBER() OVER (ORDER BY NAME)) AS ROW_PRIORITY 
				 FROM SYS.TABLES


				-- Update to JSON --
				SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
				FROM #TBLDDL ORDER BY ROW_PRIORITY


				--SET @TMPJSON = '{"Name":"--Select--","Value":"0"}' + @TMPJSON
				
				SET @TMPJSON = '{"Name":"--Select--","Value":"0"}' + CASE WHEN  ISNULL(@TMPJSON,'')='' THEN '' ELSE ',' END + @TMPJSON

				SET @TableJSON += '{"DDLName": "TABLE_NAMES","Items": ['+ @TMPJSON +']}';
			END
		 
		-- FUNCTION FOR DDL ------
		-- LOAD Course Categories ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'COURSECATEGORY')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
				
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'COURSECATEGORY';

				
				
			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [CategoryName] AS Name
						,[CourseCategoryId] AS Value 
					FROM [dbo].[TBL_CourseCategory] WITh(NOLOCK)
					WHERE DeletedDate IS NULL
					AND (ISNULL([ParentCategoryId],0)  = 0 OR ISNULL(@Param1,'') = 'ALL')

				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "COURSECATEGORY","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END
		
		-- FUNCTION FOR DDL ------
		-- LOAD Sub Categories ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'SUBCATEGORY')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'SUBCATEGORY';

				 
			SET @Id = CASE WHEN ISNUMERIC(@Param1) = 1 THEN CAST(@Param1 AS INT) ELSE NULL END;

			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [CategoryName] AS Name
						,[CourseCategoryId] AS Value 
					FROM [dbo].[TBL_CourseCategory] WITh(NOLOCK)
					WHERE DeletedDate IS NULL
					AND ISNULL([ParentCategoryId],0)  > 0
					AND (ISNULL(@Id,0) = 0 OR ParentCategoryId = @Id)
					
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "SUBCATEGORY","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD Course Levels ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'COURSELEVEL')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'COURSELEVEL';
			 

			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [LevelName] AS Name
						,[CourseLevelId] AS Value 
					FROM [dbo].[TBL_CourseLevel] WITh(NOLOCK)
					WHERE DeletedDate IS NULL 
					
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "COURSELEVEL","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD Period types ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'PERIODTYPE')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'PERIODTYPE';

				  
			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [PeriodTypeName] AS Name
						,[CoursePeriodTypeId] AS Value 
					FROM [dbo].[TBL_CoursePeriodType] WITh(NOLOCK)
					WHERE DeletedDate IS NULL 
					
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "PERIODTYPE","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		
		-- FUNCTION FOR DDL ------
		-- LOAD Trainee Levels ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'TRAINEELEVELS')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'TRAINEELEVELS';
			 

			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [TraineeLevelName] AS Name
						,[TraineeLevelId] AS Value 
					FROM [dbo].[TBL_TraineeLevel] WITh(NOLOCK)
					WHERE DeletedDate IS NULL 
					
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "TRAINEELEVELS","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  
		-- FUNCTION FOR DDL ------

		-- LOAD MEMBER Levels ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'MEMBERLEVELS')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'MEMBERLEVELS';
			 

			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [TraineeLevelName] AS Name
						,[TraineeLevelName] AS Value 
					FROM [dbo].[TBL_TraineeLevel] WITh(NOLOCK)
					WHERE DeletedDate IS NULL 
					
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "MEMBERLEVELS","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  
		-- FUNCTION FOR DDL ------

		-- LOAD TRAINEE  ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'TRAINEE')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'TRAINEE';
			 

			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [FirstName] + ' ' + [LastName] AS Name
						,[MemberId] AS Value 
					FROM [dbo].[TBL_Member] WITh(NOLOCK)
					WHERE DeletedDate IS NULL 
					AND Designation = 'TRAINEE'
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "TRAINEE","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD TRAINER  ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'TRAINER')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'TRAINER';
			 

			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [FirstName] + ' ' + [LastName] AS Name
						,[MemberId] AS Value 
					FROM [dbo].[TBL_Member] WITh(NOLOCK)
					WHERE DeletedDate IS NULL 
					AND Designation = 'TRAINER'
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "TRAINER","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD MODULES ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'MODULES')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'MODULES';
			 

			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [ModuleTitle] AS Name
						,[ModuleId] AS Value 
					FROM [dbo].[TBL_Modules] WITh(NOLOCK)
					WHERE DeletedDate IS NULL  
					AND ISNULL(@Param1,0)=0 OR  (AssociatedCourseId=@Param1)
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "MODULES","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD COURSES ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'COURSES')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'COURSES';
			 

			INSERT INTO  #TBLDDL(Name, Value)
			 
				SELECT [CourseFullName]+ '('+ [CourseCode] +')' AS Name
						,[CourseId] AS Value 
					FROM [dbo].[TBL_Course] WITh(NOLOCK)
					WHERE DeletedDate IS NULL  
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "COURSES","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD COURSES ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'BATCHCOURSES')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'BATCHCOURSES';
			 

			INSERT INTO  #TBLDDL(Name, Value)
			 
				SELECT [CourseFullName]+ '('+ [CourseCode] +')' AS Name
						,[CourseId] AS Value 
					FROM [dbo].[TBL_Course] WITh(NOLOCK)
					WHERE courseId IN (SELECT CourseId FROM TBL_BatchCourse WHERE BatchId = CAST(@Param1 AS INT) AND DeletedDate IS NULL)
					AND DeletedDate IS NULL  
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "BATCHCOURSES","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD MEDIAS ------

			-- FUNCTION FOR DDL ------
		-- LOAD MEDIAS ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'MEDIAS')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'MEDIAS';
			 

			INSERT INTO  #TBLDDL(Name, Value)
			 
				SELECT [MediaName] AS Name
						,[MediaId] AS Value 
					FROM [dbo].[TBL_Media] WITh(NOLOCK)
					WHERE DeletedDate IS NULL  
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "MEDIAS","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD BATCH STATUS ------

		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'BATCH_STATUS')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'BATCH_STATUS';
			 

			INSERT INTO  #TBLDDL(Name, Value)
			 
				SELECT [StatusName] AS Name
						,[StatusId] AS Value 
					FROM [dbo].[TBL_Status] WITh(NOLOCK)
					WHERE DeletedDate IS NULL
					AND StatusGroup = 'BATCH'
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "BATCH_STATUS","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD BATCHES ------

		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'BATCHES')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'BATCHES';
			 

			INSERT INTO  #TBLDDL(Name, Value)
			 
				SELECT [BatchName] + ' ( '+ BatchCode+' )' AS Name
						,[BatchId] AS Value 
					FROM [dbo].[TBL_Batch] WITh(NOLOCK)
					WHERE DeletedDate IS NULL 
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "BATCHES","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  


		-- FINALLY THE RESULT PAGE
		SELECT 
			@PageName AS PageName,
			'['+ @TableJSON +']'as DDLItems
	END













GO
/****** Object:  StoredProcedure [dbo].[csp.EmailFormat.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

/*
Created by: Prashant 
Created on: 10/13/2017 1:05:29 AM 
DESC: GET DATA FROM TABLE [dbo].[EmailFormat]


EXEC dbo.[csp.EmailFormat.Get]
@EmailFormatId = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.EmailFormat.Get]
(
	@EmailFormatId 		INT = NULL 
	,@Subject			NVARCHAR(100) = NULL
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 
	,@SearchText        NVARCHAR(256)= NULL

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TE.EmailFormatId)) AS RowNumber 
				,TE.[EmailFormatId] AS [EmailFormatId] 
				,ISNULL(TE.[From],'') AS [From]
				,ISNULL(TE.[FromName],'') AS [FromName]
				,ISNULL(TE.[Bcc],'') AS [Bcc]
				,TE.[Subject] AS [Subject]
				,TE.[Body] AS [Body]
				,COUNT(TE.[EmailFormatId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_EmailFormat] TE WITH(NOLOCK) 
			WHERE 
				(ISNULL(@EmailFormatId,0) = 0 OR TE.[EmailFormatId] = @EmailFormatId )
				AND(ISNULL(@SearchText,'')='' OR ((TE.FromName LIKE '%'+@SearchText+'%')  OR (TE.Bcc LIKE '%'+@SearchText+'%') OR (TE.[From] LIKE '%'+@SearchText+'%') ))
				AND (ISNULL(@Subject,'') = '' OR TE.Subject = @Subject)
				AND TE.DeletedDate IS NULL 
		) 


		SELECT 
			RowNumber 
			,[EmailFormatId] 
			,[From]
			,[FromName]
			,[Bcc]
			,[Subject]
			,[Body]
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 










GO
/****** Object:  StoredProcedure [dbo].[csp.EmailFormat.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
/*
Created by: Prashant 
Created on: 13/10/2017 
DESC: UPDATE DATA TO TABLE [dbo].[EmailFormat]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.EmailFormat.Update]
	@ActionType = 'ADD' 
	,@EmailFormatId = NULL 
	,@OrganizationId = NULL 
	,@From = NULL 
	,@FromName = NULL 
	,@Bcc = NULL 
	,@Subject = NULL 
	,@Body = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_EmailFormat] 


*/

CREATE PROCEDURE [dbo].[csp.EmailFormat.Update]
(
	@ActionType			VARCHAR(10) 
	,@EmailFormatId 	INT = NULL  
	,@From				NVARCHAR(500) = NULL 
	,@FromName 			NVARCHAR(500) = NULL 
	,@Bcc 				NVARCHAR(500) = NULL 
	,@Subject  			NVARCHAR(500) = NULL 
	,@Body				NVARCHAR(MAX) = NULL 
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnEmailFormatId 		INT =	0 OUTPUT 
)
AS 

SET @ReturnEmailFormatId = @EmailFormatId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_EmailFormat] WHERE EmailFormatId = @EmailFormatId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_EmailFormat] 
				(  
					[From]  
					,[FromName]  
					,[Bcc]  
					,[Subject]  
					,[Body]  
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					 @From  
					,@FromName  
					,@Bcc  
					,@Subject  
					,@Body  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnEmailFormatId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_EmailFormat] 
				SET 
					 [From]				= @From  
					,[FromName] 		= @FromName  
					,[Bcc] 				= @Bcc  
					,[Subject]  		= @Subject  
					,[Body]				= @Body  
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE EmailFormatId = @EmailFormatId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_EmailFormat] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE EmailFormatId = @EmailFormatId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 









GO
/****** Object:  StoredProcedure [dbo].[csp.Media.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 6/21/2018 7:33:10 AM 
DESC: GET DATA FROM TABLE [dbo].[Media]


EXEC dbo.[csp.Media.Get]
@MediaId = NULL 
,@OrganizationId = NULL 
,@MediaName = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.Media.Get]
(
	@MediaId  			INT = NULL  
	,@MediaName			NVARCHAR(500) = NULL 
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TM.MediaId)) AS RowNumber 
				,TM.[MediaId] AS [MediaId] 
				,TM.[MediaName] AS [MediaName] 
				,TM.Remarks AS Remarks
				,COUNT(TM.[MediaId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_Media] TM WITH(NOLOCK) 
			WHERE 
				(ISNULL(@MediaId,0) = 0 OR TM.[MediaId] = @MediaId ) 
				AND (ISNULL(@MediaName,'') = '' OR TM.[MediaName] = @MediaName )
				AND TM.DeletedDate IS NULL 
		) 


		SELECT 
			RowNumber 
			,[MediaId]
			,[MediaName]
			,Remarks 
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 


GO
/****** Object:  StoredProcedure [dbo].[csp.Media.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 21/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[Media]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.Media.Update]
	@ActionType = 'ADD' 
	,@MediaId = NULL 
	,@OrganizationId = NULL 
	,@MediaName = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_Media] 


*/

CREATE PROCEDURE [dbo].[csp.Media.Update]
(
	@ActionType			VARCHAR(10) 
	,@MediaId  			INT = NULL 
	,@OrganizationId	UNIQUEIDENTIFIER = NULL 
	,@MediaName			NVARCHAR(500) = NULL 
	,@Remarks			NVARCHAR(MAX) = NULL 
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnMediaId 	INT =	0 OUTPUT 
)
AS 

SET @ReturnMediaId = @MediaId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_Media] WHERE MediaId = @MediaId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_Media] 
				( 
					[OrganizationId]  
					,[MediaName] 
					,[Remarks]
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@MediaName
					,@Remarks  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnMediaId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_Media] 
				SET 
					 [MediaName]		= @MediaName  
					 ,[Remarks]			= @Remarks
					,[UpdatedDate] 		= @CurrentDate  
					,[UpdatedBy] 		= ISNULL(@UserId,[UpdatedBy])  
				WHERE MediaId = @MediaId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_Media] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE MediaId = @MediaId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 


GO
/****** Object:  StoredProcedure [dbo].[csp.Member.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 6/17/2018 8:08:40 PM 
DESC: GET DATA FROM TABLE [dbo].[Member]


EXEC dbo.[csp.Member.Get]
@MemberId = NULL 
,@Designation = NULL 
,@Phone = NULL 
,@Mobile = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 
,@Keyword =''

*/


CREATE PROCEDURE [dbo].[csp.Member.Get]
(
	@MemberId 		INT = NULL 
	,@Designation   NVARCHAR(250) = NULL 
	,@Phone    		VARCHAR(25) = NULL 
	,@Mobile   		VARCHAR(25) = NULL 
	,@UserId		NVARCHAR(256) = NULL 
	,@PageNumber	INT = 1 
	,@PageSize 		INT = 20 
	,@ShowAll		INT = 0 
	,@Keyword		NVARCHAR(200)=NULL

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TM.MemberId)) AS RowNumber 
				,TM.[MemberId] AS [MemberId]
				,TM.[OrganizationId] AS [OrganizationId]
				,TM.[FirstName] AS [FirstName]
				,TM.[LastName] AS [LastName]
				,TM.[SystemRoleId] AS [SystemRoleId]
				,ISNULL(TM.[Designation],'') AS [Designation]
				,ISNULL(TM.[Level],'') AS [Level]
				,ISNULL(TM.[Department],'') AS [Department]
				,ISNULL(TM.[Address],'') AS [Address]
				,ISNULL(TM.[PrimaryContactEmail],'') AS [PrimaryContactEmail]
				,ISNULL(TM.[Phone],'') AS [Phone]
				,ISNULL(TM.[Mobile],'') AS [Mobile]
				,ISNULL(TM.[ProfileImageUrl],'') AS [ProfileImageUrl] 
				,ISNULL(TM.[Education],'') AS [Education]
				,ISNULL(TM.[Notes],'') AS [Notes]
				,ISNULL(TM.Position,'') AS Position
				,ISNULL(TM.CompanyName,'') AS CompanyName
				,COUNT(TM.[MemberId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_Member] TM WITH(NOLOCK) 
			WHERE 
				(ISNULL(@MemberId,0) = 0 OR TM.[MemberId] = @MemberId )
				AND (TM.[Designation] = 'TRAINER' )
				AND (ISNULL(@Phone,'') = '' OR TM.[Phone] = @Phone )
				AND (ISNULL(@Mobile,'') = '' OR TM.[Mobile] = @Mobile )
				AND (ISNULL(@Keyword,'')='' OR (TM.Mobile LIKE '%'+@Keyword+'%' OR TM.Phone LIKE '%'+@Keyword+'%' OR TM.FirstName LIKE '%'+@Keyword+'%' OR TM.LastName LIKE '%'+@Keyword+'%' 
				OR TM.PrimaryContactEmail LIKE '%'+@Keyword+'%'))
				AND TM.DeletedDate IS NULL 
		) 


		SELECT 
			RowNumber 
			,[MemberId]
			,[OrganizationId]
			,[FirstName]
			,[LastName]
			,[SystemRoleId]
			,[Designation]
			,[Level]
			,[Department]
			,[Address]
			,[PrimaryContactEmail]
			,[Phone]
			,[Mobile]
			,[ProfileImageUrl] 
			,[Education]
			,[Notes]
			,[Position]
			,[CompanyName]
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 


GO
/****** Object:  StoredProcedure [dbo].[csp.Member.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
/*
Created by: Prashant 
Created on: 17/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[Member]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.Member.Update]
	@ActionType = 'ADD' 
	,@MemberId = NULL 
	,@OrganizationId = NULL 
	,@FirstName = NULL 
	,@LastName = NULL 
	,@SystemRoleId = NULL 
	,@Designation = NULL 
	,@Level = NULL 
	,@Department = NULL 
	,@Address = NULL 
	,@PrimaryContactEmail = NULL 
	,@Phone = NULL 
	,@Mobile = NULL 
	,@ProfileImageUrl = NULL 
	,@Education = NULL 
	,@Notes = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_Member] 


*/

CREATE PROCEDURE [dbo].[csp.Member.Update]
(
	@ActionType			VARCHAR(10) 
	,@MemberId 			INT = NULL 
	,@OrganizationId	UNIQUEIDENTIFIER = NULL 
	,@FirstName			NVARCHAR(150) = NULL 
	,@LastName 			NVARCHAR(150) = NULL 
	,@SystemRoleId  	INT = NULL 
	,@Designation   	NVARCHAR(250) = NULL 
	,@Level    			NVARCHAR(250) = NULL 
	,@Department    	NVARCHAR(250) = NULL 
	,@Address  			NVARCHAR(250) = NULL 
	,@PrimaryContactEmail	VARCHAR(150) = NULL 
	,@Phone    				VARCHAR(25) = NULL 
	,@Mobile   				VARCHAR(25) = NULL 
	,@ProfileImageUrl    	NVARCHAR(500) = NULL 
	,@Education			NVARCHAR(200) = NULL 
	,@Notes    			NVARCHAR(250) = NULL 
	,@Position			NVARCHAR(200)=NULL
	,@Company			NVARCHAR(200)=NULL
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnMemberId 	INT =	0 OUTPUT 
)
AS 

SET @ReturnMemberId = @MemberId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_Member] WHERE MemberId = @MemberId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_Member] 
				( 
					[OrganizationId]  
					,[FirstName]  
					,[LastName]  
					,[SystemRoleId]  
					,[Designation]  
					,[Level]  
					,[Department]  
					,[Address]  
					,[PrimaryContactEmail]  
					,[Phone]  
					,[Mobile]  
					,[ProfileImageUrl]  
					,[Education]  
					,[Notes]  
					,[Position]
					,[CompanyName]
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@FirstName  
					,@LastName  
					,@SystemRoleId  
					,@Designation  
					,@Level  
					,@Department  
					,@Address  
					,@PrimaryContactEmail  
					,@Phone  
					,@Mobile  
					,@ProfileImageUrl  
					,@Education  
					,@Notes  
					,@Position
					,@Company
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnMemberId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_Member] 
				SET 
					[OrganizationId]		 = @OrganizationId  
					,[FirstName]			 = @FirstName  
					,[LastName] 			 = @LastName  
					,[SystemRoleId]  		 = @SystemRoleId  
					,[Designation]   		 = @Designation  
					,[Level]    			 = @Level  
					,[Department]    		 = @Department  
					,[Address]  			 = @Address  
					,[PrimaryContactEmail]	 = @PrimaryContactEmail  
					,[Phone]    			 = @Phone  
					,[Mobile]   			 = @Mobile  
					,[ProfileImageUrl]    	 = @ProfileImageUrl  
					,[Education]			 = @Education  
					,[Notes]    			 = @Notes  
					,[Position]				 =@Position
					,[CompanyName]			 =@Company
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE MemberId = @MemberId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_Member] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE MemberId = @MemberId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 


GO
/****** Object:  StoredProcedure [dbo].[csp.Modules.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 6/22/2018 11:56:40 PM 
DESC: GET DATA FROM TABLE [dbo].[Modules]


EXEC dbo.[csp.Modules.Get]
@ModuleId = NULL 
,@ModuleTitle = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.Modules.Get]
(
	@ModuleId 		INT = NULL 
	,@ModuleTitle   NVARCHAR(500) = NULL 
	,@AssociatedCourseId	INT  =NULL
	,@UserId		NVARCHAR(256) = NULL 
	,@PageNumber	INT = 1 
	,@PageSize 		INT = 20 
	,@ShowAll		INT = 0 

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TM.ModuleId)) AS RowNumber 
				,TM.[ModuleId] AS [ModuleId]
				,TM.[OrganizationId] AS [OrganizationId]
				,TM.[ModuleTitle] AS [ModuleTitle]
				,ISNULL(TM.[ModuleDescription],'') AS [ModuleDescription]
				,ISNULL(TM.[ModuleContent],'') AS [ModuleContent]
				,TM.[AssociatedCourseId] AS [AssociatedCourseId]
				,ISNULL(TM.[ModulePeriod],0) AS [ModulePeriod]
				,ISNULL(TM.[ModulePeriodTypeId],0) AS [ModulePeriodTypeId] 
				,ISNULL(CPT.[PeriodTypeName],'') AS [PeriodTypeName]
				,COUNT(TM.[ModuleId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_Modules] TM WITH(NOLOCK) 
			LEFT JOIN 
				[dbo].[TBL_CoursePeriodType] CPT WITH(NOLOCK) ON (CPT.[CoursePeriodTypeId] = TM.[ModulePeriodTypeId])
			WHERE 
				(ISNULL(@ModuleId,0) = 0 OR TM.[ModuleId] = @ModuleId )
				AND (ISNULL(@ModuleTitle,'') = '' OR TM.[ModuleTitle] = @ModuleTitle )
				AND (ISNULL(@AssociatedCourseId,0) = 0 OR TM.[AssociatedCourseId] = @AssociatedCourseId)
				AND TM.DeletedDate IS NULL 
		) 


		SELECT 
			RowNumber 
			,[ModuleId]
			,[OrganizationId]
			,[ModuleTitle]
			,[ModuleDescription]
			,[ModuleContent]
			,[AssociatedCourseId]
			,[ModulePeriod]
			,[ModulePeriodTypeId] 
			,[PeriodTypeName]
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 


GO
/****** Object:  StoredProcedure [dbo].[csp.Modules.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 22/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[Modules]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.Modules.Update]
	@ActionType = 'ADD' 
	,@ModuleId = NULL 
	,@OrganizationId = NULL 
	,@ModuleTitle = NULL 
	,@ModuleDescription = NULL 
	,@ModuleContent = NULL 
	,@AssociatedCourseId = NULL 
	,@ModulePeriod = NULL 
	,@ModulePeriodTypeId = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_Modules] 


*/

CREATE PROCEDURE [dbo].[csp.Modules.Update]
(
	@ActionType			VARCHAR(10) 
	,@ModuleId 			INT = NULL 
	,@OrganizationId	UNIQUEIDENTIFIER = NULL 
	,@ModuleTitle   	NVARCHAR(500) = NULL 
	,@ModuleDescription NVARCHAR(1000) = NULL 
	,@ModuleContent 	NTEXT = NULL 
	,@AssociatedCourseId INT = NULL 
	,@ModulePeriod  	INT = NULL 
	,@ModulePeriodTypeId INT = NULL 
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnModulesId 		INT =	0 OUTPUT 
)
AS 

SET @ReturnModulesId = @ModuleId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_Modules] WHERE ModuleId = @ModuleId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_Modules] 
				( 
					[OrganizationId]  
					,[ModuleTitle]  
					,[ModuleDescription]  
					,[ModuleContent]  
					,[AssociatedCourseId]  
					,[ModulePeriod]  
					,[ModulePeriodTypeId]  
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@ModuleTitle  
					,@ModuleDescription  
					,@ModuleContent  
					,@AssociatedCourseId  
					,@ModulePeriod  
					,@ModulePeriodTypeId  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnModulesId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_Modules] 
				SET 
					[OrganizationId]		 = @OrganizationId  
					,[ModuleTitle]   		 = @ModuleTitle  
					,[ModuleDescription]  	 = @ModuleDescription  
					,[ModuleContent] 		 = @ModuleContent  
					,[AssociatedCourseId] 	 = @AssociatedCourseId  
					,[ModulePeriod]  		 = @ModulePeriod  
					,[ModulePeriodTypeId] 	 = @ModulePeriodTypeId  
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE ModuleId = @ModuleId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_Modules] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE ModuleId = @ModuleId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 


GO
/****** Object:  StoredProcedure [dbo].[csp.QueuedEmail.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
/*
Created by: Prashant 
Created on: 10/12/2017 11:44:34 PM 
DESC: GET DATA FROM TABLE [dbo].[QueuedEmail]


EXEC dbo.[csp.QueuedEmail.Get]
@QueuedEmailId = NULL 
,@LoadNotSentItemsOnly = 0
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.QueuedEmail.Get]
(
	@QueuedEmailId 		INT = NULL 
	,@LoadNotSentItemsOnly BIT = NULL
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TQ.QueuedEmailId)) AS RowNumber 
				,TQ.[QueuedEmailId] AS [QueuedEmailId] 
				,TQ.[FromAddress] AS [FromAddress]
				,TQ.[FromName] AS [FromName]
				,TQ.[ToAddress] AS [ToAddress]
				,TQ.[ToName] AS [ToName]
				,ISNULL(TQ.[Cc],'') AS [Cc]
				,ISNULL(TQ.[Bcc],'') AS [Bcc]
				,TQ.[SubjectCode] AS [SubjectCode]
				,TQ.[Subject] AS [Subject]
				,TQ.[Body] AS [Body]
				,ISNULL(TQ.[SentDate],'') AS [SentDate]
				,ISNULL(TQ.[SentTries],0) AS [SentTries]
				,ISNULL(TQ.[Priority],0) AS [Priority]
				,COUNT(TQ.[QueuedEmailId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_QueuedEmail] TQ WITH(NOLOCK) 
			WHERE 
				(ISNULL(@QueuedEmailId,0) = 0 OR TQ.[QueuedEmailId] = @QueuedEmailId)
				AND (@LoadNotSentItemsOnly IS NULL OR (TQ.SentDate IS NULL AND @LoadNotSentItemsOnly=1) OR (TQ.SentDate IS NOT NULL AND @LoadNotSentItemsOnly=0))
		) 


		SELECT 
			RowNumber 
			,[QueuedEmailId] 
			,[FromAddress]
			,[FromName]
			,[ToAddress]
			,[ToName]
			,[Cc]
			,[Bcc]
			,[SubjectCode]
			,[Subject]
			,[Body]
			,[SentDate]
			,[SentTries]
			,[Priority]
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 










GO
/****** Object:  StoredProcedure [dbo].[csp.QueuedEmail.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
/*
Created by: Prashant 
Created on: 12/10/2017 
DESC: UPDATE DATA TO TABLE [dbo].[QueuedEmail]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.QueuedEmail.Update]
	@ActionType = 'ADD' 
	,@QueuedEmailId = NULL 
	,@OrganizationId = NULL 
	,@FromAddress = NULL 
	,@FromName = NULL 
	,@ToAddress = NULL 
	,@ToName = NULL 
	,@Cc = NULL 
	,@Bcc = NULL 
	,@SubjectCode = NULL 
	,@Subject = NULL 
	,@Body = NULL 
	,@SentDate = NULL 
	,@SentTries = NULL 
	,@Priority = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_QueuedEmail] 


*/

CREATE PROCEDURE [dbo].[csp.QueuedEmail.Update]
(
	@ActionType			VARCHAR(10) 
	,@QueuedEmailId 	INT = NULL  
	,@FromAddress   	NVARCHAR(200) = NULL 
	,@FromName 			NVARCHAR(200) = NULL 
	,@ToAddress			NVARCHAR(200) = NULL 
	,@ToName   			NVARCHAR(200) = NULL 
	,@Cc  				NVARCHAR(1000) = NULL 
	,@Bcc 				NVARCHAR(1000) = NULL 
	,@SubjectCode   	NVARCHAR(100) = NULL 
	,@Subject  			NVARCHAR(200) = NULL 
	,@Body				NVARCHAR(MAX) = NULL 
	,@SentDate 			DATETIME = NULL 
	,@SentTries			INT = NULL 
	,@Priority 			INT = NULL 
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(20) =	NULL OUTPUT 
	,@MsgText			VARCHAR(200) =	NULL OUTPUT 
	,@ReturnQueuedEmailId 		INT =	0 OUTPUT 
)
AS 

SET @ReturnQueuedEmailId = @QueuedEmailId; 
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_QueuedEmail] WHERE QueuedEmailId = @QueuedEmailId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_QueuedEmail] 
				( 
					 [FromAddress]  
					,[FromName]  
					,[ToAddress]  
					,[ToName]  
					,[Cc]  
					,[Bcc]  
					,[SubjectCode]  
					,[Subject]  
					,[Body]  
					,[SentDate]  
					,[SentTries]  
					,[Priority]  
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					 @FromAddress  
					,@FromName  
					,@ToAddress  
					,@ToName  
					,@Cc  
					,@Bcc  
					,@SubjectCode  
					,@Subject  
					,@Body  
					,@SentDate  
					,@SentTries  
					,@Priority  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnQueuedEmailId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_QueuedEmail] 
				SET 
					 [FromAddress]   		 = @FromAddress  
					,[FromName] 			 = @FromName  
					,[ToAddress]			 = @ToAddress  
					,[ToName]   			 = @ToName  
					,[Cc]  					 = @Cc  
					,[Bcc] 					 = @Bcc  
					,[SubjectCode]   		 = @SubjectCode  
					,[Subject]  			 = @Subject  
					,[Body]					 = @Body  
					,[SentDate] 			 = @SentDate  
					,[SentTries]			 = @SentTries  
					,[Priority] 			 = @Priority  
				WHERE QueuedEmailId = @QueuedEmailId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		DELETE FROM [dbo].[TBL_QueuedEmail] 
		WHERE QueuedEmailId = @QueuedEmailId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 










GO
/****** Object:  StoredProcedure [dbo].[csp.Status.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 6/24/2018 9:23:42 PM 
DESC: GET DATA FROM TABLE [dbo].[Status]


EXEC dbo.[csp.Status.Get]
@StatusId = NULL 
,@StatusGroup = NULL 
,@StatusName = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.Status.Get]
(
	@StatusId 			INT = NULL 
	,@StatusGroup   	VARCHAR(100) = NULL 
	,@StatusName    	VARCHAR(100) = NULL 
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TS.StatusId)) AS RowNumber 
				,TS.[StatusId] AS [StatusId]
				,TS.[OrganizationId] AS [OrganizationId]
				,TS.[StatusGroup] AS [StatusGroup]
				,TS.[StatusName] AS [StatusName]
				,TS.[StatusCode] AS [StatusCode]
				,TS.[StatusOrder] AS [StatusOrder]
				,ISNULL(TS.[AllowBackRevert],'') AS [AllowBackRevert] 
				,COUNT(TS.[StatusId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_Status] TS WITH(NOLOCK) 
			WHERE 
				(ISNULL(@StatusId,0) = 0 OR TS.[StatusId] = @StatusId )
				AND (ISNULL(@StatusGroup,'') = '' OR TS.[StatusGroup] = @StatusGroup )
				AND (ISNULL(@StatusName,'') = '' OR TS.[StatusName] = @StatusName )
				AND TS.DeletedDate IS NULL 
		) 


		SELECT 
			RowNumber 
			,[StatusId]
			,[OrganizationId]
			,[StatusGroup]
			,[StatusName]
			,[StatusCode]
			,[StatusOrder]
			,[AllowBackRevert] 
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.Status.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 24/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[Status]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.Status.Update]
	@ActionType = 'ADD' 
	,@StatusId = NULL 
	,@OrganizationId = NULL 
	,@StatusGroup = NULL 
	,@StatusName = NULL 
	,@StatusCode = NULL 
	,@StatusOrder = NULL 
	,@AllowBackRevert = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_Status] 


*/

CREATE PROCEDURE [dbo].[csp.Status.Update]
(
	@ActionType			VARCHAR(10) 
	,@StatusId 			INT = NULL 
	,@OrganizationId	UNIQUEIDENTIFIER = NULL 
	,@StatusGroup   	VARCHAR(100) = NULL 
	,@StatusName    	VARCHAR(100) = NULL 
	,@StatusCode    	NVARCHAR(20) = NULL 
	,@StatusOrder   	INT = NULL 
	,@AllowBackRevert   BIT = NULL 
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnStatusId 		INT =	0 OUTPUT 
)
AS 

SET @ReturnStatusId = @StatusId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_Status] WHERE StatusId = @StatusId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				IF(ISNULL(@StatusOrder,0) = 0) 
				BEGIN
					SELECT 
						@StatusOrder = StatusOrder
					FROM [dbo].[TBL_Status] WHERE StatusGroup =@StatusGroup 
					AND DeletedDAte IS NULL;
					
					SET @StatusOrder = ISNULL(@StatusOrder,0) + 1;

				END

				INSERT INTO [dbo].[TBL_Status] 
				( 
					[OrganizationId]  
					,[StatusGroup]  
					,[StatusName]  
					,[StatusCode]  
					,[StatusOrder]  
					,[AllowBackRevert]  
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@StatusGroup  
					,@StatusName  
					,@StatusCode  
					,@StatusOrder  
					,@AllowBackRevert  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnStatusId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_Status] 
				SET 
					[StatusGroup]   		 = @StatusGroup  
					,[StatusName]    		 = @StatusName  
					,[StatusCode]    		 = @StatusCode  
					,[StatusOrder]   		 = @StatusOrder  
					,[AllowBackRevert]    	 = @AllowBackRevert  
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE StatusId = @StatusId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_Status] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE StatusId = @StatusId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.TableColumns.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

/*
Created by: Prashant 
Created on: 5/3/2016
DESC: GET List of Table Columns


EXEC [dbo].[csp.TableColumns.Get]
@TableId = 'Vendor'


*/

CREATE PROCEDURE [dbo].[csp.TableColumns.Get]
(
	@TableId VARCHAR(100)
)
AS 
	BEGIN 
		
		SELECT	
			--C1.TABLE_NAME,
			C1.COLUMN_NAME, 
			C1.DATA_TYPE, 
			ISNULL(C1.CHARACTER_MAXIMUM_LENGTH,0) AS CHARACTER_MAXIMUM_LENGTH, 
			C1.IS_NULLABLE,
			CASE WHEN COLUMNPROPERTY(object_id(C1.TABLE_NAME), C1.COLUMN_NAME, 'IsIdentity') = 1 THEN 'TRUE' ELSE 'FALSE' END AS IS_IDENTITY,
			CASE WHEN  C1.COLUMN_NAME IN( 'CreatedDate','CreatedBy','UpdatedDate','UpdatedBy','DeletedDate','DeletedBy') OR COLUMNPROPERTY(object_id(C1.TABLE_NAME), C1.COLUMN_NAME, 'IsIdentity') = 1 THEN '' ELSE C1.COLUMN_NAME  END AS DISPLAY_NAME,
			CASE WHEN  C1.COLUMN_NAME IN( 'CreatedDate','CreatedBy','UpdatedDate','UpdatedBy','DeletedDate','DeletedBy') OR COLUMNPROPERTY(object_id(C1.TABLE_NAME), C1.COLUMN_NAME, 'IsIdentity') = 1 THEN 'Disabled' ELSE 
			CASE 
				 WHEN C1.COLUMN_NAME LIKE '%Email%' THEN 'Email' 
				 WHEN C1.COLUMN_NAME LIKE '%Id' AND C1.DATA_TYPE ='int' THEN 'Dropdown'
				 WHEN C1.COLUMN_NAME LIKE '%on' OR C1.DATA_TYPE LIKE '%date%' THEN 'Date'
				 WHEN C1.COLUMN_NAME LIKE '%Date%' THEN 'Date'
				 WHEN C1.COLUMN_NAME LIKE 'Is%' OR C1.COLUMN_NAME LIKE 'Has%'  OR C1.DATA_TYPE  = 'bit' THEN 'Checkbox'
				 ELSE 'Textbox' END
			END AS SHOW_AS,
			CASE WHEN  C1.COLUMN_NAME IN( 'CreatedDate','CreatedBy','UpdatedDate','UpdatedBy','DeletedDate','DeletedBy') OR COLUMNPROPERTY(object_id(C1.TABLE_NAME), C1.COLUMN_NAME, 'IsIdentity') = 1 THEN 'False' ELSE 'True'  END AS SHOW_IN_FILTER,
			 ISNULL(FC.constraint_column_id,0) AS IsForeignKey
		FROM 
			sys.tables T
		INNER JOIN 
			INFORMATION_SCHEMA.COLUMNS  C1 ON C1.TABLE_NAME = T.name
		INNER JOIN 
			SYS.COLUMNS C2 ON C2.name = C1.COLUMN_NAME  AND C2.object_id = T.object_id
		LEFT JOIN 
			SYS.foreign_key_columns FC ON FC.parent_object_id = T.object_id AND FC.parent_column_id = C2.column_id
		WHERE 
			C1.TABLE_NAME = @TableId ORDER BY C1.ORDINAL_POSITION

	END












GO
/****** Object:  StoredProcedure [dbo].[csp.Trainee.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
/*
Created by: Prashant 
Created on: 6/17/2018 8:08:40 PM 
DESC: GET DATA FROM TABLE [dbo].[Member]

ALTER TABLE [TBL_Member]
ADD Position NVARCHAR(200) NULL

ALTER TABLE [TBL_Member]
ADD CompanyName NVARCHAR(200) NULL

EXEC dbo.[csp.Member.Get]
@MemberId = NULL 
,@Designation = NULL 
,@Phone = NULL 
,@Mobile = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/




CREATE PROCEDURE [dbo].[csp.Trainee.Get]
(
	@MemberId 		INT = NULL 
	,@Designation   NVARCHAR(250) = NULL 
	,@Phone    		VARCHAR(25) = NULL 
	,@Mobile   		VARCHAR(25) = NULL 
	,@UserId		NVARCHAR(256) = NULL 
	,@PageNumber	INT = 1 
	,@PageSize 		INT = 20 
	,@ShowAll		INT = 0 
	,@Keyword		NVARCHAR(200)=NULL

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TM.MemberId)) AS RowNumber 
				,TM.[MemberId] AS [MemberId]
				,TM.[OrganizationId] AS [OrganizationId]
				,TM.[FirstName] AS [FirstName]
				,TM.[LastName] AS [LastName]
				,TM.[SystemRoleId] AS [SystemRoleId]
				,ISNULL(TM.[Designation],'') AS [Designation]
				,ISNULL(TM.[Level],'') AS [Level]
				,ISNULL(TM.[Department],'') AS [Department]
				,ISNULL(TM.[Address],'') AS [Address]
				,ISNULL(TM.[PrimaryContactEmail],'') AS [PrimaryContactEmail]
				,ISNULL(TM.[Phone],'') AS [Phone]
				,ISNULL(TM.[Mobile],'') AS [Mobile]
				,ISNULL(TM.[ProfileImageUrl],'') AS [ProfileImageUrl] 
				,ISNULL(TM.[Education],'') AS [Education]
				,ISNULL(TM.[Notes],'') AS [Notes]
				,ISNULL(TM.Position,'') AS Position
				,ISNULL(TM.CompanyName,'') AS CompanyName
				,COUNT(TM.[MemberId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_Member] TM WITH(NOLOCK) 
			WHERE 
				 (ISNULL(@MemberId,0) = 0 OR TM.[MemberId] = @MemberId )
				 AND (TM.[Designation] = 'TRAINEE' )
				AND (ISNULL(@Phone,'') = '' OR TM.[Phone] = @Phone )
				AND (ISNULL(@Mobile,'') = '' OR TM.[Mobile] = @Mobile )
				AND (ISNULL(@Keyword,'')='' OR (TM.Mobile LIKE '%'+@Keyword+'%' OR TM.Phone LIKE '%'+@Keyword+'%' OR TM.FirstName LIKE '%'+@Keyword+'%' OR TM.LastName LIKE '%'+@Keyword+'%' 
				OR TM.PrimaryContactEmail LIKE '%'+@Keyword+'%'))
				AND TM.DeletedDate IS NULL 
		) 


		SELECT 
			RowNumber 
			,[MemberId]
			,[OrganizationId]
			,[FirstName]
			,[LastName]
			,[SystemRoleId]
			,[Designation]
			,[Level]
			,[Department]
			,[Address]
			,[PrimaryContactEmail]
			,[Phone]
			,[Mobile]
			,[ProfileImageUrl] 
			,[Education]
			,[Notes]
			,[Position]
			,[CompanyName]
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 


GO
/****** Object:  StoredProcedure [dbo].[csp.TraineeLevel.Get]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 6/26/2018 3:18:10 PM 
DESC: GET DATA FROM TABLE [dbo].[TraineeLevel]


EXEC dbo.[csp.TraineeLevel.Get]
@TraineeLevelId = NULL 
,@TraineeLevelName = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

CREATE PROCEDURE [dbo].[csp.TraineeLevel.Get]
(
	@TraineeLevelId		INT = NULL 
	,@TraineeLevelName  NVARCHAR(100) = NULL 
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TT.TraineeLevelId)) AS RowNumber 
				,TT.[TraineeLevelId] AS [TraineeLevelId] 
				,TT.[TraineeLevelName] AS [TraineeLevelName]
				,ISNULL(TT.[Remarks],'') AS [Remarks] 
				,COUNT(TT.[TraineeLevelId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_TraineeLevel] TT WITH(NOLOCK) 
			WHERE 
				(ISNULL(@TraineeLevelId,0) = 0 OR TT.[TraineeLevelId] = @TraineeLevelId )
				AND (ISNULL(@TraineeLevelName,'') = '' OR TT.[TraineeLevelName] = @TraineeLevelName )
				AND TT.DeletedDate IS NULL 
		) 


		SELECT 
			RowNumber 
			,[TraineeLevelId] 
			,[TraineeLevelName]
			,[Remarks] 
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 

GO
/****** Object:  StoredProcedure [dbo].[csp.TraineeLevel.Update]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 26/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[TraineeLevel]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.TraineeLevel.Update]
	@ActionType = 'ADD' 
	,@TraineeLevelId = NULL 
	,@OrganizationId = NULL 
	,@TraineeLevelName = NULL 
	,@Remarks = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_TraineeLevel] 


*/

CREATE PROCEDURE [dbo].[csp.TraineeLevel.Update]
(
	@ActionType			VARCHAR(10) 
	,@TraineeLevelId	INT = NULL 
	,@OrganizationId	UNIQUEIDENTIFIER = NULL 
	,@TraineeLevelName  NVARCHAR(100) = NULL 
	,@Remarks  			NVARCHAR(1000) = NULL 
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnTraineeLevelId 		INT =	0 OUTPUT 
)
AS 

SET @ReturnTraineeLevelId = @TraineeLevelId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_TraineeLevel] WHERE TraineeLevelId = @TraineeLevelId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_TraineeLevel] 
				( 
					[OrganizationId]  
					,[TraineeLevelName]  
					,[Remarks]  
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@TraineeLevelName  
					,@Remarks  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnTraineeLevelId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_TraineeLevel] 
				SET 
					[OrganizationId]		 = @OrganizationId  
					,[TraineeLevelName]   	 = @TraineeLevelName  
					,[Remarks]  			 = @Remarks  
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE TraineeLevelId = @TraineeLevelId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_TraineeLevel] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE TraineeLevelId = @TraineeLevelId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 

GO
/****** Object:  Table [dbo].[Applications]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Applications](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[ApplicationName] [nvarchar](235) NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[RoleGroup] [nvarchar](50) NULL,
	[OrganizationId] [uniqueidentifier] NULL,
	[Discriminator] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[IsEmailVerified] [bit] NULL,
	[Discriminator] [nvarchar](128) NULL,
	[FirstName] [nvarchar](150) NULL,
	[LastName] [nvarchar](150) NULL,
	[OrganizationId] [uniqueidentifier] NULL,
	[StaffId] [int] NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Memberships]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Memberships](
	[UserId] [uniqueidentifier] NOT NULL,
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordFormat] [int] NOT NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[PasswordQuestion] [nvarchar](256) NULL,
	[PasswordAnswer] [nvarchar](128) NULL,
	[IsApproved] [bit] NOT NULL,
	[IsLockedOut] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastLoginDate] [datetime] NOT NULL,
	[LastPasswordChangedDate] [datetime] NOT NULL,
	[LastLockoutDate] [datetime] NOT NULL,
	[FailedPasswordAttemptCount] [int] NOT NULL,
	[FailedPasswordAttemptWindowStart] [datetime] NOT NULL,
	[FailedPasswordAnswerAttemptCount] [int] NOT NULL,
	[FailedPasswordAnswerAttemptWindowsStart] [datetime] NOT NULL,
	[Comment] [nvarchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NLogEntries]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NLogEntries](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[Level] [nvarchar](10) NULL,
	[Logger] [nvarchar](128) NULL,
	[CreatedOnUtc] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Profiles]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Profiles](
	[UserId] [uniqueidentifier] NOT NULL,
	[PropertyNames] [nvarchar](max) NOT NULL,
	[PropertyValueStrings] [nvarchar](max) NOT NULL,
	[PropertyValueBinary] [varbinary](max) NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleId] [uniqueidentifier] NOT NULL,
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[RoleName] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_Batch]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Batch](
	[BatchId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[BatchCode] [varchar](100) NOT NULL,
	[BatchName] [varchar](500) NULL,
	[BatchLevel] [nvarchar](100) NULL,
	[BatchCategory] [nvarchar](100) NULL,
	[Coordinator] [nvarchar](200) NULL,
	[ASSCoordinator] [nvarchar](200) NULL,
	[BatchStartDate] [datetime] NOT NULL,
	[BatchEndDate] [datetime] NULL,
	[BatchSummary] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_Batch] PRIMARY KEY CLUSTERED 
(
	[BatchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_BatchCourse]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_BatchCourse](
	[BatchCourseId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[BatchId] [int] NOT NULL,
	[CourseId] [int] NOT NULL,
	[CourseStartDate] [datetime] NULL,
	[Remarks] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_BatchCourse] PRIMARY KEY CLUSTERED 
(
	[BatchCourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_BatchSummary]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_BatchSummary](
	[BatchSummaryId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[BatchId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[Summary] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_BatchSummary] PRIMARY KEY CLUSTERED 
(
	[BatchSummaryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_BatchTrainee]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_BatchTrainee](
	[BatchTraineeId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[BatchId] [int] NOT NULL,
	[MemberId] [int] NOT NULL,
	[JoinedDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_BatchTrainee] PRIMARY KEY CLUSTERED 
(
	[BatchTraineeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_BatchTrainer]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_BatchTrainer](
	[BatchTrainerId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[MemberId] [int] NOT NULL,
	[BatchId] [int] NOT NULL,
	[ModuleId] [int] NULL,
	[TrainingStartDate] [datetime] NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_BatchTrainer] PRIMARY KEY CLUSTERED 
(
	[BatchTrainerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_Course]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Course](
	[CourseId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[CourseFullName] [nvarchar](500) NOT NULL,
	[CourseCode] [nvarchar](100) NOT NULL,
	[CourseShortName] [nvarchar](200) NOT NULL,
	[CourseDescription] [nvarchar](1000) NULL,
	[CourseCategoryId] [int] NULL,
	[CourseSubCategoryId] [int] NULL,
	[CourseLevelId] [int] NULL,
	[CourseContent] [ntext] NULL,
	[CoursePeriod] [int] NULL,
	[CoursePeriodTypeId] [int] NULL,
	[TraineeLevelDesc] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_Course] PRIMARY KEY CLUSTERED 
(
	[CourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_CourseCategory]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_CourseCategory](
	[CourseCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[CategoryName] [nvarchar](100) NOT NULL,
	[Remarks] [nvarchar](100) NULL,
	[ParentCategoryId] [int] NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CourseCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_CourseLevel]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_CourseLevel](
	[CourseLevelId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[LevelName] [nvarchar](100) NOT NULL,
	[Remarks] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CourseLevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_CourseMedia]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_CourseMedia](
	[CourseMediaId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[CourseId] [int] NOT NULL,
	[TopicId] [int] NULL,
	[MediaURL] [nvarchar](500) NOT NULL,
	[MediaType] [int] NOT NULL,
	[Remarks] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_CourseMedia] PRIMARY KEY CLUSTERED 
(
	[CourseMediaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_CoursePeriodType]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_CoursePeriodType](
	[CoursePeriodTypeId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[PeriodTypeName] [nvarchar](100) NOT NULL,
	[Remarks] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CoursePeriodTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_CourseTraineeLevel]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_CourseTraineeLevel](
	[CourseTraineeLevelId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[TraineeLevelId] [int] NOT NULL,
	[CourseId] [int] NOT NULL,
 CONSTRAINT [PK_TBL_CourseTraineeLevel] PRIMARY KEY CLUSTERED 
(
	[CourseTraineeLevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_EmailFormat]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_EmailFormat](
	[EmailFormatId] [int] IDENTITY(1,1) NOT NULL,
	[From] [nvarchar](500) NULL,
	[FromName] [nvarchar](500) NULL,
	[Bcc] [nvarchar](500) NULL,
	[Subject] [nvarchar](500) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_dbo.TBL_EmailFormat] PRIMARY KEY CLUSTERED 
(
	[EmailFormatId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_Media]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Media](
	[MediaId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[MediaName] [nvarchar](500) NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
	[Remarks] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_Member]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Member](
	[MemberId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[LastName] [nvarchar](150) NOT NULL,
	[SystemRoleId] [int] NOT NULL,
	[Designation] [nvarchar](250) NULL,
	[Level] [nvarchar](250) NULL,
	[Department] [nvarchar](250) NULL,
	[Address] [nvarchar](250) NULL,
	[PrimaryContactEmail] [varchar](150) NULL,
	[Phone] [varchar](25) NULL,
	[Mobile] [varchar](25) NULL,
	[ProfileImageUrl] [nvarchar](500) NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
	[Education] [nvarchar](200) NULL,
	[Notes] [nvarchar](250) NULL,
	[Position] [nvarchar](200) NULL,
	[CompanyName] [nvarchar](200) NULL,
 CONSTRAINT [PK_TBL_Members] PRIMARY KEY CLUSTERED 
(
	[MemberId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Modules]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Modules](
	[ModuleId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[ModuleTitle] [nvarchar](500) NOT NULL,
	[ModuleDescription] [nvarchar](1000) NULL,
	[ModuleContent] [ntext] NULL,
	[AssociatedCourseId] [int] NOT NULL,
	[ModulePeriod] [int] NULL,
	[ModulePeriodTypeId] [int] NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_Topic] PRIMARY KEY CLUSTERED 
(
	[ModuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_Organization]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Organization](
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[OrganizationName] [nvarchar](150) NOT NULL,
	[OrganizationCode] [varchar](25) NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
	[AssociatedSupplierId] [int] NULL,
	[OrganizationAddress] [nvarchar](200) NULL,
	[OrganizationLogoUrl] [nvarchar](200) NULL,
	[Telephone] [nvarchar](200) NULL,
	[Fax] [nvarchar](200) NULL,
	[ContactPerson] [nvarchar](200) NULL,
	[Email] [nvarchar](200) NULL,
	[DomainName] [nvarchar](200) NULL,
	[ServiceBaseUrl] [nvarchar](200) NULL,
	[TimeOffsetInMins] [int] NULL,
 CONSTRAINT [PK_TBL_Organization] PRIMARY KEY CLUSTERED 
(
	[OrganizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_QueuedEmail]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_QueuedEmail](
	[QueuedEmailId] [int] IDENTITY(1,1) NOT NULL,
	[FromAddress] [nvarchar](200) NOT NULL,
	[FromName] [nvarchar](200) NOT NULL,
	[ToAddress] [nvarchar](200) NOT NULL,
	[ToName] [nvarchar](200) NOT NULL,
	[Cc] [nvarchar](1000) NULL,
	[Bcc] [nvarchar](1000) NULL,
	[SubjectCode] [nvarchar](100) NOT NULL,
	[Subject] [nvarchar](200) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[SentDate] [datetime] NULL,
	[SentTries] [int] NULL,
	[Priority] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_TBL_QueuedEmail] PRIMARY KEY CLUSTERED 
(
	[QueuedEmailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_Status]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Status](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[StatusGroup] [varchar](100) NOT NULL,
	[StatusName] [varchar](100) NOT NULL,
	[StatusCode] [nvarchar](20) NOT NULL,
	[StatusOrder] [int] NOT NULL,
	[AllowBackRevert] [bit] NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_Status] PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_SystemRole]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_SystemRole](
	[SystemRoleId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[RoleName] [varchar](100) NOT NULL,
	[RoleGroup] [varchar](100) NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_Designation] PRIMARY KEY CLUSTERED 
(
	[SystemRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_TraineeLevel]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_TraineeLevel](
	[TraineeLevelId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[TraineeLevelName] [nvarchar](100) NOT NULL,
	[Remarks] [nvarchar](1000) NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_TraineeLevel] PRIMARY KEY CLUSTERED 
(
	[TraineeLevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [uniqueidentifier] NOT NULL,
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[IsAnonymous] [bit] NOT NULL,
	[LastActivityDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UsersInRoles]    Script Date: 7/2/2018 5:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersInRoles](
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [RoleGroup], [OrganizationId], [Discriminator]) VALUES (N'438de475-127b-4adc-8812-9c21410e3602', N'ADMIN', N'ALL', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'ApplicationRole')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [RoleGroup], [OrganizationId], [Discriminator]) VALUES (N'6FF94A8A-4FDE-4AE2-A470-9D6521B0E638', N'SUPERADMIN', N'ALL', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'ApplicationRole')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [RoleGroup], [OrganizationId], [Discriminator]) VALUES (N'fb474377-84d3-4a9a-83f5-7809a775cd79', N'COORDINATOR', N'ALL', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'ApplicationRole')
GO
SET IDENTITY_INSERT [dbo].[AspNetUserClaims] ON 

GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (31539, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (31540, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (31541, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (31542, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32539, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32540, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32541, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32542, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32543, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32544, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32545, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32546, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32547, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32548, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32549, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32550, N'85b25794-5974-4b2b-bd6b-04b9fd44059d', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32551, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32552, N'85b25794-5974-4b2b-bd6b-04b9fd44059d', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32553, N'85b25794-5974-4b2b-bd6b-04b9fd44059d', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32554, N'85b25794-5974-4b2b-bd6b-04b9fd44059d', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32555, N'85b25794-5974-4b2b-bd6b-04b9fd44059d', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32556, N'685c6e80-7b21-4a6b-985a-a441d0a01333', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32557, N'85b25794-5974-4b2b-bd6b-04b9fd44059d', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (32558, N'85b25794-5974-4b2b-bd6b-04b9fd44059d', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
SET IDENTITY_INSERT [dbo].[AspNetUserClaims] OFF
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'685c6e80-7b21-4a6b-985a-a441d0a01333', N'fb474377-84d3-4a9a-83f5-7809a775cd79')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'85b25794-5974-4b2b-bd6b-04b9fd44059d', N'6FF94A8A-4FDE-4AE2-A470-9D6521B0E638')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'438de475-127b-4adc-8812-9c21410e3602')
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [IsEmailVerified], [Discriminator], [FirstName], [LastName], [OrganizationId], [StaffId]) VALUES (N'685c6e80-7b21-4a6b-985a-a441d0a01333', N'prashant@rolpotech.com', 1, N'ABl+BlTL4zZMcLXGGjnEV0d6e7Np2zZ359Qj31wrQGPw7TwiOY4gaHIQgJjHqHwLIA==', N'e30833ef-faf7-45c9-9187-cc18936e6936', NULL, 0, 0, NULL, 0, 0, N'prashant@rolpotech.com', NULL, NULL, N'Prashant', N'Chalise', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 0)
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [IsEmailVerified], [Discriminator], [FirstName], [LastName], [OrganizationId], [StaffId]) VALUES (N'85b25794-5974-4b2b-bd6b-04b9fd44059d', N'superadmin@rolpo.com', 1, N'AD4/j/PuFvrzaGKOCCMEgABaIJeb3dYMwOMCUWZ+KFjk810mGwnzwd9uzWzVhus34g==', N'25a5bb4e-5c6d-459d-8de2-ea0995ac1534', NULL, 0, 0, NULL, 0, 0, N'superadmin@rolpo.com', 1, N'ApplicationUser ', N'SuperAdmin', N'SuperAdmin', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 0)
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [IsEmailVerified], [Discriminator], [FirstName], [LastName], [OrganizationId], [StaffId]) VALUES (N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'info@rolpotech.com', 1, N'AAGE9CRNPkhYx5wat74c7IKmCgAjjQ79dBtBrkhCbGeCQV75C2qkL0ghzM44YeWvxw==', N'c232fc53-9672-433d-a744-b25882637c22', NULL, 0, 0, NULL, 0, 0, N'info@rolpotech.com', NULL, NULL, N'Prashant', N'Chalise', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 27)
GO
SET IDENTITY_INSERT [dbo].[TBL_Batch] ON 

GO
INSERT [dbo].[TBL_Batch] ([BatchId], [OrganizationId], [BatchCode], [BatchName], [BatchLevel], [BatchCategory], [Coordinator], [ASSCoordinator], [BatchStartDate], [BatchEndDate], [BatchSummary], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (1, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Batch code 1', N'Batch name1', N'fasdf', N'fasdf', N'coord 1', N'asst coord 1', CAST(0x0000A8FE012CC030 AS DateTime), CAST(0x0000A90C012CC030 AS DateTime), N'test', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90600B5B394 AS DateTime), N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A911002D7AD3 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TBL_Batch] ([BatchId], [OrganizationId], [BatchCode], [BatchName], [BatchLevel], [BatchCategory], [Coordinator], [ASSCoordinator], [BatchStartDate], [BatchEndDate], [BatchSummary], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (2, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'fasdf', N'fasdf', NULL, NULL, NULL, NULL, CAST(0x0000A903012CC030 AS DateTime), NULL, NULL, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90600B725F1 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_Batch] ([BatchId], [OrganizationId], [BatchCode], [BatchName], [BatchLevel], [BatchCategory], [Coordinator], [ASSCoordinator], [BatchStartDate], [BatchEndDate], [BatchSummary], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (3, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Bat-075/76-01', N'First batch in 075/76', NULL, NULL, N'Ramesh Dhungel', N'Sita Aryal', CAST(0x0000A8FC012CC030 AS DateTime), CAST(0x0000A8F5012CC030 AS DateTime), N'batch is for induction', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00B435FC AS DateTime), NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TBL_Batch] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_BatchCourse] ON 

GO
INSERT [dbo].[TBL_BatchCourse] ([BatchCourseId], [OrganizationId], [BatchId], [CourseId], [CourseStartDate], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (1, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 1, CAST(0x0000A900012CC030 AS DateTime), N'fadfs', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A909009EEA95 AS DateTime), N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A91100307EA8 AS DateTime), N'superadmin@rolpo.com', CAST(0x0000A9110075F8A6 AS DateTime))
GO
INSERT [dbo].[TBL_BatchCourse] ([BatchCourseId], [OrganizationId], [BatchId], [CourseId], [CourseStartDate], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (2, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 1, CAST(0x0000A914012CC030 AS DateTime), N'fafasfsda', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A909009EF5CB AS DateTime), NULL, NULL, N'superadmin@rolpo.com', CAST(0x0000A9110075FD19 AS DateTime))
GO
INSERT [dbo].[TBL_BatchCourse] ([BatchCourseId], [OrganizationId], [BatchId], [CourseId], [CourseStartDate], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (3, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 1, CAST(0x0000A90C012CC030 AS DateTime), N'adsfa af dafd', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A909009F013D AS DateTime), NULL, NULL, N'superadmin@rolpo.com', CAST(0x0000A9110076031A AS DateTime))
GO
INSERT [dbo].[TBL_BatchCourse] ([BatchCourseId], [OrganizationId], [BatchId], [CourseId], [CourseStartDate], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (4, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 2, 1, CAST(0x0000A8FE012CC030 AS DateTime), NULL, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90900A4E986 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_BatchCourse] ([BatchCourseId], [OrganizationId], [BatchId], [CourseId], [CourseStartDate], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (5, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 3, 2, CAST(0x0000A8FC012CC030 AS DateTime), N'course remarks', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00B6974F AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_BatchCourse] ([BatchCourseId], [OrganizationId], [BatchId], [CourseId], [CourseStartDate], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (6, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 3, 1, CAST(0x0000A8F6012CC030 AS DateTime), NULL, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00B74CF8 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_BatchCourse] ([BatchCourseId], [OrganizationId], [BatchId], [CourseId], [CourseStartDate], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (7, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 1, CAST(0x0000A912012CC030 AS DateTime), N'fafdasfttest', N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A9110073E666 AS DateTime), N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A9110076177A AS DateTime), N'superadmin@rolpo.com', CAST(0x0000A9110076355E AS DateTime))
GO
INSERT [dbo].[TBL_BatchCourse] ([BatchCourseId], [OrganizationId], [BatchId], [CourseId], [CourseStartDate], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (8, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 2, CAST(0x0000A921012CC030 AS DateTime), NULL, N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A91100760CB4 AS DateTime), NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TBL_BatchCourse] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_BatchSummary] ON 

GO
INSERT [dbo].[TBL_BatchSummary] ([BatchSummaryId], [OrganizationId], [BatchId], [StatusId], [Summary], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (1, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 5, N'test', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A9090114DA88 AS DateTime), NULL, NULL, N'info@rolpotech.com', CAST(0x0000A90901163A4E AS DateTime))
GO
INSERT [dbo].[TBL_BatchSummary] ([BatchSummaryId], [OrganizationId], [BatchId], [StatusId], [Summary], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (2, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 5, N'fadfasf ff asdfsaf', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A9090126FC00 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_BatchSummary] ([BatchSummaryId], [OrganizationId], [BatchId], [StatusId], [Summary], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (3, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 5, N'FADSFASFA', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A9090128616E AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_BatchSummary] ([BatchSummaryId], [OrganizationId], [BatchId], [StatusId], [Summary], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (4, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 5, N'FASDFDASFASF', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A909012865B8 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_BatchSummary] ([BatchSummaryId], [OrganizationId], [BatchId], [StatusId], [Summary], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (5, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 3, 5, N'fasdfdaf', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00B8B06E AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_BatchSummary] ([BatchSummaryId], [OrganizationId], [BatchId], [StatusId], [Summary], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (6, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 3, 5, NULL, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00B8B793 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_BatchSummary] ([BatchSummaryId], [OrganizationId], [BatchId], [StatusId], [Summary], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (7, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 3, 5, N'gsdfgsg gs', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00B8BDA8 AS DateTime), NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TBL_BatchSummary] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_BatchTrainee] ON 

GO
INSERT [dbo].[TBL_BatchTrainee] ([BatchTraineeId], [OrganizationId], [BatchId], [MemberId], [JoinedDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (1, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 1, CAST(0x0000A90C012CC030 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A9090090867F AS DateTime), NULL, NULL, N'superadmin@rolpo.com', CAST(0x0000A911006FB016 AS DateTime))
GO
INSERT [dbo].[TBL_BatchTrainee] ([BatchTraineeId], [OrganizationId], [BatchId], [MemberId], [JoinedDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (2, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 1, CAST(0x0000A904012CC030 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A9090090962D AS DateTime), NULL, NULL, N'superadmin@rolpo.com', CAST(0x0000A911006FB1BC AS DateTime))
GO
INSERT [dbo].[TBL_BatchTrainee] ([BatchTraineeId], [OrganizationId], [BatchId], [MemberId], [JoinedDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (3, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 1, CAST(0x0000A90C012CC030 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A9090090D3D7 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A9090093185D AS DateTime), N'superadmin@rolpo.com', CAST(0x0000A911006FB378 AS DateTime))
GO
INSERT [dbo].[TBL_BatchTrainee] ([BatchTraineeId], [OrganizationId], [BatchId], [MemberId], [JoinedDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (4, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 1, CAST(0x0000A8FC012CC030 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A909009106A7 AS DateTime), NULL, NULL, N'superadmin@rolpo.com', CAST(0x0000A911006FB4EB AS DateTime))
GO
INSERT [dbo].[TBL_BatchTrainee] ([BatchTraineeId], [OrganizationId], [BatchId], [MemberId], [JoinedDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (5, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 2, 1, CAST(0x0000A913012CC030 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90900A4FF17 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_BatchTrainee] ([BatchTraineeId], [OrganizationId], [BatchId], [MemberId], [JoinedDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (8, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 3, CAST(0x0000A922012CC030 AS DateTime), N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A911006F7D2B AS DateTime), NULL, NULL, N'superadmin@rolpo.com', CAST(0x0000A911006FB915 AS DateTime))
GO
INSERT [dbo].[TBL_BatchTrainee] ([BatchTraineeId], [OrganizationId], [BatchId], [MemberId], [JoinedDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (9, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 5, CAST(0x0000A929012CC030 AS DateTime), N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A911006FA8AB AS DateTime), NULL, NULL, N'superadmin@rolpo.com', CAST(0x0000A911006FBAEE AS DateTime))
GO
INSERT [dbo].[TBL_BatchTrainee] ([BatchTraineeId], [OrganizationId], [BatchId], [MemberId], [JoinedDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (10, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 1, CAST(0x0000A928012CC030 AS DateTime), N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A911006FC233 AS DateTime), NULL, NULL, N'superadmin@rolpo.com', CAST(0x0000A911006FCDA3 AS DateTime))
GO
INSERT [dbo].[TBL_BatchTrainee] ([BatchTraineeId], [OrganizationId], [BatchId], [MemberId], [JoinedDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (11, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 3, CAST(0x0000A928012CC030 AS DateTime), N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A911006FC384 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_BatchTrainee] ([BatchTraineeId], [OrganizationId], [BatchId], [MemberId], [JoinedDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (12, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 5, CAST(0x0000A928012CC030 AS DateTime), N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A911006FC4AB AS DateTime), NULL, NULL, N'superadmin@rolpo.com', CAST(0x0000A91100702A51 AS DateTime))
GO
INSERT [dbo].[TBL_BatchTrainee] ([BatchTraineeId], [OrganizationId], [BatchId], [MemberId], [JoinedDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (13, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 1, CAST(0x0000A929012CC030 AS DateTime), N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A911007031B7 AS DateTime), NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TBL_BatchTrainee] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_BatchTrainer] ON 

GO
INSERT [dbo].[TBL_BatchTrainer] ([BatchTrainerId], [OrganizationId], [MemberId], [BatchId], [ModuleId], [TrainingStartDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (1, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 2, 1, 1, CAST(0x0000A8FE012CC030 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90900637BDA AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A909009436DE AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TBL_BatchTrainer] ([BatchTrainerId], [OrganizationId], [MemberId], [BatchId], [ModuleId], [TrainingStartDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (2, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 2, 1, 1, CAST(0x0000A914012CC030 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A909009492C3 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_BatchTrainer] ([BatchTrainerId], [OrganizationId], [MemberId], [BatchId], [ModuleId], [TrainingStartDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (3, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 2, 2, 1, CAST(0x0000A904012CC030 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90900A4F800 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_BatchTrainer] ([BatchTrainerId], [OrganizationId], [MemberId], [BatchId], [ModuleId], [TrainingStartDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (4, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 4, 3, 3, CAST(0x0000A90B012CC030 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00BD1DEE AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_BatchTrainer] ([BatchTrainerId], [OrganizationId], [MemberId], [BatchId], [ModuleId], [TrainingStartDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (5, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 4, 3, 4, CAST(0x0000A90C012CC030 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00BD2ABD AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_BatchTrainer] ([BatchTrainerId], [OrganizationId], [MemberId], [BatchId], [ModuleId], [TrainingStartDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (6, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 2, 1, 3, CAST(0x0000A921012CC030 AS DateTime), N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A9110079166A AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_BatchTrainer] ([BatchTrainerId], [OrganizationId], [MemberId], [BatchId], [ModuleId], [TrainingStartDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (7, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 2, 1, 3, NULL, N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A911007BCCA5 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_BatchTrainer] ([BatchTrainerId], [OrganizationId], [MemberId], [BatchId], [ModuleId], [TrainingStartDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (8, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 2, 1, 0, NULL, N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A911007C02EB AS DateTime), NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TBL_BatchTrainer] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_Course] ON 

GO
INSERT [dbo].[TBL_Course] ([CourseId], [OrganizationId], [CourseFullName], [CourseCode], [CourseShortName], [CourseDescription], [CourseCategoryId], [CourseSubCategoryId], [CourseLevelId], [CourseContent], [CoursePeriod], [CoursePeriodTypeId], [TraineeLevelDesc], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (1, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'test', N'test', N'test', N'des test tes tste uPDATED.', 2, 3, 1, N'<meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge">
<title></title>
<!-- Tell the browser to be responsive to screen width --><meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"><!-- Bootstrap 3.3.7 -->
<link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" /><!-- Font Awesome -->
<link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" /><!-- Ionicons -->
<link href="../../bower_components/Ionicons/css/ionicons.min.css" rel="stylesheet" /><!-- Theme style -->
<link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" /><!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link href="../../dist/css/skins/_all-skins.min.css" rel="stylesheet" /><!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries --><!-- WARNING: Respond.js doesn''t work if you view the page via file:// --><!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]--><!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" rel="stylesheet" /><script type="text/javascript" src="https://gc.kis.v2.scr.kaspersky-labs.com/FF3630FD-A663-BB45-8FF9-5B6F34ED4E1B/main.js" charset="UTF-8"></script>
<style type="text/css">.color-palette {
      height: 35px;
      line-height: 35px;
      text-align: center;
    }

    .color-palette-set {
      margin-bottom: 15px;
    }

    .color-palette span {
      display: none;
      font-size: 12px;
    }

    .color-palette:hover span {
      display: block;
    }

    .color-palette-box h4 {
      position: absolute;
      top: 100%;
      left: 25px;
      margin-top: -40px;
      color: rgba(255, 255, 255, 0.8);
      font-size: 12px;
      display: block;
      z-index: 7;
    }
</style>
<div class="wrapper">
<header class="main-header"><b>TESTTET TESTSTE</b></header>
</div>
<!-- ./wrapper --><!-- jQuery 3 --><script src="../../bower_components/jquery/dist/jquery.min.js"></script><!-- Bootstrap 3.3.7 --><script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script><!-- FastClick --><script src="../../bower_components/fastclick/lib/fastclick.js"></script><!-- AdminLTE App --><script src="../../dist/js/adminlte.min.js"></script><!-- AdminLTE for demo purposes --><script src="../../dist/js/demo.js"></script>', 2, 1, N'fasdfsd', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A907008F0721 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90800F96318 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TBL_Course] ([CourseId], [OrganizationId], [CourseFullName], [CourseCode], [CourseShortName], [CourseDescription], [CourseCategoryId], [CourseSubCategoryId], [CourseLevelId], [CourseContent], [CoursePeriod], [CoursePeriodTypeId], [TraineeLevelDesc], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (2, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Networking Fundamentals', N'IT-NET01', N'IT-NET01', N'Networking Fundamentals', 8, 9, 1, N'<p>summary goes here.</p>
', 5, 4, NULL, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00A0CDFF AS DateTime), NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TBL_Course] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_CourseCategory] ON 

GO
INSERT [dbo].[TBL_CourseCategory] ([CourseCategoryId], [OrganizationId], [CategoryName], [Remarks], [ParentCategoryId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (1, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Category1', N'Remarksfadf asfdas', 0, NULL, NULL, N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A9100101EF8B AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TBL_CourseCategory] ([CourseCategoryId], [OrganizationId], [CategoryName], [Remarks], [ParentCategoryId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (2, N'36d03f8a-5a7f-4f37-8e83-d82e801ebc52', N'Category2', N'Remarks', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_CourseCategory] ([CourseCategoryId], [OrganizationId], [CategoryName], [Remarks], [ParentCategoryId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (3, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'SubCategory111', N'Remarks', 1, NULL, NULL, N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A9100101F928 AS DateTime), N'superadmin@rolpo.com', CAST(0x0000A91001020D70 AS DateTime))
GO
INSERT [dbo].[TBL_CourseCategory] ([CourseCategoryId], [OrganizationId], [CategoryName], [Remarks], [ParentCategoryId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (4, N'36d03f8a-5a7f-4f37-8e83-d82e801ebc52', N'SubCategory12', NULL, 1, NULL, NULL, NULL, NULL, N'superadmin@rolpo.com', CAST(0x0000A91001024EB3 AS DateTime))
GO
INSERT [dbo].[TBL_CourseCategory] ([CourseCategoryId], [OrganizationId], [CategoryName], [Remarks], [ParentCategoryId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (5, N'36d03f8a-5a7f-4f37-8e83-d82e801ebc52', N'SubCategory21', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_CourseCategory] ([CourseCategoryId], [OrganizationId], [CategoryName], [Remarks], [ParentCategoryId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (6, N'36d03f8a-5a7f-4f37-8e83-d82e801ebc52', N'SubCategory22', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_CourseCategory] ([CourseCategoryId], [OrganizationId], [CategoryName], [Remarks], [ParentCategoryId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (7, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Category3', N'Remarksfadf asfdas', NULL, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90B00A3F84B AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90B00A4ADCC AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TBL_CourseCategory] ([CourseCategoryId], [OrganizationId], [CategoryName], [Remarks], [ParentCategoryId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (8, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Information Technology', N'Information Technology', NULL, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F009DFBE0 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_CourseCategory] ([CourseCategoryId], [OrganizationId], [CategoryName], [Remarks], [ParentCategoryId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (9, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Networking', N'Networking', 8, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F009EF57E AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_CourseCategory] ([CourseCategoryId], [OrganizationId], [CategoryName], [Remarks], [ParentCategoryId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (10, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Category111', N'Remarksfadf asfdas', 1, N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A91001025B44 AS DateTime), NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TBL_CourseCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_CourseLevel] ON 

GO
INSERT [dbo].[TBL_CourseLevel] ([CourseLevelId], [OrganizationId], [LevelName], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (1, N'36d03f8a-5a7f-4f37-8e83-d82e801ebc52', N'Level1', N'', NULL, NULL, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90C00F16138 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TBL_CourseLevel] ([CourseLevelId], [OrganizationId], [LevelName], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (2, N'36d03f8a-5a7f-4f37-8e83-d82e801ebc52', N'Level2', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TBL_CourseLevel] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_CourseMedia] ON 

GO
INSERT [dbo].[TBL_CourseMedia] ([CourseMediaId], [OrganizationId], [CourseId], [TopicId], [MediaURL], [MediaType], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (1, 0, 1, 1, N'http://localhost:61546//Images/Courses/Azure_hosting_Charges_Geeksum_270618034932.jpg', 1, N'fafaf', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90C00A60ECF AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90D00B9A394 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TBL_CourseMedia] ([CourseMediaId], [OrganizationId], [CourseId], [TopicId], [MediaURL], [MediaType], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (2, 0, 2, 3, N'http://localhost:61546//Images/Courses/CC_Instruction_5142018_300618034454.pdf', 2, N'document related to the course Networking Fundamentals', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00A51627 AS DateTime), N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A91001100988 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TBL_CourseMedia] ([CourseMediaId], [OrganizationId], [CourseId], [TopicId], [MediaURL], [MediaType], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (3, 0, 2, 3, N'https://www.investopedia.com/terms/n/networking.asp', 3, N'more resourses from the url', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00A6AC18 AS DateTime), NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TBL_CourseMedia] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_CoursePeriodType] ON 

GO
INSERT [dbo].[TBL_CoursePeriodType] ([CoursePeriodTypeId], [OrganizationId], [PeriodTypeName], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (1, N'36d03f8a-5a7f-4f37-8e83-d82e801ebc52', N'HOUR/S', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_CoursePeriodType] ([CoursePeriodTypeId], [OrganizationId], [PeriodTypeName], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (4, N'36d03f8a-5a7f-4f37-8e83-d82e801ebc52', N'DAY/S', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_CoursePeriodType] ([CoursePeriodTypeId], [OrganizationId], [PeriodTypeName], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (5, N'36d03f8a-5a7f-4f37-8e83-d82e801ebc52', N'MONTH/S', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TBL_CoursePeriodType] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_CourseTraineeLevel] ON 

GO
INSERT [dbo].[TBL_CourseTraineeLevel] ([CourseTraineeLevelId], [OrganizationId], [TraineeLevelId], [CourseId]) VALUES (17, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 2, 1)
GO
INSERT [dbo].[TBL_CourseTraineeLevel] ([CourseTraineeLevelId], [OrganizationId], [TraineeLevelId], [CourseId]) VALUES (18, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 1, 1)
GO
INSERT [dbo].[TBL_CourseTraineeLevel] ([CourseTraineeLevelId], [OrganizationId], [TraineeLevelId], [CourseId]) VALUES (19, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 2, 2)
GO
INSERT [dbo].[TBL_CourseTraineeLevel] ([CourseTraineeLevelId], [OrganizationId], [TraineeLevelId], [CourseId]) VALUES (20, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 4, 2)
GO
SET IDENTITY_INSERT [dbo].[TBL_CourseTraineeLevel] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_EmailFormat] ON 

GO
INSERT [dbo].[TBL_EmailFormat] ([EmailFormatId], [From], [FromName], [Bcc], [Subject], [Body], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, N'mail@rolpo.com', N'Rolpo', NULL, N'Rolpo.ForgotPassword', N'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>[SUBJECT]</title>
	<style type="text/css">
		body {
			padding-top: 0 !important;
			padding-bottom: 0 !important;
			padding-top: 0 !important;
			padding-bottom: 0 !important;
			margin: 0 !important;
			width: 100% !important;
			-webkit-text-size-adjust: 100% !important;
			-ms-text-size-adjust: 100% !important;
			-webkit-font-smoothing: antialiased !important;
		}

		.tableContent img {
			border: 0 !important;
			display: block !important;
			outline: none !important;
		}

		a {
			color: #382F2E;
		}

		p,
		h1,
		h2,
		ul,
		ol,
		li,
		div {
			margin: 0;
			padding: 0;
		}

		h1,
		h2 {
			font-weight: normal;
			background: transparent !important;
			border: none !important;
		}

		@media only screen and (max-width: 480px) {
			table[class="MainContainer"],
			td[class="cell"] {
				width: 100% !important;
				height: auto !important;
			}

			td[class="specbundle"] {
				width: 100% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 15px !important;
			}

			td[class="specbundle2"] {
				width: 80% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 10px !important;
				padding-left: 10% !important;
				padding-right: 10% !important;
			}

			td[class="spechide"] {
				display: none !important;
			}

			img[class="banner"] {
				width: 100% !important;
				height: auto !important;
			}

			td[class="left_pad"] {
				padding-left: 15px !important;
				padding-right: 15px !important;
			}
		}

		@media only screen and (max-width: 540px) {
			table[class="MainContainer"],
			td[class="cell"] {
				width: 100% !important;
				height: auto !important;
			}

			td[class="specbundle"] {
				width: 100% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 15px !important;
			}

			td[class="specbundle2"] {
				width: 80% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 10px !important;
				padding-left: 10% !important;
				padding-right: 10% !important;
			}

			td[class="spechide"] {
				display: none !important;
			}

			img[class="banner"] {
				width: 100% !important;
				height: auto !important;
			}

			td[class="left_pad"] {
				padding-left: 15px !important;
				padding-right: 15px !important;
			}
		}

		.contentEditable h2.big,
		.contentEditable h1.big {
			font-size: 26px !important;
		}

		.contentEditable h2.bigger,
		.contentEditable h1.bigger {
			font-size: 37px !important;
		}

		td,
		table {
			vertical-align: top;
		}

			td.middle {
				vertical-align: middle;
			}

		a.link1 {
			font-size: 13px;
			color: #27A1E5;
			line-height: 24px;
			text-decoration: none;
		}

		a {
			text-decoration: none;
		}

		.link2 {
			color: #ffffff;
			border-top: 10px solid #27A1E5;
			border-bottom: 10px solid #27A1E5;
			border-left: 18px solid #27A1E5;
			border-right: 18px solid #27A1E5;
			border-radius: 3px;
			-moz-border-radius: 3px;
			-webkit-border-radius: 3px;
			background: #27A1E5;
		}

		.link3 {
			color: #555555;
			border: 1px solid #cccccc;
			padding: 10px 18px;
			border-radius: 3px;
			-moz-border-radius: 3px;
			-webkit-border-radius: 3px;
			background: #ffffff;
		}

		.link4 {
			color: #27A1E5;
			line-height: 24px;
		}

		h2,
		h1 {
			line-height: 20px;
		}

		p {
			font-size: 14px;
			line-height: 21px;
			color: #555555;
		}

		.contentEditable li {
		}

		.appart p {
		}

		.bgItem {
			background: #ffffff;
		}

		.bgBody {
			background: #ffffff;
		}

		img {
			outline: none;
			text-decoration: none;
			-ms-interpolation-mode: bicubic;
			width: auto;
			max-width: 100%;
			clear: both;
			display: block;
			float: none;
		}
	</style>


	<script type="colorScheme" class="swatch active">
		{ "name":"Default", "bgBody":"ffffff", "link":"27A1E5", "color":"AAAAAA", "bgItem":"ffffff", "title":"444444" }
	</script>


</head>

<body paddingwidth="0" paddingheight="0" bgcolor="#d1d3d4" style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td>
					<table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" style="font-family:helvetica, sans-serif;" class="MainContainer">
						<!-- =============== START HEADER =============== -->
						<tbody>
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tbody>
											<tr>
												<td valign="top" width="20">&nbsp;</td>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td class="movableContentContainer">
																	<div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tbody>
																				<tr>
																					<td height="15"></td>
																				</tr>
																				<tr>
																					<td>
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tbody>
																								<tr>
																									<td valign="top">
																										<table width="100%" border="0" cellspacing="0" cellpadding="0">
																											<tbody>
																												<tr>
																													<td valign="top" width="120">
																														 
																													</td>
																													<td width="10" valign="top">&nbsp;</td>
																													<td valign="middle" style="vertical-align: middle;">
																														<div class="contentEditableContainer contentTextEditable">
																															<div class="contentEditable" style="text-align: left;font-weight: light; color:#555555;font-size:26;line-height: 39px;font-family: Helvetica Neue;">
																															</div>
																														</div>
																													</td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<hr style="height:1px;background:#DDDDDD;border:none;" />
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																	<div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">


																	</div>
																	<div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tbody>
																				<tr>
																					<td height="40"></td>
																				</tr>
																				<tr>
																					<td>
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tbody>
																								<tr>
																									<td class="specbundle">
																										<div class="contentEditableContainer contentTextEditable">
																											<div style="text-align: left;">
																												<br />
																												<p>
																													Dear [[TO_NAME]],
																													<br />
																													<br /> Please click <a href="[[PROFILE_URL]]" style="color:blue"> here</a> to reset your password.!
																													<br /> If you have any questions, comments, or concerns, Please feel free to contact us via email at [[ORG_EMAIL]] or call us at [[ORG_PHONE]]
																													<br />
																													<br /> Regards,
																													<br /> Ensure Dental Team
																													<br />

																												</p>
																												<br />
																												<p style="font-size:10px;">THIS IS AN AUTOMATICALLY GENERATED EMAIL.PLEASE DO NOT REPLY.</p>
																											</div>
																										</div>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																			</tbody>
																		</table>



																	</div>

																</td>
															</tr>
														</tbody>
													</table>
												</td>
												<td valign="top" width="20">&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>


</body>

</html>', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_EmailFormat] ([EmailFormatId], [From], [FromName], [Bcc], [Subject], [Body], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (4, N'mail@rolpo.com', N'Rolpo', NULL, N'Rolpo.RegisterUser', N'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>[SUBJECT]</title>
	<style type="text/css">
		body {
			padding-top: 0 !important;
			padding-bottom: 0 !important;
			padding-top: 0 !important;
			padding-bottom: 0 !important;
			margin: 0 !important;
			width: 100% !important;
			-webkit-text-size-adjust: 100% !important;
			-ms-text-size-adjust: 100% !important;
			-webkit-font-smoothing: antialiased !important;
		}

		.tableContent img {
			border: 0 !important;
			display: block !important;
			outline: none !important;
		}

		a {
			color: #382F2E;
		}

		p, h1, h2, ul, ol, li, div {
			margin: 0;
			padding: 0;
		}

		h1, h2 {
			font-weight: normal;
			background: transparent !important;
			border: none !important;
		}

		@media only screen and (max-width:480px) {

			table[class="MainContainer"], td[class="cell"] {
				width: 100% !important;
				height: auto !important;
			}

			td[class="specbundle"] {
				width: 100% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 15px !important;
			}

			td[class="specbundle2"] {
				width: 80% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 10px !important;
				padding-left: 10% !important;
				padding-right: 10% !important;
			}

			td[class="spechide"] {
				display: none !important;
			}

			img[class="banner"] {
				width: 100% !important;
				height: auto !important;
			}

			td[class="left_pad"] {
				padding-left: 15px !important;
				padding-right: 15px !important;
			}
		}

		@media only screen and (max-width:540px) {

			table[class="MainContainer"], td[class="cell"] {
				width: 100% !important;
				height: auto !important;
			}

			td[class="specbundle"] {
				width: 100% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 15px !important;
			}

			td[class="specbundle2"] {
				width: 80% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 10px !important;
				padding-left: 10% !important;
				padding-right: 10% !important;
			}

			td[class="spechide"] {
				display: none !important;
			}

			img[class="banner"] {
				width: 100% !important;
				height: auto !important;
			}

			td[class="left_pad"] {
				padding-left: 15px !important;
				padding-right: 15px !important;
			}
		}

		.contentEditable h2.big, .contentEditable h1.big {
			font-size: 26px !important;
		}

		.contentEditable h2.bigger, .contentEditable h1.bigger {
			font-size: 37px !important;
		}

		td, table {
			vertical-align: top;
		}

			td.middle {
				vertical-align: middle;
			}

		a.link1 {
			font-size: 13px;
			color: #27A1E5;
			line-height: 24px;
			text-decoration: none;
		}

		a {
			text-decoration: none;
		}

		.link2 {
			color: #ffffff;
			border-top: 10px solid #27A1E5;
			border-bottom: 10px solid #27A1E5;
			border-left: 18px solid #27A1E5;
			border-right: 18px solid #27A1E5;
			border-radius: 3px;
			-moz-border-radius: 3px;
			-webkit-border-radius: 3px;
			background: #27A1E5;
		}

		.link3 {
			color: #555555;
			border: 1px solid #cccccc;
			padding: 10px 18px;
			border-radius: 3px;
			-moz-border-radius: 3px;
			-webkit-border-radius: 3px;
			background: #ffffff;
		}

		.link4 {
			color: #27A1E5;
			line-height: 24px;
		}

		h2, h1 {
			line-height: 20px;
		}

		p {
			font-size: 14px;
			line-height: 21px;
			color: #555555;
		}

		.contentEditable li {
		}

		.appart p {
		}

		.bgItem {
			background: #ffffff;
		}

		.bgBody {
			background: #ffffff;
		}

		img {
			outline: none;
			text-decoration: none;
			-ms-interpolation-mode: bicubic;
			width: auto;
			max-width: 100%;
			clear: both;
			display: block;
			float: none;
		}
	</style>

	<script type="colorScheme" class="swatch active">
		{
		"name":"Default",
		"bgBody":"ffffff",
		"link":"27A1E5",
		"color":"AAAAAA",
		"bgItem":"ffffff",
		"title":"444444"
		}
	</script>

</head>
<body paddingwidth="0" paddingheight="0" bgcolor="#d1d3d4" style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td>
					<table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" style="font-family:helvetica, sans-serif;" class="MainContainer">
						<!-- =============== START HEADER =============== -->
						<tbody>
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tbody>
											<tr>
												<td valign="top" width="20">&nbsp;</td>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td class="movableContentContainer">
																	<div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tbody>
																				<tr>
																					<td height="15"></td>
																				</tr>
																				<tr>
																					<td>
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tbody>
																								<tr>
																									<td valign="top">
																										<table width="100%" border="0" cellspacing="0" cellpadding="0">
																											<tbody>
																												<tr>
																													<td valign="top" width="120"> </td>
																													<td width="10" valign="top">&nbsp;</td>
																													<td valign="middle" style="vertical-align: middle;">
																														<div class="contentEditableContainer contentTextEditable">
																															<div class="contentEditable" style="text-align: left;font-weight: light; color:#555555;font-size:26;line-height: 39px;font-family: Helvetica Neue;">
																															</div>
																														</div>
																													</td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																				<tr>
																					<td><hr style="height:1px;background:#DDDDDD;border:none;" /></td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																	<div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">

																	</div>
																	<div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tbody>
																				<tr>
																					<td height="40"></td>
																				</tr>
																				<tr>
																					<td>
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tbody>
																								<tr>
																									<td class="specbundle">
																										<div class="contentEditableContainer contentTextEditable">
																											<div style="text-align: left;">
																												<br />
																												<p>
																													Dear <b> [[TO_NAME]] </b>,
																													<br /><br />
																													You are now registered with <a href="[[ORG_URL]]">[[USER_DOMAIN]]</a>.
																													You have received this email because this email address was used during registration.
																													<br />
																													If you did not register to our service, please disregard this email.
																													You do not need to unsubscribe or take any further action. <br /><br />
																													<b>Account Activation Instructions</b><br />      <br />
																													We require that you verify your email to ensure that the email address you entered was correct.
																													To verify your email, simply click      <a href="[[CONFIRM_LINK]]" style="color:blue;">here.</a><br />
																													<br />
																													Your login/password details are as follows:
																													<br /><br />
																													<b>Username: </b> [[USER_NAME]]<br />
																													<b>Password: </b> [[USER_PASSWORD]]
																													<br /><br />
																													Regards,<br />
																													<b>[[CREATEDBY_USER]]</b><br />
																													--------------------------------
																												</p>
																												<br />
																												<p style="font-size:10px;">THIS IS AN AUTOMATICALLY GENERATED EMAIL.PLEASE DO NOT REPLY.</p>
																											</div>
																										</div>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																			</tbody>
																		</table>

																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
												<td valign="top" width="20">&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>

</body>
</html>
', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_EmailFormat] ([EmailFormatId], [From], [FromName], [Bcc], [Subject], [Body], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (5, N'mail@rolpo.com', N'Rolpo', NULL, N'Rolpo.ConfirmEmail', N'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>[SUBJECT]</title>
	<style type="text/css">
		body {
			padding-top: 0 !important;
			padding-bottom: 0 !important;
			padding-top: 0 !important;
			padding-bottom: 0 !important;
			margin: 0 !important;
			width: 100% !important;
			-webkit-text-size-adjust: 100% !important;
			-ms-text-size-adjust: 100% !important;
			-webkit-font-smoothing: antialiased !important;
		}

		.tableContent img {
			border: 0 !important;
			display: block !important;
			outline: none !important;
		}

		a {
			color: #382F2E;
		}

		p, h1, h2, ul, ol, li, div {
			margin: 0;
			padding: 0;
		}

		h1, h2 {
			font-weight: normal;
			background: transparent !important;
			border: none !important;
		}

		@media only screen and (max-width:480px) {

			table[class="MainContainer"], td[class="cell"] {
				width: 100% !important;
				height: auto !important;
			}

			td[class="specbundle"] {
				width: 100% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 15px !important;
			}

			td[class="specbundle2"] {
				width: 80% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 10px !important;
				padding-left: 10% !important;
				padding-right: 10% !important;
			}

			td[class="spechide"] {
				display: none !important;
			}

			img[class="banner"] {
				width: 100% !important;
				height: auto !important;
			}

			td[class="left_pad"] {
				padding-left: 15px !important;
				padding-right: 15px !important;
			}
		}

		@media only screen and (max-width:540px) {

			table[class="MainContainer"], td[class="cell"] {
				width: 100% !important;
				height: auto !important;
			}

			td[class="specbundle"] {
				width: 100% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 15px !important;
			}

			td[class="specbundle2"] {
				width: 80% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 10px !important;
				padding-left: 10% !important;
				padding-right: 10% !important;
			}

			td[class="spechide"] {
				display: none !important;
			}

			img[class="banner"] {
				width: 100% !important;
				height: auto !important;
			}

			td[class="left_pad"] {
				padding-left: 15px !important;
				padding-right: 15px !important;
			}
		}

		.contentEditable h2.big, .contentEditable h1.big {
			font-size: 26px !important;
		}

		.contentEditable h2.bigger, .contentEditable h1.bigger {
			font-size: 37px !important;
		}

		td, table {
			vertical-align: top;
		}

			td.middle {
				vertical-align: middle;
			}

		a.link1 {
			font-size: 13px;
			color: #27A1E5;
			line-height: 24px;
			text-decoration: none;
		}

		a {
			text-decoration: none;
		}

		.link2 {
			color: #ffffff;
			border-top: 10px solid #27A1E5;
			border-bottom: 10px solid #27A1E5;
			border-left: 18px solid #27A1E5;
			border-right: 18px solid #27A1E5;
			border-radius: 3px;
			-moz-border-radius: 3px;
			-webkit-border-radius: 3px;
			background: #27A1E5;
		}

		.link3 {
			color: #555555;
			border: 1px solid #cccccc;
			padding: 10px 18px;
			border-radius: 3px;
			-moz-border-radius: 3px;
			-webkit-border-radius: 3px;
			background: #ffffff;
		}

		.link4 {
			color: #27A1E5;
			line-height: 24px;
		}

		h2, h1 {
			line-height: 20px;
		}

		p {
			font-size: 14px;
			line-height: 21px;
			color: #555555;
		}

		.contentEditable li {
		}

		.appart p {
		}

		.bgItem {
			background: #ffffff;
		}

		.bgBody {
			background: #ffffff;
		}

		img {
			outline: none;
			text-decoration: none;
			-ms-interpolation-mode: bicubic;
			width: auto;
			max-width: 100%;
			clear: both;
			display: block;
			float: none;
		}
	</style>

	<script type="colorScheme" class="swatch active">
		{
		"name":"Default",
		"bgBody":"ffffff",
		"link":"27A1E5",
		"color":"AAAAAA",
		"bgItem":"ffffff",
		"title":"444444"
		}
	</script>

</head>
<body paddingwidth="0" paddingheight="0" bgcolor="#d1d3d4" style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td>
					<table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" style="font-family:helvetica, sans-serif;" class="MainContainer">
						<!-- =============== START HEADER =============== -->
						<tbody>
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tbody>
											<tr>
												<td valign="top" width="20">&nbsp;</td>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td class="movableContentContainer">
																	<div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tbody>
																				<tr>
																					<td height="15"></td>
																				</tr>
																				<tr>
																					<td>
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tbody>
																								<tr>
																									<td valign="top">
																										<table width="100%" border="0" cellspacing="0" cellpadding="0">
																											<tbody>
																												<tr>
																													<td valign="top" width="120"> </td>
																													<td width="10" valign="top">&nbsp;</td>
																													<td valign="middle" style="vertical-align: middle;">
																														<div class="contentEditableContainer contentTextEditable">
																															<div class="contentEditable" style="text-align: left;font-weight: light; color:#555555;font-size:26;line-height: 39px;font-family: Helvetica Neue;">
																															</div>
																														</div>
																													</td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																				<tr>
																					<td><hr style="height:1px;background:#DDDDDD;border:none;" /></td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																	<div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">

																	</div>
																	<div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tbody>
																				<tr>
																					<td height="40"></td>
																				</tr>
																				<tr>
																					<td>
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tbody>
																								<tr>
																									<td class="specbundle">
																										<div class="contentEditableContainer contentTextEditable">
																											<div style="text-align: left;">
																												<br />
																												<p>
																													Dear <b> [[TO_NAME]] </b>,
																													<br /><br />
																													You are now registered with <a href="[[STORE_URL]]">[[USER_DOMAIN]]</a>.
																													You have received this email because this email address was used during registration.
																													<br />
																													If you did not register to our service, please disregard this email.
																													You do not need to unsubscribe or take any further action. <br /><br />
																													<b>Account Activation Instructions</b><br />      <br />
																													We require that you verify your email to ensure that the email address you entered was correct.
																													To verify your email, simply click below.<br /><a href="[[CONFIRM_LINK]]" style="color:blue;">[[CONFIRM_LINK]]</a><br />
																													<br />
																													Regards,<br />
																													<b>[[CREATEDBY_USER]]</b><br />
																													--------------------------------
																												</p>
																												<br />
																												<p style="font-size:10px;">THIS IS AN AUTOMATICALLY GENERATED EMAIL.PLEASE DO NOT REPLY.</p>
																											</div>
																										</div>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																			</tbody>
																		</table>

																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
												<td valign="top" width="20">&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>

</body>
</html>
', NULL, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TBL_EmailFormat] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_Media] ON 

GO
INSERT [dbo].[TBL_Media] ([MediaId], [OrganizationId], [MediaName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate], [Remarks]) VALUES (1, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Photos', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90600A7CE7F AS DateTime), N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A910010AF0B4 AS DateTime), NULL, NULL, N'fadfa sdfsdafzfsa')
GO
INSERT [dbo].[TBL_Media] ([MediaId], [OrganizationId], [MediaName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate], [Remarks]) VALUES (2, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Documents', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90600A7E9E8 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00A3BB76 AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_Media] ([MediaId], [OrganizationId], [MediaName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate], [Remarks]) VALUES (3, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'URL', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90600A826D3 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00A3C17D AS DateTime), NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TBL_Media] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_Member] ON 

GO
INSERT [dbo].[TBL_Member] ([MemberId], [OrganizationId], [FirstName], [LastName], [SystemRoleId], [Designation], [Level], [Department], [Address], [PrimaryContactEmail], [Phone], [Mobile], [ProfileImageUrl], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate], [Education], [Notes], [Position], [CompanyName]) VALUES (1, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Rashmi', N'Chalise', 0, N'TRAINEE', N'Level 1', N'Department', N'Address', N'Primary@test.com', N'PHone', N'mobile', N'http://localhost:61546///Images/Members/L5w8Rkg_240618120655.jpg', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90600A6092B AS DateTime), N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A91100298757 AS DateTime), NULL, NULL, N'test', N'test', N'ASSt. GUFF', N'NEA')
GO
INSERT [dbo].[TBL_Member] ([MemberId], [OrganizationId], [FirstName], [LastName], [SystemRoleId], [Designation], [Level], [Department], [Address], [PrimaryContactEmail], [Phone], [Mobile], [ProfileImageUrl], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate], [Education], [Notes], [Position], [CompanyName]) VALUES (2, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Prashant', N'Chalise', 0, N'TRAINER', N'BCT', N'BCT', N'Nakhipot', N'prashantchalise@hotmail.com', N'984150', N'984150', N'http://localhost:61546///Images/Members/NSIssue_240618120707.png', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A909005F7B90 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A9090068F4F9 AS DateTime), NULL, NULL, N'BE', N'NOTEST', NULL, NULL)
GO
INSERT [dbo].[TBL_Member] ([MemberId], [OrganizationId], [FirstName], [LastName], [SystemRoleId], [Designation], [Level], [Department], [Address], [PrimaryContactEmail], [Phone], [Mobile], [ProfileImageUrl], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate], [Education], [Notes], [Position], [CompanyName]) VALUES (3, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Rabindra', N'Ghimire', 0, N'TRAINEE', N'TRAINEE LEVEL 1', N'Information Technology Services', N'Jawlakhel', N'er.rabindra.ghimire@gmail.com', N'9843829658', N'9843829658', N'http://localhost:61546///Images/Members/interesting-modern-meeting-table-white-modern-style-office-furniture-meeting-desk-conference-table_300618040419.jpg', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00AAE92F AS DateTime), NULL, NULL, NULL, NULL, N'Bachelors in Comp Engineering', N'Working since 2010', N'Senior Engineer', N'Nepal Electricity Authority')
GO
INSERT [dbo].[TBL_Member] ([MemberId], [OrganizationId], [FirstName], [LastName], [SystemRoleId], [Designation], [Level], [Department], [Address], [PrimaryContactEmail], [Phone], [Mobile], [ProfileImageUrl], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate], [Education], [Notes], [Position], [CompanyName]) VALUES (4, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Sandeep', N'Pradhananga', 0, N'TRAINER', N'TRAINEE LEVEL 3', N'Dept_abc', N'New Baneshwor', N'sandeep.pradhananga@gmail.com', N'8054342434', N'8054342434', N'http://localhost:61546///Images/Members/L5w8Rkg_300618043532.jpg', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00B2B331 AS DateTime), NULL, NULL, NULL, NULL, N'EDucation', N'nothing for now.', N'Best position', N'abc company')
GO
INSERT [dbo].[TBL_Member] ([MemberId], [OrganizationId], [FirstName], [LastName], [SystemRoleId], [Designation], [Level], [Department], [Address], [PrimaryContactEmail], [Phone], [Mobile], [ProfileImageUrl], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate], [Education], [Notes], [Position], [CompanyName]) VALUES (5, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Prashant', N'Chalise', 0, N'TRAINEE', N'TRAINEE LEVEL 2', N'NEA', N'aDdr', NULL, N'11111', NULL, NULL, N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A911002B2C90 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, N'fafdas', N'NEA')
GO
INSERT [dbo].[TBL_Member] ([MemberId], [OrganizationId], [FirstName], [LastName], [SystemRoleId], [Designation], [Level], [Department], [Address], [PrimaryContactEmail], [Phone], [Mobile], [ProfileImageUrl], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate], [Education], [Notes], [Position], [CompanyName]) VALUES (6, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'PrashantTrainer', N'Trainer', 0, N'TRAINER', NULL, N'Department', N'Address', N'', N'Phone', N'Mob', NULL, N'85b25794-5974-4b2b-bd6b-04b9fd44059d', CAST(0x0000A911002B8A3A AS DateTime), NULL, NULL, NULL, NULL, N'fasd', N'fasdfs', N'pos', N'Rolpo TEch')
GO
SET IDENTITY_INSERT [dbo].[TBL_Member] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_Modules] ON 

GO
INSERT [dbo].[TBL_Modules] ([ModuleId], [OrganizationId], [ModuleTitle], [ModuleDescription], [ModuleContent], [AssociatedCourseId], [ModulePeriod], [ModulePeriodTypeId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (1, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Module Title', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.

Someone famous in Source Title', N'<div class="box box-solid">
<div class="box-header with-border">
<h3 class="box-title">Description Horizontal</h3>
</div>
<!-- /.box-header -->

<div class="box-body">
<dl class="dl-horizontal">
	<dt>Description lists</dt>
	<dd>A description list is perfect for defining terms.</dd>
	<dt>Euismod</dt>
	<dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
	<dd>Donec id elit non mi porta gravida at eget metus.</dd>
	<dt>Malesuada porta</dt>
	<dd>Etiam porta sem malesuada magna mollis euismod.</dd>
	<dt>Felis euismod semper eget lacinia</dt>
	<dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</dd>
</dl>
</div>
<!-- /.box-body --></div>
', 1, 22, 1, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90800E503CB AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90800FB38DD AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TBL_Modules] ([ModuleId], [OrganizationId], [ModuleTitle], [ModuleDescription], [ModuleContent], [AssociatedCourseId], [ModulePeriod], [ModulePeriodTypeId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (2, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'fasdf', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.

Someone famous in Source Title', N'<div class="box box-solid">
<div class="box-header with-border">
<h3 class="box-title">Description Horizontal</h3>
</div>
<!-- /.box-header -->

<div class="box-body">
<dl class="dl-horizontal">
	<dt>Description lists</dt>
	<dd>A description list is perfect for defining terms.</dd>
	<dt>Euismod</dt>
	<dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
	<dd>Donec id elit non mi porta gravida at eget metus.</dd>
	<dt>Malesuada porta</dt>
	<dd>Etiam porta sem malesuada magna mollis euismod.</dd>
	<dt>Felis euismod semper eget lacinia</dt>
	<dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</dd>
</dl>
</div>
<!-- /.box-body --></div>
', 1, 11, 5, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90800E73192 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90800FB4275 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TBL_Modules] ([ModuleId], [OrganizationId], [ModuleTitle], [ModuleDescription], [ModuleContent], [AssociatedCourseId], [ModulePeriod], [ModulePeriodTypeId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (3, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Module#1 Introduction to Networking', N'This module gives you a description to Networking', N'<p>Networking relates to the network of computers and networking devices.</p>

<p>Networking relates to the network of computers and networking devices.</p>

<p>Networking relates to the network of computers and networking devices.</p>

<p>Networking relates to the network of computers and networking devices.</p>
', 2, 1, 4, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00A2D100 AS DateTime), NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_Modules] ([ModuleId], [OrganizationId], [ModuleTitle], [ModuleDescription], [ModuleContent], [AssociatedCourseId], [ModulePeriod], [ModulePeriodTypeId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (4, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'Networking Architecture', N'Networking Architecture Description', N'<p><em><strong>Types of Network</strong></em></p>

<p>LAN</p>

<p>WAN</p>

<p>&nbsp;</p>
', 2, 3, 4, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00A347CA AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F00A35615 AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TBL_Modules] OFF
GO
INSERT [dbo].[TBL_Organization] ([OrganizationId], [OrganizationName], [OrganizationCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate], [AssociatedSupplierId], [OrganizationAddress], [OrganizationLogoUrl], [Telephone], [Fax], [ContactPerson], [Email], [DomainName], [ServiceBaseUrl], [TimeOffsetInMins]) VALUES (N'36d03f8a-5a7f-4f37-8e83-d82e801ebc52', N'NEA - LMS', N'NEA - LMS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Kharipati Bhaktapur', N'http://localhost:61546/images/logo.png', N'0144332211', NULL, N'Rabindra Ghimire', N'info@lea.com', N'NEA - LMS', N'http://localhost:61546/', 345)
GO
SET IDENTITY_INSERT [dbo].[TBL_QueuedEmail] ON 

GO
INSERT [dbo].[TBL_QueuedEmail] ([QueuedEmailId], [FromAddress], [FromName], [ToAddress], [ToName], [Cc], [Bcc], [SubjectCode], [Subject], [Body], [SentDate], [SentTries], [Priority], [CreatedDate], [CreatedBy]) VALUES (1, N'mail@rolpo.com', N'Rolpo', N'prashant@rolpotech.com', N'prashant@rolpotech.com', N'', N'', N'Rolpo.RegisterUser', N'[!Important Login Notice] Your registration with LMS', N'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>[SUBJECT]</title>
	<style type="text/css">
		body {
			padding-top: 0 !important;
			padding-bottom: 0 !important;
			padding-top: 0 !important;
			padding-bottom: 0 !important;
			margin: 0 !important;
			width: 100% !important;
			-webkit-text-size-adjust: 100% !important;
			-ms-text-size-adjust: 100% !important;
			-webkit-font-smoothing: antialiased !important;
		}

		.tableContent img {
			border: 0 !important;
			display: block !important;
			outline: none !important;
		}

		a {
			color: #382F2E;
		}

		p, h1, h2, ul, ol, li, div {
			margin: 0;
			padding: 0;
		}

		h1, h2 {
			font-weight: normal;
			background: transparent !important;
			border: none !important;
		}

		@media only screen and (max-width:480px) {

			table[class="MainContainer"], td[class="cell"] {
				width: 100% !important;
				height: auto !important;
			}

			td[class="specbundle"] {
				width: 100% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 15px !important;
			}

			td[class="specbundle2"] {
				width: 80% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 10px !important;
				padding-left: 10% !important;
				padding-right: 10% !important;
			}

			td[class="spechide"] {
				display: none !important;
			}

			img[class="banner"] {
				width: 100% !important;
				height: auto !important;
			}

			td[class="left_pad"] {
				padding-left: 15px !important;
				padding-right: 15px !important;
			}
		}

		@media only screen and (max-width:540px) {

			table[class="MainContainer"], td[class="cell"] {
				width: 100% !important;
				height: auto !important;
			}

			td[class="specbundle"] {
				width: 100% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 15px !important;
			}

			td[class="specbundle2"] {
				width: 80% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 10px !important;
				padding-left: 10% !important;
				padding-right: 10% !important;
			}

			td[class="spechide"] {
				display: none !important;
			}

			img[class="banner"] {
				width: 100% !important;
				height: auto !important;
			}

			td[class="left_pad"] {
				padding-left: 15px !important;
				padding-right: 15px !important;
			}
		}

		.contentEditable h2.big, .contentEditable h1.big {
			font-size: 26px !important;
		}

		.contentEditable h2.bigger, .contentEditable h1.bigger {
			font-size: 37px !important;
		}

		td, table {
			vertical-align: top;
		}

			td.middle {
				vertical-align: middle;
			}

		a.link1 {
			font-size: 13px;
			color: #27A1E5;
			line-height: 24px;
			text-decoration: none;
		}

		a {
			text-decoration: none;
		}

		.link2 {
			color: #ffffff;
			border-top: 10px solid #27A1E5;
			border-bottom: 10px solid #27A1E5;
			border-left: 18px solid #27A1E5;
			border-right: 18px solid #27A1E5;
			border-radius: 3px;
			-moz-border-radius: 3px;
			-webkit-border-radius: 3px;
			background: #27A1E5;
		}

		.link3 {
			color: #555555;
			border: 1px solid #cccccc;
			padding: 10px 18px;
			border-radius: 3px;
			-moz-border-radius: 3px;
			-webkit-border-radius: 3px;
			background: #ffffff;
		}

		.link4 {
			color: #27A1E5;
			line-height: 24px;
		}

		h2, h1 {
			line-height: 20px;
		}

		p {
			font-size: 14px;
			line-height: 21px;
			color: #555555;
		}

		.contentEditable li {
		}

		.appart p {
		}

		.bgItem {
			background: #ffffff;
		}

		.bgBody {
			background: #ffffff;
		}

		img {
			outline: none;
			text-decoration: none;
			-ms-interpolation-mode: bicubic;
			width: auto;
			max-width: 100%;
			clear: both;
			display: block;
			float: none;
		}
	</style>

	<script type="colorScheme" class="swatch active">
		{
		"name":"Default",
		"bgBody":"ffffff",
		"link":"27A1E5",
		"color":"AAAAAA",
		"bgItem":"ffffff",
		"title":"444444"
		}
	</script>

</head>
<body paddingwidth="0" paddingheight="0" bgcolor="#d1d3d4" style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td>
					<table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" style="font-family:helvetica, sans-serif;" class="MainContainer">
						<!-- =============== START HEADER =============== -->
						<tbody>
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tbody>
											<tr>
												<td valign="top" width="20">&nbsp;</td>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td class="movableContentContainer">
																	<div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tbody>
																				<tr>
																					<td height="15"></td>
																				</tr>
																				<tr>
																					<td>
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tbody>
																								<tr>
																									<td valign="top">
																										<table width="100%" border="0" cellspacing="0" cellpadding="0">
																											<tbody>
																												<tr>
																													<td valign="top" width="120"> </td>
																													<td width="10" valign="top">&nbsp;</td>
																													<td valign="middle" style="vertical-align: middle;">
																														<div class="contentEditableContainer contentTextEditable">
																															<div class="contentEditable" style="text-align: left;font-weight: light; color:#555555;font-size:26;line-height: 39px;font-family: Helvetica Neue;">
																															</div>
																														</div>
																													</td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																				<tr>
																					<td><hr style="height:1px;background:#DDDDDD;border:none;" /></td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																	<div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">

																	</div>
																	<div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tbody>
																				<tr>
																					<td height="40"></td>
																				</tr>
																				<tr>
																					<td>
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tbody>
																								<tr>
																									<td class="specbundle">
																										<div class="contentEditableContainer contentTextEditable">
																											<div style="text-align: left;">
																												<br />
																												<p>
																													Dear <b> Prashant Chalise </b>,
																													<br /><br />
																													You are now registered with <a href="http://localhost:61546/">LMS</a>.
																													You have received this email because this email address was used during registration.
																													<br />
																													If you did not register to our service, please disregard this email.
																													You do not need to unsubscribe or take any further action. <br /><br />
																													<b>Account Activation Instructions</b><br />      <br />
																													We require that you verify your email to ensure that the email address you entered was correct.
																													To verify your email, simply click      <a href="http://localhost:61546/Account/ConfirmEmail?userId=685c6e80-7b21-4a6b-985a-a441d0a01333&code=B8s6cW4lgDKLNNMceMUPPD59%2B%2B0pLikx9QSiScJDqry8DIPfk0b3JguSmwLmACb6PHNabpx0GUHiJTgtbP9EP8r9%2FbgHFsNPioQ%2FnbFjYKSUcSyzDEJ7TqUSgEQgoBVTY3cqWule%2FJ97JmhXyyLdqUnPmc%2FMv5oZ96EFj01NWy4woyps0eg1xMPc0EfbCawI" style="color:blue;">here.</a><br />
																													<br />
																													Your login/password details are as follows:
																													<br /><br />
																													<b>Username: </b> prashant@rolpotech.com<br />
																													<b>Password: </b> b9f3544-
																													<br /><br />
																													Regards,<br />
																													<b>SuperAdmin SuperAdmin</b><br />
																													--------------------------------
																												</p>
																												<br />
																												<p style="font-size:10px;">THIS IS AN AUTOMATICALLY GENERATED EMAIL.PLEASE DO NOT REPLY.</p>
																											</div>
																										</div>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																			</tbody>
																		</table>

																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
												<td valign="top" width="20">&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>

</body>
</html>
', CAST(0x0000A91000C596EB AS DateTime), 1, 0, CAST(0x0000A91000C58920 AS DateTime), N'')
GO
SET IDENTITY_INSERT [dbo].[TBL_QueuedEmail] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_Status] ON 

GO
INSERT [dbo].[TBL_Status] ([StatusId], [OrganizationId], [StatusGroup], [StatusName], [StatusCode], [StatusOrder], [AllowBackRevert], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (1, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'BATCH', N'Started', N'STARTED', 1, 1, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A9090111BD5C AS DateTime), NULL, NULL, N'info@rolpotech.com', CAST(0x0000A9090111FB89 AS DateTime))
GO
INSERT [dbo].[TBL_Status] ([StatusId], [OrganizationId], [StatusGroup], [StatusName], [StatusCode], [StatusOrder], [AllowBackRevert], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (2, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'BATCH', N'Started', N'STARTED', 2, 1, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A9090111C184 AS DateTime), NULL, NULL, N'info@rolpotech.com', CAST(0x0000A9090111FE39 AS DateTime))
GO
INSERT [dbo].[TBL_Status] ([StatusId], [OrganizationId], [StatusGroup], [StatusName], [StatusCode], [StatusOrder], [AllowBackRevert], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (3, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'BATCH', N'Started', N'STARTED', 3, 1, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A9090111C866 AS DateTime), NULL, NULL, N'info@rolpotech.com', CAST(0x0000A90901120040 AS DateTime))
GO
INSERT [dbo].[TBL_Status] ([StatusId], [OrganizationId], [StatusGroup], [StatusName], [StatusCode], [StatusOrder], [AllowBackRevert], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (4, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'BATCH', N'Started', N'STARTED', 4, 1, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A9090111ED9A AS DateTime), NULL, NULL, N'info@rolpotech.com', CAST(0x0000A90901120248 AS DateTime))
GO
INSERT [dbo].[TBL_Status] ([StatusId], [OrganizationId], [StatusGroup], [StatusName], [StatusCode], [StatusOrder], [AllowBackRevert], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (5, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'BATCH', N'Started', N'STARTED', 1, 1, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A9090112105C AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90901124CBF AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TBL_Status] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_TraineeLevel] ON 

GO
INSERT [dbo].[TBL_TraineeLevel] ([TraineeLevelId], [OrganizationId], [TraineeLevelName], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (1, N'36d03f8a-5a7f-4f37-8e83-d82e801ebc52', N'TRAINEE LEVEL 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_TraineeLevel] ([TraineeLevelId], [OrganizationId], [TraineeLevelName], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (2, N'36d03f8a-5a7f-4f37-8e83-d82e801ebc52', N'TRAINEE LEVEL 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TBL_TraineeLevel] ([TraineeLevelId], [OrganizationId], [TraineeLevelName], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (3, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'TRAINEE LEVEL 3', N'fadff', NULL, NULL, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90B009FC49C AS DateTime), N'info@rolpotech.com', CAST(0x0000A90B009FCCE8 AS DateTime))
GO
INSERT [dbo].[TBL_TraineeLevel] ([TraineeLevelId], [OrganizationId], [TraineeLevelName], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (4, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'TRAINEE LEVEL 3', N'fasdfdaf fafsd', N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90B009FD2D9 AS DateTime), N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90B009FD6B3 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TBL_TraineeLevel] ([TraineeLevelId], [OrganizationId], [TraineeLevelName], [Remarks], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate]) VALUES (5, N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'TRAINEE LEVEL 3', NULL, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', CAST(0x0000A90F009C9AF8 AS DateTime), NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TBL_TraineeLevel] OFF
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Memberships]  WITH CHECK ADD  CONSTRAINT [MembershipEntity_Application] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Applications] ([ApplicationId])
GO
ALTER TABLE [dbo].[Memberships] CHECK CONSTRAINT [MembershipEntity_Application]
GO
ALTER TABLE [dbo].[Memberships]  WITH CHECK ADD  CONSTRAINT [MembershipEntity_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Memberships] CHECK CONSTRAINT [MembershipEntity_User]
GO
ALTER TABLE [dbo].[Profiles]  WITH CHECK ADD  CONSTRAINT [ProfileEntity_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Profiles] CHECK CONSTRAINT [ProfileEntity_User]
GO
ALTER TABLE [dbo].[Roles]  WITH CHECK ADD  CONSTRAINT [RoleEntity_Application] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Applications] ([ApplicationId])
GO
ALTER TABLE [dbo].[Roles] CHECK CONSTRAINT [RoleEntity_Application]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [User_Application] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Applications] ([ApplicationId])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [User_Application]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [UsersInRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [UsersInRole_Role]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [UsersInRole_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [UsersInRole_User]
GO

 
/*
Created by: Prashant 
Created on: 6/21/2018 4:07:53 PM 
DESC: GET DATA FROM TABLE [dbo].[Batch]


EXEC dbo.[csp.Batch.Get]
@BatchId = NULL 
,@BatchCode = NULL 
,@Coordinator = NULL 
,@BatchStartDate = NULL 
,@BatchEndDate = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

ALTER PROCEDURE [dbo].[csp.Batch.Get]
(
	@BatchId  			INT = NULL 
	,@BatchCode			VARCHAR(100) = NULL 
	,@Coordinator   	NVARCHAR(200) = NULL 
	,@BatchStartDate	DATETIME = NULL 
	,@BatchEndDate  	DATETIME = NULL 
	,@SearchText		NVARCHAR(100) = NULL
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 
	BEGIN 

		DECLARE @FromDate DATE
		DECLARE @Todate DATE
		DECLARE @CurrentDate DATE = GETDATE()
		DECLARE @TimeOffSet  INT=NULL
		SET  @TimeOffSet=(SELECT TimeOffsetInMins    FROM [dbo].[TBL_Organization] WHERE OrganizationId IN (SELECT OrganizationId FROM [dbo].[AspNetUsers] WHERE Id=@UserId))					
		IF ISDATE(@BatchStartDate)=1
			SET @FromDate = CAST(@BatchStartDate AS DATE)
		ELSE
			SET @FromDate = '2000-01-01'
		
		IF ISDATE(@BatchEndDate)=1
			SET @Todate = CAST(@BatchEndDate as DATE)
		ELSE
			SET @Todate = '2100-01-01'


		;WITH TMP_TBL AS 
		( 
			SELECT  
				TB.[BatchId] AS [BatchId] 
			FROM 
				[dbo].[TBL_Batch] TB WITH(NOLOCK) 
			LEFT JOIN 
				[dbo].[TBL_BatchCourse] TBC WITH(NOLOCK) ON (TBC.BatchId  = TB.BatchId)
			LEFT JOIN 
				[dbo].[TBL_Course] C WITH(NOLOCK) ON (C.CourseId = TBC.CourseId)
			LEFT JOIN 
				[dbo].[TBL_CourseLevel] CL WITH(NOLOCK) ON (CL.CourseLevelId = C.CourseLevelId )
			LEFT JOIN
				[dbo].[TBL_Member] TMC WITH(NOLOCK) ON (TMC.MemberId=TB.CoordinatorId AND TMC.Designation='COORDINATOR')
			LEFT JOIN
				[dbo].[TBL_Member] TAC WITH(NOLOCK) ON (TAC.MemberId=TB.AssistantCoordinatorId AND TAC.Designation='ASSISTANT COORDINATOR')

			WHERE 
				(ISNULL(@BatchId,0) = 0 OR TB.[BatchId] = @BatchId )
				AND (ISNULL(@BatchCode,'') = '' OR TB.[BatchCode] = @BatchCode )
				AND (ISNULL(@Coordinator,'') = '' OR TMC.FirstName LIKE '%'+ @Coordinator +'%' )
				AND (CAST(ISNULL(TB.[BatchStartDate],@CurrentDate) AS DATE) BETWEEN @FromDate AND @Todate)
				AND (CAST(ISNULL(TB.[BatchEndDate],@CurrentDate) AS DATE) BETWEEN @FromDate AND @Todate) 
				AND (
						(ISNULL(@SearchText,'' ) = '')
						OR (TB.BatchCode LIKE '%'+@SearchText+'%')
						OR (TB.BatchName LIKE '%'+@SearchText+'%')
						OR (C.CourseCode LIKE '%'+@SearchText+'%')
						OR (C.CourseFullName LIKE '%'+@SearchText+'%')
						OR (CL.LevelName LIKE '%'+@SearchText+'%')
					)
				AND TB.DeletedDate IS NULL 
				AND C.DeletedDate IS NULL
				AND CL.DeletedDate IS NULL
				AND TBC.DeletedDate IS NULL
		) 

		SELECT 
			(ROW_NUMBER() OVER (ORDER BY BatchId)) AS RowNumber 
			,BatchId
		INTO #TMP_Batch
		FROM TMP_TBL
		GROUP BY BatchId



		SELECT 
				T.RowNumber
				,T.BatchId
				,TB.[BatchCode] AS [BatchCode]
				,ISNULL(TB.[BatchName],'') AS [BatchName]
				,ISNULL(TB.[BatchLevel],'') AS [BatchLevel]
				,ISNULL(TB.[BatchCategory],'') AS [BatchCategory]
				,ISNULL(TMC.FirstName +' '+TMC.LastName,'') AS [Coordinator]
				,ISNULL(TAC.FirstName +' '+TAC.LastName,'') AS [ASSCoordinator]
				,ISNULL(TB.CoordinatorId,0) AS CoordinatorId
				,ISNULL(TB.AssistantCoordinatorId,0) AS AssistantCoordinatorId
				,TB.[BatchStartDate] AS [BatchStartDate]
				,TB.[BatchEndDate]  AS [BatchEndDate]
				,TB.[BatchSummary]  AS [BatchSummary] 
				,ISNULL(TB.TrainingAddress,'') AS TrainingAddress
				,COUNT(T.[BatchId]) OVER () AS TotalCount  
		FROM #TMP_Batch T
		INNER JOIN 
			 [dbo].[TBL_Batch] TB WITH(NOLOCK) ON (TB.BatchId = T.BatchId)
		LEFT JOIN
			[dbo].[TBL_Member] TMC WITH(NOLOCK) ON (TMC.MemberId=TB.CoordinatorId AND TMC.Designation='COORDINATOR')
		LEFT JOIN
			[dbo].[TBL_Member] TAC WITH(NOLOCK) ON (TAC.MemberId=TB.AssistantCoordinatorId AND TAC.Designation='ASSISTANT COORDINATOR')
		WHERE 
			(@ShowAll = 1 OR (T.RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR T.RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 



/*
Created by: Prashant 
Created on: 6/21/2018 4:07:53 PM 
DESC: GET DATA FROM TABLE [dbo].[Batch]


EXEC dbo.[csp.Batch.GetForReport]
@BatchId = NULL 
,@BatchCode = NULL 
,@Coordinator = NULL 
,@BatchStartDate = NULL 
,@BatchEndDate = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/

ALTER PROCEDURE [dbo].[csp.Batch.GetForReport]
(
	@BatchId  			INT = NULL 
	,@BatchCode			VARCHAR(100) = NULL 
	,@CoordinatorId   	INT = NULL 
	,@TraineeId   		INT = NULL 
	,@BatchStartDate	DATETIME = NULL 
	,@BatchEndDate  	DATETIME = NULL 
	,@SearchText		NVARCHAR(100) = NULL
	,@UserId			NVARCHAR(256) = NULL 
	,@PageNumber		INT = 1 
	,@PageSize 			INT = 20 
	,@ShowAll			INT = 0 

)
AS 
	BEGIN 

		DECLARE @FromDate DATE
		DECLARE @Todate DATE
		DECLARE @CurrentDate DATE = GETDATE()
		DECLARE @TimeOffSet  INT=NULL
		SET  @TimeOffSet=(SELECT TimeOffsetInMins    FROM [dbo].[TBL_Organization] WHERE OrganizationId IN (SELECT OrganizationId FROM [dbo].[AspNetUsers] WHERE Id=@UserId))					
		IF ISDATE(@BatchStartDate)=1
			SET @FromDate = CAST(@BatchStartDate AS DATE)
		ELSE
			SET @FromDate = '2000-01-01'
		
		IF ISDATE(@BatchEndDate)=1
			SET @Todate = CAST(@BatchEndDate as DATE)
		ELSE
			SET @Todate = '2100-01-01'


		;WITH TMP_TBL AS 
		( 
			SELECT  
				TB.[BatchId] AS [BatchId] 
				,ISNULL(TBC.CourseId,0) AS CourseId
			FROM 
				[dbo].[TBL_Batch] TB WITH(NOLOCK) 
			LEFT JOIN 
				[dbo].[TBL_BatchCourse] TBC WITH(NOLOCK) ON (TBC.BatchId  = TB.BatchId)
			LEFT JOIN 
				[dbo].[TBL_BatchTrainee] TBT  WITH(NOLOCK) ON (TBT.BatchId = TB.BatchId)
			LEFT JOIN 
				[dbo].[TBL_Course] C WITH(NOLOCK) ON (C.CourseId = TBC.CourseId)
			LEFT JOIN 
				[dbo].[TBL_CourseLevel] CL WITH(NOLOCK) ON (CL.CourseLevelId = C.CourseLevelId )
			LEFT JOIN
				[dbo].[TBL_Member] TMC WITH(NOLOCK) ON (TMC.MemberId=TB.CoordinatorId AND TMC.Designation='COORDINATOR')
			LEFT JOIN
				[dbo].[TBL_Member] TAC WITH(NOLOCK) ON (TAC.MemberId=TB.AssistantCoordinatorId AND TAC.Designation='ASSISTANT COORDINATOR')

			WHERE 
				(ISNULL(@BatchId,0) = 0 OR TB.[BatchId] = @BatchId )
				AND (ISNULL(@BatchCode,'') = '' OR TB.[BatchCode] = @BatchCode )
				AND (ISNULL(@CoordinatorId,0) = 0 OR TMC.MemberId = @CoordinatorId )
				AND (ISNULL(@TraineeId,0) = 0 OR TBT.MemberId = @TraineeId)
				AND (
						(CAST(ISNULL(TB.[BatchStartDate],@CurrentDate) AS DATE) BETWEEN @FromDate AND @Todate)
						OR (CAST(ISNULL(TB.[BatchEndDate],@CurrentDate) AS DATE) BETWEEN @FromDate AND @Todate) 
					)
				AND (
						(ISNULL(@SearchText,'' ) = '')
						OR (TB.BatchCode LIKE '%'+@SearchText+'%')
						OR (TB.BatchName LIKE '%'+@SearchText+'%')
						OR (C.CourseCode LIKE '%'+@SearchText+'%')
						OR (C.CourseFullName LIKE '%'+@SearchText+'%')
						OR (CL.LevelName LIKE '%'+@SearchText+'%')
					)
				AND TB.DeletedDate IS NULL 
				AND C.DeletedDate IS NULL
				AND CL.DeletedDate IS NULL
				AND TBC.DeletedDate IS NULL
		) 

		SELECT 
			(ROW_NUMBER() OVER (ORDER BY BatchId)) AS RowNumber 
			,BatchId
			,CourseId

		INTO #TMP_Batch
		FROM TMP_TBL
		GROUP BY BatchId,CourseId

		SELECT 
			T.BatchId
			,COUNT(TBT.memberId) AS TotalTrainees
		INTO #TMP_BatchByTraineeCount
		FROM 
			#TMP_Batch T
		INNER JOIN 
			[dbo].[TBL_BatchTrainee] TBT WITH(NOLOCK) ON (TBT.BatchId = T.BatchId)
		WHERE TBT.DeletedDate IS NULL
		GROUP BY T.BatchId

		SELECT 
				T.RowNumber
				,T.BatchId
				,T.CourseId
				,TB.[BatchCode] AS [BatchCode]
				,ISNULL(TB.[BatchName],'') AS [BatchName]
				,ISNULL(C.CourseCode,'') AS CourseCode
				,ISNULL(C.CourseFullName,'') AS CourseFullName
				,ISNULL(CL.LevelName,'') AS LevelName
				,ISNULL(TB.[BatchLevel],'') AS [BatchLevel]
				,ISNULL(TB.[BatchCategory],'') AS [BatchCategory]
				,ISNULL(TMC.FirstName +' '+TMC.LastName,'') AS [Coordinator]
				,ISNULL(TAC.FirstName +' '+TAC.LastName,'') AS [ASSCoordinator]
				,ISNULL(TB.CoordinatorId,0) AS CoordinatorId
				,ISNULL(TB.AssistantCoordinatorId,0) AS AssistantCoordinatorId
				,TB.[BatchStartDate] AS [BatchStartDate]
				,TB.[BatchEndDate]  AS [BatchEndDate]
				,CONVERT(VARCHAR,DATEADD(MINUTE,345,TB.BatchStartDate),107) AS BatchStartDateLOCAL
				,CONVERT(VARCHAR,DATEADD(MINUTE,345,TB.BatchEndDate),107) AS BatchEndDateLOCAL
				,TB.[BatchSummary]  AS [BatchSummary] 
				,ISNULL(C.[CoursePeriod],0) AS [CoursePeriod]
				,ISNULL(C.[CoursePeriodTypeId],0) AS [CoursePeriodTypeId]
				,ISNULL(CPT.[PeriodTypeName],'') AS [PeriodTypeName]
				,CAST(ISNULL(C.[CoursePeriod],0) AS VARCHAR(10)) + ' ' + ISNULL(CPT.[PeriodTypeName],'') AS Duration
				,ISNULL(TB.TrainingAddress,'') AS TrainingAddress
				,ISNULL(BBTC.TotalTrainees,0) AS TotalTrainees
				,COUNT(T.[BatchId]) OVER () AS TotalCount  
		FROM #TMP_Batch T
		INNER JOIN 
			 [dbo].[TBL_Batch] TB WITH(NOLOCK) ON (TB.BatchId = T.BatchId)
		LEFT JOIN 
				[dbo].[TBL_BatchCourse] TBC WITH(NOLOCK) ON (TBC.BatchId  = TB.BatchId AND T.CourseId = TBC.CourseId)
		LEFT JOIN 
				[dbo].[TBL_Course] C WITH(NOLOCK) ON (C.CourseId = TBC.CourseId)
		LEFT JOIN 
				[dbo].[TBL_CourseLevel] CL WITH(NOLOCK) ON (CL.CourseLevelId = C.CourseLevelId )
		LEFT JOIN
			[dbo].[TBL_Member] TMC WITH(NOLOCK) ON (TMC.MemberId=TB.CoordinatorId AND TMC.Designation='COORDINATOR')
		LEFT JOIN
			[dbo].[TBL_Member] TAC WITH(NOLOCK) ON (TAC.MemberId=TB.AssistantCoordinatorId AND TAC.Designation='ASSISTANT COORDINATOR')
		LEFT JOIN 
				[dbo].[TBL_CoursePeriodType] CPT WITH(NOLOCK) ON (CPT.[CoursePeriodTypeId] = C.[CoursePeriodTypeId] AND CPT.DeletedDate IS NULL)
		LEFT JOIN 
			#TMP_BatchByTraineeCount BBTC ON (BBTC.BatchId = T.BatchId)

		WHERE 
			(@ShowAll = 1 OR (T.RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR T.RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
		AND TB.DeletedDate IS NULL 
				AND C.DeletedDate IS NULL
				AND CL.DeletedDate IS NULL
				AND TBC.DeletedDate IS NULL
				AND CPT.DeletedDate IS NULL
	END 



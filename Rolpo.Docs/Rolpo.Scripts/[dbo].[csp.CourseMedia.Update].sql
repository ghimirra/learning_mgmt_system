

/****** Object:  StoredProcedure [dbo].[csp.CourseMedia.Update]    Script Date: 6/26/2018 4:42:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*
Created by: Prashant 
Created on: 26/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[CourseMedia]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.CourseMedia.Update]
	@ActionType = 'ADD' 
	,@CourseMediaId = NULL 
	,@OrganizationId = NULL 
	,@CourseId = NULL 
	,@TopicId = NULL 
	,@MediaURL = NULL 
	,@MediaType = NULL 
	,@Remarks = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_CourseMedia] 


*/

CREATE PROCEDURE [dbo].[csp.CourseMedia.Update]
(
	@ActionType			VARCHAR(10) 
	,@CourseMediaId 		INT = NULL 
	,@OrganizationId		INT = 0 
	,@CourseId 				INT = NULL 
	,@TopicId  				INT = NULL 
	,@MediaURL 				NVARCHAR(500) = NULL 
	,@MediaType			INT = NULL 
	,@Remarks  				NVARCHAR(500) = NULL 
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnCourseMediaId 		INT =	0 OUTPUT 
)
AS 

SET @ReturnCourseMediaId = @CourseMediaId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_CourseMedia] WHERE CourseMediaId = @CourseMediaId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 
DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_CourseMedia] 
				( 
					[OrganizationId]  
					,[CourseId]  
					,[TopicId]  
					,[MediaURL]  
					,[MediaType]  
					,[Remarks]  
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@CourseId  
					,@TopicId  
					,@MediaURL  
					,@MediaType  
					,@Remarks  
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnCourseMediaId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_CourseMedia] 
				SET 
					[OrganizationId]		 = @OrganizationId  
					,[CourseId] 				 = @CourseId  
					,[TopicId]  				 = @TopicId  
					,[MediaURL] 				 = @MediaURL  
					,[MediaType]			 = @MediaType  
					,[Remarks]  				 = @Remarks  
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE CourseMediaId = @CourseMediaId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_CourseMedia] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE CourseMediaId = @CourseMediaId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 

GO



USE [NEA_Trainning]
GO
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserLogins] DROP CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserClaims] DROP CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
/****** Object:  Table [dbo].[TBL_SystemRole]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[TBL_SystemRole]
GO
/****** Object:  Table [dbo].[TBL_Status]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[TBL_Status]
GO
/****** Object:  Table [dbo].[TBL_QueuedEmail]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[TBL_QueuedEmail]
GO
/****** Object:  Table [dbo].[TBL_Organization]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[TBL_Organization]
GO
/****** Object:  Table [dbo].[TBL_Modules]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[TBL_Modules]
GO
/****** Object:  Table [dbo].[TBL_Member]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[TBL_Member]
GO
/****** Object:  Table [dbo].[TBL_Media]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[TBL_Media]
GO
/****** Object:  Table [dbo].[TBL_EmailFormat]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[TBL_EmailFormat]
GO
/****** Object:  Table [dbo].[TBL_CourseMedia]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[TBL_CourseMedia]
GO
/****** Object:  Table [dbo].[TBL_Course]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[TBL_Course]
GO
/****** Object:  Table [dbo].[TBL_BatchTrainer]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[TBL_BatchTrainer]
GO
/****** Object:  Table [dbo].[TBL_BatchTrainee]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[TBL_BatchTrainee]
GO
/****** Object:  Table [dbo].[TBL_BatchSummary]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[TBL_BatchSummary]
GO
/****** Object:  Table [dbo].[TBL_Batch]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[TBL_Batch]
GO
/****** Object:  Table [dbo].[NLogEntries]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[NLogEntries]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[AspNetUsers]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[AspNetUserRoles]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[AspNetUserLogins]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[AspNetUserClaims]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[AspNetRoles]
GO
/****** Object:  Table [dbo].[Applications]    Script Date: 6/17/2018 12:57:24 PM ******/
DROP TABLE [dbo].[Applications]
GO
/****** Object:  Table [dbo].[Applications]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Applications](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[ApplicationName] [nvarchar](235) NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[RoleGroup] [nvarchar](50) NULL,
	[OrganizationId] [uniqueidentifier] NULL,
	[Discriminator] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[IsEmailVerified] [bit] NULL,
	[Discriminator] [nvarchar](128) NULL,
	[FirstName] [nvarchar](150) NULL,
	[LastName] [nvarchar](150) NULL,
	[OrganizationId] [uniqueidentifier] NULL,
	[StaffId] [int] NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NLogEntries]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NLogEntries](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[Level] [nvarchar](10) NULL,
	[Logger] [nvarchar](128) NULL,
	[CreatedOnUtc] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_Batch]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Batch](
	[BatchId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [nchar](10) NOT NULL,
	[BatchCode] [varchar](100) NOT NULL,
	[BatchName] [varchar](500) NULL,
	[BatchLevel] [nvarchar](100) NULL,
	[BatchCategory] [nvarchar](100) NULL,
	[Coordinator] [nvarchar](200) NULL,
	[ASSCoordinator] [nvarchar](200) NULL,
	[BatchStartDate] [datetime] NOT NULL,
	[BatchEndDate] [datetime] NULL,
	[BatchSummary] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_Batch] PRIMARY KEY CLUSTERED 
(
	[BatchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_BatchSummary]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_BatchSummary](
	[BatchSummaryId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[BatchId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[Summary] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_BatchSummary] PRIMARY KEY CLUSTERED 
(
	[BatchSummaryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_BatchTrainee]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_BatchTrainee](
	[BatchTraineeId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[BatchId] [int] NOT NULL,
	[MemberId] [int] NOT NULL,
	[JoinedDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_BatchTrainee] PRIMARY KEY CLUSTERED 
(
	[BatchTraineeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_BatchTrainer]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_BatchTrainer](
	[BatchTrainerId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[MemberId] [int] NOT NULL,
	[BatchId] [int] NOT NULL,
	[TopicId] [int] NULL,
	[TrainingStartDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_BatchTrainer] PRIMARY KEY CLUSTERED 
(
	[BatchTrainerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_Course]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Course](
	[CourseId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[CourseFullName] [nvarchar](500) NOT NULL,
	[CourseShortName] [nvarchar](200) NOT NULL,
	[CourseDescription] [nvarchar](1000) NULL,
	[CourseCategory] [nvarchar](100) NULL,
	[CourseLevel] [nvarchar](100) NULL,
	[CourseContent] [ntext] NULL,
	[PeriodInDays] [int] NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_Course] PRIMARY KEY CLUSTERED 
(
	[CourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_CourseMedia]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_CourseMedia](
	[CourseMediaId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[CourseId] [int] NOT NULL,
	[TopicId] [int] NULL,
	[MediaURL] [nvarchar](500) NOT NULL,
	[MediaType] [int] NOT NULL,
	[Remarks] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_CourseMedia] PRIMARY KEY CLUSTERED 
(
	[CourseMediaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_EmailFormat]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_EmailFormat](
	[EmailFormatId] [int] IDENTITY(1,1) NOT NULL,
	[From] [nvarchar](500) NULL,
	[FromName] [nvarchar](500) NULL,
	[Bcc] [nvarchar](500) NULL,
	[Subject] [nvarchar](500) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_dbo.TBL_EmailFormat] PRIMARY KEY CLUSTERED 
(
	[EmailFormatId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_Media]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Media](
	[MediaId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[MediaName] [nvarchar](500) NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_Member]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Member](
	[MemberId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[LastName] [nvarchar](150) NOT NULL,
	[SystemRoleId] [int] NOT NULL,
	[Designation] [nvarchar](250) NULL,
	[Level] [nvarchar](250) NULL,
	[Department] [nvarchar](250) NULL,
	[Address] [nvarchar](250) NULL,
	[PrimaryContactEmail] [varchar](150) NULL,
	[Phone] [varchar](25) NULL,
	[Mobile] [varchar](25) NULL,
	[ProfileImageUrl] [nvarchar](500) NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
	[Education] [nvarchar](200) NULL,
	[Notes] [nvarchar](250) NULL,
 CONSTRAINT [PK_TBL_Members] PRIMARY KEY CLUSTERED 
(
	[MemberId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Modules]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_Modules](
	[ModuleId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[ModuleTitle] [nvarchar](500) NOT NULL,
	[ModuleDescription] [nvarchar](1000) NULL,
	[ModuleContent] [ntext] NULL,
	[AssociatedCourseId] [int] NOT NULL,
	[ModuleDurationInHours] [money] NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_Topic] PRIMARY KEY CLUSTERED 
(
	[ModuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_Organization]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Organization](
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[OrganizationName] [nvarchar](150) NOT NULL,
	[OrganizationCode] [varchar](25) NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
	[AssociatedSupplierId] [int] NULL,
	[OrganizationAddress] [nvarchar](200) NULL,
	[OrganizationLogoUrl] [nvarchar](200) NULL,
	[Telephone] [nvarchar](200) NULL,
	[Fax] [nvarchar](200) NULL,
	[ContactPerson] [nvarchar](200) NULL,
	[Email] [nvarchar](200) NULL,
	[DomainName] [nvarchar](200) NULL,
	[ServiceBaseUrl] [nvarchar](200) NULL,
	[TimeOffsetInMins] [int] NULL,
 CONSTRAINT [PK_TBL_Organization] PRIMARY KEY CLUSTERED 
(
	[OrganizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_QueuedEmail]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_QueuedEmail](
	[QueuedEmailId] [int] IDENTITY(1,1) NOT NULL,
	[FromAddress] [nvarchar](200) NOT NULL,
	[FromName] [nvarchar](200) NOT NULL,
	[ToAddress] [nvarchar](200) NOT NULL,
	[ToName] [nvarchar](200) NOT NULL,
	[Cc] [nvarchar](1000) NULL,
	[Bcc] [nvarchar](1000) NULL,
	[SubjectCode] [nvarchar](100) NOT NULL,
	[Subject] [nvarchar](200) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[SentDate] [datetime] NULL,
	[SentTries] [int] NULL,
	[Priority] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_TBL_QueuedEmail] PRIMARY KEY CLUSTERED 
(
	[QueuedEmailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_Status]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Status](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[StatusGroup] [varchar](100) NOT NULL,
	[StatusName] [varchar](100) NOT NULL,
	[StatusCode] [nvarchar](20) NOT NULL,
	[StatusOrder] [int] NOT NULL,
	[AllowBackRevert] [bit] NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_Status] PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_SystemRole]    Script Date: 6/17/2018 12:57:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_SystemRole](
	[SystemRoleId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[RoleName] [varchar](100) NOT NULL,
	[RoleGroup] [varchar](100) NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](256) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_Designation] PRIMARY KEY CLUSTERED 
(
	[SystemRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [RoleGroup], [OrganizationId], [Discriminator]) VALUES (N'438de475-127b-4adc-8812-9c21410e3602', N'MANAGER', N'ALL', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'ApplicationRole')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [RoleGroup], [OrganizationId], [Discriminator]) VALUES (N'6FF94A8A-4FDE-4AE2-A470-9D6521B0E638', N'SUPERADMIN', N'ALL', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'ApplicationRole')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [RoleGroup], [OrganizationId], [Discriminator]) VALUES (N'fb474377-84d3-4a9a-83f5-7809a775cd79', N'EDITOR', N'ALL', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', N'ApplicationRole')
GO
SET IDENTITY_INSERT [dbo].[AspNetUserClaims] ON 

GO
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (31539, N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'OrgId', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a')
GO
SET IDENTITY_INSERT [dbo].[AspNetUserClaims] OFF
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'2fd61827-f6db-416a-b9e7-13aa6719158d', N'fb474377-84d3-4a9a-83f5-7809a775cd79')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'85b25794-5974-4b2b-bd6b-04b9fd44059d', N'6FF94A8A-4FDE-4AE2-A470-9D6521B0E638')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'aee207a2-d66a-4054-a733-12343b235cd1', N'fb474377-84d3-4a9a-83f5-7809a775cd79')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c5064924-41f9-4edf-ae9a-697fe9e2fe55', N'438de475-127b-4adc-8812-9c21410e3602')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c5064924-41f9-4edf-ae9a-697fe9e2fe55', N'fb474377-84d3-4a9a-83f5-7809a775cd79')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'438de475-127b-4adc-8812-9c21410e3602')
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [IsEmailVerified], [Discriminator], [FirstName], [LastName], [OrganizationId], [StaffId]) VALUES (N'2fd61827-f6db-416a-b9e7-13aa6719158d', N'prashant@rolpotech.com', 0, N'ADOQxLwgqEg7JJl5JB8b0jxe4QWD1PZc0DD9Gcpc+nLLa8SWIzMR19jYfztzzzHv+A==', N'ed67269f-77ce-4235-add4-911a468d9669', NULL, 0, 0, NULL, 0, 0, N'prashant@rolpotech.com', NULL, NULL, N'prashant', N'Chalise', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 0)
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [IsEmailVerified], [Discriminator], [FirstName], [LastName], [OrganizationId], [StaffId]) VALUES (N'85b25794-5974-4b2b-bd6b-04b9fd44059d', N'superadmin@rolpo.com', 1, N'ABloBtiow54W3tjmLu5q6Eks9eRQeHFuAscVjqv9lZ8zhpSh7aa92CqsCe+XgiG9dg==', N'8419b0d1-fa8c-4dfa-bdb4-f95942c26d18', NULL, 0, 0, NULL, 0, 0, N'superadmin@rolpo.com', 1, N'ApplicationUser ', N'SuperAdmin', N'SuperAdmin', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 0)
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [IsEmailVerified], [Discriminator], [FirstName], [LastName], [OrganizationId], [StaffId]) VALUES (N'aee207a2-d66a-4054-a733-12343b235cd1', N'prashantchalise@hotmail.com', 1, N'AHVJ6g9QxakiObIHApjvr0AXss/T1xecPfXvfJmqG0Ikk0n1SFp2uhSKJZbzorHV+Q==', N'a903853d-b678-4fa8-bc43-f68e2353bf4e', NULL, 0, 0, NULL, 0, 0, N'prashantchalise@hotmail.com', NULL, NULL, N'Admin', N'Admin Role', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 0)
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [IsEmailVerified], [Discriminator], [FirstName], [LastName], [OrganizationId], [StaffId]) VALUES (N'c5064924-41f9-4edf-ae9a-697fe9e2fe55', N'SPatil@careconnectors.com', 1, N'AIlawV2jr7zi4iwi3oVTFcgcuLgw4O99JmE+GKHTmWh/7EZOJgC0lHQcR6psn4XEug==', N'cdb1f16f-301a-495d-a849-adb811db547d', NULL, 0, 0, NULL, 0, 0, N'SPatil@careconnectors.com', NULL, NULL, N'Sanjay', N'Patil', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 0)
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [IsEmailVerified], [Discriminator], [FirstName], [LastName], [OrganizationId], [StaffId]) VALUES (N'ca02b6fd-fb31-4482-a60c-64c700349a6f', N'info@rolpotech.com', 1, N'AAGE9CRNPkhYx5wat74c7IKmCgAjjQ79dBtBrkhCbGeCQV75C2qkL0ghzM44YeWvxw==', N'c232fc53-9672-433d-a744-b25882637c22', NULL, 0, 0, NULL, 0, 0, N'info@rolpotech.com', NULL, NULL, N'Prashant', N'Chalise', N'3ec0cbce-7d8b-40e8-b6b7-7ab0fc48666a', 27)
GO
INSERT [dbo].[TBL_Organization] ([OrganizationId], [OrganizationName], [OrganizationCode], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [DeletedBy], [DeletedDate], [AssociatedSupplierId], [OrganizationAddress], [OrganizationLogoUrl], [Telephone], [Fax], [ContactPerson], [Email], [DomainName], [ServiceBaseUrl], [TimeOffsetInMins]) VALUES (N'36d03f8a-5a7f-4f37-8e83-d82e801ebc52', N'NEA - LMS', N'NEA - LMS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Kharipati Bhaktapur', N'http://localhost:61546/images/logo.png', N'0144332211', NULL, N'Rabindra Ghimire', N'info@lea.com', N'NEA - LMS', N'http://localhost:61546', 345)
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO

 
/*
//// CURRENTLY IMPLEMENTED DDL JSONS /////
 
------------------------------------------------------------------------
NAME: TABLE_NAMES
PARAM1: '' = NOTHING
PARAM2: ''  = NOTHING
------------------------------------------------------------------------
NAME: COURSECATEGORY
PARAM1: 'ALL' = ALL: Show all categories including sub categories
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: SUBCATEGORY
PARAM1: 'Id' = Id: Show Sub categories for given Id
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: COURSELEVEL 
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: PERIODTYPE 
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: TRAINEELEVELS 
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: MEMBERLEVELS 
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------ 

NAME: TRAINEE
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: TRAINER
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: MODULES
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: COURSES
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: BATCHCOURSE
PARAM1: =	BATCHID NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: BATCH_STATUS
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------
NAME: BATCHES
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------

NAME: ASSTCOORDINATOR
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------

NAME: COORDINATOR
PARAM1: =	'' NOT SET
PARAM2: HIDE_DEFAULT =  HIDE THE DEFAULT LOADING OF EMPTY ITEM

------------------------------------------------------------------------


*/
 

/*
Created by: Prashant 
Created on: 5/7/2016
DESC: GET DDL VAlues in Terms of JSON

 

EXEC [dbo].[csp.DDLItems.Get]

@OrganizationId = ''
,@DDLListXML = '<root>
  <Data>
    <DDLName>MEMBERLEVELS</DDLName>
    <Param1>NULL</Param1>
    <Param2>HIDE_DEFAULT</Param2>
  </Data>
</root>'


*/

ALTER PROCEDURE [dbo].[csp.DDLItems.Get]
(
	@PageName VARCHAR(100) = ''
	,@DDLListXML	NVARCHAR(MAX) = NULL
	,@OrganizationId	NVARCHAR(50) = NULL
	,@UserId NVARCHAR(256)  = NULL
)
AS 
	BEGIN 
		
		-- Get OranizationId
		DECLARE @OrgId UNIQUEIDENTIFIER = NULL;
		SELECT @OrgId = TRY_CONVERT(UNIQUEIDENTIFIER, @OrganizationId)

		DECLARE @DDLXML AS XML = CAST(@DDLListXML AS XML)
		DECLARE @TableJSON VARCHAR(MAX) = ''
		DECLARE @TMPJSON AS VARCHAR(MAX) = ''

		DECLARE @Param1 VARCHAR(150)
		DECLARE @Param2 VARCHAR(150)
		DECLARE @Id INT 

   	CREATE TABLE #TBLDDL(Name VARCHAR(100) , VALUE VARCHAR(100),ROW_PRIORITY SMALLINT NULL)

		SELECT
			ISNULL(_root._data.value('(DDLName)[1]', 'varchar(50)'),0) AS 'DDLName',
			_root._data.value('(Param1)[1]', 'varchar(50)') AS 'Param1',
			_root._data.value('(Param2)[1]', 'varchar(50)') AS 'Param2'
			INTO #TMP_DDLListWithParams
			FROM
				@DDLXML.nodes('/root/Data') as _root(_data)

		-- FUNCTION FOR DDL ------
		-- LOAD TABLES NAME ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'TABLE_NAMES')
			BEGIN
				-- Clean TEMP VARIABLES  --
				DELETE FROM #TBLDDL
				SET @TMPJSON = NULL;
				SET @Param1 = '';
				SET @Param2 = '';

				-- Fill Table with New Value --
				INSERT INTO  #TBLDDL(Name, Value,ROW_PRIORITY)
				SELECT 
					NAME AS Name
					,NAME  AS Value
					,(ROW_NUMBER() OVER (ORDER BY NAME)) AS ROW_PRIORITY 
				 FROM SYS.TABLES


				-- Update to JSON --
				SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
				FROM #TBLDDL ORDER BY ROW_PRIORITY


				--SET @TMPJSON = '{"Name":"--Select--","Value":"0"}' + @TMPJSON
				
				SET @TMPJSON = '{"Name":"--Select--","Value":"0"}' + CASE WHEN  ISNULL(@TMPJSON,'')='' THEN '' ELSE ',' END + @TMPJSON

				SET @TableJSON += '{"DDLName": "TABLE_NAMES","Items": ['+ @TMPJSON +']}';
			END
		 
		-- FUNCTION FOR DDL ------
		-- LOAD Course Categories ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'COURSECATEGORY')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
				
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'COURSECATEGORY';

				
				
			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [CategoryName] AS Name
						,[CourseCategoryId] AS Value 
					FROM [dbo].[TBL_CourseCategory] WITh(NOLOCK)
					WHERE DeletedDate IS NULL
					AND (ISNULL([ParentCategoryId],0)  = 0 OR ISNULL(@Param1,'') = 'ALL')

				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "COURSECATEGORY","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END
		
		-- FUNCTION FOR DDL ------
		-- LOAD Sub Categories ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'SUBCATEGORY')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'SUBCATEGORY';

				 
			SET @Id = CASE WHEN ISNUMERIC(@Param1) = 1 THEN CAST(@Param1 AS INT) ELSE NULL END;

			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [CategoryName] AS Name
						,[CourseCategoryId] AS Value 
					FROM [dbo].[TBL_CourseCategory] WITh(NOLOCK)
					WHERE DeletedDate IS NULL
					AND ISNULL([ParentCategoryId],0)  > 0
					AND (ISNULL(@Param1,0) = 0 OR ParentCategoryId = @Param1)
					
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "SUBCATEGORY","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  


		-- LOAD Sub Categories ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'SUBCATEGORYBYID')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'SUBCATEGORYBYID';

				 
			SET @Id = CASE WHEN ISNUMERIC(@Param1) = 1 THEN CAST(@Param1 AS INT) ELSE NULL END;

			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [CategoryName] AS Name
						,[CourseCategoryId] AS Value 
					FROM [dbo].[TBL_CourseCategory] WITh(NOLOCK)
					WHERE DeletedDate IS NULL
					AND ISNULL([ParentCategoryId],0)  > 0
					AND (ParentCategoryId = @Param1)
					
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "SUBCATEGORYBYID","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD Course Levels ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'COURSELEVEL')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'COURSELEVEL';
			 

			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [LevelName] AS Name
						,[CourseLevelId] AS Value 
					FROM [dbo].[TBL_CourseLevel] WITh(NOLOCK)
					WHERE DeletedDate IS NULL 
					
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "COURSELEVEL","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD Period types ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'PERIODTYPE')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'PERIODTYPE';

				  
			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [PeriodTypeName] AS Name
						,[CoursePeriodTypeId] AS Value 
					FROM [dbo].[TBL_CoursePeriodType] WITh(NOLOCK)
					WHERE DeletedDate IS NULL 
					
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "PERIODTYPE","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		
		-- FUNCTION FOR DDL ------
		-- LOAD Trainee Levels ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'TRAINEELEVELS')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'TRAINEELEVELS';
			 

			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [TraineeLevelName] AS Name
						,[TraineeLevelId] AS Value 
					FROM [dbo].[TBL_TraineeLevel] WITh(NOLOCK)
					WHERE DeletedDate IS NULL 
					
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "TRAINEELEVELS","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  
		-- FUNCTION FOR DDL ------

		-- LOAD MEMBER Levels ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'MEMBERLEVELS')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'MEMBERLEVELS';
			 

			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [TraineeLevelName] AS Name
						,CAST(TraineeLevelId AS VARCHAR(4)) AS Value 
					FROM [dbo].[TBL_TraineeLevel] WITh(NOLOCK)
					WHERE DeletedDate IS NULL 
					
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "MEMBERLEVELS","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  
		-- FUNCTION FOR DDL ------

		-- LOAD TRAINEE  ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'TRAINEE')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'TRAINEE';
			 

			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [FirstName] + ' ' + [LastName] AS Name
						,[MemberId] AS Value 
					FROM [dbo].[TBL_Member] WITh(NOLOCK)
					WHERE DeletedDate IS NULL 
					AND Designation = 'TRAINEE'
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "TRAINEE","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD TRAINER  ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'TRAINER')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'TRAINER';
			 

			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [FirstName] + ' ' + [LastName] AS Name
						,[MemberId] AS Value 
					FROM [dbo].[TBL_Member] WITh(NOLOCK)
					WHERE DeletedDate IS NULL 
					AND Designation = 'TRAINER'
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "TRAINER","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD MODULES ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'MODULES')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'MODULES';
			 

			INSERT INTO  #TBLDDL(Name, Value)
				
				SELECT [ModuleTitle] AS Name
						,[ModuleId] AS Value 
					FROM [dbo].[TBL_Modules] WITh(NOLOCK)
					WHERE DeletedDate IS NULL  
					AND ISNULL(@Param1,0)=0 OR  (AssociatedCourseId=@Param1)
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "MODULES","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD COURSES ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'COURSES')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'COURSES';
			 

			INSERT INTO  #TBLDDL(Name, Value)
			 
				SELECT [CourseFullName]+ '('+ [CourseCode] +')' AS Name
						,[CourseId] AS Value 
					FROM [dbo].[TBL_Course] WITh(NOLOCK)
					WHERE DeletedDate IS NULL  
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "COURSES","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD COURSES ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'BATCHCOURSES')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'BATCHCOURSES';
			 

			INSERT INTO  #TBLDDL(Name, Value)
			 
				SELECT [CourseFullName]+ '('+ [CourseCode] +')' AS Name
						,[CourseId] AS Value 
					FROM [dbo].[TBL_Course] WITh(NOLOCK)
					WHERE courseId IN (SELECT CourseId FROM TBL_BatchCourse WHERE BatchId = CAST(@Param1 AS INT) AND DeletedDate IS NULL)
					AND DeletedDate IS NULL  
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "BATCHCOURSES","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD MEDIAS ------

			-- FUNCTION FOR DDL ------
		-- LOAD MEDIAS ------
		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'MEDIAS')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'MEDIAS';
			 

			INSERT INTO  #TBLDDL(Name, Value)
			 
				SELECT [MediaName] AS Name
						,[MediaId] AS Value 
					FROM [dbo].[TBL_Media] WITh(NOLOCK)
					WHERE DeletedDate IS NULL  
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "MEDIAS","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD BATCH STATUS ------

		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'BATCH_STATUS')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'BATCH_STATUS';
			 

			INSERT INTO  #TBLDDL(Name, Value)
			 
				SELECT [StatusName] AS Name
						,[StatusId] AS Value 
					FROM [dbo].[TBL_Status] WITh(NOLOCK)
					WHERE DeletedDate IS NULL
					AND StatusGroup = 'BATCH'
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "BATCH_STATUS","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FUNCTION FOR DDL ------
		-- LOAD BATCHES ------

		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'BATCHES')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'BATCHES';
			 

			INSERT INTO  #TBLDDL(Name, Value)
			 
				SELECT [BatchName] + ' ( '+ BatchCode+' )' AS Name
						,[BatchId] AS Value 
					FROM [dbo].[TBL_Batch] WITh(NOLOCK)
					WHERE DeletedDate IS NULL 
				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "BATCHES","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  


		-- LOAD ASSISTANT COORDINATOR ------

		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'ASSTCOORDINATOR')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'ASSTCOORDINATOR';
			 

			INSERT INTO  #TBLDDL(Name, Value)
			 
			 SELECT 
				TM.FirstName + ' ' + ISNULL(TM.LastName,'') AS Name,
				TM.MemberId AS Value
			  FROM [dbo].[TBL_Member] TM WITH(NOLOCK)
			  WHERE DeletedDate IS NULL AND TM.Designation='ASSISTANT COORDINATOR'
				 				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "ASSTCOORDINATOR","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  


			-- LOAD  COORDINATOR ------

		IF EXISTS (SELECT 1 FROM #TMP_DDLListWithParams WHERE DDLName = 'COORDINATOR')
		BEGIN
			-- Clean TEMP VARIABLES  --
			DELETE FROM #TBLDDL
			SET @TMPJSON = NULL;
			SET @Param1 = '';
			SET @Param2 = '';
			
			-- Fill Table with New Value --
			-- Load Param1 First
			SELECT @Param1 = Param1, @Param2 = Param2 FROM  #TMP_DDLListWithParams WHERE DDLName = 'COORDINATOR';
			 

			INSERT INTO  #TBLDDL(Name, Value)
			 
			 SELECT 
				TM.FirstName + ' ' + ISNULL(TM.LastName,'') AS Name,
				TM.MemberId AS Value
			  FROM [dbo].[TBL_Member] TM WITH(NOLOCK)
			  WHERE DeletedDate IS NULL AND TM.Designation='COORDINATOR'
				 				
			-- Update to JSON --
				
			SELECT @TMPJSON = COALESCE(@TMPJSON + ',', '') + '{"Name": "'+ Name +'", "Value": "'+ Value +'"}'
			FROM #TBLDDL ORDER BY ROW_PRIORITY

			SET @TMPJSON = CASE WHEN @Param2 <> 'HIDE_DEFAULT' THEN '{"Name":"--Select--","Value":"0"}' + CASE WHEN ISNULL(@TMPJSON,'') <> '' THEN ',' ELSE '' END    ELSE '' END + @TMPJSON
			SET @TableJSON += CASE WHEN  ISNULL(@TableJSON,'')='' THEN '' ELSE ',' END +  '{"DDLName": "COORDINATOR","Items": ['+ ISNULL(@TMPJSON,'') +']}';
		END  

		-- FINALLY THE RESULT PAGE
		SELECT 
			@PageName AS PageName,
			'['+ @TableJSON +']'as DDLItems
	END














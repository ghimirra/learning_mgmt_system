 
/*
Created by: Prashant 
Created on: 6/17/2018 8:08:40 PM 
DESC: GET DATA FROM TABLE [dbo].[Member]

ALTER TABLE [TBL_Member]
ADD Position NVARCHAR(200) NULL

ALTER TABLE [TBL_Member]
ADD CompanyName NVARCHAR(200) NULL

EXEC dbo.[csp.Member.Get]
@MemberId = NULL 
,@Designation = NULL 
,@Phone = NULL 
,@Mobile = NULL 
,@UserId = NULL 
,@PageNumber = 1 
,@PageSize	= 20 
,@ShowAll	= 0 

*/




ALTER PROCEDURE [dbo].[csp.Trainee.Get]
(
	@MemberId 		INT = NULL 
	,@Designation   NVARCHAR(250) = NULL 
	,@Phone    		VARCHAR(25) = NULL 
	,@Mobile   		VARCHAR(25) = NULL 
	,@UserId		NVARCHAR(256) = NULL 
	,@PageNumber	INT = 1 
	,@PageSize 		INT = 20 
	,@ShowAll		INT = 0 
	,@Keyword		NVARCHAR(200)=NULL

)
AS 
	BEGIN 
		WITH TMP_TBL AS 
		( 
			SELECT 
				(ROW_NUMBER() OVER (ORDER BY TM.MemberId)) AS RowNumber 
				,TM.[MemberId] AS [MemberId]
				,TM.[OrganizationId] AS [OrganizationId]
				,TM.[FirstName] AS [FirstName]
				,TM.[LastName] AS [LastName]
				,TM.[SystemRoleId] AS [SystemRoleId]
				,ISNULL(TM.[Designation],'') AS [Designation]
				,ISNULL(TM.[LevelId],0) AS [LevelId]
				,ISNULL(TM.[Department],'') AS [Department]
				,ISNULL(TM.[Address],'') AS [Address]
				,ISNULL(TM.[PrimaryContactEmail],'') AS [PrimaryContactEmail]
				,ISNULL(TM.[Phone],'') AS [Phone]
				,ISNULL(TM.[Mobile],'') AS [Mobile]
				,ISNULL(TM.[ProfileImageUrl],'') AS [ProfileImageUrl] 
				,ISNULL(TM.[Education],'') AS [Education]
				,ISNULL(TM.[Notes],'') AS [Notes]
				,ISNULL(TM.Position,'') AS Position
				,ISNULL(TM.CompanyName,'') AS CompanyName
				,COUNT(TM.[MemberId]) OVER () AS TotalCount 
			FROM 
				[dbo].[TBL_Member] TM WITH(NOLOCK) 
			WHERE 
				 (ISNULL(@MemberId,0) = 0 OR TM.[MemberId] = @MemberId )
				 AND (TM.[Designation] = 'TRAINEE' )
				AND (ISNULL(@Phone,'') = '' OR TM.[Phone] = @Phone )
				AND (ISNULL(@Mobile,'') = '' OR TM.[Mobile] = @Mobile )
				AND (ISNULL(@Keyword,'')='' OR (TM.Mobile LIKE '%'+@Keyword+'%' OR TM.Phone LIKE '%'+@Keyword+'%' OR TM.FirstName LIKE '%'+@Keyword+'%' OR TM.LastName LIKE '%'+@Keyword+'%' 
				OR TM.PrimaryContactEmail LIKE '%'+@Keyword+'%'))
				AND TM.DeletedDate IS NULL 
		) 


		SELECT 
			RowNumber 
			,[MemberId]
			,[OrganizationId]
			,[FirstName]
			,[LastName]
			,[SystemRoleId]
			,[Designation]
			,[LevelId]
			,[Department]
			,[Address]
			,[PrimaryContactEmail]
			,[Phone]
			,[Mobile]
			,[ProfileImageUrl] 
			,[Education]
			,[Notes]
			,[Position]
			,[CompanyName]
			,[TotalCount]
		FROM TMP_TBL 
		WHERE 
			(@ShowAll = 1 OR (RowNumber > ((@PageNumber-1) * @PageSize))) 
			AND ((@ShowAll = 1 OR RowNumber <= ((@PageNumber-1) * @PageSize) + @PageSize)) 
	END 



/****** Object:  StoredProcedure [dbo].[csp.Batch.Update]    Script Date: 7/12/2018 4:24:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created by: Prashant 
Created on: 21/06/2018 
DESC: UPDATE DATA TO TABLE [dbo].[Batch]


DECLARE @MsgType VARCHAR(10) = '' 
DECLARE @MsgText VARCHAR(100) = '' 
EXEC dbo.[csp.Batch.Update]
	@ActionType = 'ADD' 
	,@BatchId = NULL 
	,@OrganizationId = NULL 
	,@BatchCode = NULL 
	,@BatchName = NULL 
	,@BatchLevel = NULL 
	,@BatchCategory = NULL 
	,@Coordinator = NULL 
	,@ASSCoordinator = NULL 
	,@BatchStartDate = NULL 
	,@BatchEndDate = NULL 
	,@BatchSummary = NULL 
	,@UserId = '' 
	,@MsgType	= @MsgType OUTPUT 
	,@MsgText	= @MsgText OUTPUT 

	SELECT @MsgType,@MsgText 
	SELECT * FROM [dbo].[TBL_Batch] 


*/

ALTER PROCEDURE [dbo].[csp.Batch.Update]
(
	@ActionType			VARCHAR(10) 
	,@BatchId  			INT = NULL 
	,@OrganizationId	UNIQUEIDENTIFIER = NULL 
	,@BatchCode			VARCHAR(100) = NULL 
	,@BatchName			VARCHAR(500) = NULL 
	,@BatchLevel    	NVARCHAR(100) = NULL 
	,@BatchCategory 	NVARCHAR(100) = NULL 
	,@CoordinatorId   	INT = NULL 
	,@AssistantCoordinatorId	INT = NULL 
	,@BatchStartDate	DATETIME = NULL 
	,@BatchEndDate  	DATETIME = NULL 
	,@BatchSummary  	NVARCHAR(MAX) = NULL 
	,@TrainingAddress	nvarchar(500)=NULL
	,@UserId	 		NVARCHAR(256) = NULL 
	,@MsgType			VARCHAR(10) =	NULL OUTPUT 
	,@MsgText			VARCHAR(100) =	NULL OUTPUT 
	,@ReturnBatchId 	INT =	0 OUTPUT 
)
AS 

SET @ReturnBatchId = @BatchId; 

-- CHECK IF THE USER'S ORGANIZATION MATCHES WITH THE ORGANIZATION PASSED INSIDE.
-- PERFORM OTHER VALIDATION RELATED WITH USER & ROLES HERE.... SHOW ERROR IF NOT MATCHES..
IF (
	@ActionType <> 'ADD'  
	AND NOT EXISTS (SELECT 1 FROM [dbo].[TBL_Batch] WHERE BatchId = @BatchId) 
)
BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Record does not exist.' 
	RETURN; 

END 

IF (
	@ActionType <> 'DELETE'  
	AND (@BatchEndDate< @BatchStartDate ))

BEGIN 
	SET @MsgType='ERROR' 
	SET @MsgText='Batch end date must be greater than start date.' 
	RETURN; 

END 

DECLARE @CurrentDate DATETIME = GETUTCDATE(); 
IF (@ActionType <> 'DELETE') 
	BEGIN 
		IF (@ActionType = 'ADD') 
			BEGIN 
				INSERT INTO [dbo].[TBL_Batch] 
				( 
					[OrganizationId]  
					,[BatchCode]  
					,[BatchName]  
					,[BatchLevel]  
					,[BatchCategory]  
					,[CoordinatorId]  
					,[AssistantCoordinatorId]  
					,[BatchStartDate]  
					,[BatchEndDate]  
					,[BatchSummary] 
					,[TrainingAddress] 
					,[CreatedDate]  
					,[CreatedBy]  
				)  
				VALUES  
				(  
					@OrganizationId  
					,@BatchCode  
					,@BatchName  
					,@BatchLevel  
					,@BatchCategory  
					,@CoordinatorId 
					,@AssistantCoordinatorId  
					,@BatchStartDate  
					,@BatchEndDate  
					,@BatchSummary  
					,@TrainingAddress
					,@CurrentDate  
					,@UserId 
				)  


				SET @MsgType='OK'  
				SET @MsgText='Row added successfully.'  

				SET @ReturnBatchId = @@IDENTITY;  

			END  
		ELSE IF (@ActionType = 'UPDATE')  
			BEGIN 
				UPDATE [dbo].[TBL_Batch] 
				SET 
					[OrganizationId]		 = @OrganizationId  
					,[BatchCode]			 = @BatchCode  
					,[BatchName]			 = @BatchName  
					,[BatchLevel]    		 = @BatchLevel  
					,[BatchCategory] 		 = @BatchCategory  
					,[CoordinatorId]   		 = @CoordinatorId  
					,[AssistantCoordinatorId] = @AssistantCoordinatorId 
					,[BatchStartDate]		 = @BatchStartDate  
					,[BatchEndDate]  		 = @BatchEndDate  
					,[BatchSummary]  		 = @BatchSummary  
					,[TrainingAddress]		 =@TrainingAddress
					,[UpdatedDate] 	= @CurrentDate  
					,[UpdatedBy] 	= ISNULL(@UserId,[UpdatedBy])  
				WHERE BatchId = @BatchId


				SET @MsgType='OK' 
				SET @MsgText='Data edited successfully.' 

			END 
	END 
ELSE 
	BEGIN 
		UPDATE [dbo].[TBL_Batch] 
		SET 
			[DeletedDate] 	= @CurrentDate 
			,[DeletedBy] 	= ISNULL(@UserId,[DeletedBy]) 
		WHERE BatchId = @BatchId 


		SET @MsgType='OK' 
		SET @MsgText='Data deleted successfully.' 

	END 



﻿using Autofac;
using Rolpo.Model;
using System;
using System.Web;

namespace Rolpo.UI.Modules
{

	public class EFModule : Autofac.Module
	{

		protected override void Load(ContainerBuilder builder)
		{
            builder.RegisterType(typeof(RolpoContext)).As(typeof(IContext)).InstancePerLifetimeScope();

          
        }
	} 
}
﻿using Autofac;
using System.Linq;
using System.Reflection;

namespace Rolpo.UI.Modules
{
	public class ServiceModule : Autofac.Module
	{

		protected override void Load(ContainerBuilder builder)
		{

			builder.RegisterAssemblyTypes(Assembly.Load("Rolpo.Service"))

					  .Where(t => t.Name.EndsWith("Service"))

					  .AsImplementedInterfaces()

					  .InstancePerLifetimeScope();

		}

	}
}
﻿//Created By: Prashant 
//Created On: 03/04/2018 
// Controller for Services 
// Initialization for Services 


rolpo_app.requires.push('ngSanitize', 'ui.select', 'ui.bootstrap');
rolpo_app.constant('ngAuthSettings', {
    apiServiceBaseUri: serviceBase,
    clientId: 'rolpo.com'
});

// Service For Services 
; (function () {
    'use strict';
    rolpo_app.factory('servicesService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

        var serviceBase = ngAuthSettings.apiServiceBaseUri;
        var servicesServiceFactory = {};

        //Default Filter 
        var _defaultDDLFilter = function () {
            return {
                PageName: "AddEditServices",
                FilterList: [
					{
					    DDLName: "MEDIAID",
					    Param1: "",
					    Param2: "HIDE_DEFAULT"
					}
                ]
            };
        };

        //Services Empty Filter 
        var _servicesEmptyFilter = function () {
            return {
                ServiceId: 0,
                ServiceGroup: "",
                ServiceName: "",
                PageNumber: 1,
                PageSize: 20,
                ShowAll: 0
            };
        };

        // Get DDL List by Filter
        var _getDDLList = function (ddlFilter) {
            return $http({
                url: serviceBase + 'api/Home/LoadDDLs',
                method: "post",
                data: ddlFilter
            });
        };

        // Get Servicess by Filter
        var _getServicess = function (tsfilter) {
            return $http({
                url: serviceBase + 'api/Services/GetServicessList',
                method: "post",
                data: tsfilter
            });
        };

        //Create New Services
        var _createServices = function (services) {
            var request = $http({
                method: 'post',
                url: serviceBase + 'api/Services/SaveServices',
                data: services
            });
            return request;
        };

        //Update Services 
        var _updateServices = function (services) {
            var request = $http({
                method: "post",
                url: serviceBase + "api/Services/UpdateServices",
                data: services
            });
            return request;
        };

        //Delete Services
        var _deleteServices = function (serviceid) {
            var request = $http({
                method: "delete",
                url: serviceBase + "api/Services/DeleteServices/" + serviceid
            });
            return request;
        };

        servicesServiceFactory.DDLDefaultFilter = _defaultDDLFilter;
        servicesServiceFactory.GetDDLByFilter = _getDDLList;
        servicesServiceFactory.getServicess = _getServicess;
        servicesServiceFactory.createServices = _createServices;
        servicesServiceFactory.updateServices = _updateServices;
        servicesServiceFactory.deleteServices = _deleteServices;
        servicesServiceFactory.ServicesEmptyFilter = _servicesEmptyFilter;

        return servicesServiceFactory;
    }]);
}());


// Controller Starts Here.. 
; (function () {
    'use strict';
    rolpo_app.controller('servicesController', ['$scope', '$rootScope', 'servicesService', 'modalService', '$uibModal', '$uibModalStack', '$filter', function ($scope, $rootScope, servicesService, modalService, $uibModal, $uibModalStack, $filter) {

        // Variables and declarations 

        $scope.loading = true;
        $scope.servicess = [];
        $scope.services = {};
        $scope.ServicesPageInfo = {};

        //Populate DDLs
        var ddlFilter = servicesService.DDLDefaultFilter();
        servicesService.GetDDLByFilter(ddlFilter).then(function (results) {
            $scope.ddLItems = angular.fromJson(results.data.DDLItems);


            //Get mediaid 
            $scope.mediaid = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "MEDIAID" })[0].Items;
        });

        // Methods

        // Get Services by Filter

        $scope.GetServicesByFilter = function () {
            GetServicess($scope.tsfilter);
        };

        // Reset Services Filter
        $scope.ResetServicesFilter = function () {
            var pageSize = $scope.tsfilter.PageSize;

            $scope.tsfilter = servicesService.ServicesEmptyFilter();
            $scope.tsfilter.PageSize = pageSize;

            GetServicess($scope.tsfilter);
        };

        //On Services Page Changed
        $scope.OnServicesPageChanged = function () {
            GetServicess($scope.tsfilter);
        };

        //On Page Size Changed
        $scope.OnServicesPageSizeChanged = function () {
            GetServicess($scope.tsfilter);
        };

        // Open Window for Saving new Services
        $scope.OpenServicesSaveDialog = function () {
            $scope.services = { ServiceId: 0 };
            MSG({}); //Init
            $scope.servicesActionTitle = "Add New Services";
            var modalInstance = $uibModal.open({
                animation: true,
                scope: $scope,
                templateUrl: 'customUpdateServices',
                backdrop: 'static',
                keyboard: false,
                modalFade: true,
                size: ''
            });

        };

        // Open Window for updating Services
        $scope.OpenServicesUpdateDialog = function (ServiceId) {
            var tsfilter = servicesService.ServicesEmptyFilter();
            tsfilter.ServiceId = ServiceId;
            $scope.loading = true;
            MSG({}); //Init

            servicesService.getServicess(tsfilter).then(function (results) {
                if (results.data.length != 1) {
                    $scope.loading = false;
                    MSG({ 'elm': "Services_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading servicess!', 'MsgAsModel': error.data });
                    return;
                }
                $scope.services = results.data[0];
                $scope.servicesActionTitle = "Update Services";

                var modalInstance = $uibModal.open({
                    animation: true,
                    scope: $scope,
                    templateUrl: 'customUpdateServices',
                    backdrop: 'static',
                    keyboard: false,
                    modalFade: true,
                    size: ''
                });
                $scope.loading = false;
            }, function (error) {
                MSG({ 'elm': "Services_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading servicess!', 'MsgAsModel': error.data });
                $scope.loading = false;
            });

        };

        //Update Services
        $scope.CreateUpdateServices = function (ServiceId) {
            if (ServiceId == 0) { CreateNewServices($scope.services); } else { UpdateServices($scope.services); }
        };

        //Delete Services
        $scope.DeleteServices = function (ServiceId) {
            MSG({}); //Init
            var modalOptions = {
                closeButtonText: 'Cancel',
                actionButtonText: 'Delete Services',
                headerText: 'Delete Item',
                bodyText: 'Are you sure you want to delete this?'
            };
            modalService.showModal({}, modalOptions).then(function (result) {
                $scope.loading = true;
                servicesService.deleteServices(ServiceId).then(function (results) {
                    angular.forEach($scope.servicess, function (value, key) {
                        if ($scope.servicess[key].ServiceId === ServiceId) {
                            $scope.servicess.splice(key, 1);
                            return false;
                        }
                    });

                    $scope.loading = false;
                    MSG({ 'elm': "Services_alert", "MsgType": "OK", "MsgText": "Services deleted successfully." });
                }, function (error) {
                    MSG({ 'elm': "Services_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting servicess!', 'MsgAsModel': error.data });
                    $scope.loading = false;
                });
            });
        };

        // Cancel  Editing
        $scope.cancelEditing = function () {
            $uibModalStack.dismissAll();
        };

        // Functions 
        // Function to Get Services
        function GetServicess(tsfilter) {
            $scope.loading = true;
            $scope.HasTS_Records = false;
            servicesService.getServicess(tsfilter).then(function (results) {
                $scope.servicess = results.data;
                var tmp_page_start = (($scope.tsfilter.PageNumber - 1) * ($scope.tsfilter.PageSize) + 1), tmp_page_end = ($scope.tsfilter.PageNumber) * ($scope.tsfilter.PageSize);
                if (results.data.length > 0) {
                    $scope.ServicesPageInfo = {
                        Has_record: true,
                        TotalItems: results.data[0]["TotalCount"],
                        PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0,
                        PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"]
                    };
                } else { $scope.ServicesPageInfo = {}; }
                $scope.loading = false;
            }, function (error) {
                MSG({ 'elm': "Services_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading servicess!', 'MsgAsModel': error.data });
                $scope.loading = false;
            });
        };

        // Create New Services Function 
        function CreateNewServices(services) {
            $scope.loading = true;
            servicesService.createServices(services).then(function (results) {
                $scope.servicess.push(results.data);
                $scope.loading = false;
                $uibModalStack.dismissAll();
                MSG({ 'elm': "Services_alert", "MsgType": "OK", "MsgText": "Services added successfully." });
            }, function (error) {
                MSG({ 'elm': "Services_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding services!', 'MsgAsModel': error.data });
                $scope.loading = false;
            });
        }

        //Update Services Function 
        function UpdateServices(services) {
            $scope.loading = true;
            servicesService.updateServices(services).then(function (results) {
                angular.forEach($scope.servicess, function (value, key) {
                    if ($scope.servicess[key].ServiceId === services.ServiceId) {
                        $scope.servicess[key] = services;
                        return false;
                    }
                });
                $scope.loading = false;
                $uibModalStack.dismissAll();
                MSG({ 'elm': "Services_alert", "MsgType": "OK", "MsgText": "Services updated successfully." });
            }, function (error) {
                MSG({ 'elm': "Services_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating services!', 'MsgAsModel': error.data });
                $scope.loading = false;
            });
        };

        //Datepicker
        $scope.dateOptions = {
            'year-format': "'yy'",
            'show-weeks': false
        };

        // Call Services for first time
        $scope.ServicesPageInfo = {};
        $scope.tsfilter = servicesService.ServicesEmptyFilter();
        $scope.tsfilter.PageNumber = 1;
        $scope.tsfilter.PageSize = '20';

        GetServicess($scope.tsfilter);

    }]);
}());


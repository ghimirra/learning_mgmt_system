﻿//Created By: Prashant 
//Created On: 21/06/2018 
// Controller for Batch 
// Initialization for Batch 
 
rolpo_app.constant('ngAuthSettings', {
	apiServiceBaseUri: serviceBase,
	clientId: 'rolpo.com'
});

rolpo_app.requires.push('ui.select');

// Service For Batch 
; (function () {
	'use strict';
	rolpo_app.factory('batchAddEditService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

		var serviceBase = ngAuthSettings.apiServiceBaseUri;
		var batchAddEditServiceFactory = {};

		//Default Filter 
		var _defaultDDLFilter = function () {
			return {
				PageName: "AddEditBatch",
				FilterList: [
									{
						DDLName: "TRAINER",
						Param1: "",
						Param2: "HIDE_DEFAULT"
					}
					, {
						DDLName: "TRAINEE",
						Param1: "",
						Param2: "HIDE_DEFAULT"
					}

					, {
						DDLName: "MODULES",
						Param1: "",
						Param2: "HIDE_DEFAULT"
					}, {
						DDLName: "COURSES",
						Param1: "",
						Param2: "HIDE_DEFAULT"
					}, {
						DDLName: "BATCH_STATUS",
						Param1: "",
						Param2: "HIDE_DEFAULT"
					},
                       {
                           DDLName: "COORDINATOR",
                           Param1: "",
                           Param2: "HIDE_DEFAULT"
                       },
                       {
                           DDLName: "ASSTCOORDINATOR",
                           Param1: "",
                           Param2: "HIDE_DEFAULT"
                       }
				]
			};
		};

		// Get Batch by Filter
		var _getBatchById = function (id) {
			return $http({
				url: serviceBase + 'api/Batch/GetBatchById',
				method: "get",
				params: { Id: id }
			});
		};

		// Get DDL List by Filter
		var _getDDLList = function (ddlFilter) {
			return $http({
				url: serviceBase + 'api/Home/LoadDDLs',
				method: "post",
				data: ddlFilter
			});
		};
  

		//Update Batch 
		var _updateBatch = function (batch) {
			var request = $http({
				method: "post",
				url: serviceBase + "api/Batch/UpdateBatch",
				data: batch
			});
			return request;
		};

		//Delete Batch
		var _deleteBatch = function (batchid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/Batch/DeleteBatch/" + batchid
			});
			return request;
		};


		// REGION BATCH TRAINER

		//BatchTrainer Empty Filter 
		var _batchtrainerEmptyFilter = function () {
			return {
				BatchTrainerId: 0,
				MemberId: 0,
				BatchId: batchId,
				PageNumber: 1,
				PageSize: 20,
				ShowAll: 0
			};
		};

		 

		// Get BatchTrainers by Filter
		var _getBatchTrainers = function (tbfilter) {
			return $http({
				url: serviceBase + 'api/BatchTrainer/GetBatchTrainersList',
				method: "post",
				data: tbfilter
			});
		};

		//Create New BatchTrainer
		var _createBatchTrainer = function (batchtrainer) {
			var request = $http({
				method: 'post',
				url: serviceBase + 'api/BatchTrainer/SaveBatchTrainer',
				data: batchtrainer
			});
			return request;
		};

		//Update BatchTrainer 
		var _updateBatchTrainer = function (batchtrainer) {
			var request = $http({
				method: "post",
				url: serviceBase + "api/BatchTrainer/UpdateBatchTrainer",
				data: batchtrainer
			});
			return request;
		};

		//Delete BatchTrainer
		var _deleteBatchTrainer = function (batchtrainerid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/BatchTrainer/DeleteBatchTrainer/" + batchtrainerid
			});
			return request;
		};

		// ./BATCH TRAINER

		// REGION BATCH TRAINEE

	 	//BatchTrainee Empty Filter 
		var _batchtraineeEmptyFilter = function () {
			return {
				BatchTraineeId: 0,
				BatchId: batchId,
				PageNumber: 1,
				PageSize: 20,
				ShowAll: 0
			};
		};
		 

		// Get BatchTrainees by Filter
		var _getBatchTrainees = function (batchtraineefilter) {
			return $http({
				url: serviceBase + 'api/BatchTrainee/GetBatchTraineesList',
				method: "post",
				data: batchtraineefilter
			});
		};

		// Get BatchTrainees Not inlcuded by filter
		var _getBatchTraineesNotIncluded = function (input) {
			return $http({
				url: serviceBase + 'api/BatchTrainee/GetRemainingBatchTrainees',
				method: "post",
				data: input
			});
		};


		//Create New BatchTrainee
		var _createBatchTrainee = function (batchtrainee) {
			var request = $http({
				method: 'post',
				url: serviceBase + 'api/BatchTrainee/SaveBatchTrainee',
				data: batchtrainee
			});
			return request;
		};

		//Update BatchTrainee 
		var _updateBatchTrainee = function (batchtrainee) {
			var request = $http({
				method: "post",
				url: serviceBase + "api/BatchTrainee/UpdateBatchTrainee",
				data: batchtrainee
			});
			return request;
		};

		//Delete BatchTrainee
		var _deleteBatchTrainee = function (batchtraineeid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/BatchTrainee/DeleteBatchTrainee/" + batchtraineeid
			});
			return request;
		};
		// ./BATCH TRAINEE


		//REGION "BATCH COURSE"

	 
		//BatchCourse Empty Filter 
		var _batchcourseEmptyFilter = function () {
			return {
				BatchCourseId: 0,
				CourseId: 0,
				BatchId: batchId,
				SearchText: "",
				PageNumber: 1,
				PageSize: 20,
				ShowAll: 0
			};
		};

	 
		// Get BatchCourses by Filter
		var _getBatchCourses = function (batchcoursefilter) {
			return $http({
				url: serviceBase + 'api/BatchCourse/GetBatchCoursesList',
				method: "post",
				data: batchcoursefilter
			});
		};

		//Create New BatchCourse
		var _createBatchCourse = function (batchcourse) {
			var request = $http({
				method: 'post',
				url: serviceBase + 'api/BatchCourse/SaveBatchCourse',
				data: batchcourse
			});
			return request;
		};

		//Update BatchCourse 
		var _updateBatchCourse = function (batchcourse) {
			var request = $http({
				method: "post",
				url: serviceBase + "api/BatchCourse/UpdateBatchCourse",
				data: batchcourse
			});
			return request;
		};

		//Delete BatchCourse
		var _deleteBatchCourse = function (batchcourseid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/BatchCourse/DeleteBatchCourse/" + batchcourseid
			});
			return request;
		};

		// ./"BATCH COURSE"


		// REGION "BATCH SUMMARY"

		 

		//BatchSummary Empty Filter 
		var _batchsummaryEmptyFilter = function () {
			return {
				BatchSummaryId: 0,
				StatusId: 0,
				BatchId: batchId,
				PageNumber: 1,
				PageSize: 20,
				ShowAll: 0
			};
		};
		 

		// Get BatchSummarys by Filter
		var _getBatchSummarys = function (batchsummaryfilter) {
			return $http({
				url: serviceBase + 'api/BatchSummary/GetBatchSummarysList',
				method: "post",
				data: batchsummaryfilter
			});
		};

		//Create New BatchSummary
		var _createBatchSummary = function (batchsummary) {
			var request = $http({
				method: 'post',
				url: serviceBase + 'api/BatchSummary/SaveBatchSummary',
				data: batchsummary
			});
			return request;
		};

		//Update BatchSummary 
		var _updateBatchSummary = function (batchsummary) {
			var request = $http({
				method: "post",
				url: serviceBase + "api/BatchSummary/UpdateBatchSummary",
				data: batchsummary
			});
			return request;
		};

		//Delete BatchSummary
		var _deleteBatchSummary = function (batchsummaryid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/BatchSummary/DeleteBatchSummary/" + batchsummaryid
			});
			return request;
		};


		// ./BATCH SUMMARY

		batchAddEditServiceFactory.DDLDefaultFilter = _defaultDDLFilter;
		batchAddEditServiceFactory.GetDDLByFilter = _getDDLList;
 		batchAddEditServiceFactory.updateBatch = _updateBatch;
 		batchAddEditServiceFactory.deleteBatch = _deleteBatch;
 		batchAddEditServiceFactory.getBatchById = _getBatchById;
  

 		batchAddEditServiceFactory.getBatchTrainers = _getBatchTrainers;
 		batchAddEditServiceFactory.createBatchTrainer = _createBatchTrainer;
 		batchAddEditServiceFactory.updateBatchTrainer = _updateBatchTrainer;
 		batchAddEditServiceFactory.deleteBatchTrainer = _deleteBatchTrainer;
 		batchAddEditServiceFactory.BatchTrainerEmptyFilter = _batchtrainerEmptyFilter;
		 

 		batchAddEditServiceFactory.getBatchTrainees = _getBatchTrainees;
 		batchAddEditServiceFactory.createBatchTrainee = _createBatchTrainee;
 		batchAddEditServiceFactory.updateBatchTrainee = _updateBatchTrainee;
 		batchAddEditServiceFactory.deleteBatchTrainee = _deleteBatchTrainee;
 		batchAddEditServiceFactory.BatchTraineeEmptyFilter = _batchtraineeEmptyFilter;
 		batchAddEditServiceFactory.GetBatchTraineesNotIncluded = _getBatchTraineesNotIncluded;
		 
 		batchAddEditServiceFactory.getBatchCourses = _getBatchCourses;
 		batchAddEditServiceFactory.createBatchCourse = _createBatchCourse;
 		batchAddEditServiceFactory.updateBatchCourse = _updateBatchCourse;
 		batchAddEditServiceFactory.deleteBatchCourse = _deleteBatchCourse;
 		batchAddEditServiceFactory.BatchCourseEmptyFilter = _batchcourseEmptyFilter;
		 
 		batchAddEditServiceFactory.getBatchSummarys = _getBatchSummarys;
 		batchAddEditServiceFactory.createBatchSummary = _createBatchSummary;
 		batchAddEditServiceFactory.updateBatchSummary = _updateBatchSummary;
 		batchAddEditServiceFactory.deleteBatchSummary = _deleteBatchSummary;
 		batchAddEditServiceFactory.BatchSummaryEmptyFilter = _batchsummaryEmptyFilter;

		return batchAddEditServiceFactory;
	}]);
}());


// Controller Starts Here.. 
; (function () {
	'use strict';
	rolpo_app.controller('batchAddEditController', ['$scope', '$rootScope', 'batchAddEditService', 'modalService', '$uibModal', '$uibModalStack', '$filter', function ($scope, $rootScope, batchAddEditService, modalService, $uibModal, $uibModalStack, $filter) {

		// Variables and declarations 

		$scope.loading = true; 
		$scope.batch = {}; 
		$scope.BatchId = batchId;

		////Populate DDLs
		var ddlFilter = batchAddEditService.DDLDefaultFilter();
		batchAddEditService.GetDDLByFilter(ddlFilter).then(function (results) {
			$scope.ddLItems = angular.fromJson(results.data.DDLItems);

				//Get trainers 
			$scope.trainersddl = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "TRAINER" })[0].Items;

				//Get trainees 
				$scope.traineesddl = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "TRAINEE" })[0].Items;

				//Get topic 
				$scope.topicsddl = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "MODULES" })[0].Items;

				//Get Courses
				$scope.coursesddl = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "COURSES" })[0].Items;

				//Get batchstatusddl
				$scope.batchstatusddl = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "BATCH_STATUS" })[0].Items;

                 //coordinator
				$scope.coordDDL = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "COORDINATOR" })[0].Items;

		        //assistant coordinator
				$scope.asscoordDDL = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "ASSTCOORDINATOR" })[0].Items;
		});

		// Methods

	    
		//Delete Batch
		$scope.DeleteBatch = function () {
			MSG({}); //Init
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete Batch',
				headerText: 'Delete Item',
				bodyText: 'Are you sure you want to delete this?'
			};
			modalService.showModal({}, modalOptions).then(function (result) {
				$scope.loading = true;
				batchAddEditService.deleteBatch($scope.BatchId).then(function (results) {
					 
					$scope.loading = false;
					MSG({ 'elm': "Batch_alert", "MsgType": "OK", "MsgText": "Batch deleted successfully." });
				}, function (error) {
					MSG({ 'elm': "Batch_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting batchs!', 'MsgAsModel': error.data });
					$scope.loading = false;
				});
			});
		};

		// Cancel  Editing
		$scope.cancelEditing = function () {
			$uibModalStack.dismissAll();
		};

		// Functions 
		// Function to Get Batch
		function GetBatchById() {
			$scope.loading = true;
			$scope.HasTB_Records = false;
			batchAddEditService.getBatchById($scope.BatchId).then(function (results) {
				if (results.data != null) {
					$scope.batch = results.data;
					FixDate($scope.batch, 'BatchStartDate');
					FixDate($scope.batch, 'BatchEndDate');
					//$scope.batch.BatchStartDate = new Date($scope.batch.BatchStartDate + 'Z');
					//$scope.batch.BatchEndDate = new Date($scope.batch.BatchEndDate + 'Z');


					// Load Batch Trainers
					GetBatchTrainers($scope.tbfilter);

					//Load Batch Trainees
					GetBatchTrainees($scope.batchtraineefilter);

					//Load Batch Courses

					GetBatchCourses($scope.batchcoursefilter);

					//Load Batch Summary

					GetBatchSummarys($scope.batchsummaryfilter);

				} else {
					$scope.BatchId = 0;
				}
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "Batch_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchs!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};
		 

		//Update Batch Function 
		$scope.UpdateBatch = function(){
			$scope.loading = true;
			batchAddEditService.updateBatch($scope.batch).then(function (results) {
				$scope.batch = results.data;

				$scope.loading = false;
				MSG({ 'elm': "Batch_alert", "MsgType": "OK", "MsgText": "Batch updated successfully." });
			}, function (error) {
				MSG({ 'elm': "Batch_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating batch!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};
		 

		//Datepicker
		$scope.dateOptions = {
			'year-format': "'yy'",
			'show-weeks': false
		};

		function FixDate(obj,prop) {
			obj[prop] = new Date(obj[prop]+'Z');
		};

		$scope.OpenDate = function (obj, prop) {
			obj[prop] = true;
		}
		 
		// REGION BATCH TRAINER

		// Variables and declarations 
		 
		$scope.batchtrainers = [];
		$scope.batchtrainer = {};
		$scope.batchcoursesddl = [];//ddl for batch courses
		$scope.batchmodulesddl = []; // ddl for modules

		$scope.BatchTrainerPageInfo = {};

		 

		// Open Window for Saving new BatchTrainer
		$scope.OpenBatchTrainerSaveDialog = function () {
			$scope.batchtrainer = { BatchTrainerId: 0 };
			MSG({}); //Init
			$scope.batchtrainerActionTitle = "Add New Batch Trainer";

			//Default Filter 
			var _defaultDDLFilter = {

				PageName: "AddEditBatch",
				FilterList: [
                     {
                     	DDLName: "BATCHCOURSES",
                     	Param1: $scope.BatchId,
                     	Param2: "HIDE_DEFAULT_111"
                     }


				]
			};


			batchAddEditService.GetDDLByFilter(_defaultDDLFilter).then(function (results) {
				var ddLItems = angular.fromJson(results.data.DDLItems);
				$scope.batchcoursesddl = $filter('filter')(ddLItems, function (d) { return d.DDLName === "BATCHCOURSES" })[0].Items;
	

				// Open Add new Update Batch Trainer

				var modalInstance = $uibModal.open({
					animation: true,
					scope: $scope,
					templateUrl: 'customUpdateBatchTrainer',
					backdrop: 'static',
					keyboard: false,
					modalFade: true,
					size: ''
				});

			});



		};

		// Open Window for updating BatchTrainer
		$scope.OpenBatchTrainerUpdateDialog = function (BatchTrainerId) {
			var tbfilter = batchAddEditService.BatchTrainerEmptyFilter();
			tbfilter.BatchTrainerId = BatchTrainerId;
			$scope.loading = true;
			MSG({}); //Init


			//Default Filter 
		    //Default Filter 
			var _defaultDDLFilter = {

			    PageName: "AddEditBatch",
			    FilterList: [
                     {
                         DDLName: "BATCHCOURSES",
                         Param1: $scope.BatchId,
                         Param2: "HIDE_DEFAULT_111"
                     },
                       {
                           DDLName: "COORDINATOR",
                           Param1: "",
                           Param2: "HIDE_DEFAULT"
                       },
                       {
                           DDLName: "ASSTCOORDINATOR",
                           Param1: "",
                           Param2: "HIDE_DEFAULT"
                       }


			    ]
			};


			batchAddEditService.GetDDLByFilter(_defaultDDLFilter).then(function (results) {
			    var ddLItems = angular.fromJson(results.data.DDLItems);
			    $scope.batchcoursesddl = $filter('filter')(ddLItems, function (d) { return d.DDLName === "BATCHCOURSES" })[0].Items;
			    $scope.coordDDL = $filter('filter')(ddLItems, function (d) { return d.DDLName === "COORDINATOR" })[0].Items;
			    $scope.asscoordDDL = $filter('filter')(ddLItems, function (d) { return d.DDLName === "ASSTCOORDINATOR" })[0].Items;


				batchAddEditService.getBatchTrainers(tbfilter).then(function (results) {
					if (results.data.length != 1) {
						$scope.loading = false;
						MSG({ 'elm': "BatchTrainer_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchtrainers!', 'MsgAsModel': error.data });
						return;
					};

					$scope.batchtrainer = results.data[0];
					FixDate($scope.batchtrainer, 'TrainingStartDate');

					$scope.batchtrainerActionTitle = "Update Batch Trainer";

					var modalInstance = $uibModal.open({
						animation: true,
						scope: $scope,
						templateUrl: 'customUpdateBatchTrainer',
						backdrop: 'static',
						keyboard: false,
						modalFade: true,
						size: ''
					});
					$scope.loading = false;
				}, function (error) {
					MSG({ 'elm': "BatchTrainer_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchtrainers!', 'MsgAsModel': error.data });
					$scope.loading = false;
				});
			 

			});


		

		};

		$scope.CourseOnSelected = function (item) {

			console.log(item);
			//Default Filter 
			var _defaultDDLFilter = {

				PageName: "AddEditBAtch",
				FilterList: [

                    {
                    	DDLName: "MODULES",
                    	Param1: item.Value * 1,
                    	Param2: "HIDE_DEFAULT_111"
                    }

				]
			}
			batchAddEditService.GetDDLByFilter(_defaultDDLFilter).then(function (results) {
				var ddLItems = angular.fromJson(results.data.DDLItems);
				$scope.batchmodulesddl = $filter('filter')(ddLItems, function (d) { return d.DDLName === "MODULES" })[0].Items;
			});
		};

		//Update BatchTrainer
		$scope.CreateUpdateBatchTrainer = function (BatchTrainerId) {
			$scope.batchtrainer.BatchId = $scope.BatchId;

			if (BatchTrainerId == 0) { CreateNewBatchTrainer($scope.batchtrainer); } else { UpdateBatchTrainer($scope.batchtrainer); }
		};

		//Delete BatchTrainer
		$scope.DeleteBatchTrainer = function (BatchTrainerId) {
			MSG({}); //Init
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete BatchTrainer',
				headerText: 'Delete Item',
				bodyText: 'Are you sure you want to delete this?'
			};
			modalService.showModal({}, modalOptions).then(function (result) {
				$scope.loading = true;
				batchAddEditService.deleteBatchTrainer(BatchTrainerId).then(function (results) {
					angular.forEach($scope.batchtrainers, function (value, key) {
						if ($scope.batchtrainers[key].BatchTrainerId === BatchTrainerId) {
							$scope.batchtrainers.splice(key, 1);
							return false;
						}
					});

					$scope.loading = false;
					MSG({ 'elm': "BatchTrainer_alert", "MsgType": "OK", "MsgText": "BatchTrainer deleted successfully." });
				}, function (error) {
					MSG({ 'elm': "BatchTrainer_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting batchtrainers!', 'MsgAsModel': error.data });
					$scope.loading = false;
				});
			});
		};
		 

		// Functions 
		// Function to Get BatchTrainer
		function GetBatchTrainers(tbfilter) {
			$scope.loading = true;
			$scope.HasTB_Records = false;
			batchAddEditService.getBatchTrainers(tbfilter).then(function (results) {
				$scope.batchtrainers = results.data;
				var tmp_page_start = (($scope.tbfilter.PageNumber - 1) * ($scope.tbfilter.PageSize) + 1), tmp_page_end = ($scope.tbfilter.PageNumber) * ($scope.tbfilter.PageSize);
				if (results.data.length > 0) {
					$scope.BatchTrainerPageInfo = {
						Has_record: true,
						TotalItems: results.data[0]["TotalCount"],
						PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0,
						PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"]
					};
				} else { $scope.BatchTrainerPageInfo = {}; }
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "BatchTrainer_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchtrainers!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		// Create New BatchTrainer Function 
		function CreateNewBatchTrainer(batchtrainer) {
			$scope.loading = true;
			batchAddEditService.createBatchTrainer(batchtrainer).then(function (results) {
				FixDate(results.data, 'TrainingStartDate');

				$scope.batchtrainers.push(results.data);
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "BatchTrainer_alert", "MsgType": "OK", "MsgText": "BatchTrainer added successfully." });
			}, function (error) {
				MSG({ 'elm': "BatchTrainer_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding batchtrainer!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		}

		//Update BatchTrainer Function 
		function UpdateBatchTrainer(batchtrainer) {
			$scope.loading = true;
			batchAddEditService.updateBatchTrainer(batchtrainer).then(function (results) {
				angular.forEach($scope.batchtrainers, function (value, key) {
					if ($scope.batchtrainers[key].BatchTrainerId === batchtrainer.BatchTrainerId) {
						$scope.batchtrainers[key] = batchtrainer;
						return false;
					}
				});
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "BatchTrainer_alert", "MsgType": "OK", "MsgText": "BatchTrainer updated successfully." });
			}, function (error) {
				MSG({ 'elm': "BatchTrainer_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating batchtrainer!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		//Datepicker
		$scope.dateOptions = {
			'year-format': "'yy'",
			'show-weeks': false
		};

		// Call BatchTrainer for first time
		$scope.BatchTrainerPageInfo = {};
		$scope.tbfilter = batchAddEditService.BatchTrainerEmptyFilter();
		$scope.tbfilter.PageNumber = 1;
		$scope.tbfilter.PageSize = '20';

		


		// ./BATCH TRAINER


		// REGION BATCH TRAINEE

		// Variables and declarations 

		$scope.batchtrainees = [];
		$scope.batchtrainee = {};
		$scope._traineesearch = "";
		 

		// Methods

		// Get BatchTrainee by Filter

		  

		// Open Window for Saving new BatchTrainee
		$scope.OpenBatchTraineeSaveDialog = function () {
			$scope.batchtrainee = { BatchTraineeId: 0 };
			MSG({}); //Init
			$scope.batchtraineeActionTitle = "Add New Batch Trainee";
			var modalInstance = $uibModal.open({
				animation: true,
				scope: $scope,
				templateUrl: 'customUpdateBatchTrainee',
				backdrop: 'static',
				keyboard: false,
				modalFade: true,
				size: ''
			});

		};

		// Open Window for updating BatchTrainee
		$scope.OpenBatchTraineeUpdateDialog = function (BatchTraineeId) {
			var batchtraineefilter = batchAddEditService.BatchTraineeEmptyFilter();
			batchtraineefilter.BatchTraineeId = BatchTraineeId;
			$scope.loading = true;
			MSG({}); //Init

			batchAddEditService.getBatchTrainees(batchtraineefilter).then(function (results) {
				if (results.data.length != 1) {
					$scope.loading = false;
					MSG({ 'elm': "BatchTrainee_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchtrainees!', 'MsgAsModel': error.data });
					return;
				}
				$scope.batchtrainee = results.data[0];
				$scope.batchtraineeActionTitle = "Update Batch Trainee";

				var modalInstance = $uibModal.open({
					animation: true,
					scope: $scope,
					templateUrl: 'customUpdateBatchTrainee',
					backdrop: 'static',
					keyboard: false,
					modalFade: true,
					size: ''
				});
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "BatchTrainee_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchtrainees!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});

		};

		//Update BatchTrainee
		$scope.CreateUpdateBatchTrainee = function (BatchTraineeId) {
			$scope.batchtrainee.BatchId = $scope.BatchId;
			if (BatchTraineeId == 0) { CreateNewBatchTrainee($scope.batchtrainee); } else { UpdateBatchTrainee($scope.batchtrainee); }
		};

		//Delete BatchTrainee
		$scope.DeleteBatchTrainee = function (BatchTraineeId) {
			MSG({}); //Init
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete BatchTrainee',
				headerText: 'Delete Item',
				bodyText: 'Are you sure you want to delete this?'
			};
			modalService.showModal({}, modalOptions).then(function (result) {
				$scope.loading = true;
				batchAddEditService.deleteBatchTrainee(BatchTraineeId).then(function (results) {
					angular.forEach($scope.batchtrainees, function (value, key) {
						if ($scope.batchtrainees[key].BatchTraineeId === BatchTraineeId) {
							$scope.batchtrainees.splice(key, 1);
							return false;
						}
					});

					$scope.loading = false;
					MSG({ 'elm': "BatchTrainee_alert", "MsgType": "OK", "MsgText": "BatchTrainee deleted successfully." });
				}, function (error) {
					MSG({ 'elm': "BatchTrainee_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting batchtrainees!', 'MsgAsModel': error.data });
					$scope.loading = false;
				});
			});
		};
		 

		// Functions 
		// Function to Get BatchTrainee
		function GetBatchTrainees(batchtraineefilter) {
			$scope.loading = true;
			$scope.HasTB_Records = false;
			batchAddEditService.getBatchTrainees(batchtraineefilter).then(function (results) {
				$scope.batchtrainees = results.data;
				var tmp_page_start = (($scope.batchtraineefilter.PageNumber - 1) * ($scope.batchtraineefilter.PageSize) + 1), tmp_page_end = ($scope.batchtraineefilter.PageNumber) * ($scope.batchtraineefilter.PageSize);
				if (results.data.length > 0) {
					$scope.BatchTraineePageInfo = {
						Has_record: true,
						TotalItems: results.data[0]["TotalCount"],
						PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0,
						PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"]
					};
				} else { $scope.BatchTraineePageInfo = {}; }
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "BatchTrainee_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchtrainees!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		// Create New BatchTrainee Function 
		function CreateNewBatchTrainee(batchtrainee) {

			$scope.loading = true;
			batchAddEditService.createBatchTrainee(batchtrainee).then(function (results) {
				$scope.batchtrainees.push(results.data);
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "BatchTrainee_alert", "MsgType": "OK", "MsgText": "BatchTrainee added successfully." });
			}, function (error) {
				MSG({ 'elm': "BatchTrainee_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding batchtrainee!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		}

		//Update BatchTrainee Function 
		function UpdateBatchTrainee(batchtrainee) {
			$scope.loading = true;
			batchAddEditService.updateBatchTrainee(batchtrainee).then(function (results) {
				angular.forEach($scope.batchtrainees, function (value, key) {
					if ($scope.batchtrainees[key].BatchTraineeId === batchtrainee.BatchTraineeId) {
						$scope.batchtrainees[key] = batchtrainee;
						return false;
					}
				});
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "BatchTrainee_alert", "MsgType": "OK", "MsgText": "BatchTrainee updated successfully." });
			}, function (error) {
				MSG({ 'elm': "BatchTrainee_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating batchtrainee!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		//Search Batch Trainees Not Included in the list
		$scope.SearchForTrainees = function (_searchtext) {
			$scope.traineesearchresults = [];
			$scope.loading_traineesearch = true;

			var input = batchAddEditService.BatchCourseEmptyFilter();
			input.ShowAll = 1; 
			input.SearchText = _searchtext;
			

			batchAddEditService.GetBatchTraineesNotIncluded(input).then(function (results) {
				$scope.traineesearchresults = results.data; 
				$scope.loading_traineesearch = false;
			}, function (error) {
				MSG({ 'elm': "BatchTrainee_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading trainees!', 'MsgAsModel': error.data });
				$scope.loading_traineesearch = false;
			});
		};
		
		$scope.AddTraineeBatch = function (member) {
			 
			member.BatchId = $scope.BatchId;
			member.JoinedDate = $scope.batchtrainee.JoinedDate;

			$scope.loading_traineesearch = true;
			batchAddEditService.createBatchTrainee(member).then(function (results) {
				$scope.batchtrainees.push(results.data);
				$scope.loading_traineesearch = false;
				$scope.SearchForTrainees($scope._traineesearch);
				MSG({ 'elm': "BatchTrainee_AddEditAlert", "MsgType": "OK", "MsgText": "Batch Trainee added successfully." });
			}, function (error) {
				MSG({ 'elm': "BatchTrainee_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding batchtrainee!', 'MsgAsModel': error.data });
				$scope.loading_traineesearch = false;
			});
		};
		
		// Call BatchTrainee for first time
		$scope.BatchTraineePageInfo = {};
		$scope.batchtraineefilter = batchAddEditService.BatchTraineeEmptyFilter();
		$scope.batchtraineefilter.PageNumber = 1;
		$scope.batchtraineefilter.PageSize = '100';

		
		// ./BATCH TRAINEE

		// REGION BATCH COURSE

		// Variables and declarations 
		 
		$scope.batchcourses = [];
		$scope.batchcourse = {};
		$scope.BatchCoursePageInfo = {};
		
	 
		// Methods

		  
		// Open Window for Saving new BatchCourse
		$scope.OpenBatchCourseSaveDialog = function () {
			$scope.batchcourse = { BatchCourseId: 0 };
			MSG({}); //Init
			$scope.batchcourseActionTitle = "Add New Batch Course";
			var modalInstance = $uibModal.open({
				animation: true,
				scope: $scope,
				templateUrl: 'customUpdateBatchCourse',
				backdrop: 'static',
				keyboard: false,
				modalFade: true,
				size: ''
			});

		};

		// Open Window for updating BatchCourse
		$scope.OpenBatchCourseUpdateDialog = function (BatchCourseId) {
			var batchcoursefilter = batchAddEditService.BatchCourseEmptyFilter();
			batchcoursefilter.BatchCourseId = BatchCourseId;
			$scope.loading = true;
			MSG({}); //Init

			batchAddEditService.getBatchCourses(batchcoursefilter).then(function (results) {
				if (results.data.length != 1) {
					$scope.loading = false;
					MSG({ 'elm': "BatchCourse_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchcourses!', 'MsgAsModel': error.data });
					return;
				}
				$scope.batchcourse = results.data[0];
				$scope.batchcourse.CourseStartDate = new Date($scope.batchcourse.CourseStartDate + 'Z');
				$scope.batchcourseActionTitle = "Update BatchCourse";

				var modalInstance = $uibModal.open({
					animation: true,
					scope: $scope,
					templateUrl: 'customUpdateBatchCourse',
					backdrop: 'static',
					keyboard: false,
					modalFade: true,
					size: ''
				});
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "BatchCourse_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchcourses!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});

		};

		//Update BatchCourse
		$scope.CreateUpdateBatchCourse = function (BatchCourseId) {
			$scope.batchcourse.BatchId = $scope.BatchId;
			if (BatchCourseId == 0) { CreateNewBatchCourse($scope.batchcourse); } else { UpdateBatchCourse($scope.batchcourse); }
		};

		//Delete BatchCourse
		$scope.DeleteBatchCourse = function (BatchCourseId) {
			MSG({}); //Init
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete BatchCourse',
				headerText: 'Delete Item',
				bodyText: 'Are you sure you want to delete this?'
			};
			modalService.showModal({}, modalOptions).then(function (result) {
				$scope.loading = true;
				batchAddEditService.deleteBatchCourse(BatchCourseId).then(function (results) {
					angular.forEach($scope.batchcourses, function (value, key) {
						if ($scope.batchcourses[key].BatchCourseId === BatchCourseId) {
							$scope.batchcourses.splice(key, 1);
							return false;
						}
					});

					$scope.loading = false;
					MSG({ 'elm': "BatchCourse_alert", "MsgType": "OK", "MsgText": "BatchCourse deleted successfully." });
				}, function (error) {
					MSG({ 'elm': "BatchCourse_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting batchcourses!', 'MsgAsModel': error.data });
					$scope.loading = false;
				});
			});
		};

		// Cancel  Editing
		$scope.cancelEditing = function () {
			$uibModalStack.dismissAll();
		};

		// Functions 
		// Function to Get BatchCourse
		function GetBatchCourses(batchcoursefilter) {
			$scope.loading = true;
			$scope.HasTB_Records = false;
			batchAddEditService.getBatchCourses(batchcoursefilter).then(function (results) {
				$scope.batchcourses = results.data;
				var tmp_page_start = (($scope.batchcoursefilter.PageNumber - 1) * ($scope.batchcoursefilter.PageSize) + 1), tmp_page_end = ($scope.batchcoursefilter.PageNumber) * ($scope.batchcoursefilter.PageSize);
				if (results.data.length > 0) {
					$scope.BatchCoursePageInfo = {
						Has_record: true,
						TotalItems: results.data[0]["TotalCount"],
						PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0,
						PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"]
					};
				} else { $scope.BatchCoursePageInfo = {}; }
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "BatchCourse_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchcourses!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		// Create New BatchCourse Function 
		function CreateNewBatchCourse(batchcourse) {
			$scope.loading = true;
			batchAddEditService.createBatchCourse(batchcourse).then(function (results) {
				$scope.batchcourses.push(results.data);
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "BatchCourse_alert", "MsgType": "OK", "MsgText": "BatchCourse added successfully." });
			}, function (error) {
				MSG({ 'elm': "BatchCourse_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding batchcourse!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		}

		//Update BatchCourse Function 
		function UpdateBatchCourse(batchcourse) {
			$scope.loading = true;
			batchAddEditService.updateBatchCourse(batchcourse).then(function (results) {
				angular.forEach($scope.batchcourses, function (value, key) {
					if ($scope.batchcourses[key].BatchCourseId === batchcourse.BatchCourseId) {
						$scope.batchcourses[key] = results.data;
						return false;
					}
				});
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "BatchCourse_alert", "MsgType": "OK", "MsgText": "BatchCourse updated successfully." });
			}, function (error) {
				MSG({ 'elm': "BatchCourse_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating batchcourse!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		//Datepicker
		$scope.dateOptions = {
			'year-format': "'yy'",
			'show-weeks': false
		};

		// Call BatchCourse for first time
		$scope.BatchCoursePageInfo = {};
		$scope.batchcoursefilter = batchAddEditService.BatchCourseEmptyFilter();
		$scope.batchcoursefilter.PageNumber = 1;
		$scope.batchcoursefilter.PageSize = '20';

		// ./BATCH COURSE

		// REGION "BATCH SUMMARY"


		// Variables and declarations 
		 
		$scope.batchsummarys = [];
		$scope.batchsummary = {};
		$scope.BatchSummaryPageInfo = {};

	 
		// Methods

	 
		// Open Window for Saving new BatchSummary
		$scope.OpenBatchSummarySaveDialog = function () {
			$scope.batchsummary = { BatchSummaryId: 0 };
			MSG({}); //Init
			$scope.batchsummaryActionTitle = "Add New Batch Summary";
			var modalInstance = $uibModal.open({
				animation: true,
				scope: $scope,
				templateUrl: 'customUpdateBatchSummary',
				backdrop: 'static',
				keyboard: false,
				modalFade: true,
				size: ''
			});

		};

		// Open Window for updating BatchSummary
		$scope.OpenBatchSummaryUpdateDialog = function (BatchSummaryId) {
			var batchsummaryfilter = batchAddEditService.BatchSummaryEmptyFilter();
			batchsummaryfilter.BatchSummaryId = BatchSummaryId;
			$scope.loading = true;
			MSG({}); //Init

			batchAddEditService.getBatchSummarys(batchsummaryfilter).then(function (results) {
				if (results.data.length != 1) {
					$scope.loading = false;
					MSG({ 'elm': "BatchSummary_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchsummarys!', 'MsgAsModel': error.data });
					return;
				}
				$scope.batchsummary = results.data[0];
				$scope.batchsummaryActionTitle = "Update BatchSummary";

				var modalInstance = $uibModal.open({
					animation: true,
					scope: $scope,
					templateUrl: 'customUpdateBatchSummary',
					backdrop: 'static',
					keyboard: false,
					modalFade: true,
					size: ''
				});
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "BatchSummary_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchsummarys!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});

		};

		//Update BatchSummary
		$scope.CreateUpdateBatchSummary = function (BatchSummaryId) {
			$scope.batchsummary.BatchId = batchId;
			if (BatchSummaryId == 0) { CreateNewBatchSummary($scope.batchsummary); } else { UpdateBatchSummary($scope.batchsummary); }
		};

		//Delete BatchSummary
		$scope.DeleteBatchSummary = function (BatchSummaryId) {
			MSG({}); //Init
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete BatchSummary',
				headerText: 'Delete Item',
				bodyText: 'Are you sure you want to delete this?'
			};
			modalService.showModal({}, modalOptions).then(function (result) {
				$scope.loading = true;
				batchAddEditService.deleteBatchSummary(BatchSummaryId).then(function (results) {
					angular.forEach($scope.batchsummarys, function (value, key) {
						if ($scope.batchsummarys[key].BatchSummaryId === BatchSummaryId) {
							$scope.batchsummarys.splice(key, 1);
							return false;
						}
					});

					$scope.loading = false;
					MSG({ 'elm': "BatchSummary_alert", "MsgType": "OK", "MsgText": "BatchSummary deleted successfully." });
				}, function (error) {
					MSG({ 'elm': "BatchSummary_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting batchsummarys!', 'MsgAsModel': error.data });
					$scope.loading = false;
				});
			});
		};

		// Cancel  Editing
		$scope.cancelEditing = function () {
			$uibModalStack.dismissAll();
		};

		// Functions 
		// Function to Get BatchSummary
		function GetBatchSummarys(batchsummaryfilter) {
			$scope.loading = true;
			$scope.HasTB_Records = false;
			batchAddEditService.getBatchSummarys(batchsummaryfilter).then(function (results) {
				$scope.batchsummarys = results.data;
				var tmp_page_start = (($scope.batchsummaryfilter.PageNumber - 1) * ($scope.batchsummaryfilter.PageSize) + 1), tmp_page_end = ($scope.batchsummaryfilter.PageNumber) * ($scope.batchsummaryfilter.PageSize);
				if (results.data.length > 0) {
					$scope.BatchSummaryPageInfo = {
						Has_record: true,
						TotalItems: results.data[0]["TotalCount"],
						PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0,
						PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"]
					};
				} else { $scope.BatchSummaryPageInfo = {}; }
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "BatchSummary_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchsummarys!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		// Create New BatchSummary Function 
		function CreateNewBatchSummary(batchsummary) {
			$scope.loading = true;
			batchAddEditService.createBatchSummary(batchsummary).then(function (results) {
				$scope.batchsummarys.push(results.data);
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "BatchSummary_alert", "MsgType": "OK", "MsgText": "BatchSummary added successfully." });
			}, function (error) {
				MSG({ 'elm': "BatchSummary_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding batchsummary!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		}

		//Update BatchSummary Function 
		function UpdateBatchSummary(batchsummary) {
			$scope.loading = true;
			batchAddEditService.updateBatchSummary(batchsummary).then(function (results) {
				angular.forEach($scope.batchsummarys, function (value, key) {
					if ($scope.batchsummarys[key].BatchSummaryId === batchsummary.BatchSummaryId) {
						$scope.batchsummarys[key] = batchsummary;
						return false;
					}
				});
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "BatchSummary_alert", "MsgType": "OK", "MsgText": "BatchSummary updated successfully." });
			}, function (error) {
				MSG({ 'elm': "BatchSummary_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating batchsummary!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		//Datepicker
		$scope.dateOptions = {
			'year-format': "'yy'",
			'show-weeks': false
		};

		// Call BatchSummary for first time
		$scope.BatchSummaryPageInfo = {};
		$scope.batchsummaryfilter = batchAddEditService.BatchSummaryEmptyFilter();
		$scope.batchsummaryfilter.PageNumber = 1;
		$scope.batchsummaryfilter.PageSize = '20';

		// ./BATCH SUMMARY

		//Load Batch
		GetBatchById();


	}]);
}());


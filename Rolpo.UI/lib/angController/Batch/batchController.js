﻿//Created By: Prashant 
//Created On: 21/06/2018 
// Controller for Batch 
// Initialization for Batch 
 
rolpo_app.constant('ngAuthSettings', {
	apiServiceBaseUri: serviceBase,
	clientId: 'rolpo.com'
});


rolpo_app.requires.push('ui.select');
// Service For Batch 
; (function () {
	'use strict';
	rolpo_app.factory('batchService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

		var serviceBase = ngAuthSettings.apiServiceBaseUri;
		var batchServiceFactory = {};

		//Default Filter 
		var _defaultDDLFilter = function () {
			return {
				PageName: "AddEditBatch",
				FilterList: [
				]
			};
		};

		//Batch Empty Filter 
		var _batchEmptyFilter = function () {
			return {
				BatchId: 0,
				BatchCode: "",
				Coordinator: "",
				BatchStartDate: "",
				BatchEndDate: "",
				PageNumber: 1,
				PageSize: 20,
				ShowAll: 0
			};
		};

		// Get DDL List by Filter
		var _getDDLList = function (ddlFilter) {
			return $http({
				url: serviceBase + 'api/Home/LoadDDLs',
				method: "post",
				data: ddlFilter
			});
		};

		// Get Batchs by Filter
		var _getBatchs = function (tbfilter) {
			return $http({
				url: serviceBase + 'api/Batch/GetBatchsList',
				method: "post",
				data: tbfilter
			});
		};

		//Create New Batch
		var _createBatch = function (batch) {
			var request = $http({
				method: 'post',
				url: serviceBase + 'api/Batch/SaveBatch',
				data: batch
			});
			return request;
		};

		//Update Batch 
		var _updateBatch = function (batch) {
			var request = $http({
				method: "post",
				url: serviceBase + "api/Batch/UpdateBatch",
				data: batch
			});
			return request;
		};

		//Delete Batch
		var _deleteBatch = function (batchid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/Batch/DeleteBatch/" + batchid
			});
			return request;
		};

		batchServiceFactory.DDLDefaultFilter = _defaultDDLFilter;
		batchServiceFactory.GetDDLByFilter = _getDDLList;
		batchServiceFactory.getBatchs = _getBatchs;
		batchServiceFactory.createBatch = _createBatch;
		batchServiceFactory.updateBatch = _updateBatch;
		batchServiceFactory.deleteBatch = _deleteBatch;
		batchServiceFactory.BatchEmptyFilter = _batchEmptyFilter;

		return batchServiceFactory;
	}]);
}());


// Controller Starts Here.. 
; (function () {
	'use strict';
	rolpo_app.controller('batchController', ['$scope', '$rootScope', 'batchService', 'modalService', '$uibModal', '$uibModalStack', '$filter', function ($scope, $rootScope, batchService, modalService, $uibModal, $uibModalStack, $filter) {

		// Variables and declarations 

		$scope.loading = true;
		$scope.batchs = [];
		$scope.batch = {};
		$scope.BatchPageInfo = {};

		//Populate DDLs
		var ddlFilter = batchService.DDLDefaultFilter();
		batchService.GetDDLByFilter(ddlFilter).then(function (results) {
			$scope.ddLItems = angular.fromJson(results.data.DDLItems);

		});

		// Methods

		// Get Batch by Filter

		$scope.GetBatchByFilter = function () {
			GetBatchs($scope.tbfilter);
		};

		// Reset Batch Filter
		$scope.ResetBatchFilter = function () {
			var pageSize = $scope.tbfilter.PageSize;

			$scope.tbfilter = batchService.BatchEmptyFilter();
			$scope.tbfilter.PageSize = pageSize;

			GetBatchs($scope.tbfilter);
		};

		//On Batch Page Changed
		$scope.OnBatchPageChanged = function () {
			GetBatchs($scope.tbfilter);
		};

		//On Page Size Changed
		$scope.OnBatchPageSizeChanged = function () {
			GetBatchs($scope.tbfilter);
		};

		// Open Window for Saving new Batch
		$scope.OpenBatchSaveDialog = function () {

		    var _defaultDDLFilter = {

		        PageName: "AddEditBatch",
		        FilterList: [
               
                       {
                           DDLName: "COORDINATOR",
                           Param1: "",
                           Param2: "HIDE_DEFAULT"
                       },
                       {
                           DDLName: "ASSTCOORDINATOR",
                           Param1: "",
                           Param2: "HIDE_DEFAULT"
                       }


		        ]
		    };


		    batchService.GetDDLByFilter(_defaultDDLFilter).then(function (results) {
		        var ddLItems = angular.fromJson(results.data.DDLItems);		        
		        $scope.coordDDL = $filter('filter')(ddLItems, function (d) { return d.DDLName === "COORDINATOR" })[0].Items;
		        $scope.asscoordDDL = $filter('filter')(ddLItems, function (d) { return d.DDLName === "ASSTCOORDINATOR" })[0].Items;
		    })


			$scope.batch = { BatchId: 0 };
			MSG({}); //Init
			$scope.batchActionTitle = "Add New Batch";
			var modalInstance = $uibModal.open({
				animation: true,
				scope: $scope,
				templateUrl: 'customUpdateBatch',
				backdrop: 'static',
				keyboard: false,
				modalFade: true,
				size: ''
			});

		};

		// Open Window for updating Batch
		$scope.OpenBatchUpdateDialog = function (BatchId) {
		    var _defaultDDLFilter = {

		        PageName: "AddEditBatch",
		        FilterList: [

                       {
                           DDLName: "COORDINATOR",
                           Param1: "",
                           Param2: "HIDE_DEFAULT"
                       },
                       {
                           DDLName: "ASSTCOORDINATOR",
                           Param1: "",
                           Param2: "HIDE_DEFAULT"
                       }


		        ]
		    };


		    batchService.GetDDLByFilter(_defaultDDLFilter).then(function (results) {
		        var ddLItems = angular.fromJson(results.data.DDLItems);
		        $scope.coordDDL = $filter('filter')(ddLItems, function (d) { return d.DDLName === "COORDINATOR" })[0].Items;
		        $scope.asscoordDDL = $filter('filter')(ddLItems, function (d) { return d.DDLName === "ASSTCOORDINATOR" })[0].Items;
		    })
			var tbfilter = batchService.BatchEmptyFilter();
			tbfilter.BatchId = BatchId;
			$scope.loading = true;
			MSG({}); //Init

			batchService.getBatchs(tbfilter).then(function (results) {
				if (results.data.length != 1) {
					$scope.loading = false;
					MSG({ 'elm': "Batch_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchs!', 'MsgAsModel': error.data });
					return;
				}
				$scope.batch = results.data[0];
				$scope.batchActionTitle = "Update Batch";

				var modalInstance = $uibModal.open({
					animation: true,
					scope: $scope,
					templateUrl: 'customUpdateBatch',
					backdrop: 'static',
					keyboard: false,
					modalFade: true,
					size: ''
				});
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "Batch_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchs!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});

		};

		//Update Batch
		$scope.CreateUpdateBatch = function (BatchId) {
			if (BatchId == 0) { CreateNewBatch($scope.batch); } else { UpdateBatch($scope.batch); }
		};

		//Delete Batch
		$scope.DeleteBatch = function (BatchId) {
			MSG({}); //Init
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete Batch',
				headerText: 'Delete Item',
				bodyText: 'Are you sure you want to delete this?'
			};
			modalService.showModal({}, modalOptions).then(function (result) {
				$scope.loading = true;
				batchService.deleteBatch(BatchId).then(function (results) {
					angular.forEach($scope.batchs, function (value, key) {
						if ($scope.batchs[key].BatchId === BatchId) {
							$scope.batchs.splice(key, 1);
							return false;
						}
					});

					$scope.loading = false;
					MSG({ 'elm': "Batch_alert", "MsgType": "OK", "MsgText": "Batch deleted successfully." });
				}, function (error) {
					MSG({ 'elm': "Batch_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting batchs!', 'MsgAsModel': error.data });
					$scope.loading = false;
				});
			});
		};

		// Cancel  Editing
		$scope.cancelEditing = function () {
			$uibModalStack.dismissAll();
		};

		// Functions 
		// Function to Get Batch
		function GetBatchs(tbfilter) {
			$scope.loading = true;
			$scope.HasTB_Records = false;
			batchService.getBatchs(tbfilter).then(function (results) {
				$scope.batchs = results.data;
				var tmp_page_start = (($scope.tbfilter.PageNumber - 1) * ($scope.tbfilter.PageSize) + 1), tmp_page_end = ($scope.tbfilter.PageNumber) * ($scope.tbfilter.PageSize);
				if (results.data.length > 0) {
					$scope.BatchPageInfo = {
						Has_record: true,
						TotalItems: results.data[0]["TotalCount"],
						PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0,
						PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"]
					};
				} else { $scope.BatchPageInfo = {}; }
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "Batch_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchs!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		// Create New Batch Function 
		function CreateNewBatch(batch) {
			$scope.loading = true;
			batchService.createBatch(batch).then(function (results) {
				$scope.batchs.push(results.data);
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "Batch_alert", "MsgType": "OK", "MsgText": "Batch added successfully." });
			}, function (error) {
				MSG({ 'elm': "Batch_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding batch!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		}

		//Update Batch Function 
		function UpdateBatch(batch) {
			$scope.loading = true;
			batchService.updateBatch(batch).then(function (results) {
				angular.forEach($scope.batchs, function (value, key) {
					if ($scope.batchs[key].BatchId === batch.BatchId) {
						$scope.batchs[key] = batch;
						return false;
					}
				});
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "Batch_alert", "MsgType": "OK", "MsgText": "Batch updated successfully." });
			}, function (error) {
				MSG({ 'elm': "Batch_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating batch!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};
		 

		//Datepicker
		$scope.dateOptions = {
			'year-format': "'yy'",
			'show-weeks': false
		};

		$scope.OpenDate = function (obj, prop) {
			obj[prop] = true;
		}

		// Call Batch for first time
		$scope.BatchPageInfo = {};
		$scope.tbfilter = batchService.BatchEmptyFilter();
		$scope.tbfilter.PageNumber = 1;
		$scope.tbfilter.PageSize = '20';

		GetBatchs($scope.tbfilter);

	}]);
}());


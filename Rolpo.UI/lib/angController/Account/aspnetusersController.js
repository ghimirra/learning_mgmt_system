﻿//Created By: Prashant 
//Created On: 01/06/2016 
// Controller for ASPNETUsers 
// Initialization for ASPNETUsers 

rolpo_app.constant('ngAuthSettings', {
    apiServiceBaseUri: serviceBase,
    clientId: 'rolpo.com'
});

rolpo_app.filter('groupby', function () {
    return function (items, group) {
        return items.filter(function (element, index, array) {
            return element.RoleGroup == group;
        });
    }
})


rolpo_app.directive('icheck', ['$timeout', function ($timeout) {
    return {
        require: 'ngModel',
        link: function ($scope, element, $attrs, ngModel) {
            return $timeout(function () {
                var value = $attrs['value'];

                $scope.$watch($attrs['ngModel'], function (newValue) {
                    $(element).iCheck('update');
                })

                return $(element).iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                }).on('ifChanged', function (event) {
                    if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                        $scope.$apply(function () {
                            return ngModel.$setViewValue(event.target.checked);
                        });
                    }
                    if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                        return $scope.$apply(function () {
                            return ngModel.$setViewValue(value);
                        });
                    }
                });
            });
        }
    };
}]);

// Service For ASPNETUsers 
; (function () {
    'use strict';
    rolpo_app.factory('aspnetusersService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

        var serviceBase = ngAuthSettings.apiServiceBaseUri;
        var aspnetusersServiceFactory = {};

        //Default Filter 
        var _defaultDDLFilter = function () {
            return {
                PageName: "AddEditASPNETUsers",
                FilterList: [
                    //{
                    //    DDLName: "STAFFS",
                    //    Param1: "_SELF",
                    //    Param2: "0"
                    //}
                ]
            };
        };

        //ASPNETUsers Empty Filter 
        var _aspnetusersEmptyFilter = function () {
            return {
                Id: "",
                FirstName: "",
                LastName: "",
                Email: "",
				SearchText: "",
                PageNumber: 1,
                PageSize: '20',
                ShowAll: 0
            };
        };

        // Get DDL List by Filter
        var _getDDLList = function (ddlFilter) {
            return $http({
                url: serviceBase + 'api/Home/LoadDDLs',
                method: "post",
                data: ddlFilter
            });
        };

        // Get ASPNETUserss by Filter
        var _getASPNETUserss = function (tafilter) {
            return $http({
                url: serviceBase + 'api/Account/GetASPNETUserssList',
                method: "post",
                data: tafilter
            });
        };

        //Create New ASPNETUsers
        var _createASPNETUsers = function (aspnetusers) {
            var request = $http({
                method: 'post',
                url: serviceBase + 'api/Account/SaveASPNETUsers',
                data: aspnetusers
            });
            return request;
        };

        //Update ASPNETUsers 
        var _updateASPNETUsers = function (aspnetusers) {
            var request = $http({
                method: "post",
                url: serviceBase + "api/Account/UpdateASPNETUsers",
                data: aspnetusers
            });
            return request;
        };

        //Delete ASPNETUsers
        var _deleteASPNETUsers = function (id) {
            var request = $http({
                method: "delete",
                url: serviceBase + "api/Account/DeleteASPNETUsers/" + id
            });
            return request;
        };

    	//// Load Staff By Id
        //var _loadStaffById = function (staffid) {
        //	return $http({
        //		url: serviceBase + 'api/Staffs/GetStaffsById',
        //		method: "post",
        //		data: staffid,
        //		contentType: 'application/json'
        //	});
        //};


        aspnetusersServiceFactory.DDLDefaultFilter = _defaultDDLFilter;
        aspnetusersServiceFactory.GetDDLByFilter = _getDDLList;

        aspnetusersServiceFactory.getASPNETUserss = _getASPNETUserss;
        aspnetusersServiceFactory.createASPNETUsers = _createASPNETUsers;
        aspnetusersServiceFactory.updateASPNETUsers = _updateASPNETUsers;
        aspnetusersServiceFactory.deleteASPNETUsers = _deleteASPNETUsers;
        aspnetusersServiceFactory.ASPNETUsersEmptyFilter = _aspnetusersEmptyFilter;

        //aspnetusersServiceFactory.LoadStaffById = _loadStaffById;
        
        return aspnetusersServiceFactory;
    }]);
}());


// Controller Starts Here.. 
; (function () {
    'use strict';
    rolpo_app.controller('aspnetusersController', ['$scope', '$rootScope', 'aspnetusersService', 'modalService', '$uibModal', '$uibModalStack', '$filter', function ($scope, $rootScope, aspnetusersService, modalService, $uibModal, $uibModalStack, $filter) {

        // Variables and declarations 

        $scope.loading = true;
        $scope.aspnetuserss = [];
        $scope.aspnetusers = {};
        $scope.ASPNETUsersPageInfo = {};
        $scope.roles = [];
        $scope.rolesFromServer = [];
        //$scope.staffs = [];


        $scope.getRoleGroups = function () {
        	var groupArray = [];
        	angular.forEach($scope.roles, function (item, idx) {
                if (groupArray.indexOf(item.RoleGroup) == -1)
                    groupArray.push(item.RoleGroup)
            });
            return groupArray;
            //return groupArray.sort();
        };

        //Populate DDLs
        var ddlFilter = aspnetusersService.DDLDefaultFilter();
        aspnetusersService.GetDDLByFilter(ddlFilter).then(function (results) {
            $scope.ddLItems = GETJ(results.data.DDLItems);

            //Get staffs
            //$scope.staffs = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "STAFFS" })[0].Items;
            
        });

        // Methods

        // Get ASPNETUsers by Filter

        $scope.GetASPNETUsersByFilter = function () {
            GetASPNETUserss($scope.tafilter);
        };

        // Reset Filter
        $scope.ResetAspNetUserFilter = function () {
            var pageSize = $scope.tafilter.PageSize;

            $scope.tafilter = aspnetusersService.ASPNETUsersEmptyFilter();
            $scope.tafilter.PageSize = pageSize;
            GetASPNETUserss($scope.tafilter);
        };

        //On Page Changed
        $scope.OnPageChanged = function () {
            GetASPNETUserss($scope.tafilter);
        };

        //On Page Size Changed
        $scope.OnPageSizeChanged = function () {
            GetASPNETUserss($scope.tafilter);
        };

        // Open Window for Saving new ASPNETUsers
        $scope.OpenASPNETUsersSaveDialog = function () {
            $scope.aspnetusers = { Id: 0 };
            $scope.roles = $scope.rolesFromServer;

            MSG({}); //Init
            $scope.aspnetusersActionTitle = "Add New Member";
            var modalInstance = $uibModal.open({
                animation: true,
                scope: $scope,
                templateUrl: 'customUpdateASPNETUsers',
                backdrop: 'static',
                keyboard: false,
                modalFade: true,
                size: ''
            });

        };

        // Open Window for updating ASPNETUsers
        $scope.OpenASPNETUsersUpdateDialog = function (Id) {
            var tafilter = aspnetusersService.ASPNETUsersEmptyFilter();
            tafilter.Id = Id;
            $scope.loading = true;
            MSG({}); //Init

            aspnetusersService.getASPNETUserss(tafilter).then(function (results) {
            	
            	if (results.data.m_Item1.length != 1) {
                    $scope.loading = false;
                    $scope.error = "An Error has occured while loading member!";
                    return;
                } 
            	$scope.aspnetusers = results.data.m_Item1[0];
            	$scope.roles = GETJ(results.data.m_Item1[0].RolesJSON);
                $scope.aspnetusersActionTitle = "Update User";

                var modalInstance = $uibModal.open({
                    animation: true,
                    scope: $scope,
                    templateUrl: 'customUpdateASPNETUsers',
                    backdrop: 'static',
                    keyboard: false,
                    modalFade: true,
                    size: ''
                });
                $scope.loading = false;
            }, function (error) {
                $scope.error = "An Error has occured while loading member!";
                $scope.loading = false;
            });

        };

        //Update ASPNETUsers
        $scope.CreateUpdateASPNETUsers = function (frm,Id) {
        	$scope.invalid = frm.$invalid;
        	if (frm.$invalid) { return; }
            $scope.aspnetusers.RolesJSON = angular.toJson($scope.roles);
            if (Id == 0) { CreateNewASPNETUsers($scope.aspnetusers); } else { UpdateASPNETUsers($scope.aspnetusers); }
        };

        //Delete ASPNETUsers
        $scope.DeleteASPNETUsers = function (Id) {
            MSG({}); //Init
            var modalOptions = {
                closeButtonText: 'Cancel',
                actionButtonText: 'Delete Member',
                headerText: 'Delete Member',
                bodyText: 'Are you sure you want to delete this?'
            };
            modalService.showModal({}, modalOptions).then(function (result) {
                $scope.loading = true;
                aspnetusersService.deleteASPNETUsers(Id).then(function (results) {
                    angular.forEach($scope.aspnetuserss, function (value, key) {
                        if ($scope.aspnetuserss[key].Id === Id) {
                            $scope.aspnetuserss.splice(key, 1);
                            return false;
                        }
                    });

                    $scope.loading = false;
                    MSG({ "MsgType": "OK", "MsgText": "Member deleted successfully." });
                }, function (error) {
                    $scope.error = "An Error has occured while deleting member! " + error;
                    $scope.loading = false;
                });
            });
        };

    	////Staff On Selected
        //$scope.StaffOnSelected = function (staff) {
        //	$scope.frmloading = true;
        //	aspnetusersService.LoadStaffById(staff.Value).then(function (results) {
        //		$scope.frmloading = false;

        //		//Load Staff Values to aspnet users
        //		var full_name_split = results.data.StaffName.split(" ");

        //		$scope.aspnetusers.FirstName = full_name_split[0];
        //		$scope.aspnetusers.LastName = full_name_split.length > 1 ? full_name_split[full_name_split.length - 1] : null;
        //		$scope.aspnetusers.Email = results.data.PrimaryContactEmail;

        //	}, function (error) {
        //		$scope.error = "An Error has occured while loading staff!";
        //		$scope.frmloading = false;
        //	});

		//	console.log("Selected Staff",staff);
        //};

        // Cancel  Editing
        $scope.cancelEditing = function () {
            $uibModalStack.dismissAll();
        };

    	// Functions 
    	// Function to Get ASPNETUsers
        function GetASPNETUserss(tafilter) {
        	$scope.loading = true;
        	$scope.HasTA_Records = false;
        	aspnetusersService.getASPNETUserss(tafilter).then(function (results) {
        		$scope.rolesFromServer = results.data.m_Item2;
        		$scope.aspnetuserss = results.data.m_Item1;
        		angular.forEach($scope.aspnetuserss, function (au, key) {
        			au.RolesJSON = GETJ(au.RolesJSON);
        		});

        		var tmp_page_start = (($scope.tafilter.PageNumber - 1) * ($scope.tafilter.PageSize) + 1), tmp_page_end = ($scope.tafilter.PageNumber) * ($scope.tafilter.PageSize);
        		if (results.data.m_Item1.length > 0) {
        			$scope.ASPNETUsersPageInfo = {
        				Has_record: true,
        				TotalItems: results.data.m_Item1[0]["TotalCount"],
        				PageStart: (results.data.m_Item1[0]["TotalCount"] > 0) ? tmp_page_start : 0,
        				PageEnd: tmp_page_end < results.data.m_Item1[0]["TotalCount"] ? tmp_page_end : results.data.m_Item1[0]["TotalCount"]
        			};

        		} else { $scope.StaffPageInfo = {}; }
        		$scope.loading = false;
        	}, function (error) {
        		$scope.error = "An Error has occured while loading user!";
        		$scope.loading = false;
        	});
        };

        // Create New ASPNETUsers Function 
        function CreateNewASPNETUsers(aspnetusers) {
            $scope.useraddupdateloading = true;
            aspnetusersService.createASPNETUsers(aspnetusers).then(function (results) {
                var newRecord = results.data;
                newRecord.RolesJSON = GETJ(newRecord.RolesJSON);
                $scope.aspnetuserss.push(newRecord);
                $scope.useraddupdateloading = false;
                $uibModalStack.dismissAll();
                MSG({ 'elm': 'ASPNETUsers_alert', "MsgType": "OK", "MsgText": "User added successfully." });
            }, function (error) {
                MSG({ 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding user!', 'MsgAsModel': error.data });
                $scope.useraddupdateloading = false;
            });
        }

        //Update ASPNETUsers Function 
        function UpdateASPNETUsers(aspnetusers) {
        	$scope.useraddupdateloading = true;
            aspnetusersService.updateASPNETUsers(aspnetusers).then(function (results) {
                angular.forEach($scope.aspnetuserss, function (value, key) {
                    if ($scope.aspnetuserss[key].Id === aspnetusers.Id) {
                        $scope.aspnetuserss[key] = results.data;
                        $scope.aspnetuserss[key].RolesJSON = GETJ(results.data.RolesJSON);
                        return false;
                    }
                });
                $scope.useraddupdateloading = false;
                $uibModalStack.dismissAll();
                MSG({ 'elm': 'ASPNETUsers_alert', "MsgType": "OK", "MsgText": "User updated successfully." });
            }, function (error) {
                MSG({ 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating user!', 'MsgAsModel': error.data });
                $scope.useraddupdateloading = false;
            });
        };

        //Datepicker
        $scope.dateOptions = {
            'year-format': "'yy'",
            'show-weeks': false
        };

        // Call ASPNETUsers for first time
        $scope.tafilter = aspnetusersService.ASPNETUsersEmptyFilter();
        $scope.tafilter.PageNumber = 1;
        $scope.tafilter.PageSize = '30';

        GetASPNETUserss($scope.tafilter);

    }]);
}());


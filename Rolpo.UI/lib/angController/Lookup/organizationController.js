﻿

rolpo_app.requires.push('thatisuday.dropzone');

; (function () {
    'use strict';
    rolpo_app.service('organizationService', ['$http', function ($http) {

        
        this.GetOrganizationInfo = function () {
            
            var request = $http({
                method: 'GET',
                url: serviceBase + '/api/Setting/GetOrganizationInfo'
                
            });

            return request;
           
        }

        this.UpdateOrganizationInfo = function (organization) {
            return $http({
                method: 'POST',
                url: serviceBase + '/api/Setting/UpdateOrganizationInfo',
                data:organization
            })
        }

    

       this.RemoveFile = function (file) {
            var request = $http({
                method: 'POST',
                url: baseUrl + 'Members/RemoveFile?filePath=' + file
            });
            return request;
        }

    }]);
}());


// Controller Starts Here.. 
; (function () {
    'use strict';
    rolpo_app.controller('organizationController', ['$scope', '$rootScope', 'organizationService', 'modalService', '$uibModal', '$uibModalStack', '$filter', '$timeout', function ($scope, $rootScope, organizationService, modalService, $uibModal, $uibModalStack, $filter, $timeout) {

        //region images
        //save file
        $scope.myDz = null;
        $scope.dzMethods = {};
        $scope.dzOptions = {
            url: serviceBase + '/Home/SaveLogo',
            maxFilesize: '10',
            acceptedFiles: 'image/jpeg, images/jpg, image/png',
            addRemoveLinks: true,
            maxFiles: 1

        };
        $scope.dzCallbacks = {
            'addedfile': function (file) { 
                if (file.isMock) {

                    $scope.myDz.createThumbnailFromUrl(file, file.serverImgUrl, null, true);

                }

            },
            'success': function (file, xhr) {

                if ($scope.myDz != null) {
                    if ($scope.myDz.files[1] != null) {
                        $scope.myDz.removeFile($scope.myDz.files[0]);
                    }

                }
                $scope.organization.OrganizationLogoUrl = baseUrl + '/' + xhr.ImagePath.substr(1);
            },
            'removedfile': function (file) {
            	if (confirm('are you sure you want to delete this?')) {
            		$scope.RemoveFile(file, true); 
            	}
            	return;
            	
            }
       , 'maxfilesexceeded': function (file) {
           file.previewElement.innerHTML = "";

           alert("You cannot upload any more files. Please remove old one.")



       }
       , 'error': function (file) {

           var errorDisplay = document.querySelectorAll('[data-dz-errormessage]');
           errorDisplay[errorDisplay.length - 1].innerHTML = 'Error occured while uploading image. The maximum file limit is one.';
       }
        };

        $scope.RemoveFile = function (file) {
        	$scope.organization.OrganizationLogoUrl = "";
            RemoveFile($scope.file);
        }

        function RemoveFile(file) {
        	console.log(file);
            organizationService.RemoveFile(file).then(function (results) {
                if (results.data == "ok") {
                    MSG({ "elm": "FileUpload_alert", 'MsgType': 'SUCCESS', 'MsgText': 'File updated successfully.' });
                }
                else {
                	MSG({ "elm": "FileUpload_alert", 'MsgType': 'ERROR', 'MsgText': 'Error occured while removing file.' });
                }


            })
        }

        //end region files


        $scope.organization = {};
        $scope.submitted=false;

        $scope.LoadOrganizationInfo = function () {
            GetOrganizationInfo();
        }

        function GetOrganizationInfo() {            
            organizationService.GetOrganizationInfo().then(function (results) {                
                $scope.organization = results.data;
                $timeout(function () {
                    if ($scope.organization.OrganizationLogoUrl.length > 0) {
                        $scope.mockFiles = [];

                        $scope.myDz = $scope.dzMethods.getDropzone();
                        $scope.myDz.files = [];
                        $scope.mockFiles.push
                        ({
                            name: "Image",
                            serverImgUrl: $scope.organization.OrganizationLogoUrl,
                            isMock: true

                        });

                        $scope.mockFiles.forEach(function (mockFile) {
                            $scope.myDz.emit('addedfile', mockFile);
                            $scope.myDz.emit('complete', mockFile);
                            $scope.myDz.options.maxFiles = 1;
                            $scope.myDz.files.push(mockFile);


                        });
                    }

                });
            }, function (error) {
                $scope.organization = {};
            })
        }

        $scope.getImageFile = function (el) {
            console.log(el.files);
        }

        $scope.SaveOrganizationDetails = function (form) {
          
            $scope.submitted=true;
            if (!form.$valid) {
                return;
            }
            else
            {
                $scope.loading = true;
                organizationService.UpdateOrganizationInfo($scope.organization).then(function (results) {
                    $scope.organization = results.data;
                    $scope.loading = false;
                    MSG({ "elm": "Org_Alert", 'MsgType': 'SUCCESS', 'MsgText': 'Organization details updated successfully.' });
                }, function (error) {
                    $scope.loading = false;
                    MSG({ "elm": "Org_Alert", 'MsgType': 'ERROR', 'MsgText': 'Error occured while updating organization details.' });
                })
            }
        }

        $scope.LoadOrganizationInfo();

    }]);
}());


﻿//Created By: Prashant 
//Created On: 27/06/2018 
// Controller for CoursePeriodType 
// Initialization for CoursePeriodType 
 

rolpo_app.constant('ngAuthSettings', {
	apiServiceBaseUri: serviceBase,
	clientId: 'rolpo.com'
});

// Service For CoursePeriodType 
; (function () {
	'use strict';
	rolpo_app.factory('courseperiodtypeService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

		var serviceBase = ngAuthSettings.apiServiceBaseUri;
		var courseperiodtypeServiceFactory = {};

		//Default Filter 
		var _defaultDDLFilter = function () {
			return {
				PageName: "AddEditCoursePeriodType",
				FilterList: [
				]
			};
		};

		//CoursePeriodType Empty Filter 
		var _courseperiodtypeEmptyFilter = function () {
			return {
				CoursePeriodTypeId: 0,
				PeriodTypeName: "",
				PageNumber: 1,
				PageSize: 20,
				ShowAll: 0
			};
		};

		// Get DDL List by Filter
		var _getDDLList = function (ddlFilter) {
			return $http({
				url: serviceBase + 'api/Home/LoadDDLs',
				method: "post",
				data: ddlFilter
			});
		};

		// Get CoursePeriodTypes by Filter
		var _getCoursePeriodTypes = function (tcfilter) {
			return $http({
				url: serviceBase + 'api/CoursePeriodType/GetCoursePeriodTypesList',
				method: "post",
				data: tcfilter
			});
		};

		//Create New CoursePeriodType
		var _createCoursePeriodType = function (courseperiodtype) {
			var request = $http({
				method: 'post',
				url: serviceBase + 'api/CoursePeriodType/SaveCoursePeriodType',
				data: courseperiodtype
			});
			return request;
		};

		//Update CoursePeriodType 
		var _updateCoursePeriodType = function (courseperiodtype) {
			var request = $http({
				method: "post",
				url: serviceBase + "api/CoursePeriodType/UpdateCoursePeriodType",
				data: courseperiodtype
			});
			return request;
		};

		//Delete CoursePeriodType
		var _deleteCoursePeriodType = function (courseperiodtypeid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/CoursePeriodType/DeleteCoursePeriodType/" + courseperiodtypeid
			});
			return request;
		};

		courseperiodtypeServiceFactory.DDLDefaultFilter = _defaultDDLFilter;
		courseperiodtypeServiceFactory.GetDDLByFilter = _getDDLList;
		courseperiodtypeServiceFactory.getCoursePeriodTypes = _getCoursePeriodTypes;
		courseperiodtypeServiceFactory.createCoursePeriodType = _createCoursePeriodType;
		courseperiodtypeServiceFactory.updateCoursePeriodType = _updateCoursePeriodType;
		courseperiodtypeServiceFactory.deleteCoursePeriodType = _deleteCoursePeriodType;
		courseperiodtypeServiceFactory.CoursePeriodTypeEmptyFilter = _courseperiodtypeEmptyFilter;

		return courseperiodtypeServiceFactory;
	}]);
}());


// Controller Starts Here.. 
; (function () {
	'use strict';
	rolpo_app.controller('courseperiodtypeController', ['$scope', '$rootScope', 'courseperiodtypeService', 'modalService', '$uibModal', '$uibModalStack', '$filter', function ($scope, $rootScope, courseperiodtypeService, modalService, $uibModal, $uibModalStack, $filter) {

		// Variables and declarations 

		$scope.loading = true;
		$scope.courseperiodtypes = [];
		$scope.courseperiodtype = {};
		$scope.CoursePeriodTypePageInfo = {};

		//Populate DDLs
		var ddlFilter = courseperiodtypeService.DDLDefaultFilter();
		courseperiodtypeService.GetDDLByFilter(ddlFilter).then(function (results) {
			$scope.ddLItems = angular.fromJson(results.data.DDLItems);

		});

		// Methods

		// Get CoursePeriodType by Filter

		$scope.GetCoursePeriodTypeByFilter = function () {
			GetCoursePeriodTypes($scope.tcfilter);
		};

		// Reset CoursePeriodType Filter
		$scope.ResetCoursePeriodTypeFilter = function () {
			var pageSize = $scope.tcfilter.PageSize;

			$scope.tcfilter = courseperiodtypeService.CoursePeriodTypeEmptyFilter();
			$scope.tcfilter.PageSize = pageSize;

			GetCoursePeriodTypes($scope.tcfilter);
		};

		//On CoursePeriodType Page Changed
		$scope.OnCoursePeriodTypePageChanged = function () {
			GetCoursePeriodTypes($scope.tcfilter);
		};

		//On Page Size Changed
		$scope.OnCoursePeriodTypePageSizeChanged = function () {
			GetCoursePeriodTypes($scope.tcfilter);
		};

		// Open Window for Saving new CoursePeriodType
		$scope.OpenCoursePeriodTypeSaveDialog = function () {
			$scope.courseperiodtype = { CoursePeriodTypeId: 0 };
			MSG({}); //Init
			$scope.courseperiodtypeActionTitle = "Add New CoursePeriodType";
			var modalInstance = $uibModal.open({
				animation: true,
				scope: $scope,
				templateUrl: 'customUpdateCoursePeriodType',
				backdrop: 'static',
				keyboard: false,
				modalFade: true,
				size: ''
			});

		};

		// Open Window for updating CoursePeriodType
		$scope.OpenCoursePeriodTypeUpdateDialog = function (CoursePeriodTypeId) {
			var tcfilter = courseperiodtypeService.CoursePeriodTypeEmptyFilter();
			tcfilter.CoursePeriodTypeId = CoursePeriodTypeId;
			$scope.loading = true;
			MSG({}); //Init

			courseperiodtypeService.getCoursePeriodTypes(tcfilter).then(function (results) {
				if (results.data.length != 1) {
					$scope.loading = false;
					MSG({ 'elm': "CoursePeriodType_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading courseperiodtypes!', 'MsgAsModel': error.data });
					return;
				}
				$scope.courseperiodtype = results.data[0];
				$scope.courseperiodtypeActionTitle = "Update CoursePeriodType";

				var modalInstance = $uibModal.open({
					animation: true,
					scope: $scope,
					templateUrl: 'customUpdateCoursePeriodType',
					backdrop: 'static',
					keyboard: false,
					modalFade: true,
					size: ''
				});
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "CoursePeriodType_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading courseperiodtypes!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});

		};

		//Update CoursePeriodType
		$scope.CreateUpdateCoursePeriodType = function (CoursePeriodTypeId) {
			if (CoursePeriodTypeId == 0) { CreateNewCoursePeriodType($scope.courseperiodtype); } else { UpdateCoursePeriodType($scope.courseperiodtype); }
		};

		//Delete CoursePeriodType
		$scope.DeleteCoursePeriodType = function (CoursePeriodTypeId) {
			MSG({}); //Init
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete CoursePeriodType',
				headerText: 'Delete Item',
				bodyText: 'Are you sure you want to delete this?'
			};
			modalService.showModal({}, modalOptions).then(function (result) {
				$scope.loading = true;
				courseperiodtypeService.deleteCoursePeriodType(CoursePeriodTypeId).then(function (results) {
					angular.forEach($scope.courseperiodtypes, function (value, key) {
						if ($scope.courseperiodtypes[key].CoursePeriodTypeId === CoursePeriodTypeId) {
							$scope.courseperiodtypes.splice(key, 1);
							return false;
						}
					});

					$scope.loading = false;
					MSG({ 'elm': "CoursePeriodType_alert", "MsgType": "OK", "MsgText": "CoursePeriodType deleted successfully." });
				}, function (error) {
					MSG({ 'elm': "CoursePeriodType_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting courseperiodtypes!', 'MsgAsModel': error.data });
					$scope.loading = false;
				});
			});
		};

		// Cancel  Editing
		$scope.cancelEditing = function () {
			$uibModalStack.dismissAll();
		};

		// Functions 
		// Function to Get CoursePeriodType
		function GetCoursePeriodTypes(tcfilter) {
			$scope.loading = true;
			$scope.HasTC_Records = false;
			courseperiodtypeService.getCoursePeriodTypes(tcfilter).then(function (results) {
				$scope.courseperiodtypes = results.data;
				var tmp_page_start = (($scope.tcfilter.PageNumber - 1) * ($scope.tcfilter.PageSize) + 1), tmp_page_end = ($scope.tcfilter.PageNumber) * ($scope.tcfilter.PageSize);
				if (results.data.length > 0) {
					$scope.CoursePeriodTypePageInfo = {
						Has_record: true,
						TotalItems: results.data[0]["TotalCount"],
						PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0,
						PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"]
					};
				} else { $scope.CoursePeriodTypePageInfo = {}; }
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "CoursePeriodType_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading courseperiodtypes!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		// Create New CoursePeriodType Function 
		function CreateNewCoursePeriodType(courseperiodtype) {
			$scope.loading = true;
			courseperiodtypeService.createCoursePeriodType(courseperiodtype).then(function (results) {
				$scope.courseperiodtypes.push(results.data);
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "CoursePeriodType_alert", "MsgType": "OK", "MsgText": "CoursePeriodType added successfully." });
			}, function (error) {
				MSG({ 'elm': "CoursePeriodType_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding courseperiodtype!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		}

		//Update CoursePeriodType Function 
		function UpdateCoursePeriodType(courseperiodtype) {
			$scope.loading = true;
			courseperiodtypeService.updateCoursePeriodType(courseperiodtype).then(function (results) {
				angular.forEach($scope.courseperiodtypes, function (value, key) {
					if ($scope.courseperiodtypes[key].CoursePeriodTypeId === courseperiodtype.CoursePeriodTypeId) {
						$scope.courseperiodtypes[key] = courseperiodtype;
						return false;
					}
				});
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "CoursePeriodType_alert", "MsgType": "OK", "MsgText": "CoursePeriodType updated successfully." });
			}, function (error) {
				MSG({ 'elm': "CoursePeriodType_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating courseperiodtype!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		//Datepicker
		$scope.dateOptions = {
			'year-format': "'yy'",
			'show-weeks': false
		};

		// Call CoursePeriodType for first time
		$scope.CoursePeriodTypePageInfo = {};
		$scope.tcfilter = courseperiodtypeService.CoursePeriodTypeEmptyFilter();
		$scope.tcfilter.PageNumber = 1;
		$scope.tcfilter.PageSize = '20';

		GetCoursePeriodTypes($scope.tcfilter);

	}]);
}());


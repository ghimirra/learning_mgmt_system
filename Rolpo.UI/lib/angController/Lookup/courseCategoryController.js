﻿//Created By: Prashant 
//Created On: 26/06/2018 
// Controller for CourseCategory 
// Initialization for CourseCategory 
 
rolpo_app.constant('ngAuthSettings', {
	apiServiceBaseUri: serviceBase,
	clientId: 'rolpo.com'
});

// Service For CourseCategory 
; (function () {
	'use strict';
	rolpo_app.factory('coursecategoryService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

		var serviceBase = ngAuthSettings.apiServiceBaseUri;
		var coursecategoryServiceFactory = {};

		//Default Filter 
		var _defaultDDLFilter = function () {
			return {
				PageName: "AddEditCourseCategory",
				FilterList: [
					{
						DDLName: "COURSECATEGORY",
						Param1: "",
						Param2: "HIDE_DEFAULT"
					}
				]
			};
		};

		//CourseCategory Empty Filter 
		var _coursecategoryEmptyFilter = function () {
			return {
				CourseCategoryId: 0,
				CategoryName: "",
				PageNumber: 1,
				PageSize: 20,
				ShowAll: 0
			};
		};

		// Get DDL List by Filter
		var _getDDLList = function (ddlFilter) {
			return $http({
				url: serviceBase + 'api/Home/LoadDDLs',
				method: "post",
				data: ddlFilter
			});
		};

		// Get CourseCategorys by Filter
		var _getCourseCategorys = function (tcfilter) {
			return $http({
				url: serviceBase + 'api/CourseCategory/GetParentCategorysList',
				method: "post",
				data: tcfilter
			});
		};

		// Get CourseCategorys by Filter
		var _getCourseCategoryById = function (id) {

			var request = $http({
				method: 'GET',
				url: serviceBase + 'api/CourseCategory/GetCourseCategoryById',
				params: { id: id }

			}); 
			return request;

			 
		};

		//Create New CourseCategory
		var _createCourseCategory = function (coursecategory) {
			var request = $http({
				method: 'post',
				url: serviceBase + 'api/CourseCategory/SaveCourseCategory',
				data: coursecategory
			});
			return request;
		};

		//Update CourseCategory 
		var _updateCourseCategory = function (coursecategory) {
			var request = $http({
				method: "post",
				url: serviceBase + "api/CourseCategory/UpdateCourseCategory",
				data: coursecategory
			});
			return request;
		};

		//Delete CourseCategory
		var _deleteCourseCategory = function (coursecategoryid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/CourseCategory/DeleteCourseCategory/" + coursecategoryid
			});
			return request;
		};

		coursecategoryServiceFactory.DDLDefaultFilter = _defaultDDLFilter;
		coursecategoryServiceFactory.GetDDLByFilter = _getDDLList;
		coursecategoryServiceFactory.getCourseCategorys = _getCourseCategorys;
		coursecategoryServiceFactory.createCourseCategory = _createCourseCategory;
		coursecategoryServiceFactory.updateCourseCategory = _updateCourseCategory;
		coursecategoryServiceFactory.deleteCourseCategory = _deleteCourseCategory;
		coursecategoryServiceFactory.CourseCategoryEmptyFilter = _coursecategoryEmptyFilter;
		coursecategoryServiceFactory.GetCourseCategoryById = _getCourseCategoryById;

		return coursecategoryServiceFactory;
	}]);
}());


// Controller Starts Here.. 
; (function () {
	'use strict';
	rolpo_app.controller('coursecategoryController', ['$scope', '$rootScope', 'coursecategoryService', 'modalService', '$uibModal', '$uibModalStack', '$filter', function ($scope, $rootScope, coursecategoryService, modalService, $uibModal, $uibModalStack, $filter) {

		// Variables and declarations 

		$scope.loading = true;
		$scope.coursecategorys = [];
		$scope.coursecategory = {};
		$scope.CourseCategoryPageInfo = {};

		//Populate DDLs
		var ddlFilter = coursecategoryService.DDLDefaultFilter();
		coursecategoryService.GetDDLByFilter(ddlFilter).then(function (results) {
			$scope.ddLItems = angular.fromJson(results.data.DDLItems);


			//Get parentcategory 
			$scope.categoriesddl = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "COURSECATEGORY" })[0].Items;
		});

		// Methods

		// Get CourseCategory by Filter

		$scope.GetCourseCategoryByFilter = function () {
			GetCourseCategorys($scope.tcfilter);
		};

		// Reset CourseCategory Filter
		$scope.ResetCourseCategoryFilter = function () {
			var pageSize = $scope.tcfilter.PageSize;

			$scope.tcfilter = coursecategoryService.CourseCategoryEmptyFilter();
			$scope.tcfilter.PageSize = pageSize;

			GetCourseCategorys($scope.tcfilter);
		};

		//On CourseCategory Page Changed
		$scope.OnCourseCategoryPageChanged = function () {
			GetCourseCategorys($scope.tcfilter);
		};

		//On Page Size Changed
		$scope.OnCourseCategoryPageSizeChanged = function () {
			GetCourseCategorys($scope.tcfilter);
		};

		// Open Window for Saving new CourseCategory
		$scope.OpenCourseCategorySaveDialog = function () {
			$scope.coursecategory = { CourseCategoryId: 0 };
			MSG({}); //Init
			$scope.coursecategoryActionTitle = "Add New Category or Sub-Category";
			var modalInstance = $uibModal.open({
				animation: true,
				scope: $scope,
				templateUrl: 'customUpdateCourseCategory',
				backdrop: 'static',
				keyboard: false,
				modalFade: true,
				size: ''
			});

		};

		// Open Window for updating CourseCategory
		$scope.OpenCourseCategoryUpdateDialog = function (CourseCategoryId) {
			//var tcfilter = coursecategoryService.CourseCategoryEmptyFilter();
			//tcfilter.CourseCategoryId = CourseCategoryId;
			$scope.loading = true;
			MSG({}); //Init

			coursecategoryService.GetCourseCategoryById(CourseCategoryId).then(function (results) {
				if (results.data ==null) {
					$scope.loading = false;
					MSG({ 'elm': "CourseCategory_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading category!', 'MsgAsModel': error.data });
					return;
				}
				$scope.coursecategory = results.data;
				$scope.coursecategoryActionTitle = "Update Category/Sub-Category";

				var modalInstance = $uibModal.open({
					animation: true,
					scope: $scope,
					templateUrl: 'customUpdateCourseCategory',
					backdrop: 'static',
					keyboard: false,
					modalFade: true,
					size: ''
				});
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "CourseCategory_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading category!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});

		};

		//Update CourseCategory
		$scope.CreateUpdateCourseCategory = function (CourseCategoryId) {
			if (CourseCategoryId == 0) { CreateNewCourseCategory($scope.coursecategory); } else { UpdateCourseCategory($scope.coursecategory); }
		};

		//Delete CourseCategory
		$scope.DeleteCourseCategory = function (CourseCategoryId) {
			MSG({}); //Init
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete CourseCategory',
				headerText: 'Delete Item',
				bodyText: 'Are you sure you want to delete this?'
			};
			modalService.showModal({}, modalOptions).then(function (result) {
				$scope.loading = true;
				coursecategoryService.deleteCourseCategory(CourseCategoryId).then(function (results) {
					//angular.forEach($scope.coursecategorys, function (value, key) {
					//	if ($scope.coursecategorys[key].CourseCategoryId === CourseCategoryId) {
					//		$scope.coursecategorys.splice(key, 1);
					//		return false;
					//	}
					//});

					//$scope.loading = false;
					GetCourseCategorys($scope.tcfilter);

					MSG({ 'elm': "CourseCategory_alert", "MsgType": "OK", "MsgText": "Category deleted successfully." });
				}, function (error) {
					MSG({ 'elm': "CourseCategory_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting category!', 'MsgAsModel': error.data });
					$scope.loading = false;
				});
			});
		};

		// Cancel  Editing
		$scope.cancelEditing = function () {
			$uibModalStack.dismissAll();
		};

		// Functions 
		// Function to Get CourseCategory
		function GetCourseCategorys(tcfilter) {
			$scope.loading = true;
			$scope.HasTC_Records = false;
			coursecategoryService.getCourseCategorys(tcfilter).then(function (results) {
				$scope.coursecategorys = results.data;
				angular.forEach($scope.coursecategorys, function (item) {
					item.ChildChildCategories = GETJ(item.ChildCategoryJSON);
				});

				var tmp_page_start = (($scope.tcfilter.PageNumber - 1) * ($scope.tcfilter.PageSize) + 1), tmp_page_end = ($scope.tcfilter.PageNumber) * ($scope.tcfilter.PageSize);
				if (results.data.length > 0) {
					$scope.CourseCategoryPageInfo = {
						Has_record: true,
						TotalItems: results.data[0]["TotalCount"],
						PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0,
						PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"]
					};
				} else { $scope.CourseCategoryPageInfo = {}; }



				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "CourseCategory_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading category!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		// Create New CourseCategory Function 
		function CreateNewCourseCategory(coursecategory) {
			$scope.loading = true;
			coursecategoryService.createCourseCategory(coursecategory).then(function (results) {
				//$scope.coursecategorys.push(results.data);
				//$scope.loading = false;
				$uibModalStack.dismissAll();

				MSG({ 'elm': "CourseCategory_alert", "MsgType": "OK", "MsgText": "Category added successfully." });
				GetCourseCategorys($scope.tcfilter);
			}, function (error) {
				MSG({ 'elm': "CourseCategory_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding category!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		}

		//Update CourseCategory Function 
		function UpdateCourseCategory(coursecategory) {
			$scope.loading = true;
			coursecategoryService.updateCourseCategory(coursecategory).then(function (results) {
				//angular.forEach($scope.coursecategorys, function (value, key) {
				//	if ($scope.coursecategorys[key].CourseCategoryId === coursecategory.CourseCategoryId) {
				//		$scope.coursecategorys[key] = coursecategory;
				//		return false;
				//	}
				//});
				//$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "CourseCategory_alert", "MsgType": "OK", "MsgText": "Category updated successfully." });
				GetCourseCategorys($scope.tcfilter);
			}, function (error) {
				MSG({ 'elm': "CourseCategory_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating category!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		//Datepicker
		$scope.dateOptions = {
			'year-format': "'yy'",
			'show-weeks': false
		};

		// Call CourseCategory for first time
		$scope.CourseCategoryPageInfo = {};
		$scope.tcfilter = coursecategoryService.CourseCategoryEmptyFilter();
		$scope.tcfilter.PageNumber = 1;
		$scope.tcfilter.PageSize = '20';

		GetCourseCategorys($scope.tcfilter);

	}]);
}());


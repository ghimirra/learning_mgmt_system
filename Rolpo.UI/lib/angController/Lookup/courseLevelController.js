﻿//Created By: Prashant 
//Created On: 27/06/2018 
// Controller for CourseLevel 
// Initialization for CourseLevel 
 
rolpo_app.constant('ngAuthSettings', {
	apiServiceBaseUri: serviceBase,
	clientId: 'rolpo.com'
});

// Service For CourseLevel 
; (function () {
	'use strict';
	rolpo_app.factory('courselevelService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

		var serviceBase = ngAuthSettings.apiServiceBaseUri;
		var courselevelServiceFactory = {};

		//Default Filter 
		var _defaultDDLFilter = function () {
			return {
				PageName: "AddEditCourseLevel",
				FilterList: [
				]
			};
		};

		//CourseLevel Empty Filter 
		var _courselevelEmptyFilter = function () {
			return {
				CourseLevelId: 0,
				LevelName: "",
				PageNumber: 1,
				PageSize: 20,
				ShowAll: 0
			};
		};

		// Get DDL List by Filter
		var _getDDLList = function (ddlFilter) {
			return $http({
				url: serviceBase + 'api/Home/LoadDDLs',
				method: "post",
				data: ddlFilter
			});
		};

		// Get CourseLevels by Filter
		var _getCourseLevels = function (tcfilter) {
			return $http({
				url: serviceBase + 'api/CourseLevel/GetCourseLevelsList',
				method: "post",
				data: tcfilter
			});
		};

		//Create New CourseLevel
		var _createCourseLevel = function (courselevel) {
			var request = $http({
				method: 'post',
				url: serviceBase + 'api/CourseLevel/SaveCourseLevel',
				data: courselevel
			});
			return request;
		};

		//Update CourseLevel 
		var _updateCourseLevel = function (courselevel) {
			var request = $http({
				method: "post",
				url: serviceBase + "api/CourseLevel/UpdateCourseLevel",
				data: courselevel
			});
			return request;
		};

		//Delete CourseLevel
		var _deleteCourseLevel = function (courselevelid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/CourseLevel/DeleteCourseLevel/" + courselevelid
			});
			return request;
		};

		courselevelServiceFactory.DDLDefaultFilter = _defaultDDLFilter;
		courselevelServiceFactory.GetDDLByFilter = _getDDLList;
		courselevelServiceFactory.getCourseLevels = _getCourseLevels;
		courselevelServiceFactory.createCourseLevel = _createCourseLevel;
		courselevelServiceFactory.updateCourseLevel = _updateCourseLevel;
		courselevelServiceFactory.deleteCourseLevel = _deleteCourseLevel;
		courselevelServiceFactory.CourseLevelEmptyFilter = _courselevelEmptyFilter;

		return courselevelServiceFactory;
	}]);
}());


// Controller Starts Here.. 
; (function () {
	'use strict';
	rolpo_app.controller('courselevelController', ['$scope', '$rootScope', 'courselevelService', 'modalService', '$uibModal', '$uibModalStack', '$filter', function ($scope, $rootScope, courselevelService, modalService, $uibModal, $uibModalStack, $filter) {

		// Variables and declarations 

		$scope.loading = true;
		$scope.courselevels = [];
		$scope.courselevel = {};
		$scope.CourseLevelPageInfo = {};

		//Populate DDLs
		var ddlFilter = courselevelService.DDLDefaultFilter();
		courselevelService.GetDDLByFilter(ddlFilter).then(function (results) {
			$scope.ddLItems = angular.fromJson(results.data.DDLItems);

		});

		// Methods

		// Get CourseLevel by Filter

		$scope.GetCourseLevelByFilter = function () {
			GetCourseLevels($scope.tcfilter);
		};

		// Reset CourseLevel Filter
		$scope.ResetCourseLevelFilter = function () {
			var pageSize = $scope.tcfilter.PageSize;

			$scope.tcfilter = courselevelService.CourseLevelEmptyFilter();
			$scope.tcfilter.PageSize = pageSize;

			GetCourseLevels($scope.tcfilter);
		};

		//On CourseLevel Page Changed
		$scope.OnCourseLevelPageChanged = function () {
			GetCourseLevels($scope.tcfilter);
		};

		//On Page Size Changed
		$scope.OnCourseLevelPageSizeChanged = function () {
			GetCourseLevels($scope.tcfilter);
		};

		// Open Window for Saving new CourseLevel
		$scope.OpenCourseLevelSaveDialog = function () {
			$scope.courselevel = { CourseLevelId: 0 };
			MSG({}); //Init
			$scope.courselevelActionTitle = "Add New Course Level";
			var modalInstance = $uibModal.open({
				animation: true,
				scope: $scope,
				templateUrl: 'customUpdateCourseLevel',
				backdrop: 'static',
				keyboard: false,
				modalFade: true,
				size: ''
			});

		};

		// Open Window for updating CourseLevel
		$scope.OpenCourseLevelUpdateDialog = function (CourseLevelId) {
			var tcfilter = courselevelService.CourseLevelEmptyFilter();
			tcfilter.CourseLevelId = CourseLevelId;
			$scope.loading = true;
			MSG({}); //Init

			courselevelService.getCourseLevels(tcfilter).then(function (results) {
				if (results.data.length != 1) {
					$scope.loading = false;
					MSG({ 'elm': "CourseLevel_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading course levels!', 'MsgAsModel': error.data });
					return;
				}
				$scope.courselevel = results.data[0];
				$scope.courselevelActionTitle = "Update CourseLevel";

				var modalInstance = $uibModal.open({
					animation: true,
					scope: $scope,
					templateUrl: 'customUpdateCourseLevel',
					backdrop: 'static',
					keyboard: false,
					modalFade: true,
					size: ''
				});
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "CourseLevel_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading course levels!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});

		};

		//Update CourseLevel
		$scope.CreateUpdateCourseLevel = function (CourseLevelId) {
			if (CourseLevelId == 0) { CreateNewCourseLevel($scope.courselevel); } else { UpdateCourseLevel($scope.courselevel); }
		};

		//Delete CourseLevel
		$scope.DeleteCourseLevel = function (CourseLevelId) {
			MSG({}); //Init
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete Course Level',
				headerText: 'Delete Item',
				bodyText: 'Are you sure you want to delete this?'
			};
			modalService.showModal({}, modalOptions).then(function (result) {
				$scope.loading = true;
				courselevelService.deleteCourseLevel(CourseLevelId).then(function (results) {
					angular.forEach($scope.courselevels, function (value, key) {
						if ($scope.courselevels[key].CourseLevelId === CourseLevelId) {
							$scope.courselevels.splice(key, 1);
							return false;
						}
					});

					$scope.loading = false;
					MSG({ 'elm': "CourseLevel_alert", "MsgType": "OK", "MsgText": "Course Level deleted successfully." });
				}, function (error) {
					MSG({ 'elm': "CourseLevel_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting course levels!', 'MsgAsModel': error.data });
					$scope.loading = false;
				});
			});
		};

		// Cancel  Editing
		$scope.cancelEditing = function () {
			$uibModalStack.dismissAll();
		};

		// Functions 
		// Function to Get CourseLevel
		function GetCourseLevels(tcfilter) {
			$scope.loading = true;
			$scope.HasTC_Records = false;
			courselevelService.getCourseLevels(tcfilter).then(function (results) {
				$scope.courselevels = results.data;
				var tmp_page_start = (($scope.tcfilter.PageNumber - 1) * ($scope.tcfilter.PageSize) + 1), tmp_page_end = ($scope.tcfilter.PageNumber) * ($scope.tcfilter.PageSize);
				if (results.data.length > 0) {
					$scope.CourseLevelPageInfo = {
						Has_record: true,
						TotalItems: results.data[0]["TotalCount"],
						PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0,
						PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"]
					};
				} else { $scope.CourseLevelPageInfo = {}; }
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "CourseLevel_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading course levels!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		// Create New CourseLevel Function 
		function CreateNewCourseLevel(courselevel) {
			$scope.loading = true;
			courselevelService.createCourseLevel(courselevel).then(function (results) {
				$scope.courselevels.push(results.data);
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "CourseLevel_alert", "MsgType": "OK", "MsgText": "Course Level added successfully." });
			}, function (error) {
				MSG({ 'elm': "CourseLevel_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding course level!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		}

		//Update CourseLevel Function 
		function UpdateCourseLevel(courselevel) {
			$scope.loading = true;
			courselevelService.updateCourseLevel(courselevel).then(function (results) {
				angular.forEach($scope.courselevels, function (value, key) {
					if ($scope.courselevels[key].CourseLevelId === courselevel.CourseLevelId) {
						$scope.courselevels[key] = courselevel;
						return false;
					}
				});
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "CourseLevel_alert", "MsgType": "OK", "MsgText": "Course Level updated successfully." });
			}, function (error) {
				MSG({ 'elm': "CourseLevel_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating course level!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		//Datepicker
		$scope.dateOptions = {
			'year-format': "'yy'",
			'show-weeks': false
		};

		// Call CourseLevel for first time
		$scope.CourseLevelPageInfo = {};
		$scope.tcfilter = courselevelService.CourseLevelEmptyFilter();
		$scope.tcfilter.PageNumber = 1;
		$scope.tcfilter.PageSize = '20';

		GetCourseLevels($scope.tcfilter);

	}]);
}());


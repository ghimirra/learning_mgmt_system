﻿//Created By: Prashant 
//Created On: 21/06/2018 
// Controller for Media 
// Initialization for Media 
 
rolpo_app.constant('ngAuthSettings', {
	apiServiceBaseUri: serviceBase,
	clientId: 'rolpo.com'
});

// Service For Media 
; (function () {
	'use strict';
	rolpo_app.factory('mediaService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

		var serviceBase = ngAuthSettings.apiServiceBaseUri;
		var mediaServiceFactory = {};

		//Default Filter 
		var _defaultDDLFilter = function () {
			return {
				PageName: "AddEditMedia",
				FilterList: [
				]
			};
		};

		//Media Empty Filter 
		var _mediaEmptyFilter = function () {
			return {
				MediaId: 0,
				OrganizationId: "",
				MediaName: "",
				PageNumber: 1,
				PageSize: 20,
				ShowAll: 0
			};
		};

		// Get DDL List by Filter
		var _getDDLList = function (ddlFilter) {
			return $http({
				url: serviceBase + 'api/Home/LoadDDLs',
				method: "post",
				data: ddlFilter
			});
		};

		// Get Medias by Filter
		var _getMedias = function (tmfilter) {
			return $http({
				url: serviceBase + 'api/Media/GetMediasList',
				method: "post",
				data: tmfilter
			});
		};

		//Create New Media
		var _createMedia = function (media) {
			var request = $http({
				method: 'post',
				url: serviceBase + 'api/Media/SaveMedia',
				data: media
			});
			return request;
		};

		//Update Media 
		var _updateMedia = function (media) {
			var request = $http({
				method: "post",
				url: serviceBase + "api/Media/UpdateMedia",
				data: media
			});
			return request;
		};

		//Delete Media
		var _deleteMedia = function (mediaid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/Media/DeleteMedia/" + mediaid
			});
			return request;
		};

		mediaServiceFactory.DDLDefaultFilter = _defaultDDLFilter;
		mediaServiceFactory.GetDDLByFilter = _getDDLList;
		mediaServiceFactory.getMedias = _getMedias;
		mediaServiceFactory.createMedia = _createMedia;
		mediaServiceFactory.updateMedia = _updateMedia;
		mediaServiceFactory.deleteMedia = _deleteMedia;
		mediaServiceFactory.MediaEmptyFilter = _mediaEmptyFilter;

		return mediaServiceFactory;
	}]);
}());


// Controller Starts Here.. 
; (function () {
	'use strict';
	rolpo_app.controller('mediaController', ['$scope', '$rootScope', 'mediaService', 'modalService', '$uibModal', '$uibModalStack', '$filter', function ($scope, $rootScope, mediaService, modalService, $uibModal, $uibModalStack, $filter) {

		// Variables and declarations 

		$scope.loading = true;
		$scope.medias = [];
		$scope.media = {};
		$scope.MediaPageInfo = {};

		//Populate DDLs
		var ddlFilter = mediaService.DDLDefaultFilter();
		mediaService.GetDDLByFilter(ddlFilter).then(function (results) {
			$scope.ddLItems = angular.fromJson(results.data.DDLItems);

		});

		// Methods

		// Get Media by Filter

		$scope.GetMediaByFilter = function () {
			GetMedias($scope.tmfilter);
		};

		// Reset Media Filter
		$scope.ResetMediaFilter = function () {
			var pageSize = $scope.tmfilter.PageSize;

			$scope.tmfilter = mediaService.MediaEmptyFilter();
			$scope.tmfilter.PageSize = pageSize;

			GetMedias($scope.tmfilter);
		};

		//On Media Page Changed
		$scope.OnMediaPageChanged = function () {
			GetMedias($scope.tmfilter);
		};

		//On Page Size Changed
		$scope.OnMediaPageSizeChanged = function () {
			GetMedias($scope.tmfilter);
		};

		// Open Window for Saving new Media
		$scope.OpenMediaSaveDialog = function () {
			$scope.media = { MediaId: 0 };
			MSG({}); //Init
			$scope.mediaActionTitle = "Add New Media";
			var modalInstance = $uibModal.open({
				animation: true,
				scope: $scope,
				templateUrl: 'customUpdateMedia',
				backdrop: 'static',
				keyboard: false,
				modalFade: true,
				size: ''
			});

		};

		// Open Window for updating Media
		$scope.OpenMediaUpdateDialog = function (MediaId) {
			var tmfilter = mediaService.MediaEmptyFilter();
			tmfilter.MediaId = MediaId;
			$scope.loading = true;
			MSG({}); //Init

			mediaService.getMedias(tmfilter).then(function (results) {
				if (results.data.length != 1) {
					$scope.loading = false;
					MSG({ 'elm': "Media_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading medias!', 'MsgAsModel': error.data });
					return;
				}
				$scope.media = results.data[0];
				$scope.mediaActionTitle = "Update Media";

				var modalInstance = $uibModal.open({
					animation: true,
					scope: $scope,
					templateUrl: 'customUpdateMedia',
					backdrop: 'static',
					keyboard: false,
					modalFade: true,
					size: ''
				});
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "Media_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading medias!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});

		};

		//Update Media
		$scope.CreateUpdateMedia = function (MediaId) {
			if (MediaId == 0) { CreateNewMedia($scope.media); } else { UpdateMedia($scope.media); }
		};

		//Delete Media
		$scope.DeleteMedia = function (MediaId) {
			MSG({}); //Init
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete Media',
				headerText: 'Delete Item',
				bodyText: 'Are you sure you want to delete this?'
			};
			modalService.showModal({}, modalOptions).then(function (result) {
				$scope.loading = true;
				mediaService.deleteMedia(MediaId).then(function (results) {
					angular.forEach($scope.medias, function (value, key) {
						if ($scope.medias[key].MediaId === MediaId) {
							$scope.medias.splice(key, 1);
							return false;
						}
					});

					$scope.loading = false;
					MSG({ 'elm': "Media_alert", "MsgType": "OK", "MsgText": "Media deleted successfully." });
				}, function (error) {
					MSG({ 'elm': "Media_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting medias!', 'MsgAsModel': error.data });
					$scope.loading = false;
				});
			});
		};

		// Cancel  Editing
		$scope.cancelEditing = function () {
			$uibModalStack.dismissAll();
		};

		// Functions 
		// Function to Get Media
		function GetMedias(tmfilter) {
			$scope.loading = true;
			$scope.HasTM_Records = false;
			mediaService.getMedias(tmfilter).then(function (results) {
				$scope.medias = results.data;
				var tmp_page_start = (($scope.tmfilter.PageNumber - 1) * ($scope.tmfilter.PageSize) + 1), tmp_page_end = ($scope.tmfilter.PageNumber) * ($scope.tmfilter.PageSize);
				if (results.data.length > 0) {
					$scope.MediaPageInfo = {
						Has_record: true,
						TotalItems: results.data[0]["TotalCount"],
						PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0,
						PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"]
					};
				} else { $scope.MediaPageInfo = {}; }
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "Media_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading medias!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		// Create New Media Function 
		function CreateNewMedia(media) {
			$scope.loading = true;
			mediaService.createMedia(media).then(function (results) {
				$scope.medias.push(results.data);
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "Media_alert", "MsgType": "OK", "MsgText": "Media added successfully." });
			}, function (error) {
				MSG({ 'elm': "Media_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding media!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		}

		//Update Media Function 
		function UpdateMedia(media) {
			$scope.loading = true;
			mediaService.updateMedia(media).then(function (results) {
				angular.forEach($scope.medias, function (value, key) {
					if ($scope.medias[key].MediaId === media.MediaId) {
						$scope.medias[key] = media;
						return false;
					}
				});
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "Media_alert", "MsgType": "OK", "MsgText": "Media updated successfully." });
			}, function (error) {
				MSG({ 'elm': "Media_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating media!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		//Datepicker
		$scope.dateOptions = {
			'year-format': "'yy'",
			'show-weeks': false
		};

		// Call Media for first time
		$scope.MediaPageInfo = {};
		$scope.tmfilter = mediaService.MediaEmptyFilter();
		$scope.tmfilter.PageNumber = 1;
		$scope.tmfilter.PageSize = '20';

		GetMedias($scope.tmfilter);

	}]);
}());


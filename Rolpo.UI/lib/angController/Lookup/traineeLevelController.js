﻿//Created By: Prashant 
//Created On: 26/06/2018 
// Controller for TraineeLevel 
// Initialization for TraineeLevel 
 
rolpo_app.constant('ngAuthSettings', {
	apiServiceBaseUri: serviceBase,
	clientId: 'rolpo.com'
});

// Service For TraineeLevel 
; (function () {
	'use strict';
	rolpo_app.factory('traineelevelService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

		var serviceBase = ngAuthSettings.apiServiceBaseUri;
		var traineelevelServiceFactory = {};

		//Default Filter 
		var _defaultDDLFilter = function () {
			return {
				PageName: "AddEditTraineeLevel",
				FilterList: [
				]
			};
		};

		//TraineeLevel Empty Filter 
		var _traineelevelEmptyFilter = function () {
			return {
				TraineeLevelId: 0,
				TraineeLevelName: "",
				PageNumber: 1,
				PageSize: 20,
				ShowAll: 0
			};
		};

		// Get DDL List by Filter
		var _getDDLList = function (ddlFilter) {
			return $http({
				url: serviceBase + 'api/Home/LoadDDLs',
				method: "post",
				data: ddlFilter
			});
		};

		// Get TraineeLevels by Filter
		var _getTraineeLevels = function (ttfilter) {
			return $http({
				url: serviceBase + 'api/TraineeLevel/GetTraineeLevelsList',
				method: "post",
				data: ttfilter
			});
		};

		//Create New TraineeLevel
		var _createTraineeLevel = function (traineelevel) {
			var request = $http({
				method: 'post',
				url: serviceBase + 'api/TraineeLevel/SaveTraineeLevel',
				data: traineelevel
			});
			return request;
		};

		//Update TraineeLevel 
		var _updateTraineeLevel = function (traineelevel) {
			var request = $http({
				method: "post",
				url: serviceBase + "api/TraineeLevel/UpdateTraineeLevel",
				data: traineelevel
			});
			return request;
		};

		//Delete TraineeLevel
		var _deleteTraineeLevel = function (traineelevelid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/TraineeLevel/DeleteTraineeLevel/" + traineelevelid
			});
			return request;
		};

		traineelevelServiceFactory.DDLDefaultFilter = _defaultDDLFilter;
		traineelevelServiceFactory.GetDDLByFilter = _getDDLList;
		traineelevelServiceFactory.getTraineeLevels = _getTraineeLevels;
		traineelevelServiceFactory.createTraineeLevel = _createTraineeLevel;
		traineelevelServiceFactory.updateTraineeLevel = _updateTraineeLevel;
		traineelevelServiceFactory.deleteTraineeLevel = _deleteTraineeLevel;
		traineelevelServiceFactory.TraineeLevelEmptyFilter = _traineelevelEmptyFilter;

		return traineelevelServiceFactory;
	}]);
}());


// Controller Starts Here.. 
; (function () {
	'use strict';
	rolpo_app.controller('traineelevelController', ['$scope', '$rootScope', 'traineelevelService', 'modalService', '$uibModal', '$uibModalStack', '$filter', function ($scope, $rootScope, traineelevelService, modalService, $uibModal, $uibModalStack, $filter) {

		// Variables and declarations 

		$scope.loading = true;
		$scope.traineelevels = [];
		$scope.traineelevel = {};
		$scope.TraineeLevelPageInfo = {};

		//Populate DDLs
		var ddlFilter = traineelevelService.DDLDefaultFilter();
		traineelevelService.GetDDLByFilter(ddlFilter).then(function (results) {
			$scope.ddLItems = angular.fromJson(results.data.DDLItems);

		});

		// Methods

		// Get TraineeLevel by Filter

		$scope.GetTraineeLevelByFilter = function () {
			GetTraineeLevels($scope.ttfilter);
		};

		// Reset TraineeLevel Filter
		$scope.ResetTraineeLevelFilter = function () {
			var pageSize = $scope.ttfilter.PageSize;

			$scope.ttfilter = traineelevelService.TraineeLevelEmptyFilter();
			$scope.ttfilter.PageSize = pageSize;

			GetTraineeLevels($scope.ttfilter);
		};

		//On TraineeLevel Page Changed
		$scope.OnTraineeLevelPageChanged = function () {
			GetTraineeLevels($scope.ttfilter);
		};

		//On Page Size Changed
		$scope.OnTraineeLevelPageSizeChanged = function () {
			GetTraineeLevels($scope.ttfilter);
		};

		// Open Window for Saving new TraineeLevel
		$scope.OpenTraineeLevelSaveDialog = function () {
			$scope.traineelevel = { TraineeLevelId: 0 };
			MSG({}); //Init
			$scope.traineelevelActionTitle = "Add New TraineeLevel";
			var modalInstance = $uibModal.open({
				animation: true,
				scope: $scope,
				templateUrl: 'customUpdateTraineeLevel',
				backdrop: 'static',
				keyboard: false,
				modalFade: true,
				size: ''
			});

		};

		// Open Window for updating TraineeLevel
		$scope.OpenTraineeLevelUpdateDialog = function (TraineeLevelId) {
			var ttfilter = traineelevelService.TraineeLevelEmptyFilter();
			ttfilter.TraineeLevelId = TraineeLevelId;
			$scope.loading = true;
			MSG({}); //Init

			traineelevelService.getTraineeLevels(ttfilter).then(function (results) {
				if (results.data.length != 1) {
					$scope.loading = false;
					MSG({ 'elm': "TraineeLevel_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading traineelevels!', 'MsgAsModel': error.data });
					return;
				}
				$scope.traineelevel = results.data[0];
				$scope.traineelevelActionTitle = "Update TraineeLevel";

				var modalInstance = $uibModal.open({
					animation: true,
					scope: $scope,
					templateUrl: 'customUpdateTraineeLevel',
					backdrop: 'static',
					keyboard: false,
					modalFade: true,
					size: ''
				});
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "TraineeLevel_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading traineelevels!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});

		};

		//Update TraineeLevel
		$scope.CreateUpdateTraineeLevel = function (TraineeLevelId) {
			if (TraineeLevelId == 0) { CreateNewTraineeLevel($scope.traineelevel); } else { UpdateTraineeLevel($scope.traineelevel); }
		};

		//Delete TraineeLevel
		$scope.DeleteTraineeLevel = function (TraineeLevelId) {
			MSG({}); //Init
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete TraineeLevel',
				headerText: 'Delete Item',
				bodyText: 'Are you sure you want to delete this?'
			};
			modalService.showModal({}, modalOptions).then(function (result) {
				$scope.loading = true;
				traineelevelService.deleteTraineeLevel(TraineeLevelId).then(function (results) {
					angular.forEach($scope.traineelevels, function (value, key) {
						if ($scope.traineelevels[key].TraineeLevelId === TraineeLevelId) {
							$scope.traineelevels.splice(key, 1);
							return false;
						}
					});

					$scope.loading = false;
					MSG({ 'elm': "TraineeLevel_alert", "MsgType": "OK", "MsgText": "TraineeLevel deleted successfully." });
				}, function (error) {
					MSG({ 'elm': "TraineeLevel_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting traineelevels!', 'MsgAsModel': error.data });
					$scope.loading = false;
				});
			});
		};

		// Cancel  Editing
		$scope.cancelEditing = function () {
			$uibModalStack.dismissAll();
		};

		// Functions 
		// Function to Get TraineeLevel
		function GetTraineeLevels(ttfilter) {
			$scope.loading = true;
			$scope.HasTT_Records = false;
			traineelevelService.getTraineeLevels(ttfilter).then(function (results) {
				$scope.traineelevels = results.data;
				var tmp_page_start = (($scope.ttfilter.PageNumber - 1) * ($scope.ttfilter.PageSize) + 1), tmp_page_end = ($scope.ttfilter.PageNumber) * ($scope.ttfilter.PageSize);
				if (results.data.length > 0) {
					$scope.TraineeLevelPageInfo = {
						Has_record: true,
						TotalItems: results.data[0]["TotalCount"],
						PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0,
						PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"]
					};
				} else { $scope.TraineeLevelPageInfo = {}; }
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "TraineeLevel_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading traineelevels!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		// Create New TraineeLevel Function 
		function CreateNewTraineeLevel(traineelevel) {
			$scope.loading = true;
			traineelevelService.createTraineeLevel(traineelevel).then(function (results) {
				$scope.traineelevels.push(results.data);
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "TraineeLevel_alert", "MsgType": "OK", "MsgText": "TraineeLevel added successfully." });
			}, function (error) {
				MSG({ 'elm': "TraineeLevel_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding traineelevel!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		}

		//Update TraineeLevel Function 
		function UpdateTraineeLevel(traineelevel) {
			$scope.loading = true;
			traineelevelService.updateTraineeLevel(traineelevel).then(function (results) {
				angular.forEach($scope.traineelevels, function (value, key) {
					if ($scope.traineelevels[key].TraineeLevelId === traineelevel.TraineeLevelId) {
						$scope.traineelevels[key] = traineelevel;
						return false;
					}
				});
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "TraineeLevel_alert", "MsgType": "OK", "MsgText": "TraineeLevel updated successfully." });
			}, function (error) {
				MSG({ 'elm': "TraineeLevel_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating traineelevel!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		//Datepicker
		$scope.dateOptions = {
			'year-format': "'yy'",
			'show-weeks': false
		};

		// Call TraineeLevel for first time
		$scope.TraineeLevelPageInfo = {};
		$scope.ttfilter = traineelevelService.TraineeLevelEmptyFilter();
		$scope.ttfilter.PageNumber = 1;
		$scope.ttfilter.PageSize = '20';

		GetTraineeLevels($scope.ttfilter);

	}]);
}());


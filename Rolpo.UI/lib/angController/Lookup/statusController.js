﻿//Created By: Prashant 
//Created On: 24/06/2018 
// Controller for Status 
// Initialization for Status 
 
rolpo_app.constant('ngAuthSettings', {
	apiServiceBaseUri: serviceBase,
	clientId: 'rolpo.com'
});

// Service For Status 
; (function () {
	'use strict';
	rolpo_app.factory('statusService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

		var serviceBase = ngAuthSettings.apiServiceBaseUri;
		var statusServiceFactory = {};

		//Default Filter 
		var _defaultDDLFilter = function () {
			return {
				PageName: "AddEditStatus",
				FilterList: [
				]
			};
		};

		//Status Empty Filter 
		var _statusEmptyFilter = function () {
			return {
				StatusId: 0,
				StatusGroup: "",
				StatusName: "",
				PageNumber: 1,
				PageSize: 20,
				ShowAll: 0
			};
		};

		// Get DDL List by Filter
		var _getDDLList = function (ddlFilter) {
			return $http({
				url: serviceBase + 'api/Home/LoadDDLs',
				method: "post",
				data: ddlFilter
			});
		};

		// Get Statuss by Filter
		var _getStatuss = function (tsfilter) {
			return $http({
				url: serviceBase + 'api/Status/GetStatussList',
				method: "post",
				data: tsfilter
			});
		};

		//Create New Status
		var _createStatus = function (status) {
			var request = $http({
				method: 'post',
				url: serviceBase + 'api/Status/SaveStatus',
				data: status
			});
			return request;
		};

		//Update Status 
		var _updateStatus = function (status) {
			var request = $http({
				method: "post",
				url: serviceBase + "api/Status/UpdateStatus",
				data: status
			});
			return request;
		};

		//Delete Status
		var _deleteStatus = function (statusid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/Status/DeleteStatus/" + statusid
			});
			return request;
		};

		statusServiceFactory.DDLDefaultFilter = _defaultDDLFilter;
		statusServiceFactory.GetDDLByFilter = _getDDLList;
		statusServiceFactory.getStatuss = _getStatuss;
		statusServiceFactory.createStatus = _createStatus;
		statusServiceFactory.updateStatus = _updateStatus;
		statusServiceFactory.deleteStatus = _deleteStatus;
		statusServiceFactory.StatusEmptyFilter = _statusEmptyFilter;

		return statusServiceFactory;
	}]);
}());


// Controller Starts Here.. 
; (function () {
	'use strict';
	rolpo_app.controller('statusController', ['$scope', '$rootScope', 'statusService', 'modalService', '$uibModal', '$uibModalStack', '$filter', function ($scope, $rootScope, statusService, modalService, $uibModal, $uibModalStack, $filter) {

		// Variables and declarations 

		$scope.loading = true;
		$scope.statuss = [];
		$scope.status = {};
		$scope.StatusPageInfo = {};

		//Populate DDLs
		var ddlFilter = statusService.DDLDefaultFilter();
		statusService.GetDDLByFilter(ddlFilter).then(function (results) {
			$scope.ddLItems = angular.fromJson(results.data.DDLItems);

		});

		// Methods

		// Get Status by Filter

		$scope.GetStatusByFilter = function () {
			GetStatuss($scope.tsfilter);
		};

		// Reset Status Filter
		$scope.ResetStatusFilter = function () {
			var pageSize = $scope.tsfilter.PageSize;

			$scope.tsfilter = statusService.StatusEmptyFilter();
			$scope.tsfilter.PageSize = pageSize;

			GetStatuss($scope.tsfilter);
		};

		//On Status Page Changed
		$scope.OnStatusPageChanged = function () {
			GetStatuss($scope.tsfilter);
		};

		//On Page Size Changed
		$scope.OnStatusPageSizeChanged = function () {
			GetStatuss($scope.tsfilter);
		};

		// Open Window for Saving new Status
		$scope.OpenStatusSaveDialog = function () {
			$scope.status = { StatusId: 0 };
			MSG({}); //Init
			$scope.statusActionTitle = "Add New Status";
			var modalInstance = $uibModal.open({
				animation: true,
				scope: $scope,
				templateUrl: 'customUpdateStatus',
				backdrop: 'static',
				keyboard: false,
				modalFade: true,
				size: ''
			});

		};

		// Open Window for updating Status
		$scope.OpenStatusUpdateDialog = function (StatusId) {
			var tsfilter = statusService.StatusEmptyFilter();
			tsfilter.StatusId = StatusId;
			$scope.loading = true;
			MSG({}); //Init

			statusService.getStatuss(tsfilter).then(function (results) {
				if (results.data.length != 1) {
					$scope.loading = false;
					MSG({ 'elm': "Status_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading statuss!', 'MsgAsModel': error.data });
					return;
				}
				$scope.status = results.data[0];
				$scope.statusActionTitle = "Update Status";

				var modalInstance = $uibModal.open({
					animation: true,
					scope: $scope,
					templateUrl: 'customUpdateStatus',
					backdrop: 'static',
					keyboard: false,
					modalFade: true,
					size: ''
				});
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "Status_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading statuss!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});

		};

		//Update Status
		$scope.CreateUpdateStatus = function (StatusId) {
			if (StatusId == 0) { CreateNewStatus($scope.status); } else { UpdateStatus($scope.status); }
		};

		//Delete Status
		$scope.DeleteStatus = function (StatusId) {
			MSG({}); //Init
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete Status',
				headerText: 'Delete Item',
				bodyText: 'Are you sure you want to delete this?'
			};
			modalService.showModal({}, modalOptions).then(function (result) {
				$scope.loading = true;
				statusService.deleteStatus(StatusId).then(function (results) {
					angular.forEach($scope.statuss, function (value, key) {
						if ($scope.statuss[key].StatusId === StatusId) {
							$scope.statuss.splice(key, 1);
							return false;
						}
					});

					$scope.loading = false;
					MSG({ 'elm': "Status_alert", "MsgType": "OK", "MsgText": "Status deleted successfully." });
				}, function (error) {
					MSG({ 'elm': "Status_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting statuss!', 'MsgAsModel': error.data });
					$scope.loading = false;
				});
			});
		};

		// Cancel  Editing
		$scope.cancelEditing = function () {
			$uibModalStack.dismissAll();
		};

		// Functions 
		// Function to Get Status
		function GetStatuss(tsfilter) {
			$scope.loading = true;
			$scope.HasTS_Records = false;
			statusService.getStatuss(tsfilter).then(function (results) {
				$scope.statuss = results.data;
				var tmp_page_start = (($scope.tsfilter.PageNumber - 1) * ($scope.tsfilter.PageSize) + 1), tmp_page_end = ($scope.tsfilter.PageNumber) * ($scope.tsfilter.PageSize);
				if (results.data.length > 0) {
					$scope.StatusPageInfo = {
						Has_record: true,
						TotalItems: results.data[0]["TotalCount"],
						PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0,
						PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"]
					};
				} else { $scope.StatusPageInfo = {}; }
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "Status_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading statuss!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		// Create New Status Function 
		function CreateNewStatus(status) {
			$scope.loading = true;
			statusService.createStatus(status).then(function (results) {
				$scope.statuss.push(results.data);
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "Status_alert", "MsgType": "OK", "MsgText": "Status added successfully." });
			}, function (error) {
				MSG({ 'elm': "Status_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding status!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		}

		//Update Status Function 
		function UpdateStatus(status) {
			$scope.loading = true;
			statusService.updateStatus(status).then(function (results) {
				angular.forEach($scope.statuss, function (value, key) {
					if ($scope.statuss[key].StatusId === status.StatusId) {
						$scope.statuss[key] = status;
						return false;
					}
				});
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "Status_alert", "MsgType": "OK", "MsgText": "Status updated successfully." });
			}, function (error) {
				MSG({ 'elm': "Status_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating status!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		//Datepicker
		$scope.dateOptions = {
			'year-format': "'yy'",
			'show-weeks': false
		};

		// Call Status for first time
		$scope.StatusPageInfo = {};
		$scope.tsfilter = statusService.StatusEmptyFilter();
		$scope.tsfilter.PageNumber = 1;
		$scope.tsfilter.PageSize = '20';

		GetStatuss($scope.tsfilter);

	}]);
}());


﻿//Created By: Prashant 
//Created On: 17/06/2018 
// Controller for Member 
// Initialization for Member 

rolpo_app.requires.push('ngSanitize', 'ui.select', 'ui.bootstrap', 'thatisuday.dropzone');

rolpo_app.constant('ngAuthSettings', {
    apiServiceBaseUri: serviceBase,
    clientId: 'rolpo.com'
});

// Service For Member 
; (function () {
    'use strict';
    rolpo_app.factory('memberService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

        var serviceBase = ngAuthSettings.apiServiceBaseUri;
        var memberServiceFactory = {};

        //Default Filter 
        var _defaultDDLFilter = function () {
            return {
                PageName: "AddEditMember",
                FilterList: [
			
                    	{
                    	    DDLName: "MEMBERLEVELS",
                    	    Param1: "",
                    	    Param2: "HIDE_DEFAULT"
                    	}


                    
                ]
            };
        };

        //Member Empty Filter 
        var _memberEmptyFilter = function () {
            return {
                MemberId: 0,
                Designation: "COORDINATOR",
                Phone: "",
                Mobile: "",
                PageNumber: 1,
                PageSize: 20,
                ShowAll: 0,
                Keyword: ''
            };
        };

        // Get DDL List by Filter
        var _getDDLList = function (ddlFilter) {
        	
            return $http({
                url: serviceBase + 'api/Home/LoadDDLs',
                method: "post",
                data: ddlFilter
            });
        };

        // Get Members by Filter
        var _getMembers = function (tmfilter) {  
        	return $http({
        	    url: serviceBase + 'api/Member/GetTrainersList',
                method: "post",
                data: tmfilter
            });
        };

        //Create New Member
        var _createMember = function (member) {
            var request = $http({
                method: 'post',
                url: serviceBase + 'api/Member/SaveMember',
                data: member
            });
            return request;
        };

        //Update Member 
        var _updateMember = function (member) {
            var request = $http({
                method: "post",
                url: serviceBase + "api/Member/UpdateMember",
                data: member
            });
            return request;
        };

        //Delete Member
        var _deleteMember = function (memberid) {
            var request = $http({
                method: "delete",
                url: serviceBase + "api/Member/DeleteMember/" + memberid
            });
            return request;
        };

        var _removeFile = function (file) {
            var request = $http({
                method: 'POST',
                url: baseUrl + 'Members/RemoveFile?filePath=' + file
            });
            return request;
        }

        memberServiceFactory.DDLDefaultFilter = _defaultDDLFilter;
        memberServiceFactory.GetDDLByFilter = _getDDLList;
        memberServiceFactory.getMembers = _getMembers;

        memberServiceFactory.createMember = _createMember;
        memberServiceFactory.updateMember = _updateMember;
        memberServiceFactory.deleteMember = _deleteMember;
        memberServiceFactory.MemberEmptyFilter = _memberEmptyFilter;
        memberServiceFactory.RemoveFile = _removeFile;

        return memberServiceFactory;
    }]);
}());


// Controller Starts Here.. 
; (function () {
    'use strict';
    rolpo_app.controller('memberController', ['$scope', '$rootScope', 'memberService', 'modalService', '$uibModal', '$uibModalStack', '$filter', '$timeout', function ($scope, $rootScope, memberService, modalService, $uibModal, $uibModalStack, $filter, $timeout) {

        // Variables and declarations 

        $scope.loading = true;
        $scope.members = [];
        $scope.member = {};
        $scope.MemberPageInfo = {};

        //region images
        //save file
        $scope.myDz = null;
        $scope.dzMethods = {};
        $scope.dzOptions = {
            url: serviceBase + '/Members/SaveUploadedFile',
            maxFilesize: '10',
            acceptedFiles: 'image/jpeg, images/jpg, image/png',
            addRemoveLinks: true,
            maxFiles: 1

        };
        $scope.dzCallbacks = {
            'addedfile': function (file) {


                if (file.isMock) {

                    $scope.myDz.createThumbnailFromUrl(file, file.serverImgUrl, null, true);

                }

            },
            'success': function (file, xhr) {

                if ($scope.myDz != null) {
                    if ($scope.myDz.files[1] != null) {
                        $scope.myDz.removeFile($scope.myDz.files[0]);
                    }

                }              
                $scope.member.ProfileImageUrl = baseUrl + '/' + xhr.ImagePath.substr(1);
            },
            'removedfile': function (file) {

                $scope.RemoveFile(file, true);

            }
       , 'maxfilesexceeded': function (file) {
           file.previewElement.innerHTML = "";

           alert("You cannot upload any more files. Please remove old one.")



       }
       , 'error': function (file) {

           var errorDisplay = document.querySelectorAll('[data-dz-errormessage]');
           errorDisplay[errorDisplay.length - 1].innerHTML = 'Error occured while uploading image. The maximum file limit is one.';
       }
        };

        $scope.RemoveFile = function (file) {
         
            $scope.member.ProfileImageUrl = "";
            RemoveFile($scope.file);
        }

        function RemoveFile(file) {
            memberService.RemoveFile(file).then(function (results) {
                if (results.data == "ok") {
                    MSG({ "elm": "FileUpload_alert", 'MsgType': 'SUCCESS', 'MsgText': 'File updated successfully.' });
                }
                else {
                    MSG({ "elm": "FileUpload_alert", 'MsgType': 'ERROR', 'MsgText': 'Error occured while removing file.' });
                }


            })
        }

        //end region files


        //Populate DDLs
        var ddlFilter = memberService.DDLDefaultFilter();
        memberService.GetDDLByFilter(ddlFilter).then(function (results) {
            $scope.ddLItems = angular.fromJson(results.data.DDLItems);


          
            $scope.levels = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "MEMBERLEVELS" })[0].Items;
        });

        // Methods

        // Get Member by Filter

        $scope.GetMemberByFilter = function () {
            GetMembers($scope.tmfilter);
        };

        // Reset Member Filter
        $scope.ResetMemberFilter = function () {
            var pageSize = $scope.tmfilter.PageSize;

            $scope.tmfilter = memberService.MemberEmptyFilter();
            $scope.tmfilter.PageSize = pageSize;

            GetMembers($scope.tmfilter);
        };

        //On Member Page Changed
        $scope.OnMemberPageChanged = function () {
            GetMembers($scope.tmfilter);
        };

        //On Page Size Changed
        $scope.OnMemberPageSizeChanged = function () {
            GetMembers($scope.tmfilter);
        };

        // Open Window for Saving new Member
        $scope.OpenMemberSaveDialog = function () {
            $scope.member = { MemberId: 0, Designation: 'COORDINATOR' };
            MSG({}); //Init
            $scope.memberActionTitle = "Add New Coordinator";
            var modalInstance = $uibModal.open({
                animation: true,
                scope: $scope,
                templateUrl: 'customUpdateMember',
                backdrop: 'static',
                keyboard: false,
                modalFade: true,
                size: ''
            });

        };

        // Open Window for updating Member
        $scope.OpenMemberUpdateDialog = function (MemberId) {
            var tmfilter = memberService.MemberEmptyFilter();
            tmfilter.MemberId = MemberId;
            $scope.loading = true;
            MSG({}); //Init

            memberService.getMembers(tmfilter).then(function (results) {
                if (results.data.length != 1) {
                    $scope.loading = false;
                    MSG({ 'elm': "Member_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading Coordinators!', 'MsgAsModel': error.data });
                    return;
                }
                $scope.member = results.data[0];
                $scope.memberActionTitle = "Update Coordinator";

                var modalInstance = $uibModal.open({
                    animation: true,
                    scope: $scope,
                    templateUrl: 'customUpdateMember',
                    backdrop: 'static',
                    keyboard: false,
                    modalFade: true,
                    size: ''
                });

                $timeout(function () {
                    if ($scope.member.ProfileImageUrl.length > 0) {
                        $scope.mockFiles = [];

                        $scope.myDz = $scope.dzMethods.getDropzone();
                        $scope.myDz.files = [];
                        $scope.mockFiles.push
                        ({
                            name: "Image",
                            serverImgUrl: $scope.member.ProfileImageUrl,
                            isMock: true

                        });

                        $scope.mockFiles.forEach(function (mockFile) {
                            $scope.myDz.emit('addedfile', mockFile);
                            $scope.myDz.emit('complete', mockFile);
                            $scope.myDz.options.maxFiles = 1;
                            $scope.myDz.files.push(mockFile);


                        });
                    }

                });


                $scope.loading = false;
            }, function (error) {
                MSG({ 'elm': "Member_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading members!', 'MsgAsModel': error.data });
                $scope.loading = false;
            });

        };

        //Update Member
        $scope.CreateUpdateMember = function (MemberId) {
            if (MemberId == 0) { CreateNewMember($scope.member); } else { UpdateMember($scope.member); }
        };

        //Delete Member
        $scope.DeleteMember = function (MemberId) {
            MSG({}); //Init
            var modalOptions = {
                closeButtonText: 'Cancel',
                actionButtonText: 'Delete Member',
                headerText: 'Delete Item',
                bodyText: 'Are you sure you want to delete this?'
            };
            modalService.showModal({}, modalOptions).then(function (result) {
                $scope.loading = true;
                memberService.deleteMember(MemberId).then(function (results) {
                    angular.forEach($scope.members, function (value, key) {
                        if ($scope.members[key].MemberId === MemberId) {
                            $scope.members.splice(key, 1);
                            return false;
                        }
                    });

                    $scope.loading = false;
                    MSG({ 'elm': "Member_alert", "MsgType": "OK", "MsgText": "Coordinator deleted successfully." });
                }, function (error) {
                    MSG({ 'elm': "Member_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting coordinator!', 'MsgAsModel': error.data });
                    $scope.loading = false;
                });
            });
        };

        // Cancel  Editing
        $scope.cancelEditing = function () {
            $uibModalStack.dismissAll();
        };

        // Functions 
        // Function to Get Member
        function GetMembers(tmfilter) {
            $scope.loading = true;
            $scope.HasTM_Records = false; 
            memberService.getMembers(tmfilter).then(function (results) {
                $scope.members = results.data;
                var tmp_page_start = (($scope.tmfilter.PageNumber - 1) * ($scope.tmfilter.PageSize) + 1), tmp_page_end = ($scope.tmfilter.PageNumber) * ($scope.tmfilter.PageSize);
                if (results.data.length > 0) {
                    $scope.MemberPageInfo = {
                        Has_record: true,
                        TotalItems: results.data[0]["TotalCount"],
                        PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0,
                        PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"]
                    };
                } else { $scope.MemberPageInfo = {}; }
                $scope.loading = false;
            }, function (error) {
                MSG({ 'elm': "Member_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading members!', 'MsgAsModel': error.data });
                $scope.loading = false;
            });
        };

        // Create New Member Function 
        function CreateNewMember(member) {
            $scope.loading = true;
            memberService.createMember(member).then(function (results) {
                $scope.members.push(results.data);
                $scope.loading = false;
                $uibModalStack.dismissAll();
                $scope.tmfilter = memberService.MemberEmptyFilter();
                GetMembers($scope.tmfilter);
                MSG({ 'elm': "Member_alert", "MsgType": "OK", "MsgText": "Coordinator added successfully." });
            }, function (error) {
                MSG({ 'elm': "Member_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding coordinator!', 'MsgAsModel': error.data });
                $scope.loading = false;
            });
        }

        //Update Member Function 
        function UpdateMember(member) {
            $scope.loading = true;
            memberService.updateMember(member).then(function (results) {
                angular.forEach($scope.members, function (value, key) {
                    if ($scope.members[key].MemberId === member.MemberId) {
                        $scope.members[key] = member;
                        return false;
                    }
                });
                $scope.loading = false;
                $uibModalStack.dismissAll();
                $scope.tmfilter = memberService.MemberEmptyFilter();
                GetMembers($scope.tmfilter);
                MSG({ 'elm': "Member_alert", "MsgType": "OK", "MsgText": "Coordinator updated successfully." });
            }, function (error) {
                MSG({ 'elm': "Member_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating coordinator!', 'MsgAsModel': error.data });
                $scope.loading = false;
            });
        };

        //Datepicker
        $scope.dateOptions = {
            'year-format': "'yy'",
            'show-weeks': false
        };

        // Call Member for first time
        $scope.MemberPageInfo = {};
        $scope.tmfilter = memberService.MemberEmptyFilter();
        $scope.tmfilter.PageNumber = 1;
        $scope.tmfilter.PageSize = '20';

        GetMembers($scope.tmfilter);

    }]);
}());


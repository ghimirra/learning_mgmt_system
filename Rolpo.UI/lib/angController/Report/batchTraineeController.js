﻿//Created By: Prashant 
//Created On: 02/07/2018 
// Controller for BatchTrainee 
// Initialization for BatchTrainee 
 
rolpo_app.constant('ngAuthSettings', {
	apiServiceBaseUri: serviceBase,
	clientId: 'rolpo.com'
});


rolpo_app.requires.push('ui.select');

// Service For BatchTrainee 
; (function () {
	'use strict';
	rolpo_app.factory('batchtraineeService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

		var serviceBase = ngAuthSettings.apiServiceBaseUri;
		var batchtraineeServiceFactory = {};

		//Default Filter 
		var _defaultDDLFilter = function () {
			return {
				PageName: "AddEditBatchTrainee",
				FilterList: [
					{
						DDLName: "BATCHES",
						Param1: "",
						Param2: "HIDE_DEFAULT__"
					}
					 
				]
			};
		};

		//BatchTrainee Empty Filter 
		var _batchtraineeEmptyFilter = function () {
			return {
				BatchId: 0,
				PageNumber: 1,
				PageSize: 20,
				ShowAll: 0
			};
		};

		// Get DDL List by Filter
		var _getDDLList = function (ddlFilter) {
			return $http({
				url: serviceBase + 'api/Home/LoadDDLs',
				method: "post",
				data: ddlFilter
			});
		};

		// Get BatchTrainees by Filter
		var _getBatchTrainees = function (tbfilter) {
			return $http({
				url: serviceBase + 'api/BatchTrainee/GetBatchTraineesListForReport',
				method: "post",
				data: tbfilter
			});
		};

		//Create New BatchTrainee
		var _createBatchTrainee = function (batchtrainee) {
			var request = $http({
				method: 'post',
				url: serviceBase + 'api/BatchTrainee/SaveBatchTrainee',
				data: batchtrainee
			});
			return request;
		};

		//Update BatchTrainee 
		var _updateBatchTrainee = function (batchtrainee) {
			var request = $http({
				method: "post",
				url: serviceBase + "api/BatchTrainee/UpdateBatchTrainee",
				data: batchtrainee
			});
			return request;
		};

		//Delete BatchTrainee
		var _deleteBatchTrainee = function (batchtraineeid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/BatchTrainee/DeleteBatchTrainee/" + batchtraineeid
			});
			return request;
		};

		batchtraineeServiceFactory.DDLDefaultFilter = _defaultDDLFilter;
		batchtraineeServiceFactory.GetDDLByFilter = _getDDLList;
		batchtraineeServiceFactory.getBatchTrainees = _getBatchTrainees;
		batchtraineeServiceFactory.createBatchTrainee = _createBatchTrainee;
		batchtraineeServiceFactory.updateBatchTrainee = _updateBatchTrainee;
		batchtraineeServiceFactory.deleteBatchTrainee = _deleteBatchTrainee;
		batchtraineeServiceFactory.BatchTraineeEmptyFilter = _batchtraineeEmptyFilter;

		return batchtraineeServiceFactory;
	}]);
}());


// Controller Starts Here.. 
; (function () {
	'use strict';
	rolpo_app.controller('batchtraineeController', ['$scope', '$rootScope', 'batchtraineeService', 'modalService', '$uibModal', '$uibModalStack', '$filter', '$httpParamSerializer', '$window', 'ngAuthSettings', function ($scope, $rootScope, batchtraineeService, modalService, $uibModal, $uibModalStack, $filter, $httpParamSerializer, $window, ngAuthSettings) {

		// Variables and declarations 

		//$scope.loading = true;
		$scope.batchtrainees = [];
		$scope.batchtrainee = {};
		$scope.BatchTraineePageInfo = {};

		//Populate DDLs
		var ddlFilter = batchtraineeService.DDLDefaultFilter();
		batchtraineeService.GetDDLByFilter(ddlFilter).then(function (results) {
			$scope.ddLItems = angular.fromJson(results.data.DDLItems);
			 
			//Get batchid 
			$scope.batchesddl = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "BATCHES" })[0].Items;
 
		});

		// Methods

		// Get BatchTrainee by Filter

		$scope.GetBatchTraineeByFilter = function () {
			GetBatchTrainees($scope.tbfilter);
		};

		// Reset BatchTrainee Filter
		$scope.ResetBatchTraineeFilter = function () {
			var pageSize = $scope.tbfilter.PageSize;

			$scope.tbfilter = batchtraineeService.BatchTraineeEmptyFilter();
			$scope.tbfilter.PageSize = pageSize;

			GetBatchTrainees($scope.tbfilter);
		};

		//On BatchTrainee Page Changed
		$scope.OnBatchTraineePageChanged = function () {
			GetBatchTrainees($scope.tbfilter);
		};

		//On Page Size Changed
		$scope.OnBatchTraineePageSizeChanged = function () {
			GetBatchTrainees($scope.tbfilter);
		};
		 
		// Cancel  Editing
		$scope.cancelEditing = function () {
			$uibModalStack.dismissAll();
		};

		// Functions 
		// Function to Get BatchTrainee
		function GetBatchTrainees(tbfilter) {
		
			if (!(tbfilter.BatchId > 0)) { $scope.batchtrainees = []; return; }

			$scope.loading = true;
			$scope.HasTB_Records = false;


			batchtraineeService.getBatchTrainees(tbfilter).then(function (results) {
				$scope.batchtrainees = results.data;
				var tmp_page_start = (($scope.tbfilter.PageNumber - 1) * ($scope.tbfilter.PageSize) + 1), tmp_page_end = ($scope.tbfilter.PageNumber) * ($scope.tbfilter.PageSize);
				if (results.data.length > 0) {
					$scope.BatchTraineePageInfo = {
						Has_record: true,
						TotalItems: results.data[0]["TotalCount"],
						PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0,
						PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"]
					};
				} else { $scope.BatchTraineePageInfo = {}; }
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "BatchTrainee_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchtrainees!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		// Create New BatchTrainee Function 
		function CreateNewBatchTrainee(batchtrainee) {
			$scope.loading = true;
			batchtraineeService.createBatchTrainee(batchtrainee).then(function (results) {
				$scope.batchtrainees.push(results.data);
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "BatchTrainee_alert", "MsgType": "OK", "MsgText": "BatchTrainee added successfully." });
			}, function (error) {
				MSG({ 'elm': "BatchTrainee_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding batchtrainee!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		}

		//Update BatchTrainee Function 
		function UpdateBatchTrainee(batchtrainee) {
			$scope.loading = true;
			batchtraineeService.updateBatchTrainee(batchtrainee).then(function (results) {
				angular.forEach($scope.batchtrainees, function (value, key) {
					if ($scope.batchtrainees[key].BatchTraineeId === batchtrainee.BatchTraineeId) {
						$scope.batchtrainees[key] = batchtrainee;
						return false;
					}
				});
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "BatchTrainee_alert", "MsgType": "OK", "MsgText": "BatchTrainee updated successfully." });
			}, function (error) {
				MSG({ 'elm': "BatchTrainee_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating batchtrainee!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		//Download Excel
		$scope.ExportTraineeListToExcel = function () {
		 	var qs = $httpParamSerializer($scope.tbfilter);
			$window.location.href = serviceBase + '/Report/ExportTraineeListToExcel?' + qs;
		}


		//Print Div
		$scope.PrintDiv = function () {
			var printContents = document.getElementById('printdiv').innerHTML;
			var popupWin = window.open('', '_blank', 'width=1024,height=800');
			popupWin.document.open();
			var html = '<html><head>'
				+ '<link rel="stylesheet" type="text/css" href="/theme/adminlte2.4.3/css/AdminLTE.css" />'
				+ '<link rel="stylesheet" type="text/css" href="/theme/adminlte2.4.3/bootstrap/dist/css/bootstrap.min.css" />'
				+ '<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />'
				+ '<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />'
				+ '</head><body onload="window.print()">' + printContents + '</body></html>';

			popupWin.document.write(html);
			popupWin.document.close();
		}

		//Datepicker
		$scope.dateOptions = {
			'year-format': "'yy'",
			'show-weeks': false
		};

		// Call BatchTrainee for first time
		$scope.BatchTraineePageInfo = {};
		$scope.tbfilter = batchtraineeService.BatchTraineeEmptyFilter();
		$scope.tbfilter.PageNumber = 1;
		$scope.tbfilter.PageSize = '20';

		//GetBatchTrainees($scope.tbfilter);

	}]);
}());


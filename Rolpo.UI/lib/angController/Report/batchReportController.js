﻿//Created By: Prashant 
//Created On: 02/07/2018 
// Controller for Batch 
// Initialization for Batch 

rolpo_app.constant('ngAuthSettings', {
	apiServiceBaseUri: serviceBase,
	clientId: 'rolpo.com'
});


rolpo_app.requires.push('ui.select');

// Service For Batch 
; (function () {
	'use strict';
	rolpo_app.factory('batchService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

		var serviceBase = ngAuthSettings.apiServiceBaseUri;
		var batchServiceFactory = {};

		//Default Filter 
		var _defaultDDLFilter = function () {
			return {
				PageName: "AddEditBatch",
				FilterList: [{
					DDLName: "TRAINEE",
					Param1: "",
					Param2: "HIDE_DEFAULT"
				}, {
					DDLName: "COORDINATOR",
					Param1: "",
					Param2: "HIDE_DEFAULT"
				}
				]
			};
		};

		//Batch Empty Filter 
		var _batchEmptyFilter = function () {
			return {
				BatchId: 0,
				SearchText: "",
				TraineeId: 0,
				CoordinatorId: 0,
				PageNumber: 1,
				PageSize: 20,
				ShowAll: 0
			};
		};

		// Get DDL List by Filter
		var _getDDLList = function (ddlFilter) {
			return $http({
				url: serviceBase + 'api/Home/LoadDDLs',
				method: "post",
				data: ddlFilter
			});
		};

		// Get Batchs by Filter
		var _getBatchs = function (tbfilter) {
			return $http({
				url: serviceBase + 'api/Batch/GetBatchsListForReport',
				method: "post",
				data: tbfilter
			});
		};
  

		batchServiceFactory.DDLDefaultFilter = _defaultDDLFilter;
		batchServiceFactory.GetDDLByFilter = _getDDLList;
		batchServiceFactory.getBatchs = _getBatchs; 
		batchServiceFactory.BatchEmptyFilter = _batchEmptyFilter;

		return batchServiceFactory;
	}]);
}());


// Controller Starts Here.. 
; (function () {
	'use strict';
	rolpo_app.controller('batchController', ['$scope', '$rootScope', 'batchService', 'modalService', '$uibModal', '$uibModalStack', '$filter', '$httpParamSerializer', '$window', 'ngAuthSettings', function ($scope, $rootScope, batchService, modalService, $uibModal, $uibModalStack, $filter, $httpParamSerializer, $window, ngAuthSettings) {

		// Variables and declarations 

		$scope.loading = true;
		$scope.batchs = [];
		$scope.batch = {};
		$scope.BatchPageInfo = {};

		//Populate DDLs
		var ddlFilter = batchService.DDLDefaultFilter();
		batchService.GetDDLByFilter(ddlFilter).then(function (results) {
			$scope.ddLItems = angular.fromJson(results.data.DDLItems);

			//Get trainees 
			$scope.traineesddl = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "TRAINEE" })[0].Items;


			//coordinator
			$scope.coordDDL = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "COORDINATOR" })[0].Items;

		});

		// Methods

		// Get Batch by Filter

		$scope.GetBatchByFilter = function () {
			GetBatchs($scope.tbfilter);
		};

		// Reset Batch Filter
		$scope.ResetBatchFilter = function () {
			var pageSize = $scope.tbfilter.PageSize;

			$scope.tbfilter = batchService.BatchEmptyFilter();
			$scope.tbfilter.PageSize = pageSize;

			GetBatchs($scope.tbfilter);
		};

		//On Batch Page Changed
		$scope.OnBatchPageChanged = function () {
			GetBatchs($scope.tbfilter);
		};

		//On Page Size Changed
		$scope.OnBatchPageSizeChanged = function () {
			GetBatchs($scope.tbfilter);
		};


		// Cancel  Editing
		$scope.cancelEditing = function () {
			$uibModalStack.dismissAll();
		};

		// Functions 
		// Function to Get Batch
		function GetBatchs(tbfilter) {
			$scope.loading = true;
			$scope.HasTB_Records = false;
			batchService.getBatchs(tbfilter).then(function (results) {
				$scope.batchs = results.data;
				var tmp_page_start = (($scope.tbfilter.PageNumber - 1) * ($scope.tbfilter.PageSize) + 1), tmp_page_end = ($scope.tbfilter.PageNumber) * ($scope.tbfilter.PageSize);
				if (results.data.length > 0) {
					$scope.BatchPageInfo = {
						Has_record: true,
						TotalItems: results.data[0]["TotalCount"],
						PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0,
						PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"]
					};
				} else { $scope.BatchPageInfo = {}; }
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "Batch_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading batchs!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		//Datepicker
		$scope.dateOptions = {
			'year-format': "'yy'",
			'show-weeks': false
		};

		$scope.OpenDate = function (obj, prop) {
			obj[prop] = true;
		}


		// Export to Excel
		$scope.ExportBatchReportToExcel = function () {
			$scope.loading = true;
			var serviceBase = ngAuthSettings.apiServiceBaseUri;
			batchService.GetProductRatePermitReportExcel($scope.tppfilter).then(function (result) {
				var response = result.data;
				window.location = serviceBase + '/Home/DownloadExcel?fileGuid=' + response.FileGuid
                                  + '&filename=' + response.FileName;
				$scope.loading = false;
			}, function (error) {
				MSG({ 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading permit rate report!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		}


		//Export Batch to excel
		$scope.ExportBatchToExcel = function () { 
			var qs = $httpParamSerializer($scope.tbfilter);
		 	$window.location.href = serviceBase+'Report/ExportBatchListToExcel?' + qs;
		} 

		//Print Div
		$scope.PrintDiv = function () {
			var printContents = document.getElementById('printdiv').innerHTML;
			var popupWin = window.open('', '_blank', 'width=1024,height=800');
			popupWin.document.open();
			var html = '<html><head>'
				+ '<link rel="stylesheet" type="text/css" href="/theme/adminlte2.4.3/css/AdminLTE.css" />'
				+ '<link rel="stylesheet" type="text/css" href="/theme/adminlte2.4.3/bootstrap/dist/css/bootstrap.min.css" />'
				+ '<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />'
				+ '<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />'
				+ '</head><body onload="window.print()">' + printContents + '</body></html>';

			popupWin.document.write(html);
			popupWin.document.close();
		}

		// Call Batch for first time
		$scope.BatchPageInfo = {};
		$scope.tbfilter = batchService.BatchEmptyFilter();
		$scope.tbfilter.PageNumber = 1;
		$scope.tbfilter.PageSize = '20';

		GetBatchs($scope.tbfilter);

	}]);
}());


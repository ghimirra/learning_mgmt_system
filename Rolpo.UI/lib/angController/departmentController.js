﻿//Created By: Prashant 
//Created On: 06/09/2016 
// Controller for Department 
// Initialization for Department 

; var rolpo_app = angular.module('RolpoApp', ['ngSanitize', 'ui.select', 'ui.bootstrap']); 

rolpo_app.constant('ngAuthSettings', { 
	apiServiceBaseUri: serviceBase, 
	clientId: 'rolpo.com' 
}); 


// Service For Department 
; (function () { 
	'use strict'; 
	rolpo_app.factory('departmentService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) { 

		var serviceBase = ngAuthSettings.apiServiceBaseUri; 
		var departmentServiceFactory = {}; 

//Default Filter 
		var _defaultDDLFilter = function () { 
			return {
				PageName: "AddEditDepartment", 
				FilterList: [ 
					,{ 
						DDLName: "SUPPLIERS",
						Param1: "LOAD_ALL", 
						//Param2: "HIDE_DEFAULT" 
						Param2: "" 
					} 
				] 
			}; 
		}; 

		//Department Empty Filter 
		var _departmentEmptyFilter = function () { 
			return {
				DepartmentId: 0, 
				DepartmentName: "", 
				SupplierId: 0, 
				PageNumber: 1, 
				PageSize: 20, 
				ShowAll: 0 
			}; 
		}; 

		// Get DDL List by Filter
		var _getDDLList = function (ddlFilter) { 
			return $http({ 
				url: serviceBase + 'api/Home/LoadDDLs', 
				method: "post", 
				data: ddlFilter 
			});
		}; 

		// Get Departments by Filter
		var _getDepartments = function (tdpfilter) { 
			return $http({ 
				url: serviceBase + 'api/Department/GetDepartmentsList', 
				method: "post", 
				data: tdpfilter 
			});
		}; 

		//Create New Department
		var _createDepartment= function (department) {
			var request = $http({
				method: 'post',
				url: serviceBase + 'api/Department/SaveDepartment',
				data: department
			});
			return request;
		}; 

		//Update Department 
		var _updateDepartment= function (department) { 
			var request = $http({ 
				method: "post", 
				url: serviceBase + "api/Department/UpdateDepartment", 
				data: department
			});
			return request;
		};

		//Delete Department
		var _deleteDepartment= function (departmentid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/Department/DeleteDepartment/" + departmentid
			});
			return request;
		}; 

		departmentServiceFactory.DDLDefaultFilter = _defaultDDLFilter; 
		departmentServiceFactory.GetDDLByFilter = _getDDLList; 
		departmentServiceFactory.getDepartments = _getDepartments; 
		departmentServiceFactory.createDepartment= _createDepartment; 
		departmentServiceFactory.updateDepartment= _updateDepartment; 
		departmentServiceFactory.deleteDepartment= _deleteDepartment;
		departmentServiceFactory.DepartmentEmptyFilter = _departmentEmptyFilter;

		return departmentServiceFactory; 
	}]); 
}()); 


// Controller Starts Here.. 
; (function () { 
	'use strict'; 
	rolpo_app.controller('departmentController', ['$scope', '$rootScope', 'departmentService','modalService', '$uibModal', '$uibModalStack',  '$filter', function ($scope, $rootScope, departmentService,modalService, $uibModal, $uibModalStack, $filter) { 

		// Variables and declarations 

		$scope.loading = true;
		$scope.departments = [];
		$scope.department = {};
		$scope.DepartmentPageInfo = {};

		//Populate DDLs
		$scope.suppliers = []; // Suppliers DDL
		var ddlFilter = departmentService.DDLDefaultFilter();

		departmentService.GetDDLByFilter(ddlFilter).then(function (results) { 
			 $scope.ddLItems = angular.fromJson(results.data.DDLItems); 

			//Get suppliers 
			 $scope.suppliers = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "SUPPLIERS" })[0].Items;
			
		}); 

		// Methods

		// Get Department by Filter

		$scope.GetDepartmentByFilter = function () {
			$scope.departments = GetDepartments($scope.tdpfilter);
		};

		// Reset Filter
		$scope.ResetDepartmentFilter = function () {
			var pageSize = $scope.tdpfilter.PageSize;

			$scope.tdpfilter = departmentService.DepartmentEmptyFilter(); 
			$scope.tdpfilter.PageSize = pageSize;

			$scope.departments = GetDepartments($scope.tdpfilter);
		};

		//On Page Changed
		$scope.OnDepartmentPageChanged = function () {
			GetDepartments($scope.tdpfilter);
		};

		//On Page Size Changed
		$scope.OnDepartmentPageSizeChanged = function () {
			GetDepartments($scope.tdpfilter);
		};

		// Open Window for Saving new Department
		$scope.OpenDepartmentSaveDialog = function () {
			$scope.department= { DepartmentId: 0 };
			MSG({}); //Init
			$scope.departmentActionTitle = "Add New Department";
			var modalInstance = $uibModal.open({ 
				animation: true, 
				scope:$scope, 
				templateUrl: 'customUpdateDepartment', 
				backdrop: 'static', 
				keyboard: false, 
				modalFade: true, 
				size: '' 
			}); 

		};

		// Open Window for updating Department
		$scope.OpenDepartmentUpdateDialog = function (DepartmentId) {
			var tdpfilter = departmentService.DepartmentEmptyFilter();
			tdpfilter.DepartmentId= DepartmentId;
			$scope.loading = true;
			MSG({}); //Init

			departmentService.getDepartments(tdpfilter).then(function (results) {
				if (results.data.length != 1) {
					$scope.loading = false;
					$scope.error = "An Error has occured while loading departments!";
					return;
				}
				$scope.department= results.data[0];
				$scope.departmentActionTitle = "Update Department";

				var modalInstance = $uibModal.open({
					animation: true, 
					scope:$scope, 
					templateUrl: 'customUpdateDepartment', 
					backdrop: 'static', 
					keyboard: false, 
					modalFade: true, 
					size: '' 
				}); 
				$scope.loading = false; 
			}, function (error) {
				$scope.error = "An Error has occured while loading departments!";
				$scope.loading = false;
			});

		};

		//Update Department
		$scope.CreateUpdateDepartment= function (DepartmentId) {
			if (DepartmentId == 0) { CreateNewDepartment($scope.department); } else { UpdateDepartment($scope.department); }
		};

		//Delete Department
		$scope.DeleteDepartment= function (DepartmentId) {
			MSG({}); //Init
			var modalOptions = { 
				closeButtonText: 'Cancel', 
				actionButtonText: 'Delete Department', 
				headerText: 'Delete Item', 
				bodyText: 'Are you sure you want to delete this?' 
			}; 
			modalService.showModal({}, modalOptions).then(function (result) {
				$scope.loading = true;
				departmentService.deleteDepartment(DepartmentId).then(function (results) {
					angular.forEach($scope.departments, function (value, key) {
						if ($scope.departments[key].DepartmentId=== DepartmentId) {
							$scope.departments.splice(key, 1);
							return false;
						}
					});

					$scope.loading = false;
					MSG({ "MsgType": "OK", "MsgText": "Department deleted successfully." });
				}, function (error) {
					$scope.error = "An Error has occured while deleting Department! " + error;
					$scope.loading = false;
				});
			});
		};

		// Cancel  Editing
		$scope.cancelEditing = function () {
			$uibModalStack.dismissAll(); 
		};

		// Functions 
		// Function to Get Department
		function GetDepartments(tdpfilter) {
			$scope.loading = true;
			$scope.HasTD_Records = false;
			departmentService.getDepartments(tdpfilter).then(function (results) {
				$scope.departments = results.data;
				var tmp_page_start = (($scope.tdpfilter.PageNumber - 1) * ($scope.tdpfilter.PageSize) + 1), tmp_page_end = ($scope.tdpfilter.PageNumber) * ($scope.tdpfilter.PageSize);
				if (results.data.length > 0) { 
					$scope.DepartmentPageInfo = { 
						Has_record: true, 
						TotalItems: results.data[0]["TotalCount"], 
						PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0, 
						PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"] ,
                        MaxSize:5
					}; 
				} else { $scope.StaffPageInfo = {}; } 
				$scope.loading = false;
			}, function (error) {
				$scope.error = "An Error has occured while loading departments!";
				$scope.loading = false;
			});
		};

		// Create New Department Function 
		function CreateNewDepartment(department) {
		    $scope.department_loading = true;
			departmentService.createDepartment(department).then(function (results) {
				$scope.departments.push(results.data);
				$scope.department_loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "Department_alert", 'MsgType': 'OK', 'MsgText': 'Department added successfully' });
				
			}, function (error) {
			    MSG({ 'elm': "Department_alert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding department!', 'MsgAsModel': error.data });
				
				$scope.department_loading = false;
			});
		}

		//Update Department Function 
		function UpdateDepartment(department) {
			$scope.department_loading = true;			
			departmentService.updateDepartment(department).then(function (results) {
				angular.forEach($scope.departments, function (value, key) { 
					if ($scope.departments[key].DepartmentId === department.DepartmentId) { 
						$scope.departments[key] = department; 
						return false; 
					}
				});
				$scope.department_loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "Department_alert", 'MsgType': 'OK', 'MsgText': 'Department updated successfully' });
				
			}, function (error) {
			    MSG({ 'elm': "Department_alert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating department!', 'MsgAsModel': error.data });
			    
				$scope.department_loading = false;
			});
		};

		//Datepicker
		$scope.dateOptions = {
			'year-format': "'yy'",
			'show-weeks': false
		};

		// Call Department for first time
		 $scope.DepartmentPageInfo = {};
		$scope.tdpfilter = departmentService.DepartmentEmptyFilter();
		$scope.tdpfilter.PageNumber = 1;
		$scope.tdpfilter.PageSize = '10';

		GetDepartments($scope.tdpfilter);

	}]);
}());


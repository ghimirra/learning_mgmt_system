﻿//Created By: Prashant 
//Created On: 22/06/2018 
// Controller for Course 
// Initialization for Course 

rolpo_app.constant('ngAuthSettings', { 
	apiServiceBaseUri: serviceBase, 
	clientId: 'rolpo.com' 
}); 
 

rolpo_app.filter("trust", ['$sce', function ($sce) {
	return function (htmlCode) {
		return $sce.trustAsHtml(htmlCode);
	}
}]);


rolpo_app.directive('customOnChange', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var onChangeHandler = scope.$eval(attrs.customOnChange);
            element.on('change', onChangeHandler);
            element.on('$destroy', function () {
                element.off();
            });

        }
    };
});

// Service For Course 
; (function () { 
	'use strict'; 
	rolpo_app.factory('courseViewService', ['$http', 'ngAuthSettings', '$q', function ($http, ngAuthSettings, $q) {

		var serviceBase = ngAuthSettings.apiServiceBaseUri; 
		var courseViewServiceFactory = {}; 

		//Default Filter 
		var _defaultDDLFilter = function () { 
			return {
				PageName: "AddEditCourse", 
				FilterList: [
					{
					    DDLName: "COURSES",
					    Param1: "",
					    Param2: "HIDE_DEFAULT"
					}				
                    , {
                        DDLName: "MEDIAS",
                        Param1: "",
                        Param2: "HIDE_DEFAULT"
                    }
				]
			}; 
		}; 
 
		// Get DDL List by Filter
		var _getDDLList = function (ddlFilter) {

		    
			return $http({ 
				url: serviceBase + 'api/Home/LoadDDLs', 
				method: "post", 
				data: ddlFilter 
			});
		}; 

		// Get Courses by Filter
		var _getCourseById = function (id) { 
			return $http({ 
				url: serviceBase + 'api/Course/GetCourseById',
				method: "get", 
				params: {Id:id}
			});
		}; 

		 

		//Delete Course
		var _deleteCourse= function (courseid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/Course/DeleteCourse/" + courseid
			});
			return request;
		}; 

		//REGION COURSE MODULES

		//Modules Empty Filter 
		var _modulesEmptyFilter = function () {
			return {
				ModuleId: 0,
				ModuleTitle: "",
				AssociatedCourseId: courseId,
				PageNumber: 1,
				PageSize: 20,
				ShowAll: 0
			};
		};

		// Get Modules by Filter
		var _getModules = function (tmfilter) {
			return $http({
				url: serviceBase + 'api/Modules/GetModulesList',
				method: "post",
				data: tmfilter
			});
		};

		 

		//Delete Modules
		var _deleteModules = function (moduleid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/Modules/DeleteModule/" + moduleid
			});
			return request;
		};
	    // ./ COURSE MODULES


	    //COURSE MEDIA 


	    //CourseMedia Empty Filter 
		var _coursemediaEmptyFilter = function () {
		    return {
		        CourseMediaId: 0,
		        CourseId: 0,
		        PageNumber: 1,
		        PageSize: 20,
		        ShowAll: 1
		    };
		};



	    // Get CourseMedias by Filter
		var _getCourseMedias = function (tcfilter) {
		    return $http({
		        url: serviceBase + 'api/CourseMedia/GetCourseMediasList',
		        method: "post",
		        data: tcfilter
		    });
		};

	    //Create New CourseMedia
		var _createCourseMedia = function (coursemedia) {
		    var request = $http({
		        method: 'post',
		        url: serviceBase + 'api/CourseMedia/SaveCourseMedia',
		        data: coursemedia
		    });
		    return request;
		};

	    //Update CourseMedia 
		var _updateCourseMedia = function (coursemedia) {
		    var request = $http({
		        method: "post",
		        url: serviceBase + "api/CourseMedia/UpdateCourseMedia",
		        data: coursemedia
		    });
		    return request;
		};

	    //Delete CourseMedia
		var _deleteCourseMedia = function (coursemediaid) {
		    var request = $http({
		        method: "delete",
		        url: serviceBase + "api/CourseMedia/DeleteCourseMedia/" + coursemediaid
		    });
		    return request;
		};

	
		var _uploadCourseFile = function (file, description) {
		    var formData = new FormData();
		    formData.append("file", file);
		    //We can send more data to server using append         
		    formData.append("description", "asfsafsa");

		    var defer = $q.defer();
		    $http.post(serviceBase + "/Courses/SaveCourseImage", formData,
                {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                })
            .success(function (d) {
                defer.resolve(d);
            })
            .error(function () {
                defer.reject("File Upload Failed!");
            });

		    return defer.promise;
		}
        // ./ COURSE MEDIA

		courseViewServiceFactory.DDLDefaultFilter = _defaultDDLFilter; 
		courseViewServiceFactory.GetDDLByFilter = _getDDLList; 
		courseViewServiceFactory.getCourseById = _getCourseById; 
		courseViewServiceFactory.deleteCourse= _deleteCourse; 


 		courseViewServiceFactory.getModules = _getModules; 
		courseViewServiceFactory.deleteModules = _deleteModules;
		courseViewServiceFactory.ModulesEmptyFilter = _modulesEmptyFilter;


		courseViewServiceFactory.getCourseMedias = _getCourseMedias;
		courseViewServiceFactory.createCourseMedia = _createCourseMedia;
		courseViewServiceFactory.updateCourseMedia = _updateCourseMedia;
		courseViewServiceFactory.deleteCourseMedia = _deleteCourseMedia;
		courseViewServiceFactory.CourseMediaEmptyFilter = _coursemediaEmptyFilter;

		courseViewServiceFactory.UploadCourseFile = _uploadCourseFile;

		return courseViewServiceFactory; 
	}]); 
}()); 


// Controller Starts Here.. 
; (function () { 
	'use strict'; 
	rolpo_app.controller('courseViewController', ['$scope', '$rootScope', 'courseViewService', 'modalService', '$uibModal', '$uibModalStack', '$filter', '$timeout', function ($scope, $rootScope, courseViewService, modalService, $uibModal, $uibModalStack, $filter, $timeout) {

		// Variables and declarations 
		 
		$scope.Course = {};
		$scope.CourseId = courseId;
		$scope.showUrl = false;

	    //course media variables
		//$scope.showuploadBtn = false;
		$scope.SelectedFileForUpload = null;

		


	    //File Select event 
		$scope.selectFileforUpload = function (file) {

		    $scope.showuploadBtn = true;
		    $scope.SelectedFileForUpload = file[0];
		    $timeout(function () {
		        angular.element('#myselector').triggerHandler('click');
		    });
		   
		}
	    //----------------------------------------------------------------------------------------
		$scope.SaveFile = function () {	
		 
		        courseViewService.UploadCourseFile($scope.SelectedFileForUpload, $scope.FileDescription).then(function (d) {
		            if (d.ImagePath.length > 0) {
		                $scope.coursemedia.MediaURL = baseUrl + d.ImagePath.substr(1);
		                $scope.showuploadBtn = false;
		                MSG({ 'elm': "CourseMedia_AddEditAlert", 'MsgType': 'SUCCESS', 'MsgText': 'File Uploaded Successfully'});
		                
		            }
		           
		        }, function (e) {
		            console.log(e);
		            $scope.showuploadBtn = false;
		        });
		
		   
		};
		$scope.showFileOption = true;

		$scope.AddUrl = function () {
		    $scope.showFileOption = false;
		    $scope.coursemedia.MediaURL = '';
		      		    		    
		}

		$scope.AddFile = function () {
		    $scope.showFileOption = true;
		}

        // ./ course media variables

		function Init() {

			////Populate DDLs
			var ddlFilter = courseViewService.DDLDefaultFilter();
			courseViewService.GetDDLByFilter(ddlFilter).then(function (results) {
			    $scope.ddLItems = angular.fromJson(results.data.DDLItems);
			    //Get cityid 
			    $scope.courses = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "COURSES" })[0].Items;			    
			    $scope.medias = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "MEDIAS" })[0].Items;

				
			});

			//ONCE DDLS are populated, Load Course by Id

			GetCoursesById();

			//LoadMOdules
			GetModules($scope.tmfilter);			

		}

		

		// Methods 

		//Delete Course
		$scope.DeleteCourse= function () {
			MSG({}); //Init
			var modalOptions = { 
				closeButtonText: 'Cancel', 
				actionButtonText: 'Delete Course', 
				headerText: 'Delete Item', 
				bodyText: 'Are you sure you want to delete this?' 
			}; 
			modalService.showModal({}, modalOptions).then(function (result) {
				$scope.loading_course = true;
				courseViewService.deleteCourse($scope.Course.CourseId).then(function (results) {
					  
					$scope.loading_course = false;
					MSG({ 'elm': "Course_alert", "MsgType": "OK", "MsgText": "Course deleted successfully." });
				}, function (error) {
					MSG({ 'elm': "Course_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting courses!', 'MsgAsModel': error.data });
					$scope.loading_course = false;
				});
			});
		};

		// Cancel  Editing
		$scope.cancelEditing = function () {
			$uibModalStack.dismissAll(); 
		};

		// Functions 
		// Function to Get Course
		function GetCoursesById() {
			$scope.loadiloading_courseng = true;
			courseViewService.getCourseById($scope.CourseId).then(function (results) {
				$scope.Course = results.data;
				JSONToObj($scope.Course);
				$scope.tcfilter = courseViewService.CourseMediaEmptyFilter();
				$scope.tcfilter.CourseId = $scope.Course.CourseId;
				$scope.tcfilter.PageSize = "20";
				GetCourseMedias($scope.tcfilter);
				$scope.loading_course = false;
			}, function (error) {
				MSG({ 'elm': "Course_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading course!', 'MsgAsModel': error.data });
				$scope.loading_course = false;
			});
		};
		 
		 

		//JSON To Object
		function JSONToObj(obj) {
			obj.TraineeLevels = GETJ(obj.TraineeLevelsJSON);
		};


		//Datepicker
		$scope.dateOptions = {
			'year-format': "'yy'",
			'show-weeks': false
		};
		 
	

		//REGION COURSE MODULES

		$scope.modules = []; 
		 
		// Methods

		// Get Modules by Filter

		$scope.GetModulesByFilter = function () {
			GetModules($scope.tmfilter);
		};

		// Reset Modules Filter
		$scope.ResetModulesFilter = function () {
			var pageSize = $scope.tmfilter.PageSize;

			$scope.tmfilter = courseViewService.ModulesEmptyFilter();
			$scope.tmfilter.PageSize = pageSize;

			GetModules($scope.tmfilter);
		};

	    

		//Delete Modules
		$scope.DeleteModules = function (ModuleId) {
			MSG({}); //Init
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete Modules',
				headerText: 'Delete Item',
				bodyText: 'Are you sure you want to delete this?'
			};
			modalService.showModal({}, modalOptions).then(function (result) {
				$scope.loading = true;
				courseViewService.deleteModules(ModuleId).then(function (results) {
					angular.forEach($scope.modules, function (value, key) {
						if ($scope.modules[key].ModuleId === ModuleId) {
							$scope.modules.splice(key, 1);
							return false;
						}
					});

					$scope.loading = false;
					MSG({ 'elm': "Modules_alert", "MsgType": "OK", "MsgText": "Modules deleted successfully." });
				}, function (error) {
					MSG({ 'elm': "Modules_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting modules!', 'MsgAsModel': error.data });
					$scope.loading = false;
				});
			});
		};


		// Functions 
		// Function to Get Modules
		function GetModules(tmfilter) {
			$scope.loading = true;
			$scope.HasTM_Records = false;
			courseViewService.getModules(tmfilter).then(function (results) {
				$scope.modules = results.data;
				var tmp_page_start = (($scope.tmfilter.PageNumber - 1) * ($scope.tmfilter.PageSize) + 1), tmp_page_end = ($scope.tmfilter.PageNumber) * ($scope.tmfilter.PageSize);
				if (results.data.length > 0) {
					$scope.ModulePageInfo = {
						Has_record: true,
						TotalItems: results.data[0]["TotalCount"],
						PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0,
						PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"]
					};
				} else { $scope.ModulePageInfo = {}; }
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "Modules_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading modules!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		  

		// Call Modules for first time
		$scope.ModulePageInfo = {};
		$scope.tmfilter = courseViewService.ModulesEmptyFilter();
		$scope.tmfilter.PageNumber = 1;
		$scope.tmfilter.PageSize = '20'; 

		// ./COURSE MODULES


	    // .COURSE MEDIAS

		$scope.coursemedias = [];
		$scope.coursemedia = {}; 
		
		$scope.OpenAddNewTrainersWindow = function () {
		    console.log($scope.Course);
		}

	    // Get CourseMedia by Filter

		$scope.GetCourseMediaByFilter = function () {
		    GetCourseMedias($scope.tcfilter);
		};

	    // Reset CourseMedia Filter
		$scope.ResetCourseMediaFilter = function () {
		    var pageSize = $scope.tcfilter.PageSize;

		    $scope.tcfilter = courseViewService.CourseMediaEmptyFilter();
		    $scope.tcfilter.PageSize = pageSize;

		    GetCourseMedias($scope.tcfilter);
		};

	    //On CourseMedia Page Changed
		$scope.OnCourseMediaPageChanged = function () {
		    GetCourseMedias($scope.tcfilter);
		};

	    //On Page Size Changed
		$scope.OnCourseMediaPageSizeChanged = function () {
		    GetCourseMedias($scope.tcfilter);
		};

	    // Open Window for Saving new CourseMedia
		$scope.OpenCourseMediaSaveDialog = function () {
		    $scope.coursemedia = { CourseMediaId: 0, CourseId: $scope.Course.CourseId };
		    //Default Filter 
		    var _defaultDDLFilter = {

		            PageName: "AddEditCourse",
		            FilterList: [
                       
                        {
                            DDLName: "MODULES",
                            Param1: $scope.Course.CourseId*1,
                            Param2: "HIDE_DEFAULT"
                        }
                      
		            ]
		        }		    
		    courseViewService.GetDDLByFilter(_defaultDDLFilter).then(function (results) {
		        $scope.ddLItems = angular.fromJson(results.data.DDLItems);
		        console.log($scope.ddLItems);
		        $scope.topics = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "MODULES" })[0].Items;
		    });
		    MSG({}); //Init
		    $scope.coursemediaActionTitle = "Add New Media";
		    var modalInstance = $uibModal.open({
		        animation: true,
		        scope: $scope,
		        templateUrl: 'customUpdateCourseMedia',
		        backdrop: 'static',
		        keyboard: false,
		        modalFade: true,
		        size: ''
		    });

		};

		

		$scope.fileSelected = function (element) {
		    var myFileSelected = element.files[0];
		    courseViewService.UploadCourseFile(myFileSelected).then(function (results) {
		        console.log(results.data);
		    })
		    
		};

	    // Open Window for updating CourseMedia
		$scope.OpenCourseMediaUpdateDialog = function (CourseMediaId) {
		    var tcfilter = courseViewService.CourseMediaEmptyFilter();
		    tcfilter.CourseMediaId = CourseMediaId;
		    var _defaultDDLFilter = {

		        PageName: "AddEditCourse",
		        FilterList: [

                    {
                        DDLName: "MODULES",
                        Param1: $scope.Course.CourseId * 1,
                        Param2: "HIDE_DEFAULT"
                    }

		        ]
		    }
		    courseViewService.GetDDLByFilter(_defaultDDLFilter).then(function (results) {
		        $scope.ddLItems = angular.fromJson(results.data.DDLItems);
		        console.log($scope.ddLItems);
		        $scope.topics = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "MODULES" })[0].Items;
		    });
		    $scope.loading = true;
		    MSG({}); //Init

		    courseViewService.getCourseMedias(tcfilter).then(function (results) {
		        //console.log(results.data);
		        //if (results.data.length != 1) {
		        //    $scope.loading = false;
		        //    MSG({ 'elm': "CourseMedia_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading coursemedias!', 'MsgAsModel': error.data });
		        //    return;
		        //}
		        $scope.coursemedia = results.data[0];
		        
		        if ($scope.coursemedia.MediaURL.indexOf(serviceBase) !== -1) {
		        
		            $scope.coursemedia.hasfile = true;
		        }
		        else
		        {
		            $scope.coursemedia.hasfile = false;
		        }
		        $scope.coursemediaActionTitle = "Update CourseMedia";

		        var modalInstance = $uibModal.open({
		            animation: true,
		            scope: $scope,
		            templateUrl: 'customUpdateCourseMedia',
		            backdrop: 'static',
		            keyboard: false,
		            modalFade: true,
		            size: ''
		        });
		        $scope.loading = false;
		    }, function (error) {
		        MSG({ 'elm': "CourseMedia_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading coursemedias!', 'MsgAsModel': error.data });
		        $scope.loading = false;
		    });

		};

	    //Update CourseMedia
		$scope.CreateUpdateCourseMedia = function (CourseMediaId) {
		    if (CourseMediaId == 0) { CreateNewCourseMedia($scope.coursemedia); } else { UpdateCourseMedia($scope.coursemedia); }
		};

	    //Delete CourseMedia
		$scope.DeleteCourseMedia = function (CourseMediaId) {
		    MSG({}); //Init
		    var modalOptions = {
		        closeButtonText: 'Cancel',
		        actionButtonText: 'Delete CourseMedia',
		        headerText: 'Delete Item',
		        bodyText: 'Are you sure you want to delete this?'
		    };
		    modalService.showModal({}, modalOptions).then(function (result) {
		        $scope.loading = true;
		        courseViewService.deleteCourseMedia(CourseMediaId).then(function (results) {
		            angular.forEach($scope.coursemedias, function (value, key) {
		                if ($scope.coursemedias[key].CourseMediaId === CourseMediaId) {
		                    $scope.coursemedias.splice(key, 1);
		                    return false;
		                }
		            });

		            $scope.loading = false;
		            MSG({ 'elm': "CourseMedia_alert", "MsgType": "OK", "MsgText": "CourseMedia deleted successfully." });
		        }, function (error) {
		            MSG({ 'elm': "CourseMedia_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting coursemedias!', 'MsgAsModel': error.data });
		            $scope.loading = false;
		        });
		    });
		};

	    // Cancel  Editing
		$scope.cancelEditing = function () {
		    $uibModalStack.dismissAll();
		};

	    // Functions 
	    // Function to Get CourseMedia
		function GetCourseMedias(tcfilter) {
		    $scope.loading = true;
		    $scope.HasTC_Records = false;
		    courseViewService.getCourseMedias(tcfilter).then(function (results) {
		        $scope.coursemedias = results.data;
		        var tmp_page_start = (($scope.tcfilter.PageNumber - 1) * ($scope.tcfilter.PageSize) + 1), tmp_page_end = ($scope.tcfilter.PageNumber) * ($scope.tcfilter.PageSize);
		        
		        $scope.loading = false;
		    }, function (error) {
		        MSG({ 'elm': "CourseMedia_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading coursemedias!', 'MsgAsModel': error.data });
		        $scope.loading = false;
		    });
		};

	    // Create New CourseMedia Function 
		function CreateNewCourseMedia(coursemedia) {
		    $scope.loading = true;
		    courseViewService.createCourseMedia(coursemedia).then(function (results) {
		        $scope.coursemedias.push(results.data);
		        $scope.loading = false;
		        $uibModalStack.dismissAll();
		        MSG({ 'elm': "CourseMedia_alert", "MsgType": "OK", "MsgText": "CourseMedia added successfully." });
		    }, function (error) {
		        MSG({ 'elm': "CourseMedia_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding coursemedia!', 'MsgAsModel': error.data });
		        $scope.loading = false;
		    });
		}

	    //Update CourseMedia Function 
		function UpdateCourseMedia(coursemedia) {
		    $scope.loading = true;
		    courseViewService.updateCourseMedia(coursemedia).then(function (results) {
		        angular.forEach($scope.coursemedias, function (value, key) {
		            if ($scope.coursemedias[key].CourseMediaId === coursemedia.CourseMediaId) {
		                $scope.coursemedias[key] = coursemedia;
		                return false;
		            }
		        });
		        $scope.loading = false;
		        $uibModalStack.dismissAll();
		        MSG({ 'elm': "CourseMedia_alert", "MsgType": "OK", "MsgText": "CourseMedia updated successfully." });
		    }, function (error) {
		        MSG({ 'elm': "CourseMedia_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating coursemedia!', 'MsgAsModel': error.data });
		        $scope.loading = false;
		    });
		};

	    //Datepicker
		$scope.dateOptions = {
		    'year-format': "'yy'",
		    'show-weeks': false
		};

	    // Call CourseMedia for first time 
		$scope.tcfilter = courseViewService.CourseMediaEmptyFilter();
		$scope.tcfilter.PageNumber = 1;
		$scope.tcfilter.PageSize = '20';

        // ./ COURSE MEDIAS


		//Call Init
		Init();


	}]);
}());


rolpo_app.directive('ckEditor', function () {
	return {
		require: '?ngModel',
		link: function (scope, elm, attr, ngModel) {
			var ck = CKEDITOR.replace(elm[0], {
				allowedContent: true
			});
			if (!ngModel) return;
			ck.on('instanceReady', function () {
				ck.setData(ngModel.$viewValue);
			});
			function updateModel() {
				scope.$apply(function () {
					ngModel.$setViewValue(ck.getData());
				});
			}
			ck.on('change', updateModel);
			ck.on('key', updateModel);
			ck.on('dataReady', updateModel);

			ngModel.$render = function (value) {
				ck.setData(ngModel.$viewValue);
			};
		}
	};
});


rolpo_app.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}]);
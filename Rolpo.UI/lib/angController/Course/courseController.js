﻿//Created By: Prashant 
//Created On: 22/06/2018 
// Controller for Course 
// Initialization for Course 

rolpo_app.constant('ngAuthSettings', { 
	apiServiceBaseUri: serviceBase, 
	clientId: 'rolpo.com' 
}); 
rolpo_app.filter("trust", ['$sce', function ($sce) {
    return function (htmlCode) {
        return $sce.trustAsHtml(htmlCode);
    }
}]);

rolpo_app.requires.push('ngCkeditor');
rolpo_app.requires.push('ui.select');


// Service For Course 
; (function () { 
	'use strict'; 
	rolpo_app.factory('courseService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) { 

		var serviceBase = ngAuthSettings.apiServiceBaseUri; 
		var courseServiceFactory = {}; 

		//Default Filter 
		var _defaultDDLFilter = function () { 
			return {
				PageName: "AddEditCourse", 
				FilterList: [ 
					{ 
						DDLName: "COURSECATEGORY", 
						Param1: "", 
						Param2: "HIDE_DEFAULT" 
					} 
					,{ 
						DDLName: "SUBCATEGORY", 
						Param1: "", 
						Param2: "HIDE_DEFAULT" 
					} 
					,{ 
						DDLName: "COURSELEVEL", 
						Param1: "", 
						Param2: "HIDE_DEFAULT" 
					} 
					,{ 
						DDLName: "PERIODTYPE", 
						Param1: "", 
						Param2: "HIDE_DEFAULT" 
					}, {
						DDLName: "TRAINEELEVELS",
						Param1: "",
						Param2: "HIDE_DEFAULT"
					}
				] 
			}; 
		}; 

		//Course Empty Filter 
		var _courseEmptyFilter = function () { 
			return {
				CourseId: 0, 
				CourseFullName: "", 
				PageNumber: 1, 
				PageSize: 20, 
				ShowAll: 0 
			}; 
		}; 

		// Get DDL List by Filter
		var _getDDLList = function (ddlFilter) { 
			return $http({ 
				url: serviceBase + 'api/Home/LoadDDLs', 
				method: "post", 
				data: ddlFilter 
			});
		}; 

		// Get Courses by Filter
		var _getCourses = function (tcfilter) { 
			return $http({ 
				url: serviceBase + 'api/Course/GetCoursesList', 
				method: "post", 
				data: tcfilter 
			});
		}; 

		//Create New Course
		var _createCourse= function (course) {
			var request = $http({
				method: 'post',
				url: serviceBase + 'api/Course/SaveCourse',
				data: course
			});
			return request;
		}; 

		//Update Course 
		var _updateCourse= function (course) { 
			var request = $http({ 
				method: "post", 
				url: serviceBase + "api/Course/UpdateCourse", 
				data: course
			});
			return request;
		};

		//Delete Course
		var _deleteCourse= function (courseid) {
			var request = $http({
				method: "delete",
				url: serviceBase + "api/Course/DeleteCourse/" + courseid
			});
			return request;
		}; 

		courseServiceFactory.DDLDefaultFilter = _defaultDDLFilter; 
		courseServiceFactory.GetDDLByFilter = _getDDLList; 
		courseServiceFactory.getCourses = _getCourses; 
		courseServiceFactory.createCourse= _createCourse; 
		courseServiceFactory.updateCourse= _updateCourse; 
		courseServiceFactory.deleteCourse= _deleteCourse;
		courseServiceFactory.CourseEmptyFilter = _courseEmptyFilter;

		return courseServiceFactory; 
	}]); 
}()); 


// Controller Starts Here.. 
; (function () { 
	'use strict'; 
	rolpo_app.controller('courseController', ['$scope', '$rootScope', 'courseService','modalService', '$uibModal', '$uibModalStack',  '$filter', function ($scope, $rootScope, courseService,modalService, $uibModal, $uibModalStack, $filter) { 

		// Variables and declarations 

		$scope.loading = true;
		$scope.courses = [];
		$scope.course = {};
		$scope.CoursePageInfo = {};

		//Populate DDLs
		var ddlFilter = courseService.DDLDefaultFilter(); 
		courseService.GetDDLByFilter(ddlFilter).then(function (results) { 
			$scope.ddLItems = angular.fromJson(results.data.DDLItems); 

		
			//Get course category 
			$scope.coursecategoryddl = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "COURSECATEGORY" })[0].Items; 
			
			//Get sub category 
			$scope.subcategoryddl = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "SUBCATEGORY" })[0].Items; 
			
			//Get course level 
			$scope.courselevelddl = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "COURSELEVEL" })[0].Items; 
			
			//Get period type 
			$scope.periodtypeddl = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "PERIODTYPE" })[0].Items;
			 
			//Get Trainee Levels
			$scope.traineelevelsddl = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "TRAINEELEVELS" })[0].Items;

		}); 

		// Methods

	    //load course subcategory
		$scope.OnCategoryChanged = function (cId) {
		    $scope.subcategoryddl = [];
		    var ddlFilter =
                 {
                     PageName: "AddEditCourse",
                     FilterList: [
                         {
                             DDLName: "SUBCATEGORYBYID",
                             Param1: cId * 1,
                             Param2:""
                         }]
                 }
		    courseService.GetDDLByFilter(ddlFilter).then(function (results) { 
		        $scope.ddLItems = angular.fromJson(results.data.DDLItems); 

		        //Get sub category 
		        $scope.subcategoryddl = $filter('filter')($scope.ddLItems, function (d) { return d.DDLName === "SUBCATEGORYBYID" })[0].Items;
		    }, function (error) {
		        console.log(error.data);
		    })
		}

		// Get Course by Filter

		$scope.GetCourseByFilter = function () {
			GetCourses($scope.tcfilter);
		};

		// Reset Course Filter
		$scope.ResetCourseFilter = function () {
			var pageSize = $scope.tcfilter.PageSize;

			$scope.tcfilter = courseService.CourseEmptyFilter(); 
			$scope.tcfilter.PageSize = pageSize;

			GetCourses($scope.tcfilter);
		};

		//On Course Page Changed
		$scope.OnCoursePageChanged = function () {
			GetCourses($scope.tcfilter);
		};

		//On Page Size Changed
		$scope.OnCoursePageSizeChanged = function () {
			GetCourses($scope.tcfilter);
		};

		// Open Window for Saving new Course
		$scope.OpenCourseSaveDialog = function () {
			$scope.course= { CourseId: 0 };
			MSG({}); //Init
			$scope.courseActionTitle = "Add New Course";
			var modalInstance = $uibModal.open({ 
				animation: true, 
				scope:$scope, 
				templateUrl: 'customUpdateCourse', 
				backdrop: 'static', 
				keyboard: false, 
				modalFade: true, 
				size: '' 
			}); 

		};

		// Open Window for updating Course
		$scope.OpenCourseUpdateDialog = function (CourseId) {
			var tcfilter = courseService.CourseEmptyFilter();
			tcfilter.CourseId= CourseId;
			$scope.loading = true;
			MSG({}); //Init

			courseService.getCourses(tcfilter).then(function (results) {
				if (results.data.length != 1) {
					$scope.loading = false;
					MSG({ 'elm': "Course_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading courses!', 'MsgAsModel': error.data });
					return;
				}

				$scope.course = results.data[0];
				JSONToObj($scope.course);

				$scope.courseActionTitle = "Update Course";

				var modalInstance = $uibModal.open({
					animation: true, 
					scope:$scope, 
					templateUrl: 'customUpdateCourse', 
					backdrop: 'static', 
					keyboard: false, 
					modalFade: true, 
					size: '' 
				}); 
				$scope.loading = false; 
			}, function (error) {
				MSG({ 'elm': "Course_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading courses!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});

		};

		//Update Course
		$scope.CreateUpdateCourse = function (CourseId) {
			$scope.course.TraineeLevelsJSON = angular.toJson($scope.course.TraineeLevels);
			if (CourseId == 0) { CreateNewCourse($scope.course); } else { UpdateCourse($scope.course); }
		};

		//Delete Course
		$scope.DeleteCourse= function (CourseId) {
			MSG({}); //Init
			var modalOptions = { 
				closeButtonText: 'Cancel', 
				actionButtonText: 'Delete Course', 
				headerText: 'Delete Item', 
				bodyText: 'Are you sure you want to delete this?' 
			}; 
			modalService.showModal({}, modalOptions).then(function (result) {
				$scope.loading = true;
				courseService.deleteCourse(CourseId).then(function (results) {
					angular.forEach($scope.courses, function (value, key) {
						if ($scope.courses[key].CourseId=== CourseId) {
							$scope.courses.splice(key, 1);
							return false;
						}
					});

					$scope.loading = false;
					MSG({ 'elm': "Course_alert", "MsgType": "OK", "MsgText": "Course deleted successfully." });
				}, function (error) {
					MSG({ 'elm': "Course_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while deleting courses!', 'MsgAsModel': error.data });
					$scope.loading = false;
				});
			});
		};

		// Cancel  Editing
		$scope.cancelEditing = function () {
			$uibModalStack.dismissAll(); 
		};

		// Functions 
		// Function to Get Course
		function GetCourses(tcfilter) {
			$scope.loading = true;
			$scope.HasTC_Records = false;
			courseService.getCourses(tcfilter).then(function (results) {
				$scope.courses = results.data;

				angular.forEach($scope.courses, function (obj) {
					JSONToObj(obj);
				});

				var tmp_page_start = (($scope.tcfilter.PageNumber - 1) * ($scope.tcfilter.PageSize) + 1), tmp_page_end = ($scope.tcfilter.PageNumber) * ($scope.tcfilter.PageSize);
				if (results.data.length > 0) { 
					$scope.CoursePageInfo = { 
						Has_record: true, 
						TotalItems: results.data[0]["TotalCount"], 
						PageStart: (results.data[0]["TotalCount"] > 0) ? tmp_page_start : 0, 
						PageEnd: tmp_page_end < results.data[0]["TotalCount"] ? tmp_page_end : results.data[0]["TotalCount"] 
					}; 
				} else { $scope.CoursePageInfo = {}; } 
				$scope.loading = false;
			}, function (error) {
				MSG({ 'elm': "Course_alert", 'MsgType': 'ERROR', 'MsgText': 'An Error has occured while loading courses!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		// Create New Course Function 
		function CreateNewCourse(course) {
			$scope.loading = true;
			courseService.createCourse(course).then(function (results) {
				$scope.courses.push(results.data);
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({ 'elm': "Course_alert", "MsgType": "OK", "MsgText": "Course added successfully." });
			}, function (error) {
				MSG({ 'elm': "Course_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while adding course!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		}

		//Update Course Function 
		function UpdateCourse(course) {
			$scope.loading = true;
			console.log(course);

			courseService.updateCourse(course).then(function (results) {
				angular.forEach($scope.courses, function (value, key) { 
					if ($scope.courses[key].CourseId === course.CourseId) { 
						$scope.courses[key] = course; 
						return false; 
					}
				});
				$scope.loading = false;
				$uibModalStack.dismissAll();
				MSG({'elm': "Course_alert",  "MsgType": "OK", "MsgText": "Course updated successfully." });
			}, function (error) {
				MSG({ 'elm': "Course_AddEditAlert", 'MsgType': 'ERROR', 'MsgText': 'An error has occured while updating course!', 'MsgAsModel': error.data });
				$scope.loading = false;
			});
		};

		//JSON To Object
		function JSONToObj(obj) {
			obj.TraineeLevels = GETJ(obj.TraineeLevelsJSON);
		};


		//Datepicker
		$scope.dateOptions = {
			'year-format': "'yy'",
			'show-weeks': false
		};

		// Call Course for first time
		$scope.CoursePageInfo = {};
		$scope.tcfilter = courseService.CourseEmptyFilter();
		$scope.tcfilter.PageNumber = 1;
		$scope.tcfilter.PageSize = '20';

		GetCourses($scope.tcfilter);

	}]);
}());


rolpo_app.directive('ckEditor', function () {
	return {
		require: '?ngModel',
		link: function (scope, elm, attr, ngModel) {
			var ck = CKEDITOR.replace(elm[0], {
				allowedContent: true
			});
			if (!ngModel) return;
			ck.on('instanceReady', function () {
				ck.setData(ngModel.$viewValue);
			});
			function updateModel() {
				scope.$apply(function () {
					ngModel.$setViewValue(ck.getData());
				});
			}
			ck.on('change', updateModel);
			ck.on('key', updateModel);
			ck.on('dataReady', updateModel);

			ngModel.$render = function (value) {
				ck.setData(ngModel.$viewValue);
			};
		}
	};
});
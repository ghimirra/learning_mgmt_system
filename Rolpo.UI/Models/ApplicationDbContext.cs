﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace Rolpo.UI.Models
{
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
	{
        //private static string connectionStr; 
        //public ApplicationDbContext() : base(connectionStr)
        //{
        //    // this.Database.Connection.ConnectionString = connectionString;
        //}

        public ApplicationDbContext() : base("RolpoContext")
        {
            
        }



        public static ApplicationDbContext Create()
        {
             
            //var subdomain = UserRights.GetSubDomain();
            //connectionStr  = System.Configuration.ConfigurationManager.AppSettings[subdomain].ToString();
            //return new ApplicationDbContext();
            return new ApplicationDbContext();
        }
    }
}

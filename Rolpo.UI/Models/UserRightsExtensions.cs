﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Rolpo.Service;
using Rolpo.UI.Models;
using System.Web;
using System.Linq;
using System.Web.Mvc;
using Rolpo.Model;
using System.Collections.Generic;
using System;

namespace Rolpo.UI
{

	public class UserRights
	{
        //private static string _subdomain = string.Empty;

        #region "ROLES"

        #region "GENERAL"

        //Has Admin Rights
        public static bool IsSuperAdmin()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated) { if (HttpContext.Current.User.IsInRole("SUPERADMIN")) { return true; } }
            return false;
        }

        // Has Admin Rights
        public static bool IsAdmin()
		{
			if (HttpContext.Current.User.Identity.IsAuthenticated) { if (HttpContext.Current.User.IsInRole("ADMIN") || IsSuperAdmin()) { return true; } }
			return false;
		}

		// Has Editor Rights
		public static bool IsCoordinator()
		{
			if (HttpContext.Current.User.Identity.IsAuthenticated) { if (HttpContext.Current.User.IsInRole("COORDINATOR") || IsAdmin()) { return true; } }
			return false;
		}
         

        #endregion

        
        #endregion

      
		//Get User full name
		public static string FullName()
		{
			string fullName = "";
			if (HttpContext.Current.User.Identity.IsAuthenticated)
			{
				ApplicationUser currentUser = GetCurrentUserAsync();
				fullName = currentUser.FirstName + " " + currentUser.LastName;
			}
			return fullName;
		}

        //// Get Staffs Info
        //public static string StaffInfo(string byName)
        //{

        //    string inforText = "";
        //    HttpSessionStateBase session = new HttpSessionStateWrapper(HttpContext.Current.Session);
        //    if (SessionExtensions.Current.staffInfo != null)
        //    {
        //        Rolpo.Model.StaffsViewModel staff = SessionExtensions.Current.staffInfo;
        //        if (staff != null)
        //        {
        //            switch (byName)
        //            {
        //                case "DESIGNATION":
        //                    inforText = staff.DesignationName;
        //                    break;
        //                case "EDUCATION":
        //                    inforText = staff.Education;
        //                    break;
        //                case "NOTES":
        //                    inforText = staff.Notes;
        //                    break;
        //                case "PHONE":
        //                    inforText = staff.Phone;
        //                    break;
        //                case "EMAIL":
        //                    inforText = staff.PrimaryContactEmail;
        //                    break;

        //            }
        //        }
        //    }
        //    return inforText;

        //}

        private static ApplicationUser GetCurrentUserAsync()
        {
            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            return user;
        }

        //// Get staff details
        //public static Rolpo.Model.StaffsViewModel GetStaffsDetails()
        //{
        //    Rolpo.Model.StaffsViewModel staffsView = null;
        //    ApplicationUser currentUser = GetCurrentUserAsync();
        //    int staffId = currentUser.StaffId ?? 0;
        //    if (staffId != 0)
        //    {
        //        IStaffsService _staffService = DependencyResolver.Current.GetService<IStaffsService>();
        //        staffsView = _staffService.GetStaffs(userId: currentUser.Id, staffid: staffId).FirstOrDefault();
        //    }
        //    return staffsView;
        //}

        ////Get Organization Id Based on User
        //public static string GetOrgId()
        //{
        //    string orgId = "";
        //    if (HttpContext.Current.User.Identity.IsAuthenticated)
        //    {
        //        ApplicationUser currentUser = GetCurrentUserAsync();
        //        orgId = currentUser.OrganizationId.ToString();
        //    }
        //    return orgId;

        //}

        public static Rolpo.Model.Organization GetOrganizationInfo()
        {

            IOrganizationService _orgService = DependencyResolver.Current.GetService<IOrganizationService>();
           return _orgService.GetOrganizationByDomain(); 
        }

        //public static Rolpo.Model.Organization GetOrganizationInfo()
        //{

        //    IOrganizationService _orgService = DependencyResolver.Current.GetService<IOrganizationService>();
        //    return _orgService.GetOrganizationByDomain();
        //}

        //public static void InjectOrganizationInfo(string domainName)
        //{
        //    if (SessionExtensions.Current.organization == null)
        //    {
        //        SessionExtensions.Current.organization = UserRights.GetOrganizationInfo(domainName);
        //    }
        //}

        //GET URL
        public static string GetUrl()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            return url.Split('.').First().Split('/').Last();

        }

   
        internal static object GetOrganizationInfo(string subDomain)
        {
            throw new NotImplementedException();
        }

       
         
    }
}

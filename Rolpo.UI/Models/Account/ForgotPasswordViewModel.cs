﻿using System.ComponentModel.DataAnnotations;

namespace Rolpo.UI.Models
{
	public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}

﻿/// <reference path="lib/EDMetronic/Min/Js/bootstrap/js/bootstrap.min.min.js" />
/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp');
var ngJsSrc = './theme/EDMetronic/assets/**/**/*.js';
var ngCssSrc = './theme/EDMetronic/assets/**/**/*.js';
var ngJsDest = './theme/EDMetronic/Min/Js/';
var ngCssDest = './theme/EDMetronic/Min/Css/';
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var clean = require('gulp-clean');
var concat = require('gulp-concat');


gulp.task('cleanJs', function () {
    return gulp.src(ngJsDest, { read: false })
        .pipe(clean());
});


gulp.task('cleanCss', function () {
    return gulp.src(ngCssDest, { read: false })
        .pipe(clean());
});

gulp.task('jsMin', function () {
    return gulp.src(ngJsDest, { read: false })
        .pipe(clean());    
});

gulp.task('cssMin', function () {
    return gulp.src(ngCssDest, { read: false })
        .pipe(clean());    
});

gulp.task('uglifyJs', function () {
    gulp.src(ngJsSrc)
    .pipe(uglify({ mangle: false }))
   .pipe(rename({
       suffix: ".min"
   }))
  .pipe(gulp.dest(ngJsDest)); //
})

gulp.task('uglifyCss', function () {
    gulp.src(ngCssSrc)
    .pipe(uglify({ mangle: false }))
   .pipe(rename({
       suffix: ".min"
   }))
  .pipe(gulp.dest(ngCssDest)); //
})

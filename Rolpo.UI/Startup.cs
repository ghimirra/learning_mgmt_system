﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Rolpo.UI.Startup))]
namespace Rolpo.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

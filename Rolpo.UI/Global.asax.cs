﻿using Autofac.Integration.Mvc;
using Autofac;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Rolpo.UI.Modules;
using Autofac.Integration.WebApi;
using System;
using NLog;

namespace Rolpo.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        //private string scheduleCacheKey = "SendEmail";
        protected void Application_Start()
        {
			AreaRegistration.RegisterAllAreas();
			GlobalConfiguration.Configure(WebApiConfig.Register);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);


			//Autofac Configuration
			var builder = new Autofac.ContainerBuilder();
			builder.RegisterControllers(typeof(MvcApplication).Assembly).PropertiesAutowired();
			builder.RegisterApiControllers(typeof(MvcApplication).Assembly).PropertiesAutowired();


			builder.RegisterModule(new ServiceModule());
			builder.RegisterModule(new EFModule());


            var container = builder.Build();

            // Set the dependency resolver for MVC.
            var mvcResolver = new AutofacDependencyResolver(container);
            DependencyResolver.SetResolver(mvcResolver);

            // Set the dependency resolver for Web API.
            var webApiResolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = webApiResolver;

            //ApplicationContainer.Container = container;
            //RegisterCacheEntry();

            
        } 

        //private bool RegisterCacheEntry()
        //{
        //    Cache c = HttpRuntime.Cache;
        //    if (null != c[scheduleCacheKey]) return false;

        //    c.Add(scheduleCacheKey, "timer", null,
        //        DateTimeOffset.MaxValue.LocalDateTime, TimeSpan.FromMinutes(1),
        //        CacheItemPriority.Normal,
        //        new CacheItemRemovedCallback(CacheItemRemovedCallback));

        //    return true;
        //}

        protected void Application_Error()
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            Exception exp = Server.GetLastError();
            logger.Error(exp.Message);
            //logger.Err(exp.Message, exp);

            Exception ex = Server.GetLastError();

            if (ex is HttpAntiForgeryException)
            {
                Response.Clear();
                Server.ClearError(); //make sure you log the exception first
                Response.Redirect("/Home/Index", true);
            }
        }

        //public void CacheItemRemovedCallback(string key, object value, CacheItemRemovedReason reason)
        //{
        //    RegisterCacheEntry();
        //    INotificationsService _notificationService = (INotificationsService)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(INotificationsService));
        //    _notificationService.ProcessNotificationChroneJob();
        //}
    }

    //public static class ApplicationContainer
    //{
    //    public static IContainer Container { get; set; }
    //}
}

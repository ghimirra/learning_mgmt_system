﻿using ClosedXML.Excel;
using Rolpo.Model;
using Rolpo.Service;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.Mvc;
using System;

namespace Rolpo.UI.Controllers
{
    public class ReportController : Controller
    {
        IBatchTraineeService _batchtraineeService;
        IBatchService _batchService;

        public ReportController(IBatchTraineeService batchtraineeService, IBatchService batchService)
        {
            _batchtraineeService = batchtraineeService;
            _batchService = batchService;
        }
        // GET: Batch
        public ActionResult Batches()
        {
            return View();
        }

        public ActionResult ListOfTrainees()
        {
            return View();
        }

        [HttpGet]
        public void ExportTraineeListToExcel(BatchTraineeViewModel_Input input)
        {

            List<DTObject> DTList = new List<DTObject>()
            {
                new DTObject{ServerObjectName="TraineeName",DisplayName="Trainee Name",DataTypeName=""},
                //new DTObject{ServerObjectName="JoinedDate",DisplayName="Joined On",DataTypeName=""},
                  new DTObject{ServerObjectName="BatchCode",DisplayName="Batch Code",DataTypeName=""},
                  new DTObject{ServerObjectName="BatchName",DisplayName="Batch Name",DataTypeName=""},
                    //new DTObject{ServerObjectName="CourseCode",DisplayName="Course Code",DataTypeName=""},
                    //new DTObject{ServerObjectName="CourseFullName",DisplayName="Course Name",DataTypeName=""},
                  new DTObject{ServerObjectName="TraineeLevelName",DisplayName="Trainee Level",DataTypeName=""},
                  new DTObject{ServerObjectName="Position",DisplayName="Position",DataTypeName=""},
                  new DTObject{ServerObjectName="CompanyName",DisplayName="Company Name",DataTypeName=""},
                  new DTObject{ServerObjectName="Department",DisplayName="Department",DataTypeName=""},
                  new DTObject{ServerObjectName="BatchStartDateLOCAL",DisplayName="Batch Start Date",DataTypeName=""},
                  new DTObject{ServerObjectName="BatchEndDateLOCAL",DisplayName="Batch End Date",DataTypeName=""},
                  new DTObject{ServerObjectName="Duration",DisplayName="Duration",DataTypeName=""}
              };


            DataTable dt = new DataTable();

            input.ShowAll = 1;
            IEnumerable<BatchTraineeReportViewModel> arr =
                _batchtraineeService.GetBatchTraineeForReport(userId: null
                , input: input);

            dt = Rolpo.UI.FileManagerExtensions.CopyToDataTable<BatchTraineeReportViewModel>(arr, DTList);
            ExportDataTableToExcel(dt, new ExcelReportProperties()
            {
                CompanyName = "NEA - Learning Management System",
                CompanyAddressLine = "Kharipati, Bhaktapur",
                ReportTitle = "Trainee_Report"

            });


        }

        [HttpGet]
        public void ExportBatchListToExcel(BatchViewModel_Input input)
        {

            List<DTObject> DTList = new List<DTObject>()
            {
                new DTObject{ServerObjectName="BatchCode",DisplayName="Batch Code",DataTypeName=""},
                new DTObject{ServerObjectName="BatchName",DisplayName="Batch Name",DataTypeName=""},
                new DTObject{ServerObjectName="CourseCode",DisplayName="Course Code",DataTypeName=""},
                new DTObject{ServerObjectName="CourseFullName",DisplayName="Course Name",DataTypeName=""},
                new DTObject{ServerObjectName="LevelName",DisplayName="Course Level",DataTypeName=""},
                new DTObject{ServerObjectName="BatchStartDateLOCAL",DisplayName="Batch Start Date",DataTypeName=""},
                new DTObject{ServerObjectName="BatchEndDateLOCAL",DisplayName="Batch End Date",DataTypeName=""},
                new DTObject{ServerObjectName="Coordinator",DisplayName="Batch Coordinator",DataTypeName=""},
                new DTObject{ServerObjectName="ASSCoordinator",DisplayName="Assistant Coordinator",DataTypeName=""},
                new DTObject{ServerObjectName="Duration",DisplayName="Course Duration",DataTypeName=""},
                new DTObject{ServerObjectName="TrainingAddress",DisplayName="Training Address",DataTypeName=""},
                new DTObject{ServerObjectName="TotalTrainees",DisplayName="# of Trainees",DataTypeName=""}
             };


            DataTable dt = new DataTable();

            input.ShowAll = 1;
            IEnumerable<BatchReportViewModel> arr =
                _batchService.GetBatchForReport(userId: null
                , input: input);

            dt = Rolpo.UI.FileManagerExtensions.CopyToDataTable<BatchReportViewModel>(arr, DTList);
            ExportDataTableToExcel(dt, new ExcelReportProperties()
            {
                CompanyName = "NEA - Learning Management System",
                CompanyAddressLine = "Kharipati, Bhaktapur",
                ReportTitle = "Batch_Report"

            });




        }

        //Export to excel
        private void ExportDataTableToExcel(DataTable dt, ExcelReportProperties property)
        {

            using (XLWorkbook wb = new XLWorkbook())
            {
                var ws = wb.Worksheets.Add(dt, property.ReportTitle);
                var listOfStrings = new List<String>();
                int columns = Convert.ToInt16((dt.Columns.Count) / 2);
                if (columns < 3) { columns = 4; }
                ws.Row(1).InsertRowsAbove(4);

                ws.Cell(1, 1).Value = property.CompanyName + " " + "," + " " + property.CompanyAddressLine;
                ws.Cell(1, 1).Style.Font.FontColor = XLColor.DarkBlue;
                ws.Cell(1, 1).Style.Font.FontSize = 18;

                if (!string.IsNullOrEmpty(property.ReportTitle))
                {
                    ws.Cell(3, 1).Value = property.ReportTitle;
                    ws.Cell(3, 1).Style.Font.FontColor = XLColor.Black;

                }

                ws.Cell(3, columns - 1).Value = "Generated on " + DateTime.Today.ToShortDateString();
                ws.Cell(3, columns - 1).Style.Font.FontColor = XLColor.Black;

                string myName = Server.UrlEncode(property.ReportTitle + "_" +
                DateTime.Now.ToShortDateString().Replace("/", "_") + ".xlsx");
                MemoryStream stream = GetStream(wb);// The method is defined below
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition",
                "attachment; filename=" + myName);
                Response.ContentType = "application/vnd.ms-excel";
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }

        private MemoryStream GetStream(XLWorkbook wb)
        {
            MemoryStream fs = new MemoryStream();
            wb.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
    }
}
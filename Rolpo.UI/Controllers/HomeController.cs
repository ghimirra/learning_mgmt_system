﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using Rolpo.Model;
using Rolpo.Service;
using Rolpo.UI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Rolpo.UI.Controllers
{
    [RoutePrefix("api/Home")]
    [Authorize]
    public class HomeController : Controller
    {
        //initialize service object 
        IContext _context;      
        INotificationsService _notificationService;
        

        public HomeController( IContext context,INotificationsService notificationService
            )
        { 
            _context = context;         
            _notificationService = notificationService;
        }

        public ActionResult Index()
        {
            return View();
        }

		public ActionResult Error()
        {
            return View();
        }

   

		public ActionResult Info(int? id)
		{
			ViewBag.Id = id;
			return View();
		}

		[HttpGet]
		public ActionResult CodeGen()
        {
            return View();
        }


        [HttpGet]
        public virtual ActionResult DownloadExcel(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", fileName);
            }
            else
            {
                // Problem - Log the error, generate a blank file,
                //           redirect to another controller action - whatever fits with your application
                return new EmptyResult();
            }
        }

        public ActionResult Organization()
        {
            return View();
        }


        public ActionResult SaveLogo()
        {

            bool isSavedSuccessfully = true;
            string fName = "";
            string fullPath = "";
            string pathW;
            string webImagePath = "~/Images/Organization";



            if (!System.IO.Directory.Exists(Server.MapPath(webImagePath)))
            {
                System.IO.Directory.CreateDirectory(Server.MapPath(webImagePath));
            }

            foreach (string fileName in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[fileName];

                fName = file.FileName;
                if (file != null && file.ContentLength > 0)
                {
                    fName = fName.Replace(" ", "_");
                    fName = fName.Insert(fName.IndexOf("."), DateTime.Now.ToString("_ddMMyyhhmmss"));
                    pathW = Path.Combine(Server.MapPath(webImagePath), fName);
                    file.SaveAs(pathW);
                    fullPath = webImagePath + "/" + fName;
                    isSavedSuccessfully = true;
                }

            }
            if (isSavedSuccessfully)
            {

                return Json(new { Message = "ok", ImagePath = fullPath });
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }

        /// <summary>
        /// Page Not Found
        /// </summary>
        /// <returns></returns>
        public ActionResult PageNotFound()
        {
            return View();
        }

        #region Helper Functions  
        private static ApplicationUser GetCurrentUser()
        {
            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            return user;
        }
        #endregion

    }

   
    
}

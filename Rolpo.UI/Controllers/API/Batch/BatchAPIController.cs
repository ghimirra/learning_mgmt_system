﻿using Microsoft.AspNet.Identity;
using Rolpo.Service;
using Rolpo.UI.Models;
using System.Web.Http;
using Microsoft.AspNet.Identity.Owin;
using System.Web;
using NLog;
using System.Web.Http.ModelBinding;
using System;
using Rolpo.Model;
using System.Linq;
using System.Collections.Generic;

namespace Rolpo.UI.Controllers.API
{

    [RoutePrefix("api")]
    [Authorize]
	public class BatchAPIController : ApiController
	{
        Logger logger = LogManager.GetCurrentClassLogger();

        ////initialize service object 

        IBatchService _batchService;
        IBatchTrainerService _batchtrainerService;
        IBatchTraineeService _batchtraineeService;
        IBatchCourseService _batchcourseService;
        IBatchSummaryService _batchsummaryService;

        /// <summary>
        /// Constructor with DI
        /// </summary>
        // Constructor
        public BatchAPIController(  
                  IBatchService batchService
            , IBatchTrainerService batchtrainerService
            , IBatchTraineeService batchtraineeService
            , IBatchCourseService batchcourseService
            , IBatchSummaryService batchsummaryService

            )
        {
            _batchtrainerService = batchtrainerService;
            _batchtraineeService = batchtraineeService;
            _batchcourseService = batchcourseService;
            _batchsummaryService = batchsummaryService;

            _batchService = batchService;

        }

        #region "BATCH MAIN"  

        /// <summary>
        /// Action: Get List of Batchs
        /// </summary>

        [HttpPost, Route("Batch/GetBatchsList")]
        public IEnumerable<BatchViewModel> GetBatchsList([FromBody] BatchViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _batchService.GetBatch(userId: user.Id
                , input: input);

        }

        /// <summary>
        /// Action: Get List of Batchs For Report
        /// </summary>

        [HttpPost, Route("Batch/GetBatchsListForReport")]
        public IEnumerable<BatchReportViewModel> GetBatchsListForReport([FromBody] BatchViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _batchService.GetBatchForReport(userId: user.Id
                , input: input);

        }

        /// <summary>
        /// Action: Get Batch by Id
        /// </summary>

        [HttpGet, Route("Batch/GetBatchById")]
        public BatchViewModel GetBatchById([FromUri] int id)
        {
            var user = GetCurrentUser();

            var input = new BatchViewModel_Input() { BatchId = id, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            var result = _batchService.GetBatch(
            userId: user.Id, input: input).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Action: Add Batch
        /// </summary>

        [HttpPost, Route("Batch/SaveBatch")]
        public IHttpActionResult SaveBatch([FromBody] Batch batch)
        {
            string msgType = "", msgText = "", actionType = "ADD";
            int BatchId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                batch.OrganizationId = user.OrganizationId;
                _batchService.UpdateBatch(batch: batch, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnBatchId: ref BatchId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new BatchViewModel_Input() { BatchId = BatchId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_batchService.GetBatch(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Update Batch
        /// </summary>

        [HttpPost, Route("Batch/UpdateBatch")]
        public IHttpActionResult UpdateBatch([FromBody] Batch batch)
        {
            string msgType = "", msgText = "", actionType = "UPDATE";
            int BatchId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                batch.OrganizationId = user.OrganizationId;
                _batchService.UpdateBatch(batch: batch, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnBatchId: ref BatchId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new BatchViewModel_Input() { BatchId = BatchId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_batchService.GetBatch(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Delete Batch
        /// </summary>

        [HttpDelete, Route("Batch/DeleteBatch/{batchid}")]
        public IHttpActionResult DeleteBatch(int batchid)
        {
            Batch batch = _batchService.GetBatchById(batchid);
            if (batch == null)
            { return NotFound(); }
            try { _batchService.Delete(batch); }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            return Ok();
        }


        #endregion

        #region "BATCH TRAINER"
        /// <summary>
        /// Action: Get List of BatchTrainers
        /// </summary>

        [HttpPost, Route("BatchTrainer/GetBatchTrainersList")]
        public IEnumerable<BatchTrainerViewModel> GetBatchTrainersList([FromBody] BatchTrainerViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _batchtrainerService.GetBatchTrainer(userId: user.Id
                , input: input);

        }

        /// <summary>
        /// Action: Get BatchTrainer by Id
        /// </summary>

        [HttpPost, Route("BatchTrainer/GetBatchTrainerById")]
        public BatchTrainerViewModel GetBatchTrainerById([FromBody] int batchtrainerid)
        {
            var user = GetCurrentUser();

            var input = new BatchTrainerViewModel_Input() { BatchTrainerId = batchtrainerid, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            var result = _batchtrainerService.GetBatchTrainer(
            userId: user.Id, input: input).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Action: Add BatchTrainer
        /// </summary>

        [HttpPost, Route("BatchTrainer/SaveBatchTrainer")]
        public IHttpActionResult SaveBatchTrainer([FromBody] BatchTrainer batchtrainer)
        {
            string msgType = "", msgText = "", actionType = "ADD";
            int BatchTrainerId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                batchtrainer.OrganizationId = user.OrganizationId;
                _batchtrainerService.UpdateBatchTrainer(batchtrainer: batchtrainer, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnBatchTrainerId: ref BatchTrainerId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new BatchTrainerViewModel_Input() { BatchTrainerId = BatchTrainerId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_batchtrainerService.GetBatchTrainer(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Update BatchTrainer
        /// </summary>

        [HttpPost, Route("BatchTrainer/UpdateBatchTrainer")]
        public IHttpActionResult UpdateBatchTrainer([FromBody] BatchTrainer batchtrainer)
        {
            string msgType = "", msgText = "", actionType = "UPDATE";
            int BatchTrainerId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                batchtrainer.OrganizationId = user.OrganizationId;
                _batchtrainerService.UpdateBatchTrainer(batchtrainer: batchtrainer, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnBatchTrainerId: ref BatchTrainerId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new BatchTrainerViewModel_Input() { BatchTrainerId = BatchTrainerId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_batchtrainerService.GetBatchTrainer(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Delete BatchTrainer
        /// </summary>

        [HttpDelete, Route("BatchTrainer/DeleteBatchTrainer/{batchtrainerid}")]
        public IHttpActionResult DeleteBatchTrainer(int batchtrainerid)
        {
            BatchTrainer batchtrainer = _batchtrainerService.GetBatchTrainerById(batchtrainerid);
            if (batchtrainer == null)
            { return NotFound(); }
            try { _batchtrainerService.Delete(batchtrainer); }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            return Ok();
        }

        #endregion

        #region    "BATCH TRAINEE"

        /// <summary>
        /// Action: Get List of BatchTrainees
        /// </summary>

        [HttpPost, Route("BatchTrainee/GetBatchTraineesList")]
        public IEnumerable<BatchTraineeViewModel> GetBatchTraineesList([FromBody] BatchTraineeViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _batchtraineeService.GetBatchTrainee(userId: user.Id
                , input: input);

        }

        /// <summary>
        ///  Get For Report
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost, Route("BatchTrainee/GetBatchTraineesListForReport")]
        public IEnumerable<BatchTraineeReportViewModel> GetBatchTraineesListForReport([FromBody] BatchTraineeViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _batchtraineeService.GetBatchTraineeForReport(userId: user.Id
                , input: input);

        }

        [HttpPost, Route("BatchTrainee/GetRemainingBatchTrainees")]
        public IEnumerable<MemberViewModel> GetRemainingBatchTrainees([FromBody] BatchTraineeViewModel_Input input)
        {
            var user = GetCurrentUser(); 
            return _batchtraineeService.GetBatchTraineeNotInList(userId: user.Id
                , input: input);

        }

        /// <summary>
        /// Action: Get BatchTrainee by Id
        /// </summary>

        [HttpPost, Route("BatchTrainee/GetBatchTraineeById")]
        public BatchTraineeViewModel GetBatchTraineeById([FromBody] int batchtraineeid)
        {
            var user = GetCurrentUser();

            var input = new BatchTraineeViewModel_Input() { BatchTraineeId = batchtraineeid, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            var result = _batchtraineeService.GetBatchTrainee(
            userId: user.Id, input: input).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Action: Add BatchTrainee
        /// </summary>

        [HttpPost, Route("BatchTrainee/SaveBatchTrainee")]
        public IHttpActionResult SaveBatchTrainee([FromBody] BatchTrainee batchtrainee)
        {
            string msgType = "", msgText = "", actionType = "ADD";
            int BatchTraineeId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                batchtrainee.OrganizationId = user.OrganizationId;
                _batchtraineeService.UpdateBatchTrainee(batchtrainee: batchtrainee, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnBatchTraineeId: ref BatchTraineeId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new BatchTraineeViewModel_Input() { BatchTraineeId = BatchTraineeId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_batchtraineeService.GetBatchTrainee(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Update BatchTrainee
        /// </summary>

        [HttpPost, Route("BatchTrainee/UpdateBatchTrainee")]
        public IHttpActionResult UpdateBatchTrainee([FromBody] BatchTrainee batchtrainee)
        {
            string msgType = "", msgText = "", actionType = "UPDATE";
            int BatchTraineeId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                batchtrainee.OrganizationId = user.OrganizationId;
                _batchtraineeService.UpdateBatchTrainee(batchtrainee: batchtrainee, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnBatchTraineeId: ref BatchTraineeId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new BatchTraineeViewModel_Input() { BatchTraineeId = BatchTraineeId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_batchtraineeService.GetBatchTrainee(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Delete BatchTrainee
        /// </summary>

        [HttpDelete, Route("BatchTrainee/DeleteBatchTrainee/{batchtraineeid}")]
        public IHttpActionResult DeleteBatchTrainee(int batchtraineeid)
        {
            BatchTrainee batchtrainee = _batchtraineeService.GetBatchTraineeById(batchtraineeid);
            if (batchtrainee == null)
            { return NotFound(); }
            try { _batchtraineeService.Delete(batchtrainee); }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            return Ok();
        }


        #endregion

        #region "BATCH COURSE"

        /// <summary>
        /// Action: Get List of BatchCourses
        /// </summary>

        [HttpPost, Route("BatchCourse/GetBatchCoursesList")]
        public IEnumerable<BatchCourseViewModel> GetBatchCoursesList([FromBody] BatchCourseViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _batchcourseService.GetBatchCourse(userId: user.Id
                , input: input);

        }

        /// <summary>
        /// Action: Get BatchCourse by Id
        /// </summary>

        [HttpPost, Route("BatchCourse/GetBatchCourseById")]
        public BatchCourseViewModel GetBatchCourseById([FromBody] int batchcourseid)
        {
            var user = GetCurrentUser();

            var input = new BatchCourseViewModel_Input() { BatchCourseId = batchcourseid, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            var result = _batchcourseService.GetBatchCourse(
            userId: user.Id, input: input).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Action: Add BatchCourse
        /// </summary>

        [HttpPost, Route("BatchCourse/SaveBatchCourse")]
        public IHttpActionResult SaveBatchCourse([FromBody] BatchCourse batchcourse)
        {
            string msgType = "", msgText = "", actionType = "ADD";
            int BatchCourseId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                batchcourse.OrganizationId = user.OrganizationId;
                _batchcourseService.UpdateBatchCourse(batchcourse: batchcourse, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnBatchCourseId: ref BatchCourseId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new BatchCourseViewModel_Input() { BatchCourseId = BatchCourseId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_batchcourseService.GetBatchCourse(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Update BatchCourse
        /// </summary>

        [HttpPost, Route("BatchCourse/UpdateBatchCourse")]
        public IHttpActionResult UpdateBatchCourse([FromBody] BatchCourse batchcourse)
        {
            string msgType = "", msgText = "", actionType = "UPDATE";
            int BatchCourseId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                batchcourse.OrganizationId = user.OrganizationId;
                _batchcourseService.UpdateBatchCourse(batchcourse: batchcourse, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnBatchCourseId: ref BatchCourseId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new BatchCourseViewModel_Input() { BatchCourseId = BatchCourseId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_batchcourseService.GetBatchCourse(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Delete BatchCourse
        /// </summary>

        [HttpDelete, Route("BatchCourse/DeleteBatchCourse/{batchcourseid}")]
        public IHttpActionResult DeleteBatchCourse(int batchcourseid)
        {
            BatchCourse batchcourse = _batchcourseService.GetBatchCourseById(batchcourseid);
            if (batchcourse == null)
            { return NotFound(); }
            try { _batchcourseService.Delete(batchcourse); }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            return Ok();
        }

        #endregion

        #region "BATCH SUMMARY"
        /// <summary>
        /// Action: Get List of BatchSummarys
        /// </summary>

        [HttpPost, Route("BatchSummary/GetBatchSummarysList")]
        public IEnumerable<BatchSummaryViewModel> GetBatchSummarysList([FromBody] BatchSummaryViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _batchsummaryService.GetBatchSummary(userId: user.Id
                , input: input);

        }

        /// <summary>
        /// Action: Get BatchSummary by Id
        /// </summary>

        [HttpPost, Route("BatchSummary/GetBatchSummaryById")]
        public BatchSummaryViewModel GetBatchSummaryById([FromBody] int batchsummaryid)
        {
            var user = GetCurrentUser();

            var input = new BatchSummaryViewModel_Input() { BatchSummaryId = batchsummaryid, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            var result = _batchsummaryService.GetBatchSummary(
            userId: user.Id, input: input).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Action: Add BatchSummary
        /// </summary>

        [HttpPost, Route("BatchSummary/SaveBatchSummary")]
        public IHttpActionResult SaveBatchSummary([FromBody] BatchSummary batchsummary)
        {
            string msgType = "", msgText = "", actionType = "ADD";
            int BatchSummaryId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                batchsummary.OrganizationId = user.OrganizationId;
                _batchsummaryService.UpdateBatchSummary(batchsummary: batchsummary, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnBatchSummaryId: ref BatchSummaryId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new BatchSummaryViewModel_Input() { BatchSummaryId = BatchSummaryId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_batchsummaryService.GetBatchSummary(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Update BatchSummary
        /// </summary>

        [HttpPost, Route("BatchSummary/UpdateBatchSummary")]
        public IHttpActionResult UpdateBatchSummary([FromBody] BatchSummary batchsummary)
        {
            string msgType = "", msgText = "", actionType = "UPDATE";
            int BatchSummaryId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                batchsummary.OrganizationId = user.OrganizationId;
                _batchsummaryService.UpdateBatchSummary(batchsummary: batchsummary, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnBatchSummaryId: ref BatchSummaryId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new BatchSummaryViewModel_Input() { BatchSummaryId = BatchSummaryId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_batchsummaryService.GetBatchSummary(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Delete BatchSummary
        /// </summary>

        [HttpDelete, Route("BatchSummary/DeleteBatchSummary/{batchsummaryid}")]
        public IHttpActionResult DeleteBatchSummary(int batchsummaryid)
        {
            BatchSummary batchsummary = _batchsummaryService.GetBatchSummaryById(batchsummaryid);
            if (batchsummary == null)
            { return NotFound(); }
            try { _batchsummaryService.Delete(batchsummary); }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            return Ok();
        }


        #endregion


        #region Helper Functions  
        private static ApplicationUser GetCurrentUser()
        {
            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            return user;
        }
        #endregion

    }
}

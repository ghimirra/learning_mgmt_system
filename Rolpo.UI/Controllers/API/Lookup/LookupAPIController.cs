﻿using Microsoft.AspNet.Identity;
using Rolpo.Model;
using Rolpo.Service;
using Rolpo.UI.Models;
using System.Web.Http;
using System.Collections.Generic;

using Microsoft.AspNet.Identity.Owin;
using System.Web;
using System.Linq;
using System;
using System.Web.Http.ModelBinding;
using Newtonsoft.Json;
using NLog;

namespace Rolpo.UI.Controllers.API
{
	[RoutePrefix("api")]
	[Authorize]
	public class LookupAPIController : ApiController
	{
        Logger logger = LogManager.GetCurrentClassLogger();

        ////initialize service object 
        IEmailFormatService _emailformatService;
        IMediaService _mediaService;
        IContext _context;
        IStatusService _statusService;

        ITraineeLevelService _traineelevelService;
        ICourseCategoryService _coursecategoryService;
        ICourseLevelService _courselevelService;
        ICoursePeriodTypeService _courseperiodtypeService;


        /// <summary>
        /// Constructor with DI
        /// </summary>
        // Constructor
        public LookupAPIController( 
             IEmailFormatService emailformatService
            , IContext context
            , IMediaService mediaService
            ,IStatusService statusService
            , ITraineeLevelService traineelevelService
            , ICourseCategoryService coursecategoryService
            , ICourseLevelService courselevelService 
            , ICoursePeriodTypeService courseperiodtypeService

            )
        { 
            _emailformatService = emailformatService;
            _mediaService = mediaService;
            _context = context;
            _statusService = statusService;
            _traineelevelService = traineelevelService;
            _coursecategoryService = coursecategoryService;
            _courselevelService = courselevelService;
            _courseperiodtypeService = courseperiodtypeService;
        }


        #region MEDIA TYPES

        /// <summary>
        /// Action: Get List of Medias
        /// </summary>

        [HttpPost, Route("Media/GetMediasList")]
        public IEnumerable<MediaViewModel> GetMediasList([FromBody] MediaViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _mediaService.GetMedia(userId: user.Id
                , input: input);

        }

        /// <summary>
        /// Action: Get Media by Id
        /// </summary>

        [HttpPost, Route("Media/GetMediaById")]
        public MediaViewModel GetMediaById([FromBody] int mediaid)
        {
            var user = GetCurrentUser();

            var input = new MediaViewModel_Input() { MediaId = mediaid, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            var result = _mediaService.GetMedia(
            userId: user.Id, input: input).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Action: Add Media
        /// </summary>

        [HttpPost, Route("Media/SaveMedia")]
        public IHttpActionResult SaveMedia([FromBody] Media media)
        {
            string msgType = "", msgText = "", actionType = "ADD";
            int MediaId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                media.OrganizationId = user.OrganizationId;
                _mediaService.UpdateMedia(media: media, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnMediaId: ref MediaId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new MediaViewModel_Input() { MediaId = MediaId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_mediaService.GetMedia(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Update Media
        /// </summary>

        [HttpPost, Route("Media/UpdateMedia")]
        public IHttpActionResult UpdateMedia([FromBody] Media media)
        {
            string msgType = "", msgText = "", actionType = "UPDATE";
            int MediaId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                media.OrganizationId = user.OrganizationId;
                _mediaService.UpdateMedia(media: media, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnMediaId: ref MediaId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new MediaViewModel_Input() { MediaId = MediaId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_mediaService.GetMedia(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Delete Media
        /// </summary>

        [HttpDelete, Route("Media/DeleteMedia/{mediaid}")]
        public IHttpActionResult DeleteMedia(int mediaid)
        {
            Media media = _mediaService.GetMediaById(mediaid);
            if (media == null)
            { return NotFound(); }
            try { _mediaService.Delete(media); }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            return Ok();
        }

        #endregion

        #region 'organizationinfo'
        /// <summary>
        /// get organization info
        /// </summary>
        /// <returns></returns>
        [ HttpGet, Route("Setting/GetOrganizationInfo")]
        public Organization GetOrganizationInfo()
        {
            Organization org = UserRights.GetOrganizationInfo();
            return org;
            
        }


        [HttpPost,Route("Setting/UpdateOrganizationInfo")]
        public IHttpActionResult UpdateOrganizationInfo([FromBody] Organization organization)
        {
            try
            {
               
                Organization organizationDetails = _context.Organization.Find(organization.OrganizationId);
                organizationDetails.OrganizationCode = organization.OrganizationCode;
                organizationDetails.OrganizationName = organization.OrganizationName;
                organizationDetails.OrganizationAddress = organization.OrganizationAddress;
                organizationDetails.Telephone = organization.Telephone;
                organizationDetails.OrganizationLogoUrl = organization.OrganizationLogoUrl;

                            
                _context.Entry(organizationDetails).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();

                { return Ok(organization); }
            }            
           
            
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

          
        }

        #endregion

        #region "STATUS"

        /// <summary>
        /// Action: Get List of Statuss
        /// </summary>

        [HttpPost, Route("Status/GetStatussList")]
        public IEnumerable<StatusViewModel> GetStatussList([FromBody] StatusViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _statusService.GetStatus(userId: user.Id
                , input: input);

        }

        /// <summary>
        /// Action: Get Status by Id
        /// </summary>

        [HttpPost, Route("Status/GetStatusById")]
        public StatusViewModel GetStatusById([FromBody] int statusid)
        {
            var user = GetCurrentUser();

            var input = new StatusViewModel_Input() { StatusId = statusid, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            var result = _statusService.GetStatus(
            userId: user.Id, input: input).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Action: Add Status
        /// </summary>

        [HttpPost, Route("Status/SaveStatus")]
        public IHttpActionResult SaveStatus([FromBody] Status status)
        {
            string msgType = "", msgText = "", actionType = "ADD";
            int StatusId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                status.OrganizationId = user.OrganizationId;
                _statusService.UpdateStatus(status: status, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnStatusId: ref StatusId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new StatusViewModel_Input() { StatusId = StatusId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_statusService.GetStatus(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Update Status
        /// </summary>

        [HttpPost, Route("Status/UpdateStatus")]
        public IHttpActionResult UpdateStatus([FromBody] Status status)
        {
            string msgType = "", msgText = "", actionType = "UPDATE";
            int StatusId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                status.OrganizationId = user.OrganizationId;
                _statusService.UpdateStatus(status: status, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnStatusId: ref StatusId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new StatusViewModel_Input() { StatusId = StatusId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_statusService.GetStatus(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Delete Status
        /// </summary>

        [HttpDelete, Route("Status/DeleteStatus/{statusid}")]
        public IHttpActionResult DeleteStatus(int statusid)
        {
            Status status = _statusService.GetStatusById(statusid);
            if (status == null)
            { return NotFound(); }
            try { _statusService.Delete(status); }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            return Ok();
        }


        #endregion

        #region "TRAINEE LEVEL"

        /// <summary>
        /// Action: Get List of TraineeLevels
        /// </summary>

        [HttpPost, Route("TraineeLevel/GetTraineeLevelsList")]
        public IEnumerable<TraineeLevelViewModel> GetTraineeLevelsList([FromBody] TraineeLevelViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _traineelevelService.GetTraineeLevel(userId: user.Id
                , input: input);

        }

        /// <summary>
        /// Action: Get TraineeLevel by Id
        /// </summary>

        [HttpPost, Route("TraineeLevel/GetTraineeLevelById")]
        public TraineeLevelViewModel GetTraineeLevelById([FromBody] int traineelevelid)
        {
            var user = GetCurrentUser();

            var input = new TraineeLevelViewModel_Input() { TraineeLevelId = traineelevelid, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            var result = _traineelevelService.GetTraineeLevel(
            userId: user.Id, input: input).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Action: Add TraineeLevel
        /// </summary>

        [HttpPost, Route("TraineeLevel/SaveTraineeLevel")]
        public IHttpActionResult SaveTraineeLevel([FromBody] TraineeLevel traineelevel)
        {
            string msgType = "", msgText = "", actionType = "ADD";
            int TraineeLevelId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;

                //Set OrganizationId  
                traineelevel.OrganizationId = user.OrganizationId;
                _traineelevelService.UpdateTraineeLevel(traineelevel: traineelevel, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnTraineeLevelId: ref TraineeLevelId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new TraineeLevelViewModel_Input() { TraineeLevelId = TraineeLevelId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_traineelevelService.GetTraineeLevel(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Update TraineeLevel
        /// </summary>

        [HttpPost, Route("TraineeLevel/UpdateTraineeLevel")]
        public IHttpActionResult UpdateTraineeLevel([FromBody] TraineeLevel traineelevel)
        {
            string msgType = "", msgText = "", actionType = "UPDATE";
            int TraineeLevelId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                traineelevel.OrganizationId = user.OrganizationId;
                _traineelevelService.UpdateTraineeLevel(traineelevel: traineelevel, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnTraineeLevelId: ref TraineeLevelId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new TraineeLevelViewModel_Input() { TraineeLevelId = TraineeLevelId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_traineelevelService.GetTraineeLevel(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Delete TraineeLevel
        /// </summary>

        [HttpDelete, Route("TraineeLevel/DeleteTraineeLevel/{traineelevelid}")]
        public IHttpActionResult DeleteTraineeLevel(int traineelevelid)
        {
            TraineeLevel traineelevel = _traineelevelService.GetTraineeLevelById(traineelevelid);
            if (traineelevel == null)
            { return NotFound(); }
            try { _traineelevelService.Delete(traineelevel); }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            return Ok();
        }

        #endregion

        #region "COURSE CATEGORY"

        /// <summary>
        /// Action: Get List of CourseCategorys
        /// </summary>

        [HttpPost, Route("CourseCategory/GetCourseCategorysList")]
        public IEnumerable<CourseCategoryViewModel> GetCourseCategorysList([FromBody] CourseCategoryViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _coursecategoryService.GetCourseCategory(userId: user.Id
                , input: input);

        }

        [HttpPost, Route("CourseCategory/GetParentCategorysList")]
        public IEnumerable<ParentCategoryViewModel> GetParentCategorysList([FromBody] CourseCategoryViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _coursecategoryService.GetCourseCategoryByParent(userId: user.Id
                , input: input);

        }

        /// <summary>
        /// Action: Get CourseCategory by Id
        /// </summary>

        [HttpGet, Route("CourseCategory/GetCourseCategoryById")]
        public CourseCategoryViewModel GetCourseCategoryById([FromUri] int id)
        {
            var user = GetCurrentUser();

            var input = new CourseCategoryViewModel_Input() { CourseCategoryId = id, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            var result = _coursecategoryService.GetCourseCategory(
            userId: user.Id, input: input).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Action: Add CourseCategory
        /// </summary>

        [HttpPost, Route("CourseCategory/SaveCourseCategory")]
        public IHttpActionResult SaveCourseCategory([FromBody] CourseCategory coursecategory)
        {
            string msgType = "", msgText = "", actionType = "ADD";
            int CourseCategoryId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                coursecategory.OrganizationId = user.OrganizationId;
                _coursecategoryService.UpdateCourseCategory(coursecategory: coursecategory, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnCourseCategoryId: ref CourseCategoryId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new CourseCategoryViewModel_Input() { CourseCategoryId = CourseCategoryId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_coursecategoryService.GetCourseCategory(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Update CourseCategory
        /// </summary>

        [HttpPost, Route("CourseCategory/UpdateCourseCategory")]
        public IHttpActionResult UpdateCourseCategory([FromBody] CourseCategory coursecategory)
        {
            string msgType = "", msgText = "", actionType = "UPDATE";
            int CourseCategoryId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                coursecategory.OrganizationId = user.OrganizationId;
                _coursecategoryService.UpdateCourseCategory(coursecategory: coursecategory, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnCourseCategoryId: ref CourseCategoryId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new CourseCategoryViewModel_Input() { CourseCategoryId = CourseCategoryId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_coursecategoryService.GetCourseCategory(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Delete CourseCategory
        /// </summary>

        [HttpDelete, Route("CourseCategory/DeleteCourseCategory/{coursecategoryid}")]
        public IHttpActionResult DeleteCourseCategory(int coursecategoryid)
        {
            CourseCategory coursecategory = _coursecategoryService.GetCourseCategoryById(coursecategoryid);
            if (coursecategory == null)
            { return NotFound(); }
            try { _coursecategoryService.Delete(coursecategory); }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            return Ok();
        }

        #endregion

        #region "COURSE LEVELS"

        /// <summary>
        /// Action: Get List of CourseLevels
        /// </summary>

        [HttpPost, Route("CourseLevel/GetCourseLevelsList")]
        public IEnumerable<CourseLevelViewModel> GetCourseLevelsList([FromBody] CourseLevelViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _courselevelService.GetCourseLevel(userId: user.Id
                , input: input);

        }

        /// <summary>
        /// Action: Get CourseLevel by Id
        /// </summary>

        [HttpPost, Route("CourseLevel/GetCourseLevelById")]
        public CourseLevelViewModel GetCourseLevelById([FromBody] int courselevelid)
        {
            var user = GetCurrentUser();

            var input = new CourseLevelViewModel_Input() { CourseLevelId = courselevelid, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            var result = _courselevelService.GetCourseLevel(
            userId: user.Id, input: input).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Action: Add CourseLevel
        /// </summary>

        [HttpPost, Route("CourseLevel/SaveCourseLevel")]
        public IHttpActionResult SaveCourseLevel([FromBody] CourseLevel courselevel)
        {
            string msgType = "", msgText = "", actionType = "ADD";
            int CourseLevelId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                courselevel.OrganizationId = user.OrganizationId;
                _courselevelService.UpdateCourseLevel(courselevel: courselevel, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnCourseLevelId: ref CourseLevelId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new CourseLevelViewModel_Input() { CourseLevelId = CourseLevelId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_courselevelService.GetCourseLevel(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Update CourseLevel
        /// </summary>

        [HttpPost, Route("CourseLevel/UpdateCourseLevel")]
        public IHttpActionResult UpdateCourseLevel([FromBody] CourseLevel courselevel)
        {
            string msgType = "", msgText = "", actionType = "UPDATE";
            int CourseLevelId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                courselevel.OrganizationId = user.OrganizationId;
                _courselevelService.UpdateCourseLevel(courselevel: courselevel, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnCourseLevelId: ref CourseLevelId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new CourseLevelViewModel_Input() { CourseLevelId = CourseLevelId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_courselevelService.GetCourseLevel(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Delete CourseLevel
        /// </summary>

        [HttpDelete, Route("CourseLevel/DeleteCourseLevel/{courselevelid}")]
        public IHttpActionResult DeleteCourseLevel(int courselevelid)
        {
            CourseLevel courselevel = _courselevelService.GetCourseLevelById(courselevelid);
            if (courselevel == null)
            { return NotFound(); }
            try { _courselevelService.Delete(courselevel); }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            return Ok();
        }

        #endregion

        #region "CoursePeriodType"

        /// <summary>
        /// Action: Get List of CoursePeriodTypes
        /// </summary>

        [HttpPost, Route("CoursePeriodType/GetCoursePeriodTypesList")]
        public IEnumerable<CoursePeriodTypeViewModel> GetCoursePeriodTypesList([FromBody] CoursePeriodTypeViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _courseperiodtypeService.GetCoursePeriodType(userId: user.Id
                , input: input);

        }

        /// <summary>
        /// Action: Get CoursePeriodType by Id
        /// </summary>

        [HttpPost, Route("CoursePeriodType/GetCoursePeriodTypeById")]
        public CoursePeriodTypeViewModel GetCoursePeriodTypeById([FromBody] int courseperiodtypeid)
        {
            var user = GetCurrentUser();

            var input = new CoursePeriodTypeViewModel_Input() { CoursePeriodTypeId = courseperiodtypeid, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            var result = _courseperiodtypeService.GetCoursePeriodType(
            userId: user.Id, input: input).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Action: Add CoursePeriodType
        /// </summary>

        [HttpPost, Route("CoursePeriodType/SaveCoursePeriodType")]
        public IHttpActionResult SaveCoursePeriodType([FromBody] CoursePeriodType courseperiodtype)
        {
            string msgType = "", msgText = "", actionType = "ADD";
            int CoursePeriodTypeId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                courseperiodtype.OrganizationId = user.OrganizationId;
                _courseperiodtypeService.UpdateCoursePeriodType(courseperiodtype: courseperiodtype, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnCoursePeriodTypeId: ref CoursePeriodTypeId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new CoursePeriodTypeViewModel_Input() { CoursePeriodTypeId = CoursePeriodTypeId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_courseperiodtypeService.GetCoursePeriodType(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Update CoursePeriodType
        /// </summary>

        [HttpPost, Route("CoursePeriodType/UpdateCoursePeriodType")]
        public IHttpActionResult UpdateCoursePeriodType([FromBody] CoursePeriodType courseperiodtype)
        {
            string msgType = "", msgText = "", actionType = "UPDATE";
            int CoursePeriodTypeId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                courseperiodtype.OrganizationId = user.OrganizationId;
                _courseperiodtypeService.UpdateCoursePeriodType(courseperiodtype: courseperiodtype, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnCoursePeriodTypeId: ref CoursePeriodTypeId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new CoursePeriodTypeViewModel_Input() { CoursePeriodTypeId = CoursePeriodTypeId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_courseperiodtypeService.GetCoursePeriodType(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Delete CoursePeriodType
        /// </summary>

        [HttpDelete, Route("CoursePeriodType/DeleteCoursePeriodType/{courseperiodtypeid}")]
        public IHttpActionResult DeleteCoursePeriodType(int courseperiodtypeid)
        {
            CoursePeriodType courseperiodtype = _courseperiodtypeService.GetCoursePeriodTypeById(courseperiodtypeid);
            if (courseperiodtype == null)
            { return NotFound(); }
            try { _courseperiodtypeService.Delete(courseperiodtype); }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            return Ok();
        }

        #endregion

        //#region "NLOG ENTRIES"

        //[HttpPost, Route("Log/GetEntries")]
        //public IHttpActionResult Get([FromBody] Nlog_Input input)
        //{
        //    List<NlogEntries> logs = new List<NlogEntries>();
        //    try
        //    {
        //        string userid = "";
        //        if (HttpContext.Current.User.Identity.IsAuthenticated) {
        //            ApplicationUser currentUser = GetCurrentUser();
        //            userid = currentUser.Id.ToString();
        //        }
        //        logs = _nlogService.GetErrors(input,userid);
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex.Message);
        //        var modelStateDictionary = new ModelStateDictionary();
        //        if (ex.InnerException != null)
        //            modelStateDictionary.AddModelError("InnerException", ex.InnerException.Message);
        //        else
        //            modelStateDictionary.AddModelError("InnerException", ex.Message);
        //        return BadRequest(modelStateDictionary);
        //    }
        //    return Ok(logs);
        //}

        //#endregion



        #region Helper Functions
        private static ApplicationUser GetCurrentUser()
		{
			ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
			return user;
		}

        private string toXML(string inputStr)
        {
            System.Xml.Linq.XDocument xml = null; //JsonConvert.DeserializeXNode("{\"Data\":" + inputStr + "}", "root");
            if (!string.IsNullOrEmpty(inputStr))
            {

                xml = JsonConvert.DeserializeXNode("{\"Data\":" + inputStr + "}", "root");
                return xml.ToString();
            }
            return string.Empty;
        }

         
        #endregion

    }
}

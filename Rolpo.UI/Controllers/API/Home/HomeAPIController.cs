﻿using Microsoft.AspNet.Identity;
using Rolpo.Model;
using Rolpo.Service;
using Rolpo.UI.Models;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

using Microsoft.AspNet.Identity.Owin;
using System.Web;
using System;

namespace Rolpo.UI.Controllers.API
{
    [RoutePrefix("api/Home")]
	
	public class HomeAPIController : ApiController
	{
        //initialize service object 
        ISettingsService _settingService; 


		/// <summary>
		/// Constructor with DI
		/// </summary>
		public HomeAPIController(ISettingsService settingService)
		{
            _settingService = settingService;
		}


		#region "CodeGen Service"

		/// <summary>
		/// Load DDLs
		/// </summary>
		[HttpPost, Route("LoadDDLs")]
		public DDLJson LoadDDLs([FromBody] DDLFilterList filterList)
		{
            Guid organizationId = new Guid("3EC0CBCE-7D8B-40E8-B6B7-7AB0FC48666A");
			
            if (HttpContext.Current.User.Identity.IsAuthenticated) {
                var user = GetCurrentUser();
                organizationId = user.OrganizationId;
            }
                var json = JsonConvert.SerializeObject(filterList.FilterList);
			System.Xml.Linq.XDocument xml = JsonConvert.DeserializeXNode("{\"Data\":" + json + "}", "root");

			return _settingService.GetDDLItemsList(pageName: filterList.PageName, ddlListXML: xml.ToString(), organizationId: organizationId);

		}

		   

		/// <summary>
		/// Call Me every 4 Minutes
		/// </summary>
		[Authorize]
		[HttpGet, Route("__Garbage_Collection_CHECK_PING")]
		public IHttpActionResult __Garbage_Collection_CHECK_PING()
		{
			return Ok();

		}

		#endregion


		#region Helper Functions
		private static ApplicationUser GetCurrentUser()
		{
			ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
			return user;
		}


		#endregion

	}
}

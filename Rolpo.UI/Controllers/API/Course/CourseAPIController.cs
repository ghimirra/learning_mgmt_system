﻿using Microsoft.AspNet.Identity;
using Rolpo.Service;
using Rolpo.UI.Models;
using System.Web.Http;
using Microsoft.AspNet.Identity.Owin;
using System.Web;
using NLog;
using System.Web.Http.ModelBinding;
using System;
using Rolpo.Model;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Xml.Linq;

namespace Rolpo.UI.Controllers.API
{

    [RoutePrefix("api")]
    [Authorize]
	public class CourseAPIController : ApiController
	{
        Logger logger = LogManager.GetCurrentClassLogger();

        ////initialize service object 

        ICourseService _courseService;
        IModulesService _modulesService;
        ICourseMediaService _coursemediaService;

        /// <summary>
        /// Constructor with DI
        /// </summary>
        // Constructor
        public CourseAPIController(
                ICourseService  courseService
                , IModulesService modulesService
            , ICourseMediaService courseMediaService
            )
        {
            _modulesService = modulesService;
            _courseService = courseService;
            _coursemediaService = courseMediaService;

        }




        #region "COURSE MAIN"  

        /// <summary>
        /// Action: Get List of Courses
        /// </summary>

        [HttpPost, Route("Course/GetCoursesList")]
        public IEnumerable<CourseViewModel> GetCoursesList([FromBody] CourseViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _courseService.GetCourse(userId: user.Id
                , input: input);

        }

        /// <summary>
        /// Action: Get Course by Id
        /// </summary>

        [HttpGet, Route("Course/GetCourseById")]
        public CourseViewModel GetCourseById([FromUri] int id)
        {
            var user = GetCurrentUser();

            var input = new CourseViewModel_Input() { CourseId = id, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            var result = _courseService.GetCourse(
            userId: user.Id, input: input).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Action: Add Course
        /// </summary>

        [HttpPost, Route("Course/SaveCourse")]
        public IHttpActionResult SaveCourse([FromBody] Course course)
        {
            string msgType = "", msgText = "", actionType = "ADD";
            int CourseId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                course.OrganizationId = user.OrganizationId;
                course.TraineeLevelsXML = toXML(course.TraineeLevelsJSON);
 
                _courseService.UpdateCourse(course: course, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnCourseId: ref CourseId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new CourseViewModel_Input() { CourseId = CourseId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_courseService.GetCourse(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Update Course
        /// </summary>

        [HttpPost, Route("Course/UpdateCourse")]
        public IHttpActionResult UpdateCourse([FromBody] Course course)
        {
            string msgType = "", msgText = "", actionType = "UPDATE";
            int CourseId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                course.OrganizationId = user.OrganizationId;
                course.TraineeLevelsXML = toXML(course.TraineeLevelsJSON);

                _courseService.UpdateCourse(course: course, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnCourseId: ref CourseId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new CourseViewModel_Input() { CourseId = CourseId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_courseService.GetCourse(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Delete Course
        /// </summary>

        [HttpDelete, Route("Course/DeleteCourse/{courseid}")]
        public IHttpActionResult DeleteCourse(int courseid)
        {
            Course course = _courseService.GetCourseById(courseid);
            if (course == null)
            { return NotFound(); }
            try { _courseService.Delete(course); }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            return Ok();
        }


        #endregion

        #region "MODULES"  

        /// <summary>
        /// Action: Get List of Moduless
        /// </summary>

        [HttpPost, Route("Modules/GetModulesList")]
        public IEnumerable<ModulesViewModel> GetModulessList([FromBody] ModulesViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _modulesService.GetModules(userId: user.Id
                , input: input);

        }

        /// <summary>
        /// Action: Get Modules by Id
        /// </summary>

        [HttpPost, Route("Modules/GetModuleById")]
        public ModulesViewModel GetModulesById([FromBody] int moduleid)
        {
            var user = GetCurrentUser();

            var input = new ModulesViewModel_Input() { ModuleId = moduleid, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            var result = _modulesService.GetModules(
            userId: user.Id, input: input).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Action: Add Modules
        /// </summary>

        [HttpPost, Route("Modules/SaveModule")]
        public IHttpActionResult SaveModules([FromBody] TModules modules)
        {
            string msgType = "", msgText = "", actionType = "ADD";
            int ModuleId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                modules.OrganizationId = user.OrganizationId;
                _modulesService.UpdateModules(modules: modules, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnModuleId: ref ModuleId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new ModulesViewModel_Input() { ModuleId = ModuleId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_modulesService.GetModules(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Update Modules
        /// </summary>

        [HttpPost, Route("Modules/UpdateModule")]
        public IHttpActionResult UpdateModules([FromBody] TModules modules)
        {
            string msgType = "", msgText = "", actionType = "UPDATE";
            int ModuleId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                modules.OrganizationId = user.OrganizationId;
                _modulesService.UpdateModules(modules: modules, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnModuleId: ref ModuleId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new ModulesViewModel_Input() { ModuleId = ModuleId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_modulesService.GetModules(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Delete Modules
        /// </summary>

        [HttpDelete, Route("Modules/DeleteModule/{moduleid}")]
        public IHttpActionResult DeleteModules(int moduleid)
        {
            TModules modules = _modulesService.GetModulesById(moduleid);
            if (modules == null)
            { return NotFound(); }
            try { _modulesService.Delete(modules); }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            return Ok();
        }



        #endregion

        #region COURSE MEDIA  

        /// <summary>
        /// Action: Get List of CourseMedias
        /// </summary>

        [HttpPost, Route("CourseMedia/GetCourseMediasList")]
        public IEnumerable<CourseMediaViewModel> GetCourseMediasList([FromBody] CourseMediaViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _coursemediaService.GetCourseMedia(userId: user.Id
                , input: input);

        }

        /// <summary>
        /// Action: Get CourseMedia by Id
        /// </summary>

        [HttpPost, Route("CourseMedia/GetCourseMediaById")]
        public CourseMediaViewModel GetCourseMediaById([FromBody] int coursemediaid)
        {
            var user = GetCurrentUser();

            var input = new CourseMediaViewModel_Input() { CourseMediaId = coursemediaid, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            var result = _coursemediaService.GetCourseMedia(
            userId: user.Id, input: input).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Action: Add CourseMedia
        /// </summary>

        [HttpPost, Route("CourseMedia/SaveCourseMedia")]
        public IHttpActionResult SaveCourseMedia([FromBody] CourseMedia coursemedia)
        {
            string msgType = "", msgText = "", actionType = "ADD";
            int CourseMediaId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                coursemedia.OrganizationId = 0;
                _coursemediaService.UpdateCourseMedia(coursemedia: coursemedia, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnCourseMediaId: ref CourseMediaId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new CourseMediaViewModel_Input() { CourseMediaId = CourseMediaId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_coursemediaService.GetCourseMedia(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Update CourseMedia
        /// </summary>

        [HttpPost, Route("CourseMedia/UpdateCourseMedia")]
        public IHttpActionResult UpdateCourseMedia([FromBody] CourseMedia coursemedia)
        {
            string msgType = "", msgText = "", actionType = "UPDATE";
            int CourseMediaId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                coursemedia.OrganizationId = 0;
                _coursemediaService.UpdateCourseMedia(coursemedia: coursemedia, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnCourseMediaId: ref CourseMediaId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new CourseMediaViewModel_Input() { CourseMediaId = CourseMediaId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_coursemediaService.GetCourseMedia(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Delete CourseMedia
        /// </summary>

        [HttpDelete, Route("CourseMedia/DeleteCourseMedia/{coursemediaid}")]
        public IHttpActionResult DeleteCourseMedia(int coursemediaid)
        {
            CourseMedia coursemedia = _coursemediaService.GetCourseMediaById(coursemediaid);
            if (coursemedia == null)
            { return NotFound(); }
            try { _coursemediaService.Delete(coursemedia); }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            return Ok();
        }

        #endregion

        #region Helper Functions
        private static ApplicationUser GetCurrentUser()
        {
            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            return user;
        }

        private string toXML(string inputStr)
        {
            System.Xml.Linq.XDocument xml = null; //JsonConvert.DeserializeXNode("{\"Data\":" + inputStr + "}", "root");
            if (!string.IsNullOrEmpty(inputStr))
            {

                xml = JsonConvert.DeserializeXNode("{\"Data\":" + inputStr + "}", "root");
                return xml.ToString();
            }
            return string.Empty;
        }


        public string ConvertJsonToXML(string JsonData, string root)
        {
            XNode node = JsonConvert.DeserializeXNode(JsonData, root);
            return node.ToString().Replace("\r\n", string.Empty);
        }


        #endregion

    }
}

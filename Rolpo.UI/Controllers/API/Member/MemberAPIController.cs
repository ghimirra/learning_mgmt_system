﻿using Microsoft.AspNet.Identity;
using Rolpo.Model;
using Rolpo.Service;
using Rolpo.UI.Models;
using System.Web.Http;
using System.Collections.Generic;

using Microsoft.AspNet.Identity.Owin;
using System.Web;
using System.Linq;
using System;
using System.Web.Http.ModelBinding;
using Newtonsoft.Json;
using NLog;

namespace Rolpo.UI.Controllers.API
{
	
	[Authorize]
	public class MemberAPIController : ApiController
	{
        Logger logger = LogManager.GetCurrentClassLogger();

        ////initialize service object 
        
        //ISettingService _settingService;
        IMemberService _memberService;
       
        /// <summary>
        /// Constructor with DI
        /// </summary>
        // Constructor
        public MemberAPIController( 
                IEmailFormatService emailformatService 
                //,ISettingService settingService
                ,IMemberService memberService
            )
        { 
            
            //_settingService = settingService;
            //_context = context;
            _memberService = memberService;
           
        }






        #region API  

        /// <summary>
        /// Action: Get List of Members
        /// </summary>

        [HttpPost, Route("api/Member/GetTrainersList")]
        public IEnumerable<MemberViewModel> GetMembersList([FromBody] MemberViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _memberService.GetMember(userId: user.Id
                , input: input);

        }


        [HttpPost, Route("api/Member/GetTraineeList")]
        public IEnumerable<MemberViewModel> GetTraineeList([FromBody] MemberViewModel_Input input)
        {
            var user = GetCurrentUser();
            return _memberService.GetTrainee(userId: user.Id
                , input: input);

        }


        /// <summary>
        /// Action: Get Member by Id
        /// </summary>

        [HttpPost, Route("api/Member/GetMemberById")]
        public MemberViewModel GetMemberById([FromBody] int memberid)
        {
            var user = GetCurrentUser();

            var input = new MemberViewModel_Input() { MemberId = memberid, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            var result = _memberService.GetMember(
            userId: user.Id, input: input).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Action: Add Member
        /// </summary>

        [HttpPost, Route("api/Member/SaveMember")]
        public IHttpActionResult SaveMember([FromBody] Member member)
        {
            string msgType = "", msgText = "", actionType = "ADD";
            int MemberId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                member.OrganizationId = user.OrganizationId;
                _memberService.UpdateMember(member: member, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnMemberId: ref MemberId);

                
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new MemberViewModel_Input() { MemberId = MemberId, PageNumber = 1, PageSize = 1, ShowAll = 0 };
            if (member.Designation == "TRAINER")
            {
                return Ok(_memberService.GetMember(
            userId: user.Id, input: input).FirstOrDefault());
            }
            else
            {
                return Ok(_memberService.GetTrainee(
                userId: user.Id, input: input).FirstOrDefault());
            }
            
        }

        /// <summary>
        /// Action: Update Member
        /// </summary>

        [HttpPost, Route("api/Member/UpdateMember")]
        public IHttpActionResult UpdateMember([FromBody] Member member)
        {
            string msgType = "", msgText = "", actionType = "UPDATE";
            int MemberId = 0;
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            try
            {
                string userId = user.Id;
                //Set OrganizationId  
                member.OrganizationId = user.OrganizationId;
                _memberService.UpdateMember(member: member, actionType: actionType, userId: userId, msgType: ref msgType, msgText: ref msgText, returnMemberId: ref MemberId);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new MemberViewModel_Input() { MemberId = MemberId, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_memberService.GetMember(
            userId: user.Id, input: input).FirstOrDefault());
        }

        /// <summary>
        /// Action: Delete Member
        /// </summary>

        [HttpDelete, Route("api/Member/DeleteMember/{memberid}")]
        public IHttpActionResult DeleteMember(int memberid)
        {
            Member member = _memberService.GetMemberById(memberid);
            if (member == null)
            { return NotFound(); }
            try { _memberService.Delete(member); }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            return Ok();
        }

        #endregion

        #region Helper Functions  
        private static ApplicationUser GetCurrentUser()
        {
            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            return user;
        }
        #endregion

    }
}

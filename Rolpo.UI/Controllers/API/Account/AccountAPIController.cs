﻿using Microsoft.AspNet.Identity;
using Rolpo.Service;
using Rolpo.UI.Models;
using System.Web.Http;

using Microsoft.AspNet.Identity.Owin;
using System.Web;
using Rolpo.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Web.Http.ModelBinding;

namespace Rolpo.UI.Controllers.API
{
    [RoutePrefix("api")]
    [Authorize]	
	public class AccountAPIController: ApiController
	{
        //initialize service object  
        IASPNETUsersService _aspnetusersService;
        INotificationsService _notificationService;



        /// <summary>
        /// Constructor with DI
        /// </summary>
        public AccountAPIController(
            IASPNETUsersService aspnetusersService
            , INotificationsService notificationService
            )
        {
            _aspnetusersService = aspnetusersService;
            _notificationService = notificationService;

        }


        public ApplicationUserManager UserManager
        {
            get
            {
                return System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }

        }

        #region "Users"  

        /// <summary>
        /// Action: Get List of ASPNETUserss
        /// </summary>

        [HttpPost, Route("Account/GetASPNETUserssList")]

        public Tuple<List<ASPNETUsersViewModel>, List<RolesViewModel>> GetASPNETUserssList([FromBody] Model.ASPNETUsersViewModel_Input input)
        {
            var user = GetCurrentUser();

            Tuple<List<ASPNETUsersViewModel>, List<RolesViewModel>> result = _aspnetusersService.GetASPNETUsers(userId: user.Id
                , input: input);

            return result;

        }

        /// <summary>
        /// Action: Get ASPNETUsers by Id
        /// </summary>

        [HttpPost, Route("Account/GetASPNETUsersById")]

        public ASPNETUsersViewModel GetASPNETUsersById([FromBody] string id)
        {
            var user = GetCurrentUser();
            var input = new ASPNETUsersViewModel_Input() { Id = id, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            var result = _aspnetusersService.GetASPNETUsers(
      userId: user.Id, input: input).Item1.FirstOrDefault<Model.ASPNETUsersViewModel>();

            return result;
        }

        /// <summary>
        /// Action: Add ASPNETUsers
        /// </summary>

        [HttpPost, Route("Account/SaveASPNETUsers")]
        public async Task<IHttpActionResult> SaveASPNETUsers([FromBody] Model.ASPNETUsersViewModel aspnetusers)
        {
            string msgType = "", msgText = "";
            string Id = "";
            var user = GetCurrentUser();
            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            var newUser = new ApplicationUser { UserName = aspnetusers.Email, Email = aspnetusers.Email, OrganizationId = user.OrganizationId, FirstName = aspnetusers.FirstName, LastName = aspnetusers.LastName, StaffId = aspnetusers.StaffId };
            try
            {
                string password = Guid.NewGuid().ToString("d").Substring(1, 8);

                var result = await UserManager.CreateAsync(newUser, password);

                if (result.Succeeded)
                {
                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=532713
                    // Send an email with this link

                    var code = await UserManager.GenerateEmailConfirmationTokenAsync(newUser.Id);


                    var callbackUrl = this.Url.Link("Default", new { Controller = "Account", Action = "ConfirmEmail", userId = newUser.Id, code = code });






                    var recentlyCreatedUser = await UserManager.FindByEmailAsync(aspnetusers.Email);

                    Id = recentlyCreatedUser.Id;

                    // Manage Roles

                    dynamic dynJson = JsonConvert.DeserializeObject(aspnetusers.RolesJSON);
                    foreach (var item in dynJson)
                    {
                        if (Convert.ToBoolean(item.Value))
                        {
                            await UserManager.AddToRoleAsync(recentlyCreatedUser.Id, Convert.ToString(item.RoleName));
                        }
                    }

                    //SEND EMAIL TO USER
                    //await UserManager.SendEmailAsync(newUser.Id, "Confirm your account", EmailMessageForNewUser(newUser, password, callbackUrl));

                    MailAddressCollection addressto_ = new MailAddressCollection() { };
                    addressto_.Add(newUser.Email);

                    //Prepare Dictionary used for Registration process
                    Dictionary<string, string> kvp = new Dictionary<string, string>();
                    kvp.Add("TO_NAME", newUser.FirstName + " " + newUser.LastName);

                    var subDomain = System.Configuration.ConfigurationManager.AppSettings["DOMAIN"].ToString();
                    kvp.Add("ORG_URL", UserRights.GetOrganizationInfo().ServiceBaseUrl);

                    kvp.Add("USER_NAME", newUser.UserName);
                    kvp.Add("USER_PASSWORD", password);
                    kvp.Add("CREATEDBY_USER", UserRights.FullName());
                    kvp.Add("USER_DOMAIN", subDomain);
                    kvp.Add("CONFIRM_LINK", callbackUrl);

                    _notificationService.AddEmail(EmailType: "Rolpo.RegisterUser", kvp: kvp, addressto: addressto_, subject: "[!Important Login Notice] Your registration with " + subDomain);

                }
                else
                {
                    AddErrors(result);
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }
            var input = new ASPNETUsersViewModel_Input() { Id = Id, PageNumber = 1, PageSize = 1, ShowAll = 0 };

            return Ok(_aspnetusersService.GetASPNETUsers(
      userId: user.Id, input: input).Item1.FirstOrDefault());
        }

        /// <summary>
        /// Action: Update ASPNETUsers
        /// </summary>
        [HttpPost, Route("Account/UpdateASPNETUsers")]
        public async Task<IHttpActionResult> UpdateASPNETUsers([FromBody] Model.ASPNETUsersViewModel aspnetusers)
        {
            string msgType = "", msgText = "";
            var user = GetCurrentUser();

            if (!ModelState.IsValid)
            { return BadRequest(ModelState); }

            var existingUser = await UserManager.FindByEmailAsync(aspnetusers.Email);

            try
            {

                if (existingUser == null)
                {
                    var modelStateDictionary = new ModelStateDictionary();
                    modelStateDictionary.AddModelError("InnerException", "User Does not exist.");
                    return BadRequest(modelStateDictionary);
                }

                existingUser.FirstName = aspnetusers.FirstName;
                existingUser.LastName = aspnetusers.LastName;
                existingUser.StaffId = aspnetusers.StaffId;

                // Manage Roles
                var roles = await UserManager.GetRolesAsync(existingUser.Id);
                await UserManager.RemoveFromRolesAsync(existingUser.Id, roles.ToArray<string>());

                dynamic dynJson = JsonConvert.DeserializeObject(aspnetusers.RolesJSON);
                foreach (var item in dynJson)
                {
                    if (Convert.ToBoolean(item.Value))
                    {
                        await UserManager.AddToRoleAsync(existingUser.Id, Convert.ToString(item.RoleName));
                    }
                }


            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }

            if (msgType == "ERROR")
            {
                var modelStateDictionary = new ModelStateDictionary();
                modelStateDictionary.AddModelError("InnerException", msgText);
                return BadRequest(modelStateDictionary);
            }

            var input = new ASPNETUsersViewModel_Input() { Id = existingUser.Id, PageNumber = 1, PageSize = 1, ShowAll = 0 };
            return Ok(_aspnetusersService.GetASPNETUsers(
      userId: user.Id, input: input).Item1.FirstOrDefault());
        }

        /// <summary>
        /// Action: Delete ASPNETUsers
        /// </summary>

        [HttpDelete]
        [Route("Account/DeleteASPNETUsers/{id}")]
        public async Task<IHttpActionResult> DeleteASPNETUsers(string id)
        {
            var existingUser = await UserManager.FindByIdAsync(id);
            if (existingUser == null)
            { return NotFound(); }


            try
            {
                var roles = await UserManager.GetRolesAsync(existingUser.Id);
                await UserManager.RemoveFromRolesAsync(existingUser.Id, roles.ToArray<string>());
                await UserManager.DeleteAsync(existingUser);
            }
            catch (Exception e)
            {
                var modelStateDictionary = new ModelStateDictionary();
                if (e.InnerException != null)
                    modelStateDictionary.AddModelError("InnerException", e.InnerException.Message);
                else
                    modelStateDictionary.AddModelError("InnerException", e.Message);
                return BadRequest(modelStateDictionary);
            }
            return Ok();
        }

        #endregion

        #region Helper Functions  

        // Get Current User
        private static ApplicationUser GetCurrentUser()
        {
            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            return user;
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error);
            }
        }


      

        #endregion

    }
}

using System.Web.Mvc;

namespace Rolpo.UI.Controllers
{
    /// <summary>
    ///  Master entry Controller
    /// </summary>
    /// 

    [Authorize]
    //[RequireHttps]
    public class LookupController : Controller
    {
        
        
        public LookupController()
        {
        }
         
	 
        public ActionResult MediaTypes()
        {
            return View();
        }
     
	 
        // <summary>
        /// Email Formats
        /// </summary>
        /// <returns></returns>
        public ActionResult EmailFormat()
        {
            return View();
        }

    
        /// <summary>
        /// Nlog Entries
        /// </summary>
        /// <returns></returns>
        public ActionResult LogEntries()
        {
            return View();
        }

        /// <summary>
        /// Status
        /// </summary>
        /// <returns></returns>
        public ActionResult Status() {
            return View();
        }

        /// <summary>
        ///  TRainee Level
        /// </summary>
        /// <returns></returns>
        public ActionResult TraineeLevel()
        {
            return View();
        }

        /// <summary>
        ///  CourseCategory
        /// </summary>
        /// <returns></returns>
        public ActionResult CourseCategory()
        {
            return View();
        }

        /// <summary>
        ///  CourseLevel
        /// </summary>
        /// <returns></returns>
        public ActionResult CourseLevel()
        {
            return View();
        }


        /// <summary>
        ///  CoursePeriodType
        /// </summary>
        /// <returns></returns>
        public ActionResult CoursePeriodType()
        {
            return View();
        }


    }


}

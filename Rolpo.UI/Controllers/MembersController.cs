﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Rolpo.UI.Controllers
{
    [Authorize]
    public class MembersController : Controller
    {
        // GET: Members
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Trainee()
        {
            return View();
        }
        public ActionResult Trainer()
        {
            return View();
        }

        public ActionResult Coordinators()
        {
            return View();
        }

        public ActionResult AsstCoordinators()
        {
            return View();
        }

        public ActionResult SaveUploadedFile()
        {

            bool isSavedSuccessfully = true;
            string fName = "";
            string fullPath = "";
            string pathW;
            string webImagePath = "~/Images/Members";
           


            if (!System.IO.Directory.Exists(Server.MapPath(webImagePath)))
            {
                System.IO.Directory.CreateDirectory(Server.MapPath(webImagePath));
            }

            foreach (string fileName in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[fileName];

                fName = file.FileName;
                if (file != null && file.ContentLength > 0)
                {
                    fName = fName.Replace(" ", "_");
                    fName = fName.Insert(fName.IndexOf("."), DateTime.Now.ToString("_ddMMyyhhmmss"));
                    pathW = Path.Combine(Server.MapPath(webImagePath), fName);
                    file.SaveAs(pathW);
                    fullPath = webImagePath + "/" + fName;
                    isSavedSuccessfully = true;
                }

            }
            if (isSavedSuccessfully)
            {

                return Json(new { Message = "ok", ImagePath = fullPath });
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }

        /// <summary>
        /// remove image file
        /// </summary>
        /// <returns></returns>
        public string RemoveFile([System.Web.Http.FromUri]string filePath = "")
        {

            string baseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["baseUrl"];
            string ImagePath = Server.MapPath(filePath.Replace(baseUrl, "~/"));

           
                try
                {
                    if (System.IO.File.Exists(ImagePath))
                    {
                        System.IO.File.Delete(ImagePath);
                    }
                    return "ok";
                }
                catch (Exception ex)
                {
                    return "error";
                }
           

            //for files not having path saved in database            

        }
    }
}
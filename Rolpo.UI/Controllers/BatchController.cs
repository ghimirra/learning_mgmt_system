﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Rolpo.UI.Controllers
{
    public class BatchController : Controller
    {
        // GET: Batch
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult View(int id)
        {
            ViewBag.BatchId = id;
            return View();
        }

        public ActionResult AddEdit(int id)
        {
            ViewBag.BatchId = id;
            return View();
        }

        
        ///// <summary>
        ///// Export product rate permit to excel
        ///// </summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public ActionResult ProductRate_PermitsListToExcel([System.Web.Http.FromBody]ProductRate_PermitViewModel_Input input)
        //{
        //    var user = GetCurrentUser();
        //    List<DTObject> DTList = new List<DTObject>()
        //    {
        //        new DTObject{ServerObjectName="SupplierName",DisplayName="Supplier",DataTypeName=""},
        //        new DTObject{ServerObjectName="TimeLineName",DisplayName="Timeline",DataTypeName=""},
        //        new DTObject{ServerObjectName="PermitName",DisplayName="Permit",DataTypeName=""},
        //        new DTObject{ServerObjectName="AgentNameDisplay",DisplayName="Agent",DataTypeName=""},
        //         new DTObject{ServerObjectName="CurrencyCode_Buy",DisplayName="Buy Currency",DataTypeName=""},
        //        new DTObject{ServerObjectName="BuyAmount",DisplayName="Buy",DataTypeName=""},
        //        new DTObject{ServerObjectName="BuyTaxMargin",DisplayName="Buy Tax",DataTypeName=""},
        //        new DTObject{ServerObjectName="TotalBuyAmount",DisplayName="Total Buy",DataTypeName=""},
        //        new DTObject{ServerObjectName="Markup",DisplayName="Markup",DataTypeName=""},
        //        new DTObject{ServerObjectName="CurrencyCode_Sell",DisplayName="Sell Currency",DataTypeName=""},
        //        new DTObject{ServerObjectName="SellAmount",DisplayName="Sell",DataTypeName=""},
        //        new DTObject{ServerObjectName="SellTaxMargin",DisplayName="Sell Tax",DataTypeName=""},
        //        new DTObject{ServerObjectName="TotalSellAmount",DisplayName="Total Sell",DataTypeName=""}


        //     };
        //    DataTable dt = new DataTable();

        //    input.ShowAll = 1;
        //    IEnumerable<ProductRate_PermitViewModel> arr = _productrate_permitService.GetProductRate_Permit(userId: user.Id,input:input);

        //    dt = Rolpo.UI.FileManagerExtensions.CopyToDataTable<ProductRate_PermitViewModel>(arr, DTList);
        //    dt.TableName = "ProductRate Permit";

        //    string handle = Guid.NewGuid().ToString();

        //    TempData[handle] = FileManagerExtensions.ExportToExcel(dt, new ExcelReportProperties() { ReportTitle = "Product Rate Permit Report", CompanyAddressLine = SessionExtensions.Current.organization.OrganizationAddress, CompanyName = SessionExtensions.Current.organization.OrganizationName, CompanyLogoUrl = SessionExtensions.Current.organization.OrganizationLogoUrl });


        //    return new JsonResult()
        //    {
        //        Data = new { FileGuid = handle, FileName = "PermitReport" + DateTime.Now.ToShortDateString() + ".xls" }
        //    };

        //}
    }
}
﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Rolpo.UI.Models;
using System;
using System.IO;

namespace Rolpo.UI.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        
        public ManageController()
        {
        }

        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Manage/Index
        [HttpGet]
        public async Task<ActionResult> Index(ManageMessageId? message = null)
        {
            ViewData["StatusMessage"] =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                : message == ManageMessageId.Error ? "An error has occurred."
                : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
                : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
                : "";

            var user = GetCurrentUser();
            var model = new IndexViewModel
            {
                HasPassword = await UserManager.HasPasswordAsync(user.Id),
                PhoneNumber = await UserManager.GetPhoneNumberAsync(user.Id),
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync(user.Id),
                Logins = await UserManager.GetLoginsAsync(user.Id)
                //BrowserRemembered = await SignInManager.IsTwoFactorClientRememberedAsync(user.Id)
            };
            return View(model);
        }

       

        //
        // GET: /Manage/ChangePassword
        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ViewProfile(int Id)
        {

            ViewBag.UserId = Id;
            return View();


        }

        //save profile picture

        public ActionResult SaveProfilePicture([System.Web.Http.FromUri] int Id)
        {

            int count = 1;
            bool isSavedSuccessfully = false;
            string fName = "";
            string imgpath = "~/Images/Profile/" + Id;


            if (!System.IO.Directory.Exists(Server.MapPath(imgpath)))
            {
                System.IO.Directory.CreateDirectory(Server.MapPath(imgpath));

            }

            foreach (string fileName in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[fileName];

                fName = file.FileName;
                if (file != null && file.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath(imgpath), fName);
                    if (Exists(path))
                    {

                        count += 1;
                        fName = fName.Insert(fName.IndexOf("."), DateTime.Now.ToString("_ddMMyyhhmmss"));
                        path = Path.Combine(Server.MapPath(imgpath), fName);

                    }
                    file.SaveAs(path);
                    isSavedSuccessfully = true;
                }

            }

            if (isSavedSuccessfully)
            {
                return Json(new { PathString = imgpath + "/" + fName });
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }

        //check if file exists
        public static bool Exists(string path)
        {
            if (System.IO.File.Exists(path))
            {
                return true;
            }
            return false;
        }

        //remove file
        public string RemoveUploadedFile(string filepath)
        {

            string path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/" + filepath));
            try
            {
                if (path != null)
                {
                    System.IO.File.Delete(path);

                }

                return "ok";
            }

            catch (Exception e)
            {
                return "error";
            }
        }

        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = GetCurrentUser();
            if (user != null)
            {
                var result = await UserManager.ChangePasswordAsync(user.Id, model.OldPassword, model.NewPassword);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    //_logger.LogInformation(3, "User changed their password successfully.");
                    return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
                }
                AddErrors(result);
                return View(model);
            }
            return RedirectToAction("Index", new { Message = ManageMessageId.Error });
        }

        //
        // GET: /Manage/SetPassword
        [HttpGet]
        public ActionResult SetPassword()
        {
            return View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = GetCurrentUser();
            if (user != null)
            {
                var result = await UserManager.AddPasswordAsync(user.Id, model.NewPassword);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
                return View(model);
            }
            return RedirectToAction("Index", new { Message = ManageMessageId.Error });
        }



        //
        // GET: /Account/User
        [HttpGet]
        public ActionResult UserList()
        {
            return View();
        }



        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error);
            }
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            AddLoginSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        private ApplicationUser GetCurrentUser()
        {
            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            return user;
        }



        #endregion
    }
}

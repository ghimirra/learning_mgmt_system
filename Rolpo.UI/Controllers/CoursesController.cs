﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Rolpo.UI.Controllers
{
    public class CoursesController : Controller
    {
        // GET: Courses
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Add Edit Course
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AddEdit(int id)
        {
            ViewBag.CourseId = id;
            return View();
        }

        /// <summary>
        ///  View Details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult View(int id) {
             ViewBag.CourseId = id;
            return View();
        }

        [HttpPost]      
        public ActionResult SaveCourseImage(string description)
        {

            bool isSavedSuccessfully = true;
            string fName = "";
            string fullPath = "";
            string pathW;
            string webImagePath = "~/Images/Courses";


            if (!System.IO.Directory.Exists(Server.MapPath(webImagePath)))
            {
                System.IO.Directory.CreateDirectory(Server.MapPath(webImagePath));
            }

            foreach (string fileName in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[fileName];

                fName = file.FileName;
                if (file != null && file.ContentLength > 0)
                {
                    fName = fName.Replace(" ", "_");
                    fName = fName.Insert(fName.IndexOf("."), DateTime.Now.ToString("_ddMMyyhhmmss"));
                    pathW = Path.Combine(Server.MapPath(webImagePath), fName);
                    file.SaveAs(pathW);
                    fullPath = webImagePath + "/" + fName;
                    isSavedSuccessfully = true;
                }

            }
            if (isSavedSuccessfully)
            {

                return Json(new { Message = "ok", ImagePath = fullPath });
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }
     }
}
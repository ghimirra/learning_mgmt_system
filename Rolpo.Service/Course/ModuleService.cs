﻿/*
 Service For Modules
Created by: Prashant
Created On: 22/06/2018
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface IModulesService : IEntityService<TModules>
    {
        TModules GetModulesById(int moduleid);
        IEnumerable<ModulesViewModel> GetModules(string userId, ModulesViewModel_Input input);
        void UpdateModules(TModules modules, string actionType, string userId, ref int returnModuleId, ref string msgType, ref string msgText);
    }

    public class ModulesService : EntityService<TModules>, IModulesService
    {

        new IContext _context;
        public ModulesService(IContext context) : base(context)
        {
            _context = context;
            _dbset = _context.Set<TModules>();
        }

        //Stored Procedures definition 
        StoredProc csp_Modules_GET = new StoredProc().HasName("[csp.Modules.Get]").ReturnsTypes(typeof(ModulesViewModel));
        StoredProc csp_Modules_UPDATE = new StoredProc().HasName("[csp.Modules.Update]");

        /// <summary>
        /// Get Modules By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

        public TModules GetModulesById(int moduleid)
        {
            return _dbset.FirstOrDefault(x => x.ModuleId == moduleid);
        }

        /// <summary>
        /// Get Modules
        /// </summary>

        public IEnumerable<ModulesViewModel> GetModules(string userId, ModulesViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[7];

            p[0] = new SqlParameter("@ModuleId", input.ModuleId);
            p[1] = new SqlParameter("@ModuleTitle", input.ModuleTitle);

            p[2] = new SqlParameter("@UserId", userId);
            p[3] = new SqlParameter("@PageNumber", input.PageNumber);
            p[4] = new SqlParameter("@PageSize", input.PageSize);
            p[5] = new SqlParameter("@ShowAll", input.ShowAll);
            p[6] = new SqlParameter("@AssociatedCourseId", input.AssociatedCourseId);
            
            var results = _context.CallSP(csp_Modules_GET, p);
            return results.ToList<ModulesViewModel>();
        }

        /// <summary>
        /// Update Modules
        /// </summary>

        public void UpdateModules(TModules modules, string actionType, string userId, ref int returnModuleId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[13];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@ModuleId", modules.ModuleId);
            p[2] = new SqlParameter("@OrganizationId", modules.OrganizationId);
            p[3] = new SqlParameter("@ModuleTitle", modules.ModuleTitle);
            p[4] = new SqlParameter("@ModuleDescription", modules.ModuleDescription);
            p[5] = new SqlParameter("@ModuleContent", modules.ModuleContent);
            p[6] = new SqlParameter("@AssociatedCourseId", modules.AssociatedCourseId);
            p[7] = new SqlParameter("@ModulePeriod", modules.ModulePeriod);
            p[8] = new SqlParameter("@ModulePeriodTypeId", modules.ModulePeriodTypeId);

            p[9] = new SqlParameter("@UserId", userId);

            p[10] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 20);
            p[10].Direction = System.Data.ParameterDirection.Output;

            p[11] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 200);
            p[11].Direction = System.Data.ParameterDirection.Output;

            p[12] = new SqlParameter("@ReturnModulesId", System.Data.SqlDbType.Int);
            p[12].Direction = System.Data.ParameterDirection.Output;

            var result = _context.CallSP(csp_Modules_UPDATE, p);

            msgType = (string)p[10].Value;
            msgText = (string)p[11].Value;
            returnModuleId = (int)p[12].Value;
        }
    }
}

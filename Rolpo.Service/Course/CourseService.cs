﻿/*
 Service For Course
Created by: Prashant
Created On: 22/06/2018
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface ICourseService : IEntityService<Course>
    {
        Course GetCourseById(int courseid);
        IEnumerable<CourseViewModel> GetCourse(string userId, CourseViewModel_Input input);
        void UpdateCourse(Course course, string actionType, string userId, ref int returnCourseId, ref string msgType, ref string msgText);
    }

    public class CourseService : EntityService<Course>, ICourseService
    {

        new IContext _context;
        public CourseService(IContext context) : base(context)
        {
            _context = context;
            _dbset = _context.Set<Course>();
        }

        //Stored Procedures definition 
        StoredProc csp_Course_GET = new StoredProc().HasName("[csp.Course.Get]").ReturnsTypes(typeof(CourseViewModel));
        StoredProc csp_Course_UPDATE = new StoredProc().HasName("[csp.Course.Update]");

        /// <summary>
        /// Get Course By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

        public Course GetCourseById(int courseid)
        {
            return _dbset.FirstOrDefault(x => x.CourseId == courseid);
        }

        /// <summary>
        /// Get Course
        /// </summary>

        public IEnumerable<CourseViewModel> GetCourse(string userId, CourseViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[6];

            p[0] = new SqlParameter("@CourseId", input.CourseId);
            p[1] = new SqlParameter("@CourseFullName", input.CourseFullName);

            p[2] = new SqlParameter("@UserId", userId);
            p[3] = new SqlParameter("@PageNumber", input.PageNumber);
            p[4] = new SqlParameter("@PageSize", input.PageSize);
            p[5] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_Course_GET, p);
            return results.ToList<CourseViewModel>();
        }

        /// <summary>
        /// Update Course
        /// </summary>

        public void UpdateCourse(Course course, string actionType, string userId, ref int returnCourseId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[19];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@CourseId", course.CourseId);
            p[2] = new SqlParameter("@OrganizationId", course.OrganizationId);
            p[3] = new SqlParameter("@CourseFullName", course.CourseFullName);
            p[4] = new SqlParameter("@CourseCode", course.CourseCode);
            p[5] = new SqlParameter("@CourseShortName", course.CourseShortName);
            p[6] = new SqlParameter("@CourseDescription", course.CourseDescription);
            p[7] = new SqlParameter("@CourseCategoryId", course.CourseCategoryId);
            p[8] = new SqlParameter("@CourseSubCategoryId", course.CourseSubCategoryId);
            p[9] = new SqlParameter("@CourseLevelId", course.CourseLevelId);
            p[10] = new SqlParameter("@CourseContent", course.CourseContent);
            p[11] = new SqlParameter("@CoursePeriod", course.CoursePeriod);
            p[12] = new SqlParameter("@CoursePeriodTypeId", course.CoursePeriodTypeId);
            p[13] = new SqlParameter("@TraineeLevelDesc", course.TraineeLevelDesc);

            p[14] = new SqlParameter("@UserId", userId);

            p[15] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 10);
            p[15].Direction = System.Data.ParameterDirection.Output;

            p[16] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 100);
            p[16].Direction = System.Data.ParameterDirection.Output;

            p[17] = new SqlParameter("@ReturnCourseId", System.Data.SqlDbType.Int);
            p[17].Direction = System.Data.ParameterDirection.Output;

            p[18] = new SqlParameter("@TraineeLevelsXML", course.TraineeLevelsXML);


            var result = _context.CallSP(csp_Course_UPDATE, p);

            msgType = (string)p[15].Value;
            msgText = (string)p[16].Value;
            returnCourseId = (int)p[17].Value;
        }
    }
}

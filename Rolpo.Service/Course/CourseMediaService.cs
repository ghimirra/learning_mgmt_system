﻿/*
 Service For CourseMedia
Created by: Prashant
Created On: 26/06/2018
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface ICourseMediaService : IEntityService<CourseMedia>
    {
        CourseMedia GetCourseMediaById(int coursemediaid);
        IEnumerable<CourseMediaViewModel> GetCourseMedia(string userId, CourseMediaViewModel_Input input);
        void UpdateCourseMedia(CourseMedia coursemedia, string actionType, string userId, ref int returnCourseMediaId, ref string msgType, ref string msgText);
    }

    public class CourseMediaService : EntityService<CourseMedia>, ICourseMediaService
    {

        new IContext _context;
        public CourseMediaService(IContext context) : base(context)
        {
            _context = context;
            _dbset = _context.Set<CourseMedia>();
        }

        //Stored Procedures definition 
        StoredProc csp_CourseMedia_GET = new StoredProc().HasName("[csp.CourseMedia.Get]").ReturnsTypes(typeof(CourseMediaViewModel));
        StoredProc csp_CourseMedia_UPDATE = new StoredProc().HasName("[csp.CourseMedia.Update]");

        /// <summary>
        /// Get CourseMedia By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

        public CourseMedia GetCourseMediaById(int coursemediaid)
        {
            return _dbset.FirstOrDefault(x => x.CourseMediaId == coursemediaid);
        }

        /// <summary>
        /// Get CourseMedia
        /// </summary>

        public IEnumerable<CourseMediaViewModel> GetCourseMedia(string userId, CourseMediaViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[6];

            p[0] = new SqlParameter("@CourseMediaId", input.CourseMediaId);
            p[1] = new SqlParameter("@CourseId", input.CourseId);

            p[2] = new SqlParameter("@UserId", userId);
            p[3] = new SqlParameter("@PageNumber", input.PageNumber);
            p[4] = new SqlParameter("@PageSize", input.PageSize);
            p[5] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_CourseMedia_GET, p);
            return results.ToList<CourseMediaViewModel>();
        }

        /// <summary>
        /// Update CourseMedia
        /// </summary>

        public void UpdateCourseMedia(CourseMedia coursemedia, string actionType, string userId, ref int returnCourseMediaId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[12];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@CourseMediaId", coursemedia.CourseMediaId);
            p[2] = new SqlParameter("@OrganizationId", 0);
            p[3] = new SqlParameter("@CourseId", coursemedia.CourseId);
            p[4] = new SqlParameter("@TopicId", coursemedia.TopicId);
            p[5] = new SqlParameter("@MediaURL", coursemedia.MediaURL);
            p[6] = new SqlParameter("@MediaType", coursemedia.MediaType);
            p[7] = new SqlParameter("@Remarks", coursemedia.Remarks);

            p[8] = new SqlParameter("@UserId", userId);

            p[9] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 10);
            p[9].Direction = System.Data.ParameterDirection.Output;

            p[10] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 100);
            p[10].Direction = System.Data.ParameterDirection.Output;

            p[11] = new SqlParameter("@ReturnCourseMediaId", System.Data.SqlDbType.Int);
            p[11].Direction = System.Data.ParameterDirection.Output;

            var result = _context.CallSP(csp_CourseMedia_UPDATE, p);

            msgType = (string)p[9].Value;
            msgText = (string)p[10].Value;
            returnCourseMediaId = (int)p[11].Value;
        }
    }
}

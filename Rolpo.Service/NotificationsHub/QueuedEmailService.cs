﻿/*
 Service For QueuedEmail
Created by: Prashant
Created On: 12/10/2017
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface IQueuedEmailService 
    {
        //QueuedEmail GetQueuedEmailById(int queuedemailid);
        IEnumerable<QueuedEmailViewModel> GetQueuedEmail(QueuedEmailViewModel_Input input, string userId = "");
        void UpdateQueuedEmail(QueuedEmail queuedemail, string actionType, string userId, ref int returnQueuedEmailId, ref string msgType, ref string msgText);
    }

    public class QueuedEmailService :  IQueuedEmailService
    {

         IContext _context;
        public QueuedEmailService(IContext context) 
        {
            _context = context;
        }

        //Stored Procedures definition 
        StoredProc csp_QueuedEmail_GET = new StoredProc().HasName("[csp.QueuedEmail.Get]").ReturnsTypes(typeof(QueuedEmailViewModel));
        StoredProc csp_QueuedEmail_UPDATE = new StoredProc().HasName("[csp.QueuedEmail.Update]");

        ///// <summary>
        ///// Get QueuedEmail By Id :: Don't forget to add the DBSet to RolpoContext
        ///// </summary>

        //public QueuedEmail GetQueuedEmailById(int queuedemailid)
        //{
        //    return _dbset.FirstOrDefault(x => x.QueuedEmailId == queuedemailid);
        //}

        /// <summary>
        /// Get QueuedEmail
        /// </summary>

        public IEnumerable<QueuedEmailViewModel> GetQueuedEmail(QueuedEmailViewModel_Input input, string userId = "")
        {

            SqlParameter[] p = new SqlParameter[6];

            p[0] = new SqlParameter("@QueuedEmailId", input.QueuedEmailId);
            p[1] = new SqlParameter("@LoadNotSentItemsOnly", input.LoadNotSentItemsOnly);

            p[2] = new SqlParameter("@UserId", userId);
            p[3] = new SqlParameter("@PageNumber", input.PageNumber);
            p[4] = new SqlParameter("@PageSize", input.PageSize);
            p[5] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_QueuedEmail_GET, p);
            return results.ToList<QueuedEmailViewModel>();
        }

        /// <summary>
        /// Update QueuedEmail
        /// </summary>

        public void UpdateQueuedEmail(QueuedEmail queuedemail, string actionType, string userId, ref int returnQueuedEmailId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[18];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@QueuedEmailId", queuedemail.QueuedEmailId); 
            p[2] = new SqlParameter("@FromAddress", queuedemail.FromAddress);
            p[3] = new SqlParameter("@FromName", queuedemail.FromName);
            p[4] = new SqlParameter("@ToAddress", queuedemail.ToAddress);
            p[5] = new SqlParameter("@ToName", queuedemail.ToName);
            p[6] = new SqlParameter("@Cc", queuedemail.Cc);
            p[7] = new SqlParameter("@Bcc", queuedemail.Bcc);
            p[8] = new SqlParameter("@SubjectCode", queuedemail.SubjectCode);
            p[9] = new SqlParameter("@Subject", queuedemail.Subject);
            p[10] = new SqlParameter("@Body", queuedemail.Body);
            p[11] = new SqlParameter("@SentDate", queuedemail.SentDate);
            p[12] = new SqlParameter("@SentTries", queuedemail.SentTries);
            p[13] = new SqlParameter("@Priority", queuedemail.Priority);

            p[14] = new SqlParameter("@UserId", userId);

            p[15] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 20);
            p[15].Direction = System.Data.ParameterDirection.Output;

            p[16] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 200);
            p[16].Direction = System.Data.ParameterDirection.Output;

            p[17] = new SqlParameter("@ReturnQueuedEmailId", System.Data.SqlDbType.Int);
            p[17].Direction = System.Data.ParameterDirection.Output;

            var result = _context.CallSP(csp_QueuedEmail_UPDATE, p);

            msgType = (string)p[15].Value;
            msgText = (string)p[16].Value;
            returnQueuedEmailId = (int)p[17].Value;
        }
    }
}

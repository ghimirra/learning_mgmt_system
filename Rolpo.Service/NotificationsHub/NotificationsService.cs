﻿/*
 Service For Notifications
Created by: Prashant
Created On: 13/10/2017
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Configuration;
using System;
using System.Text.RegularExpressions;

namespace Rolpo.Service
{
    public interface INotificationsService
    {
        

        //Notifications GetNotificationsById(int notificationid);
        IEnumerable<NotificationsViewModel> GetNotifications(string userId, NotificationsViewModel_Input input);
        void UpdateNotifications(Notifications notifications, string actionType, string userId, ref int returnNotificationId, ref string msgType, ref string msgText);

        void ProcessNotificationChroneJob();

        void AddEmail(string EmailType, Dictionary<string, string> kvp, MailAddressCollection addressto, string subjectCode = null, string subject = null);
    }

    public class NotificationsService : INotificationsService
    {

       IContext _context;
        QueuedEmailService _queuedEmailService;
        public NotificationsService(IContext context) 
        {
            _context = context;
            _queuedEmailService = new QueuedEmailService(this._context);
        }


      
        //Stored Procedures definition 
        StoredProc csp_Notifications_GET = new StoredProc().HasName("[csp.Notifications.Get]").ReturnsTypes(typeof(NotificationsViewModel));
        StoredProc csp_Notifications_UPDATE = new StoredProc().HasName("[csp.Notifications.Update]");

        /// <summary>
        /// Get Notifications By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

    
        /// <summary>
        /// Get Notifications
        /// </summary>

        public IEnumerable<NotificationsViewModel> GetNotifications(string userId, NotificationsViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[6];

            p[0] = new SqlParameter("@NotificationId", input.NotificationId);
            p[1] = new SqlParameter("@NotifyCode", input.NotifyCode);

            p[2] = new SqlParameter("@UserId", userId);
            p[3] = new SqlParameter("@PageNumber", input.PageNumber);
            p[4] = new SqlParameter("@PageSize", input.PageSize);
            p[5] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_Notifications_GET, p);
            return results.ToList<NotificationsViewModel>();
        }

        /// <summary>
        /// Update Notifications
        /// </summary>

        public void UpdateNotifications(Notifications notifications, string actionType, string userId, ref int returnNotificationId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[15];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@NotificationId", notifications.NotificationId);
            p[2] = new SqlParameter("@OrganizationId", notifications.OrganizationId);
            p[3] = new SqlParameter("@NotifyCode", notifications.NotifyCode);
            p[4] = new SqlParameter("@ScheduledDate", notifications.ScheduledDate);
            p[5] = new SqlParameter("@HasEmailQueued", notifications.HasEmailQueued);
            p[6] = new SqlParameter("@AssociatedItemId", notifications.AssociatedItemId);
            p[7] = new SqlParameter("@FromName", notifications.FromName);
            p[8] = new SqlParameter("@FromAddress", notifications.FromAddress);
            p[9] = new SqlParameter("@ToAddresses", notifications.ToAddresses);
            p[10] = new SqlParameter("@Subject", notifications.Subject);

            p[11] = new SqlParameter("@UserId", userId);

            p[12] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 10);
            p[12].Direction = System.Data.ParameterDirection.Output;

            p[13] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 100);
            p[13].Direction = System.Data.ParameterDirection.Output;

            p[14] = new SqlParameter("@ReturnNotificationsId", System.Data.SqlDbType.Int);
            p[14].Direction = System.Data.ParameterDirection.Output;

            var result = _context.CallSP(csp_Notifications_UPDATE, p);

            msgType = (string)p[12].Value;
            msgText = (string)p[13].Value;
            returnNotificationId = (int)p[14].Value;
        }


        public void ProcessNotificationChroneJob()
        {
            var input = new QueuedEmailViewModel_Input() {LoadNotSentItemsOnly = true,PageNumber=1,PageSize=10,ShowAll=1 };
            IEnumerable<QueuedEmailViewModel> emails = _queuedEmailService.GetQueuedEmail(input: input);
            string msgType = string.Empty, msgText = string.Empty;
            int returnQueuedEmailId = 0;

            foreach (QueuedEmailViewModel item in emails)
            {
                
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                msg.From = new MailAddress(item.FromAddress);
                msg.To.Add(new MailAddress(item.ToAddress));
                //msg.To.Add(new MailAddress(item.CC));
                if (!string.IsNullOrEmpty(item.Cc))
                {
                    msg.CC.Add(new MailAddress(item.Cc));
                }

                msg.Subject = item.Subject;
                msg.Body = item.Body;
                msg.IsBodyHtml = true;

                bool sendCompleted = this.sendMail(msg);
                item.SentTries += 1;
                if (sendCompleted)
                {
                    item.SentDate = DateTime.UtcNow;
                }
                else { item.SentDate = null; }
                var queuedEmail = new QueuedEmail()
                {
                    QueuedEmailId = item.QueuedEmailId
                   ,FromAddress = item.FromAddress
                   ,FromName = item.FromName
                   ,ToAddress = item.ToAddress
                   ,ToName = item.ToName
                   ,Cc  = item.Cc
                   ,Bcc  = item.Bcc 
                   ,SubjectCode = item.SubjectCode
                   ,Subject = item.Subject
                   ,Body  = item.Body
                   ,SentDate = item.SentDate
                   ,SentTries  =item.SentTries
                   ,Priority = item.Priority
                };

                _queuedEmailService.UpdateQueuedEmail(queuedemail: queuedEmail,actionType: "UPDATE",userId: ""
                    , returnQueuedEmailId: ref returnQueuedEmailId, msgType: ref msgType, msgText: ref msgText);

                //if (sendCompleted) this.UpdateSentEmail(item.QueuedEmailId, out MsgType, out MsgText);
            }
        }

        public bool sendMail(System.Net.Mail.MailMessage msg)
        {
            SmtpClient smtpClient = new SmtpClient(
                ConfigurationManager.AppSettings["email.SMTP"],
                Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["email.SMTPPort"])
                );

            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(
                ConfigurationManager.AppSettings["email.Account"],
                ConfigurationManager.AppSettings["email.Password"]
                );

            smtpClient.Credentials = credentials;
            smtpClient.EnableSsl = true;
            try
            {

                smtpClient.Send(msg);
                return true;
            }
            catch (Exception e)
            {
                //return Task.FromResult(0);
                return false;
            }

        }


        public void AddEmail(string EmailType,  Dictionary<string, string> kvp, MailAddressCollection addressto, string subjectCode = null, string subject=null)
        {

            var _emailformatService = new EmailFormatService(this._context);

            var input = new EmailFormatViewModel_Input() { Subject = EmailType, PageNumber = 1, PageSize = 1 };
            IEnumerable<EmailFormatViewModel> all_formats = _emailformatService.GetEmailFormat(input:input,userId:"");
            if (all_formats.Count() > 0)
            {

                EmailFormatViewModel email_format = all_formats.FirstOrDefault();

                Regex pattern = new Regex(@"\[\[[^[]*?\]\]");
                foreach (Match match in pattern.Matches(email_format.Body))
                {
                    var matchaftertrim_ = match.Value.Replace("[", "").Replace("]", "");
                    if (kvp.ContainsKey(matchaftertrim_))
                    {
                        email_format.Body = email_format.Body.Replace(match.Value, kvp[matchaftertrim_]);
                    }
                }
                int priority = 0; int returnQueuedEmailId = 0;
                string msgType = string.Empty, msgText = string.Empty;

                if (kvp.ContainsKey("PRIORITY"))
                {
                    int.TryParse(kvp["PRIORITY"], out priority);
                }

                foreach (var address in addressto)
                {

                    QueuedEmail email = new QueuedEmail()
                    {
                        QueuedEmailId = 0,
                        Priority = priority,
                        FromAddress = email_format.From,
                        FromName = email_format.FromName,
                        ToAddress = address.Address,
                        ToName = address.Address,
                        Cc = "",
                        Bcc = string.IsNullOrEmpty(email_format.Bcc) ? "" : email_format.Bcc,
                        Subject = (string.IsNullOrEmpty(subject) ? email_format.Subject:subject),
                        SubjectCode = (string.IsNullOrEmpty(subjectCode) ? email_format.Subject : subjectCode),
                        Body = email_format.Body,
                        CreatedDate = DateTime.UtcNow,
                        SentTries = 0
                    };

                    this._queuedEmailService.UpdateQueuedEmail(queuedemail:email,actionType:"ADD",userId:"",returnQueuedEmailId: ref returnQueuedEmailId, msgType: ref msgType, msgText: ref msgText);
                    if (msgType != "ERROR") {
                        ProcessNotificationChroneJob();
                    }
                }

                
            }


        }
    }
}

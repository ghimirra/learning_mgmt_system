﻿/*
 Service For Member
Created by: Prashant
Created On: 17/06/2018
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System;

namespace Rolpo.Service
{
    public interface IMemberService : IEntityService<Member>
    {
        Member GetMemberById(int memberid);
        IEnumerable<MemberViewModel> GetMember(string userId, MemberViewModel_Input input);
        IEnumerable<MemberViewModel> GetTrainee(string userId, MemberViewModel_Input input);
        void UpdateMember(Member member, string actionType, string userId, ref int returnMemberId, ref string msgType, ref string msgText);
    }

    public class MemberService : EntityService<Member>, IMemberService
    {

        new IContext _context;
        public MemberService(IContext context) : base(context)
        {
            _context = context;
            _dbset = _context.Set<Member>();
        }

        //Stored Procedures definition 
        StoredProc csp_Member_GET = new StoredProc().HasName("[csp.Member.Get]").ReturnsTypes(typeof(MemberViewModel));
        StoredProc csp_Trainee_GET = new StoredProc().HasName("[csp.Trainee.Get]").ReturnsTypes(typeof(MemberViewModel));        
        StoredProc csp_Member_UPDATE = new StoredProc().HasName("[csp.Member.Update]");

        /// <summary>
        /// Get Member By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

        public Member GetMemberById(int memberid)
        {
            return _dbset.FirstOrDefault(x => x.MemberId == memberid);
        }

        /// <summary>
        /// Get Member
        /// </summary>

        public IEnumerable<MemberViewModel> GetMember(string userId, MemberViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[9];

            p[0] = new SqlParameter("@MemberId", input.MemberId);
            p[1] = new SqlParameter("@Designation", input.Designation);
            p[2] = new SqlParameter("@Phone", input.Phone);
            p[3] = new SqlParameter("@Mobile", input.Mobile);

            p[4] = new SqlParameter("@UserId", userId);
            p[5] = new SqlParameter("@PageNumber", input.PageNumber);
            p[6] = new SqlParameter("@PageSize", input.PageSize);
            p[7] = new SqlParameter("@ShowAll", input.ShowAll);
            p[8] = new SqlParameter("@Keyword", input.Keyword);


            var results = _context.CallSP(csp_Member_GET, p);
            return results.ToList<MemberViewModel>();
        }

        /// <summary>
        /// Update Member
        /// </summary>

        public void UpdateMember(Member member, string actionType, string userId, ref int returnMemberId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[22];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@MemberId", member.MemberId);
            p[2] = new SqlParameter("@OrganizationId", member.OrganizationId);
            p[3] = new SqlParameter("@FirstName", member.FirstName);
            p[4] = new SqlParameter("@LastName", member.LastName);
            p[5] = new SqlParameter("@SystemRoleId", member.SystemRoleId);
            p[6] = new SqlParameter("@Designation", member.Designation);
            p[7] = new SqlParameter("@LevelId", member.LevelId);
            p[8] = new SqlParameter("@Department", member.Department);
            p[9] = new SqlParameter("@Address", member.Address);
            p[10] = new SqlParameter("@PrimaryContactEmail", member.PrimaryContactEmail);
            p[11] = new SqlParameter("@Phone", member.Phone);
            p[12] = new SqlParameter("@Mobile", member.Mobile);
            p[13] = new SqlParameter("@ProfileImageUrl", member.ProfileImageUrl);
            p[14] = new SqlParameter("@Education", member.Education);
            p[15] = new SqlParameter("@Notes", member.Notes);

            p[16] = new SqlParameter("@UserId", userId);

            p[17] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 10);
            p[17].Direction = System.Data.ParameterDirection.Output;

            p[18] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 100);
            p[18].Direction = System.Data.ParameterDirection.Output;

            p[19] = new SqlParameter("@ReturnMemberId", System.Data.SqlDbType.Int);
            p[19].Direction = System.Data.ParameterDirection.Output;

            p[20] = new SqlParameter("@Company", member.CompanyName);
            p[21] = new SqlParameter("@Position", member.Position);
            var result = _context.CallSP(csp_Member_UPDATE, p);

            msgType = (string)p[17].Value;
            msgText = (string)p[18].Value;
            returnMemberId = (int)p[19].Value;
        }

        public IEnumerable<MemberViewModel> GetTrainee(string userId, MemberViewModel_Input input)
        {
            SqlParameter[] p = new SqlParameter[9];

            p[0] = new SqlParameter("@MemberId", input.MemberId);
            p[1] = new SqlParameter("@Designation", input.Designation);
            p[2] = new SqlParameter("@Phone", input.Phone);
            p[3] = new SqlParameter("@Mobile", input.Mobile);

            p[4] = new SqlParameter("@UserId", userId);
            p[5] = new SqlParameter("@PageNumber", input.PageNumber);
            p[6] = new SqlParameter("@PageSize", input.PageSize);
            p[7] = new SqlParameter("@ShowAll", input.ShowAll);
            p[8] = new SqlParameter("@Keyword", input.Keyword);

            var results = _context.CallSP(csp_Trainee_GET, p);
            return results.ToList<MemberViewModel>();
        }
    }
}

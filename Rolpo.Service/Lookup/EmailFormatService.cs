﻿/*
 Service For EmailFormat
Created by: Prashant
Created On: 3/24/2018
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface IEmailFormatService : IEntityService<EmailFormat>
    {
        EmailFormat GetEmailFormatById(int emailformatid);
        IEnumerable<EmailFormatViewModel> GetEmailFormat(EmailFormatViewModel_Input input, string userId = "");
        void UpdateEmailFormat(EmailFormat emailformat, string actionType, string userId, ref int returnEmailFormatId, ref string msgType, ref string msgText);
    }

    public class EmailFormatService : EntityService<EmailFormat>, IEmailFormatService
    {

        new IContext _context;
        public EmailFormatService(IContext context) : base(context)
        {
            _context = context;
            _dbset = _context.Set<EmailFormat>();
        }

        //Stored Procedures definition 
        StoredProc csp_EmailFormat_GET = new StoredProc().HasName("[csp.EmailFormat.Get]").ReturnsTypes(typeof(EmailFormatViewModel));
        StoredProc csp_EmailFormat_UPDATE = new StoredProc().HasName("[csp.EmailFormat.Update]");

        /// <summary>
        /// Get EmailFormat By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

        public EmailFormat GetEmailFormatById(int emailformatid)
        {
            return _dbset.FirstOrDefault(x => x.EmailFormatId == emailformatid);
        }

        /// <summary>
        /// Get EmailFormat
        /// </summary>

        public IEnumerable<EmailFormatViewModel> GetEmailFormat(EmailFormatViewModel_Input input, string userId = "")
        {

            SqlParameter[] p = new SqlParameter[7];

            p[0] = new SqlParameter("@EmailFormatId", input.EmailFormatId);
            p[1] = new SqlParameter("@SearchText", input.SearchText);
            p[2] = new SqlParameter("@Subject", input.Subject);

            p[3] = new SqlParameter("@UserId", userId);
            p[4] = new SqlParameter("@PageNumber", input.PageNumber);
            p[5] = new SqlParameter("@PageSize", input.PageSize);
            p[6] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_EmailFormat_GET, p);
            return results.ToList<EmailFormatViewModel>();
        }

        /// <summary>
        /// Update EmailFormat
        /// </summary>

        public void UpdateEmailFormat(EmailFormat emailformat, string actionType, string userId, ref int returnEmailFormatId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[11];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@EmailFormatId", emailformat.EmailFormatId); 
            p[2] = new SqlParameter("@From", emailformat.From);
            p[3] = new SqlParameter("@FromName", emailformat.FromName);
            p[4] = new SqlParameter("@Bcc", emailformat.Bcc);
            p[5] = new SqlParameter("@Subject", emailformat.Subject);
            p[6] = new SqlParameter("@Body", emailformat.Body);

            p[7] = new SqlParameter("@UserId", userId);

            p[8] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 20);
            p[8].Direction = System.Data.ParameterDirection.Output;

            p[9] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 200);
            p[9].Direction = System.Data.ParameterDirection.Output;

            p[10] = new SqlParameter("@ReturnEmailFormatId", System.Data.SqlDbType.Int);
            p[10].Direction = System.Data.ParameterDirection.Output;

            var result = _context.CallSP(csp_EmailFormat_UPDATE, p);

            msgType = (string)p[8].Value;
            msgText = (string)p[9].Value;
            returnEmailFormatId = (int)p[10].Value;
        }
    }
}

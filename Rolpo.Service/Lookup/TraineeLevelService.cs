﻿/*
 Service For TraineeLevel
Created by: Prashant
Created On: 26/06/2018
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface ITraineeLevelService : IEntityService<TraineeLevel>
    {
        TraineeLevel GetTraineeLevelById(int traineelevelid);
        IEnumerable<TraineeLevelViewModel> GetTraineeLevel(string userId, TraineeLevelViewModel_Input input);
        void UpdateTraineeLevel(TraineeLevel traineelevel, string actionType, string userId, ref int returnTraineeLevelId, ref string msgType, ref string msgText);
    }

    public class TraineeLevelService : EntityService<TraineeLevel>, ITraineeLevelService
    {

        new IContext _context;
        public TraineeLevelService(IContext context) : base(context)
        {
            _context = context;
            _dbset = _context.Set<TraineeLevel>();
        }

        //Stored Procedures definition 
        StoredProc csp_TraineeLevel_GET = new StoredProc().HasName("[csp.TraineeLevel.Get]").ReturnsTypes(typeof(TraineeLevelViewModel));
        StoredProc csp_TraineeLevel_UPDATE = new StoredProc().HasName("[csp.TraineeLevel.Update]");

        /// <summary>
        /// Get TraineeLevel By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

        public TraineeLevel GetTraineeLevelById(int traineelevelid)
        {
            return _dbset.FirstOrDefault(x => x.TraineeLevelId == traineelevelid);
        }

        /// <summary>
        /// Get TraineeLevel
        /// </summary>

        public IEnumerable<TraineeLevelViewModel> GetTraineeLevel(string userId, TraineeLevelViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[6];

            p[0] = new SqlParameter("@TraineeLevelId", input.TraineeLevelId);
            p[1] = new SqlParameter("@TraineeLevelName", input.TraineeLevelName);

            p[2] = new SqlParameter("@UserId", userId);
            p[3] = new SqlParameter("@PageNumber", input.PageNumber);
            p[4] = new SqlParameter("@PageSize", input.PageSize);
            p[5] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_TraineeLevel_GET, p);
            return results.ToList<TraineeLevelViewModel>();
        }

        /// <summary>
        /// Update TraineeLevel
        /// </summary>

        public void UpdateTraineeLevel(TraineeLevel traineelevel, string actionType, string userId, ref int returnTraineeLevelId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[9];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@TraineeLevelId", traineelevel.TraineeLevelId);
            p[2] = new SqlParameter("@OrganizationId", traineelevel.OrganizationId);
            p[3] = new SqlParameter("@TraineeLevelName", traineelevel.TraineeLevelName);
            p[4] = new SqlParameter("@Remarks", traineelevel.Remarks);

            p[5] = new SqlParameter("@UserId", userId);

            p[6] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 10);
            p[6].Direction = System.Data.ParameterDirection.Output;

            p[7] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 100);
            p[7].Direction = System.Data.ParameterDirection.Output;

            p[8] = new SqlParameter("@ReturnTraineeLevelId", System.Data.SqlDbType.Int);
            p[8].Direction = System.Data.ParameterDirection.Output;

            var result = _context.CallSP(csp_TraineeLevel_UPDATE, p);

            msgType = (string)p[6].Value;
            msgText = (string)p[7].Value;
            returnTraineeLevelId = (int)p[8].Value;
        }
    }
}

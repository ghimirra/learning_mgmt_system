﻿/*
 Service For CourseLevel
Created by: Prashant
Created On: 27/06/2018
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface ICourseLevelService : IEntityService<CourseLevel>
    {
        CourseLevel GetCourseLevelById(int courselevelid);
        IEnumerable<CourseLevelViewModel> GetCourseLevel(string userId, CourseLevelViewModel_Input input);
        void UpdateCourseLevel(CourseLevel courselevel, string actionType, string userId, ref int returnCourseLevelId, ref string msgType, ref string msgText);
    }

    public class CourseLevelService : EntityService<CourseLevel>, ICourseLevelService
    {

        new IContext _context;
        public CourseLevelService(IContext context) : base(context)
        {
            _context = context;
            _dbset = _context.Set<CourseLevel>();
        }

        //Stored Procedures definition 
        StoredProc csp_CourseLevel_GET = new StoredProc().HasName("[csp.CourseLevel.Get]").ReturnsTypes(typeof(CourseLevelViewModel));
        StoredProc csp_CourseLevel_UPDATE = new StoredProc().HasName("[csp.CourseLevel.Update]");

        /// <summary>
        /// Get CourseLevel By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

        public CourseLevel GetCourseLevelById(int courselevelid)
        {
            return _dbset.FirstOrDefault(x => x.CourseLevelId == courselevelid);
        }

        /// <summary>
        /// Get CourseLevel
        /// </summary>

        public IEnumerable<CourseLevelViewModel> GetCourseLevel(string userId, CourseLevelViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[6];

            p[0] = new SqlParameter("@CourseLevelId", input.CourseLevelId);
            p[1] = new SqlParameter("@LevelName", input.LevelName);

            p[2] = new SqlParameter("@UserId", userId);
            p[3] = new SqlParameter("@PageNumber", input.PageNumber);
            p[4] = new SqlParameter("@PageSize", input.PageSize);
            p[5] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_CourseLevel_GET, p);
            return results.ToList<CourseLevelViewModel>();
        }

        /// <summary>
        /// Update CourseLevel
        /// </summary>

        public void UpdateCourseLevel(CourseLevel courselevel, string actionType, string userId, ref int returnCourseLevelId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[9];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@CourseLevelId", courselevel.CourseLevelId);
            p[2] = new SqlParameter("@OrganizationId", courselevel.OrganizationId);
            p[3] = new SqlParameter("@LevelName", courselevel.LevelName);
            p[4] = new SqlParameter("@Remarks", courselevel.Remarks);

            p[5] = new SqlParameter("@UserId", userId);

            p[6] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 10);
            p[6].Direction = System.Data.ParameterDirection.Output;

            p[7] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 100);
            p[7].Direction = System.Data.ParameterDirection.Output;

            p[8] = new SqlParameter("@ReturnCourseLevelId", System.Data.SqlDbType.Int);
            p[8].Direction = System.Data.ParameterDirection.Output;

            var result = _context.CallSP(csp_CourseLevel_UPDATE, p);

            msgType = (string)p[6].Value;
            msgText = (string)p[7].Value;
            returnCourseLevelId = (int)p[8].Value;
        }
    }
}

﻿/*
 Service For CourseCategory
Created by: Prashant
Created On: 26/06/2018
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface ICourseCategoryService : IEntityService<CourseCategory>
    {
        CourseCategory GetCourseCategoryById(int coursecategoryid);
        IEnumerable<CourseCategoryViewModel> GetCourseCategory(string userId, CourseCategoryViewModel_Input input);
        void UpdateCourseCategory(CourseCategory coursecategory, string actionType, string userId, ref int returnCourseCategoryId, ref string msgType, ref string msgText);
        IEnumerable<ParentCategoryViewModel> GetCourseCategoryByParent(string userId, CourseCategoryViewModel_Input input);

    }

    public class CourseCategoryService : EntityService<CourseCategory>, ICourseCategoryService
    {

        new IContext _context;
        public CourseCategoryService(IContext context) : base(context)
        {
            _context = context;
            _dbset = _context.Set<CourseCategory>();
        }

        //Stored Procedures definition 
        StoredProc csp_CourseCategory_GET = new StoredProc().HasName("[csp.CourseCategory.Get]").ReturnsTypes(typeof(CourseCategoryViewModel));
        StoredProc csp_CourseCategory_GETPARENTS = new StoredProc().HasName("[csp.CourseCategory.GetParents]").ReturnsTypes(typeof(ParentCategoryViewModel));
        StoredProc csp_CourseCategory_UPDATE = new StoredProc().HasName("[csp.CourseCategory.Update]");

        /// <summary>
        /// Get CourseCategory By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

        public CourseCategory GetCourseCategoryById(int coursecategoryid)
        {
            return _dbset.FirstOrDefault(x => x.CourseCategoryId == coursecategoryid);
        }

        /// <summary>
        /// Get CourseCategory
        /// </summary>

        public IEnumerable<CourseCategoryViewModel> GetCourseCategory(string userId, CourseCategoryViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[6];

            p[0] = new SqlParameter("@CourseCategoryId", input.CourseCategoryId);
            p[1] = new SqlParameter("@CategoryName", input.CategoryName);

            p[2] = new SqlParameter("@UserId", userId);
            p[3] = new SqlParameter("@PageNumber", input.PageNumber);
            p[4] = new SqlParameter("@PageSize", input.PageSize);
            p[5] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_CourseCategory_GET, p);
            return results.ToList<CourseCategoryViewModel>();
        }


        /// <summary>
        /// Get CourseCategory By parent
        /// </summary>

        public IEnumerable<ParentCategoryViewModel> GetCourseCategoryByParent(string userId, CourseCategoryViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[6];

            p[0] = new SqlParameter("@CourseCategoryId", input.CourseCategoryId);
            p[1] = new SqlParameter("@CategoryName", input.CategoryName);

            p[2] = new SqlParameter("@UserId", userId);
            p[3] = new SqlParameter("@PageNumber", input.PageNumber);
            p[4] = new SqlParameter("@PageSize", input.PageSize);
            p[5] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_CourseCategory_GETPARENTS, p);
            return results.ToList<ParentCategoryViewModel>();
        }
        /// <summary>
        /// Update CourseCategory
        /// </summary>

        public void UpdateCourseCategory(CourseCategory coursecategory, string actionType, string userId, ref int returnCourseCategoryId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[10];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@CourseCategoryId", coursecategory.CourseCategoryId);
            p[2] = new SqlParameter("@OrganizationId", coursecategory.OrganizationId);
            p[3] = new SqlParameter("@CategoryName", coursecategory.CategoryName);
            p[4] = new SqlParameter("@Remarks", coursecategory.Remarks);
            p[5] = new SqlParameter("@ParentCategoryId", coursecategory.ParentCategoryId);

            p[6] = new SqlParameter("@UserId", userId);

            p[7] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 10);
            p[7].Direction = System.Data.ParameterDirection.Output;

            p[8] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 100);
            p[8].Direction = System.Data.ParameterDirection.Output;

            p[9] = new SqlParameter("@ReturnCourseCategoryId", System.Data.SqlDbType.Int);
            p[9].Direction = System.Data.ParameterDirection.Output;

            var result = _context.CallSP(csp_CourseCategory_UPDATE, p);

            msgType = (string)p[7].Value;
            msgText = (string)p[8].Value;
            returnCourseCategoryId = (int)p[9].Value;
        }
    }
}

﻿using Rolpo.Model;
using System.Linq;

namespace Rolpo.Service
{

    public interface IOrganizationService : IEntityService<Organization>
        {
            Organization GetOrganizationByDomain();
        }

        public class OrganizationService : EntityService<Organization>, IOrganizationService
        {

            new IContext _context;
            public OrganizationService(IContext context) : base(context)
            {
                _context = context;
                _dbset = _context.Set<Organization>();
            }

            /// <summary>
            /// Get Organization by domain name
            /// </summary>
            /// <param name="domainName"></param>
            /// <returns></returns>
            public Organization GetOrganizationByDomain()
            {
                return _dbset.FirstOrDefault();
            }


    }
}

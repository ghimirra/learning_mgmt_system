﻿/*
 Service For Media
Created by: Prashant
Created On: 21/06/2018
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface IMediaService : IEntityService<Media>
    {
        Media GetMediaById(int mediaid);
        IEnumerable<MediaViewModel> GetMedia(string userId, MediaViewModel_Input input);
        void UpdateMedia(Media media, string actionType, string userId, ref int returnMediaId, ref string msgType, ref string msgText);
    }

    public class MediaService : EntityService<Media>, IMediaService
    {

        new IContext _context;
        public MediaService(IContext context) : base(context)
        {
            _context = context;
            _dbset = _context.Set<Media>();
        }

        //Stored Procedures definition 
        StoredProc csp_Media_GET = new StoredProc().HasName("[csp.Media.Get]").ReturnsTypes(typeof(MediaViewModel));
        StoredProc csp_Media_UPDATE = new StoredProc().HasName("[csp.Media.Update]");

        /// <summary>
        /// Get Media By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

        public Media GetMediaById(int mediaid)
        {
            return _dbset.FirstOrDefault(x => x.MediaId == mediaid);
        }

        /// <summary>
        /// Get Media
        /// </summary>

        public IEnumerable<MediaViewModel> GetMedia(string userId, MediaViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[6];

            p[0] = new SqlParameter("@MediaId", input.MediaId); 
            p[1] = new SqlParameter("@MediaName", input.MediaName);

            p[2] = new SqlParameter("@UserId", userId);
            p[3] = new SqlParameter("@PageNumber", input.PageNumber);
            p[4] = new SqlParameter("@PageSize", input.PageSize);
            p[5] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_Media_GET, p);
            return results.ToList<MediaViewModel>();
        }

        /// <summary>
        /// Update Media
        /// </summary>

        public void UpdateMedia(Media media, string actionType, string userId, ref int returnMediaId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[9];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@MediaId", media.MediaId);
            p[2] = new SqlParameter("@OrganizationId", media.OrganizationId);
            p[3] = new SqlParameter("@MediaName", media.MediaName);

            p[4] = new SqlParameter("@UserId", userId);

            p[5] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 10);
            p[5].Direction = System.Data.ParameterDirection.Output;

            p[6] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 100);
            p[6].Direction = System.Data.ParameterDirection.Output;

            p[7] = new SqlParameter("@ReturnMediaId", System.Data.SqlDbType.Int);
            p[7].Direction = System.Data.ParameterDirection.Output;

            p[8] = new SqlParameter("@Remarks", media.Remarks);

            var result = _context.CallSP(csp_Media_UPDATE, p);

            msgType = (string)p[5].Value;
            msgText = (string)p[6].Value;
            returnMediaId = (int)p[7].Value;
        }
    }
}

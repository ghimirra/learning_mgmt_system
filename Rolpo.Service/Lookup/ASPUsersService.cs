﻿/*
 Service For ASPNETUsers
Created by: Prashant
Created On: 01/06/2016
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface IASPNETUsersService 
    {
        System.Tuple<List<ASPNETUsersViewModel>, List<RolesViewModel>> GetASPNETUsers(string userId, ASPNETUsersViewModel_Input input);
        void UpdateASPNETUsers(ASPNETUsersViewModel aspnetusers, string actionType, string userId, ref string returnId, ref string msgType, ref string msgText);
    }

    public class ASPNETUsersService :  IASPNETUsersService
    {

        IContext _context;
        public ASPNETUsersService(IContext context) 
        {
            _context = context;
        }

        //Stored Procedures definition 
        StoredProc csp_ASPNETUsers_GET = new StoredProc().HasName("[csp.ASPNETUsers.Get]").ReturnsTypes(typeof(ASPNETUsersViewModel),typeof(RolesViewModel));
        StoredProc csp_ASPNETUsers_UPDATE = new StoredProc().HasName("[csp.ASPNETUsers.Update]");

        

        /// <summary>
        /// Get ASPNETUsers
        /// </summary>

        public System.Tuple<List<ASPNETUsersViewModel>, List<RolesViewModel>> GetASPNETUsers(string userId, ASPNETUsersViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[9];

            p[0] = new SqlParameter("@Id", input.Id);
            p[1] = new SqlParameter("@FirstName", input.FirstName);
            p[2] = new SqlParameter("@LastName", input.LastName);
            p[3] = new SqlParameter("@Email", input.Email);
            p[4] = new SqlParameter("@SearchText", input.SearchText);

            p[5] = new SqlParameter("@UserId", userId);
            p[6] = new SqlParameter("@PageNumber", input.PageNumber);
            p[7] = new SqlParameter("@PageSize", input.PageSize);
            p[8] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_ASPNETUsers_GET, p);
            System.Tuple<List<ASPNETUsersViewModel>, List<RolesViewModel>> rs = new System.Tuple<List<ASPNETUsersViewModel>, List<RolesViewModel>>(results.ToList<ASPNETUsersViewModel>(), results.ToList<RolesViewModel>());

            return rs;
        }

        /// <summary>
        /// Update ASPNETUsers
        /// </summary>

        public void UpdateASPNETUsers(ASPNETUsersViewModel aspnetusers, string actionType, string userId, ref string returnId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[9];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@Id", aspnetusers.Id);
            p[2] = new SqlParameter("@FirstName", aspnetusers.FirstName);
            p[3] = new SqlParameter("@LastName", aspnetusers.LastName);
            p[4] = new SqlParameter("@Email", aspnetusers.Email);

            p[5] = new SqlParameter("@UserId", userId);

            p[6] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 20);
            p[6].Direction = System.Data.ParameterDirection.Output;

            p[7] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 200);
            p[7].Direction = System.Data.ParameterDirection.Output;

            p[8] = new SqlParameter("@ReturnASPNETUsersId", System.Data.SqlDbType.VarChar,100);
            p[8].Direction = System.Data.ParameterDirection.Output;

            var result = _context.CallSP(csp_ASPNETUsers_UPDATE, p);

            msgType = (string)p[6].Value;
            msgText = (string)p[7].Value;
            returnId = (string)p[8].Value;
        }
    }
}

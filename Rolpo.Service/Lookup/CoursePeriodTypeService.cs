﻿/*
 Service For CoursePeriodType
Created by: Prashant
Created On: 27/06/2018
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface ICoursePeriodTypeService : IEntityService<CoursePeriodType>
    {
        CoursePeriodType GetCoursePeriodTypeById(int courseperiodtypeid);
        IEnumerable<CoursePeriodTypeViewModel> GetCoursePeriodType(string userId, CoursePeriodTypeViewModel_Input input);
        void UpdateCoursePeriodType(CoursePeriodType courseperiodtype, string actionType, string userId, ref int returnCoursePeriodTypeId, ref string msgType, ref string msgText);
    }

    public class CoursePeriodTypeService : EntityService<CoursePeriodType>, ICoursePeriodTypeService
    {

        new IContext _context;
        public CoursePeriodTypeService(IContext context) : base(context)
        {
            _context = context;
            _dbset = _context.Set<CoursePeriodType>();
        }

        //Stored Procedures definition 
        StoredProc csp_CoursePeriodType_GET = new StoredProc().HasName("[csp.CoursePeriodType.Get]").ReturnsTypes(typeof(CoursePeriodTypeViewModel));
        StoredProc csp_CoursePeriodType_UPDATE = new StoredProc().HasName("[csp.CoursePeriodType.Update]");

        /// <summary>
        /// Get CoursePeriodType By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

        public CoursePeriodType GetCoursePeriodTypeById(int courseperiodtypeid)
        {
            return _dbset.FirstOrDefault(x => x.CoursePeriodTypeId == courseperiodtypeid);
        }

        /// <summary>
        /// Get CoursePeriodType
        /// </summary>

        public IEnumerable<CoursePeriodTypeViewModel> GetCoursePeriodType(string userId, CoursePeriodTypeViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[6];

            p[0] = new SqlParameter("@CoursePeriodTypeId", input.CoursePeriodTypeId);
            p[1] = new SqlParameter("@PeriodTypeName", input.PeriodTypeName);

            p[2] = new SqlParameter("@UserId", userId);
            p[3] = new SqlParameter("@PageNumber", input.PageNumber);
            p[4] = new SqlParameter("@PageSize", input.PageSize);
            p[5] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_CoursePeriodType_GET, p);
            return results.ToList<CoursePeriodTypeViewModel>();
        }

        /// <summary>
        /// Update CoursePeriodType
        /// </summary>

        public void UpdateCoursePeriodType(CoursePeriodType courseperiodtype, string actionType, string userId, ref int returnCoursePeriodTypeId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[9];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@CoursePeriodTypeId", courseperiodtype.CoursePeriodTypeId);
            p[2] = new SqlParameter("@OrganizationId", courseperiodtype.OrganizationId);
            p[3] = new SqlParameter("@PeriodTypeName", courseperiodtype.PeriodTypeName);
            p[4] = new SqlParameter("@Remarks", courseperiodtype.Remarks);

            p[5] = new SqlParameter("@UserId", userId);

            p[6] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 10);
            p[6].Direction = System.Data.ParameterDirection.Output;

            p[7] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 100);
            p[7].Direction = System.Data.ParameterDirection.Output;

            p[8] = new SqlParameter("@ReturnCoursePeriodTypeId", System.Data.SqlDbType.Int);
            p[8].Direction = System.Data.ParameterDirection.Output;

            var result = _context.CallSP(csp_CoursePeriodType_UPDATE, p);

            msgType = (string)p[6].Value;
            msgText = (string)p[7].Value;
            returnCoursePeriodTypeId = (int)p[8].Value;
        }
    }
}

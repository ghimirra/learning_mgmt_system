﻿/*
 Service For Status
Created by: Prashant
Created On: 24/06/2018
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface IStatusService : IEntityService<Status>
    {
        Status GetStatusById(int statusid);
        IEnumerable<StatusViewModel> GetStatus(string userId, StatusViewModel_Input input);
        void UpdateStatus(Status status, string actionType, string userId, ref int returnStatusId, ref string msgType, ref string msgText);
    }

    public class StatusService : EntityService<Status>, IStatusService
    {

        new IContext _context;
        public StatusService(IContext context) : base(context)
        {
            _context = context;
            _dbset = _context.Set<Status>();
        }

        //Stored Procedures definition 
        StoredProc csp_Status_GET = new StoredProc().HasName("[csp.Status.Get]").ReturnsTypes(typeof(StatusViewModel));
        StoredProc csp_Status_UPDATE = new StoredProc().HasName("[csp.Status.Update]");

        /// <summary>
        /// Get Status By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

        public Status GetStatusById(int statusid)
        {
            return _dbset.FirstOrDefault(x => x.StatusId == statusid);
        }

        /// <summary>
        /// Get Status
        /// </summary>

        public IEnumerable<StatusViewModel> GetStatus(string userId, StatusViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[7];

            p[0] = new SqlParameter("@StatusId", input.StatusId);
            p[1] = new SqlParameter("@StatusGroup", input.StatusGroup);
            p[2] = new SqlParameter("@StatusName", input.StatusName);

            p[3] = new SqlParameter("@UserId", userId);
            p[4] = new SqlParameter("@PageNumber", input.PageNumber);
            p[5] = new SqlParameter("@PageSize", input.PageSize);
            p[6] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_Status_GET, p);
            return results.ToList<StatusViewModel>();
        }

        /// <summary>
        /// Update Status
        /// </summary>

        public void UpdateStatus(Status status, string actionType, string userId, ref int returnStatusId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[12];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@StatusId", status.StatusId);
            p[2] = new SqlParameter("@OrganizationId", status.OrganizationId);
            p[3] = new SqlParameter("@StatusGroup", status.StatusGroup);
            p[4] = new SqlParameter("@StatusName", status.StatusName);
            p[5] = new SqlParameter("@StatusCode", status.StatusCode);
            p[6] = new SqlParameter("@StatusOrder", status.StatusOrder);
            p[7] = new SqlParameter("@AllowBackRevert", status.AllowBackRevert);

            p[8] = new SqlParameter("@UserId", userId);

            p[9] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 10);
            p[9].Direction = System.Data.ParameterDirection.Output;

            p[10] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 100);
            p[10].Direction = System.Data.ParameterDirection.Output;

            p[11] = new SqlParameter("@ReturnStatusId", System.Data.SqlDbType.Int);
            p[11].Direction = System.Data.ParameterDirection.Output;

            var result = _context.CallSP(csp_Status_UPDATE, p);

            msgType = (string)p[9].Value;
            msgText = (string)p[10].Value;
            returnStatusId = (int)p[11].Value;
        }
    }
}

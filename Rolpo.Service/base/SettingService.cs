﻿/*
 Service For Rolpo
Created by: Prashant
Created On: 3/24/2018
*/

using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface ISettingsService
    {
        //IList<TableNames> GetTableNames();
        DDLJson GetDDLItemsList(string pageName, string ddlListXML, Guid organizationId);
        
    }

    public class SettingsService : ISettingsService
    {
        IContext _context;
        public SettingsService(IContext context)
        {
            _context = context;

        }


          
        //Stored Procedures definition 
        StoredProc csp_DDLItems_GET = new StoredProc().HasName("[csp.DDLItems.Get]").ReturnsTypes(typeof(DDLJson));
 
        public DDLJson GetDDLItemsList(string pageName, string ddlListXML, Guid organizationId)
        {
            SqlParameter[] p = new SqlParameter[3];
            p[0] = new SqlParameter("@PageName", pageName);
            p[1] = new SqlParameter("@DDLListXML", ddlListXML);
            p[2] = new SqlParameter("@OrganizationId", organizationId);

            var results = _context.CallSP(csp_DDLItems_GET, p);

            DDLJson ddlJson = null;

            if (results.Count > 0)
            {
                ddlJson = results.ToList<DDLJson>()[0];
            }
            return ddlJson;
        }
 
         
    }
}

﻿/*
 Service For BatchTrainer
Created by: Prashant
Created On: 24/06/2018
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface IBatchTrainerService : IEntityService<BatchTrainer>
    {
        BatchTrainer GetBatchTrainerById(int batchtrainerid);
        IEnumerable<BatchTrainerViewModel> GetBatchTrainer(string userId, BatchTrainerViewModel_Input input);
        void UpdateBatchTrainer(BatchTrainer batchtrainer, string actionType, string userId, ref int returnBatchTrainerId, ref string msgType, ref string msgText);
    }

    public class BatchTrainerService : EntityService<BatchTrainer>, IBatchTrainerService
    {

        new IContext _context;
        public BatchTrainerService(IContext context) : base(context)
        {
            _context = context;
            _dbset = _context.Set<BatchTrainer>();
        }

        //Stored Procedures definition 
        StoredProc csp_BatchTrainer_GET = new StoredProc().HasName("[csp.BatchTrainer.Get]").ReturnsTypes(typeof(BatchTrainerViewModel));
        StoredProc csp_BatchTrainer_UPDATE = new StoredProc().HasName("[csp.BatchTrainer.Update]");

        /// <summary>
        /// Get BatchTrainer By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

        public BatchTrainer GetBatchTrainerById(int batchtrainerid)
        {
            return _dbset.FirstOrDefault(x => x.BatchTrainerId == batchtrainerid);
        }

        /// <summary>
        /// Get BatchTrainer
        /// </summary>

        public IEnumerable<BatchTrainerViewModel> GetBatchTrainer(string userId, BatchTrainerViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[7];

            p[0] = new SqlParameter("@BatchTrainerId", input.BatchTrainerId);
            p[1] = new SqlParameter("@MemberId", input.MemberId);
            p[2] = new SqlParameter("@BatchId", input.BatchId);

            p[3] = new SqlParameter("@UserId", userId);
            p[4] = new SqlParameter("@PageNumber", input.PageNumber);
            p[5] = new SqlParameter("@PageSize", input.PageSize);
            p[6] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_BatchTrainer_GET, p);
            return results.ToList<BatchTrainerViewModel>();
        }

        /// <summary>
        /// Update BatchTrainer
        /// </summary>

        public void UpdateBatchTrainer(BatchTrainer batchtrainer, string actionType, string userId, ref int returnBatchTrainerId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[11];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@BatchTrainerId", batchtrainer.BatchTrainerId);
            p[2] = new SqlParameter("@OrganizationId", batchtrainer.OrganizationId);
            p[3] = new SqlParameter("@MemberId", batchtrainer.MemberId);
            p[4] = new SqlParameter("@BatchId", batchtrainer.BatchId);
            p[5] = new SqlParameter("@ModuleId", batchtrainer.ModuleId);
            p[6] = new SqlParameter("@TrainingStartDate", batchtrainer.TrainingStartDate);

            p[7] = new SqlParameter("@UserId", userId);

            p[8] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 10);
            p[8].Direction = System.Data.ParameterDirection.Output;

            p[9] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 100);
            p[9].Direction = System.Data.ParameterDirection.Output;

            p[10] = new SqlParameter("@ReturnBatchTrainerId", System.Data.SqlDbType.Int);
            p[10].Direction = System.Data.ParameterDirection.Output;

            var result = _context.CallSP(csp_BatchTrainer_UPDATE, p);

            msgType = (string)p[8].Value;
            msgText = (string)p[9].Value;
            returnBatchTrainerId = (int)p[10].Value;
        }
    }
}

﻿/*
 Service For BatchTrainee
Created by: Prashant
Created On: 24/06/2018
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface IBatchTraineeService : IEntityService<BatchTrainee>
    {
        BatchTrainee GetBatchTraineeById(int batchtraineeid);
        IEnumerable<BatchTraineeViewModel> GetBatchTrainee(string userId, BatchTraineeViewModel_Input input);

        IEnumerable<BatchTraineeReportViewModel> GetBatchTraineeForReport(string userId, BatchTraineeViewModel_Input input);

        void UpdateBatchTrainee(BatchTrainee batchtrainee, string actionType, string userId, ref int returnBatchTraineeId, ref string msgType, ref string msgText);
        IEnumerable<MemberViewModel> GetBatchTraineeNotInList(string userId, BatchTraineeViewModel_Input input);
    }

    public class BatchTraineeService : EntityService<BatchTrainee>, IBatchTraineeService
    {

        new IContext _context;
        public BatchTraineeService(IContext context) : base(context)
        {
            _context = context;
            _dbset = _context.Set<BatchTrainee>();
        }

        //Stored Procedures definition 
        StoredProc csp_BatchTrainee_GET = new StoredProc().HasName("[csp.BatchTrainee.Get]").ReturnsTypes(typeof(BatchTraineeViewModel));
        StoredProc csp_BatchTrainee_GETFORREPORT = new StoredProc().HasName("[csp.BatchTrainee.GetForReport]").ReturnsTypes(typeof(BatchTraineeReportViewModel));
        
        StoredProc csp_BatchTrainee_GETNOTINLIST = new StoredProc().HasName("[csp.BatchTrainee.GetNotInList]").ReturnsTypes(typeof(MemberViewModel));
        
        StoredProc csp_BatchTrainee_UPDATE = new StoredProc().HasName("[csp.BatchTrainee.Update]");

        /// <summary>
        /// Get BatchTrainee By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

        public BatchTrainee GetBatchTraineeById(int batchtraineeid)
        {
            return _dbset.FirstOrDefault(x => x.BatchTraineeId == batchtraineeid);
        }



        /// <summary>
        /// Get BatchTrainee
        /// </summary>

        public IEnumerable<BatchTraineeViewModel> GetBatchTrainee(string userId, BatchTraineeViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[6];

            p[0] = new SqlParameter("@BatchTraineeId", input.BatchTraineeId);
            p[1] = new SqlParameter("@BatchId", input.BatchId);

            p[2] = new SqlParameter("@UserId", userId);
            p[3] = new SqlParameter("@PageNumber", input.PageNumber);
            p[4] = new SqlParameter("@PageSize", input.PageSize);
            p[5] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_BatchTrainee_GET, p);
            return results.ToList<BatchTraineeViewModel>();
        }



        /// <summary>
        /// Get BatchTrainee
        /// </summary>

        public IEnumerable<BatchTraineeReportViewModel> GetBatchTraineeForReport(string userId, BatchTraineeViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[6];

            p[0] = new SqlParameter("@BatchTraineeId", input.BatchTraineeId);
            p[1] = new SqlParameter("@BatchId", input.BatchId);

            p[2] = new SqlParameter("@UserId", userId);
            p[3] = new SqlParameter("@PageNumber", input.PageNumber);
            p[4] = new SqlParameter("@PageSize", input.PageSize);
            p[5] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_BatchTrainee_GETFORREPORT, p);
            return results.ToList<BatchTraineeReportViewModel>();
        }

        /// <summary>
        /// Get BatchTrainee Not in List
        /// </summary>

        public IEnumerable<MemberViewModel> GetBatchTraineeNotInList(string userId, BatchTraineeViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[6];

            p[0] = new SqlParameter("@SearchText", input.SearchText);
            p[1] = new SqlParameter("@BatchId", input.BatchId);
            p[2] = new SqlParameter("@UserId", userId);
            p[3] = new SqlParameter("@PageNumber", input.PageNumber);
            p[4] = new SqlParameter("@PageSize", input.PageSize);
            p[5] = new SqlParameter("@ShowAll", input.ShowAll);

            var results = _context.CallSP(csp_BatchTrainee_GETNOTINLIST, p);

            return results.ToList<MemberViewModel>();
        }

        /// <summary>
        /// Update BatchTrainee
        /// </summary>

        public void UpdateBatchTrainee(BatchTrainee batchtrainee, string actionType, string userId, ref int returnBatchTraineeId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[10];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@BatchTraineeId", batchtrainee.BatchTraineeId);
            p[2] = new SqlParameter("@OrganizationId", batchtrainee.OrganizationId);
            p[3] = new SqlParameter("@BatchId", batchtrainee.BatchId);
            p[4] = new SqlParameter("@MemberId", batchtrainee.MemberId);
            p[5] = new SqlParameter("@JoinedDate", batchtrainee.JoinedDate);

            p[6] = new SqlParameter("@UserId", userId);

            p[7] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 10);
            p[7].Direction = System.Data.ParameterDirection.Output;

            p[8] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 100);
            p[8].Direction = System.Data.ParameterDirection.Output;

            p[9] = new SqlParameter("@ReturnBatchTraineeId", System.Data.SqlDbType.Int);
            p[9].Direction = System.Data.ParameterDirection.Output;

            var result = _context.CallSP(csp_BatchTrainee_UPDATE, p);

            msgType = (string)p[7].Value;
            msgText = (string)p[8].Value;
            returnBatchTraineeId = (int)p[9].Value;
        }
    }
}

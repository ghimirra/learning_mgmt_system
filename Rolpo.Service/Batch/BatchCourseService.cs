﻿/*
 Service For BatchCourse
Created by: Prashant
Created On: 24/06/2018
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface IBatchCourseService : IEntityService<BatchCourse>
    {
        BatchCourse GetBatchCourseById(int batchcourseid);
        IEnumerable<BatchCourseViewModel> GetBatchCourse(string userId, BatchCourseViewModel_Input input);
        void UpdateBatchCourse(BatchCourse batchcourse, string actionType, string userId, ref int returnBatchCourseId, ref string msgType, ref string msgText);
    }

    public class BatchCourseService : EntityService<BatchCourse>, IBatchCourseService
    {

        new IContext _context;
        public BatchCourseService(IContext context) : base(context)
        {
            _context = context;
            _dbset = _context.Set<BatchCourse>();
        }

        //Stored Procedures definition 
        StoredProc csp_BatchCourse_GET = new StoredProc().HasName("[csp.BatchCourse.Get]").ReturnsTypes(typeof(BatchCourseViewModel));
        StoredProc csp_BatchCourse_UPDATE = new StoredProc().HasName("[csp.BatchCourse.Update]");

        /// <summary>
        /// Get BatchCourse By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

        public BatchCourse GetBatchCourseById(int batchcourseid)
        {
            return _dbset.FirstOrDefault(x => x.BatchCourseId == batchcourseid);
        }

        /// <summary>
        /// Get BatchCourse
        /// </summary>

        public IEnumerable<BatchCourseViewModel> GetBatchCourse(string userId, BatchCourseViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[7];

            p[0] = new SqlParameter("@BatchCourseId", input.BatchCourseId);
            p[1] = new SqlParameter("@CourseId", input.CourseId);

            p[2] = new SqlParameter("@UserId", userId);
            p[3] = new SqlParameter("@PageNumber", input.PageNumber);
            p[4] = new SqlParameter("@PageSize", input.PageSize);
            p[5] = new SqlParameter("@ShowAll", input.ShowAll);
            p[6] = new SqlParameter("@BatchId", input.BatchId);

            var results = _context.CallSP(csp_BatchCourse_GET, p);
            return results.ToList<BatchCourseViewModel>();
        }

        /// <summary>
        /// Update BatchCourse
        /// </summary>

        public void UpdateBatchCourse(BatchCourse batchcourse, string actionType, string userId, ref int returnBatchCourseId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[11];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@BatchCourseId", batchcourse.BatchCourseId);
            p[2] = new SqlParameter("@OrganizationId", batchcourse.OrganizationId);
            p[3] = new SqlParameter("@BatchId", batchcourse.BatchId);
            p[4] = new SqlParameter("@CourseId", batchcourse.CourseId);
            p[5] = new SqlParameter("@CourseStartDate", batchcourse.CourseStartDate);
            p[6] = new SqlParameter("@Remarks", batchcourse.Remarks);

            p[7] = new SqlParameter("@UserId", userId);

            p[8] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 10);
            p[8].Direction = System.Data.ParameterDirection.Output;

            p[9] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 100);
            p[9].Direction = System.Data.ParameterDirection.Output;

            p[10] = new SqlParameter("@ReturnBatchCourseId", System.Data.SqlDbType.Int);
            p[10].Direction = System.Data.ParameterDirection.Output;

            var result = _context.CallSP(csp_BatchCourse_UPDATE, p);

            msgType = (string)p[8].Value;
            msgText = (string)p[9].Value;
            returnBatchCourseId = (int)p[10].Value;
        }
    }
}

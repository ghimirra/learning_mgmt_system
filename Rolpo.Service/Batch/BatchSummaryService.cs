﻿/*
 Service For BatchSummary
Created by: Prashant
Created On: 24/06/2018
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface IBatchSummaryService : IEntityService<BatchSummary>
    {
        BatchSummary GetBatchSummaryById(int batchsummaryid);
        IEnumerable<BatchSummaryViewModel> GetBatchSummary(string userId, BatchSummaryViewModel_Input input);
        void UpdateBatchSummary(BatchSummary batchsummary, string actionType, string userId, ref int returnBatchSummaryId, ref string msgType, ref string msgText);
    }

    public class BatchSummaryService : EntityService<BatchSummary>, IBatchSummaryService
    {

        new IContext _context;
        public BatchSummaryService(IContext context) : base(context)
        {
            _context = context;
            _dbset = _context.Set<BatchSummary>();
        }

        //Stored Procedures definition 
        StoredProc csp_BatchSummary_GET = new StoredProc().HasName("[csp.BatchSummary.Get]").ReturnsTypes(typeof(BatchSummaryViewModel));
        StoredProc csp_BatchSummary_UPDATE = new StoredProc().HasName("[csp.BatchSummary.Update]");

        /// <summary>
        /// Get BatchSummary By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

        public BatchSummary GetBatchSummaryById(int batchsummaryid)
        {
            return _dbset.FirstOrDefault(x => x.BatchSummaryId == batchsummaryid);
        }

        /// <summary>
        /// Get BatchSummary
        /// </summary>

        public IEnumerable<BatchSummaryViewModel> GetBatchSummary(string userId, BatchSummaryViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[7];

            p[0] = new SqlParameter("@BatchSummaryId", input.BatchSummaryId);
            p[1] = new SqlParameter("@StatusId", input.StatusId);

            p[2] = new SqlParameter("@UserId", userId);
            p[3] = new SqlParameter("@PageNumber", input.PageNumber);
            p[4] = new SqlParameter("@PageSize", input.PageSize);
            p[5] = new SqlParameter("@ShowAll", input.ShowAll);
            p[6] = new SqlParameter("@BatchId", input.BatchId);

            var results = _context.CallSP(csp_BatchSummary_GET, p);
            return results.ToList<BatchSummaryViewModel>();
        }

        /// <summary>
        /// Update BatchSummary
        /// </summary>

        public void UpdateBatchSummary(BatchSummary batchsummary, string actionType, string userId, ref int returnBatchSummaryId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[10];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@BatchSummaryId", batchsummary.BatchSummaryId);
            p[2] = new SqlParameter("@OrganizationId", batchsummary.OrganizationId);
            p[3] = new SqlParameter("@BatchId", batchsummary.BatchId);
            p[4] = new SqlParameter("@StatusId", batchsummary.StatusId);
            p[5] = new SqlParameter("@Summary", batchsummary.Summary);

            p[6] = new SqlParameter("@UserId", userId);

            p[7] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 10);
            p[7].Direction = System.Data.ParameterDirection.Output;

            p[8] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 100);
            p[8].Direction = System.Data.ParameterDirection.Output;

            p[9] = new SqlParameter("@ReturnBatchSummaryId", System.Data.SqlDbType.Int);
            p[9].Direction = System.Data.ParameterDirection.Output;

            var result = _context.CallSP(csp_BatchSummary_UPDATE, p);

            msgType = (string)p[7].Value;
            msgText = (string)p[8].Value;
            returnBatchSummaryId = (int)p[9].Value;
        }
    }
}

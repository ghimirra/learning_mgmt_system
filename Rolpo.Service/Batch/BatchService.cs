﻿/*
 Service For Batch
Created by: Prashant
Created On: 21/06/2018
*/
using Rolpo.Model;
using Rolpo.Model.ChaliseStoredProc;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace Rolpo.Service
{
    public interface IBatchService : IEntityService<Batch>
    {
        Batch GetBatchById(int batchid);
        IEnumerable<BatchViewModel> GetBatch(string userId, BatchViewModel_Input input);
        IEnumerable<BatchReportViewModel> GetBatchForReport(string userId, BatchViewModel_Input input);
        void UpdateBatch(Batch batch, string actionType, string userId, ref int returnBatchId, ref string msgType, ref string msgText);
    }

    public class BatchService : EntityService<Batch>, IBatchService
    {

        new IContext _context;
        public BatchService(IContext context) : base(context)
        {
            _context = context;
            _dbset = _context.Set<Batch>();
        }

        //Stored Procedures definition 
        StoredProc csp_Batch_GET = new StoredProc().HasName("[csp.Batch.Get]").ReturnsTypes(typeof(BatchViewModel));
        StoredProc csp_Batch_GETFORREPORT = new StoredProc().HasName("[csp.Batch.GetForReport]").ReturnsTypes(typeof(BatchReportViewModel)); 
        StoredProc csp_Batch_UPDATE = new StoredProc().HasName("[csp.Batch.Update]");

        /// <summary>
        /// Get Batch By Id :: Don't forget to add the DBSet to RolpoContext
        /// </summary>

        public Batch GetBatchById(int batchid)
        {
            return _dbset.FirstOrDefault(x => x.BatchId == batchid);
        }

        /// <summary>
        /// Get Batch
        /// </summary>

        public IEnumerable<BatchViewModel> GetBatch(string userId, BatchViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[10];

            p[0] = new SqlParameter("@BatchId", input.BatchId);
            p[1] = new SqlParameter("@BatchCode", input.BatchCode);
            p[2] = new SqlParameter("@Coordinator", input.Coordinator); 
            p[3] = new SqlParameter("@BatchStartDate", input.BatchStartDate);
            p[4] = new SqlParameter("@BatchEndDate", input.BatchEndDate);

            p[5] = new SqlParameter("@UserId", userId);
            p[6] = new SqlParameter("@PageNumber", input.PageNumber);
            p[7] = new SqlParameter("@PageSize", input.PageSize);
            p[8] = new SqlParameter("@ShowAll", input.ShowAll);
            p[9] = new SqlParameter("@SearchText", input.SearchText);
            
            var results = _context.CallSP(csp_Batch_GET, p);

            return results.ToList<BatchViewModel>();
        }


        /// <summary>
        /// Get Batch
        /// </summary>

        public IEnumerable<BatchReportViewModel> GetBatchForReport(string userId, BatchViewModel_Input input)
        {

            SqlParameter[] p = new SqlParameter[11];

            p[0] = new SqlParameter("@BatchId", input.BatchId);
            p[1] = new SqlParameter("@BatchCode", input.BatchCode);
            p[2] = new SqlParameter("@CoordinatorId", input.CoordinatorId);
            p[3] = new SqlParameter("@TraineeId", input.TraineeId);
            p[4] = new SqlParameter("@BatchStartDate", input.BatchStartDate);
            p[5] = new SqlParameter("@BatchEndDate", input.BatchEndDate);

            p[6] = new SqlParameter("@UserId", userId);
            p[7] = new SqlParameter("@PageNumber", input.PageNumber);
            p[8] = new SqlParameter("@PageSize", input.PageSize);
            p[9] = new SqlParameter("@ShowAll", input.ShowAll);
            p[10] = new SqlParameter("@SearchText", input.SearchText);

            var results = _context.CallSP(csp_Batch_GETFORREPORT, p);

            return results.ToList<BatchReportViewModel>();
        }

        /// <summary>
        /// Update Batch
        /// </summary>

        public void UpdateBatch(Batch batch, string actionType, string userId, ref int returnBatchId, ref string msgType, ref string msgText)
        {

            SqlParameter[] p = new SqlParameter[17];

            p[0] = new SqlParameter("@ActionType", actionType);

            p[1] = new SqlParameter("@BatchId", batch.BatchId);
            p[2] = new SqlParameter("@OrganizationId", batch.OrganizationId);
            p[3] = new SqlParameter("@BatchCode", batch.BatchCode);
            p[4] = new SqlParameter("@BatchName", batch.BatchName);
            p[5] = new SqlParameter("@BatchLevel", batch.BatchLevel);
            p[6] = new SqlParameter("@BatchCategory", batch.BatchCategory);
            p[7] = new SqlParameter("@CoordinatorId", batch.CoordinatorId);
            p[8] = new SqlParameter("@AssistantCoordinatorId", batch.AssistantCoordinatorId);
            p[9] = new SqlParameter("@BatchStartDate", batch.BatchStartDate);
            p[10] = new SqlParameter("@BatchEndDate", batch.BatchEndDate);
            p[11] = new SqlParameter("@BatchSummary", batch.BatchSummary);

            p[12] = new SqlParameter("@UserId", userId);

            p[13] = new SqlParameter("@MsgType", System.Data.SqlDbType.VarChar, 10);
            p[13].Direction = System.Data.ParameterDirection.Output;

            p[14] = new SqlParameter("@MsgText", System.Data.SqlDbType.VarChar, 100);
            p[14].Direction = System.Data.ParameterDirection.Output;

            p[15] = new SqlParameter("@ReturnBatchId", System.Data.SqlDbType.Int);
            p[15].Direction = System.Data.ParameterDirection.Output;

            p[16] = new SqlParameter("@TrainingAddress", batch.TrainingAddress);

            var result = _context.CallSP(csp_Batch_UPDATE, p);

            msgType = (string)p[13].Value;
            msgText = (string)p[14].Value;
            returnBatchId = (int)p[15].Value;
        }
    }
}

﻿/* 
Model for [dbo].[TraineeLevel] 
Created by: Prashant 
Created On: 26/06/2018 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_TraineeLevel")]
    public class TraineeLevel : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TraineeLevelId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public Guid OrganizationId { get; set; }


        [Required]
        [MaxLength(100)]
        [Display(Name = "Level Name")]
        public string TraineeLevelName { get; set; }


        [MaxLength(1000)]
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

    }

    /* TraineeLevel View Model */
    public class TraineeLevelViewModel
    {
        public Int64 RowNumber { get; set; }
        public int TraineeLevelId { get; set; }
        public string TraineeLevelName { get; set; }
        public string Remarks { get; set; }

        public int TotalCount { get; set; }
    }

    /* TraineeLevel View Model (Input) */
    public class TraineeLevelViewModel_Input
    {
        public int? TraineeLevelId { get; set; }
        public string TraineeLevelName { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }
}

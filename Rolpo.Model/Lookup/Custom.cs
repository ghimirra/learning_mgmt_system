﻿using System.Collections.Generic;

namespace Rolpo.Model
{

    //Input For DDL
    public class DDLFilterList
    {
        public string PageName { get; set; }
        public List<DDLFilter> FilterList { get; set; }

    }
    //Input For DDL Contd..
    public class DDLFilter
    {
        public string DDLName { get; set; }
        public string Param1 { get; set; }
        public string Param2 { get; set; }
    }

    //Output For DDL
    public class DDLJson {
        public string PageName { get; set; }
        public string  DDLItems { get; set; }
    }

    #region "Excel Export"

    //public class ExcelExport
    //{
    //    public string FileGuid { get; set; }
    //    public string FileName { get; set; }
    //}

    #endregion

    #region "Code Generator"

    public class TableInfo {
        public string TableName { get; set; }
        public List<TableColumns> Columns{ get; set; }
    }

    public class TableColumns {
        public string COLUMN_NAME { get; set; }
        public string DATA_TYPE { get; set; }
        public int CHARACTER_MAXIMUM_LENGTH { get; set; }
        public string IS_NULLABLE { get; set; }
        public string IS_IDENTITY { get; set; }
        public string DISPLAY_NAME { get; set; }
        public string SHOW_AS { get; set; }
        public string SHOW_IN_FILTER { get; set; }
        public int IsForeignKey { get; set; }
    }

    public class CodeResults {
        public string SP_GET { get; set; }
        public string SP_UPDATE { get; set; }
        public string MODEL_PAGE { get; set; }
        public string CONTROLLER_PAGE { get; set; }
        public string SERVICE_PAGE { get; set; }
        public string HTML_PAGE { get; set; }
        public string ANG_CONTROLLER { get; set; }
    }
    #endregion
}

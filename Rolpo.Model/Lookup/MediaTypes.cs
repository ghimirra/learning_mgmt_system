﻿/* 
Model for [dbo].[Media] 
Created by: Prashant 
Created On: 21/06/2018 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_Media")]
    public class Media : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MediaId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public Guid OrganizationId { get; set; }


        [Required]
        [MaxLength(500)]
        [Display(Name = "MediaName")]
        public string MediaName { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

    }

    /* Media View Model */
    public class MediaViewModel
    {
        public Int64 RowNumber { get; set; }
        public int MediaId { get; set; }
        public string MediaName { get; set; }
        public string Remarks { get; set; }

        public int TotalCount { get; set; }
    }

    /* Media View Model (Input) */
    public class MediaViewModel_Input
    {
        public int? MediaId { get; set; }
        public string MediaName { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }
}

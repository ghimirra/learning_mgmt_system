﻿/* 
Model for [dbo].[CoursePeriodType] 
Created by: Prashant 
Created On: 27/06/2018 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_CoursePeriodType")]
    public class CoursePeriodType : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CoursePeriodTypeId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public Guid OrganizationId { get; set; }


        [Required]
        [MaxLength(100)]
        [Display(Name = "Period Type")]
        public string PeriodTypeName { get; set; }


        [MaxLength(100)]
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

    }

    /* CoursePeriodType View Model */
    public class CoursePeriodTypeViewModel
    {
        public Int64 RowNumber { get; set; }
        public int CoursePeriodTypeId { get; set; }
        public string PeriodTypeName { get; set; }
        public string Remarks { get; set; }

        public int TotalCount { get; set; }
    }

    /* CoursePeriodType View Model (Input) */
    public class CoursePeriodTypeViewModel_Input
    {
        public int? CoursePeriodTypeId { get; set; }
        public string PeriodTypeName { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }
}

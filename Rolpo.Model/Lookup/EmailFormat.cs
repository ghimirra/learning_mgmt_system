﻿/* 
Model for [dbo].[EmailFormat] 
Created by: Prashant 
Created On: 3/24/2018
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_EmailFormat")]
    public class EmailFormat : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmailFormatId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public Guid OrganizationId { get; set; }


        [MaxLength(500)]
        [Display(Name = "From")]
        public string From { get; set; }


        [MaxLength(500)]
        [Display(Name = "FromName")]
        public string FromName { get; set; }


        [MaxLength(500)]
        [Display(Name = "Bcc")]
        public string Bcc { get; set; }


        [Required]
        [MaxLength(500)]
        [Display(Name = "Subject")]
        public string Subject { get; set; }


        [Required]
        [MaxLength(-1)]
        [Display(Name = "Body")]
        public string Body { get; set; }

    }

    /* EmailFormat View Model */
    public class EmailFormatViewModel
    {
        public Int64 RowNumber { get; set; }
        public int EmailFormatId { get; set; }
        public string From { get; set; }
        public string FromName { get; set; }
        public string Bcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public int TotalCount { get; set; }
    }

    /* EmailFormat View Model (Input) */
    public class EmailFormatViewModel_Input
    {
        public int? EmailFormatId { get; set; }
        public string Subject { get; set; }
        public string SearchText { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }
}

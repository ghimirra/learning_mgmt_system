﻿/* 
Model for [dbo].[CourseCategory] 
Created by: Prashant 
Created On: 26/06/2018 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_CourseCategory")]
    public class CourseCategory : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CourseCategoryId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public Guid OrganizationId { get; set; }


        [Required]
        [MaxLength(100)]
        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }


        [MaxLength(100)]
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }


        [Display(Name = "ParentCategory")]
        public int? ParentCategoryId { get; set; }

    }

    /* CourseCategory View Model */
    public class CourseCategoryViewModel
    {
        public Int64 RowNumber { get; set; }
        public int CourseCategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Remarks { get; set; }
        public int? ParentCategoryId { get; set; }
        //public string ParentCategoryName { get; set; }
        public int TotalCount { get; set; }
    }

    /* CourseCategory View Model */
    public class ParentCategoryViewModel
    {
        public Int64 RowNumber { get; set; }
        public int CourseCategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Remarks { get; set; }
        public string ChildCategoryJSON { get; set; }
        public int TotalCount { get; set; }
    }


    /* CourseCategory View Model (Input) */
    public class CourseCategoryViewModel_Input
    {
        public int? CourseCategoryId { get; set; }
        public string CategoryName { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }
}

﻿/* 
Model for [dbo].[CourseLevel] 
Created by: Prashant 
Created On: 27/06/2018 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_CourseLevel")]
    public class CourseLevel : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CourseLevelId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public Guid OrganizationId { get; set; }


        [Required]
        [MaxLength(100)]
        [Display(Name = "LevelName")]
        public string LevelName { get; set; }


        [MaxLength(100)]
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

    }

    /* CourseLevel View Model */
    public class CourseLevelViewModel
    {
        public Int64 RowNumber { get; set; }
        public int CourseLevelId { get; set; }
        public string LevelName { get; set; }
        public string Remarks { get; set; }

        public int TotalCount { get; set; }
    }

    /* CourseLevel View Model (Input) */
    public class CourseLevelViewModel_Input
    {
        public int? CourseLevelId { get; set; }
        public string LevelName { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }
}

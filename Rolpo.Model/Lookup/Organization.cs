﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{

    [Table("TBL_Organization")]
    public class Organization : AuditableEntity<long>
    {
        [Key]
        public Guid OrganizationId { get; set; }

        [Required]
        [StringLength(150)]
        public string OrganizationName { get; set; }

        [Required]
        [StringLength(25)]
        public string OrganizationCode { get; set; }
        public int? AssociatedSupplierId { get; set; }

        [StringLength(200)]
        public string OrganizationAddress { get; set; }

        [StringLength(200)]
        public string OrganizationLogoUrl { get; set; }

        [StringLength(200)]
        public string Telephone { get; set; }

        [StringLength(200)]
        public string Fax { get; set; }

        [StringLength(200)]
        public string ContactPerson { get; set; }

        [StringLength(200)]
        public string Email { get; set; }

        [StringLength(200)]
        public string DomainName { get; set; }

        [StringLength(200)]
        public string ServiceBaseUrl { get; set; }

        public int? TimeOffsetInMins { get; set; }

    }
}

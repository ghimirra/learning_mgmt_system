﻿/* 
Model for [dbo].[Status] 
Created by: Prashant 
Created On: 24/06/2018 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_Status")]
    public class Status : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int StatusId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public Guid OrganizationId { get; set; }


        [Required]
        [MaxLength(100)]
        [Display(Name = "StatusGroup")]
        public string StatusGroup { get; set; }


        [Required]
        [MaxLength(100)]
        [Display(Name = "StatusName")]
        public string StatusName { get; set; }


        [Required]
        [MaxLength(20)]
        [Display(Name = "StatusCode")]
        public string StatusCode { get; set; }

         
        [Display(Name = "StatusOrder")]
        public int? StatusOrder { get; set; }


        [Display(Name = "AllowBackRevert")]
        public bool? AllowBackRevert { get; set; }

    }

    /* Status View Model */
    public class StatusViewModel
    {
        public Int64 RowNumber { get; set; }
        public int StatusId { get; set; }
        public string StatusGroup { get; set; }
        public string StatusName { get; set; }
        public string StatusCode { get; set; }
        public int StatusOrder { get; set; }
        public bool? AllowBackRevert { get; set; }

        public int TotalCount { get; set; }
    }

    /* Status View Model (Input) */
    public class StatusViewModel_Input
    {
        public int? StatusId { get; set; }
        public string StatusGroup { get; set; }
        public string StatusName { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }
}

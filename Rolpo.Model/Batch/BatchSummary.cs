﻿/* 
Model for [dbo].[BatchSummary] 
Created by: Prashant 
Created On: 24/06/2018 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_BatchSummary")]
    public class BatchSummary : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BatchSummaryId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public Guid OrganizationId { get; set; }


        [Required]
        [Display(Name = "BatchId")]
        public int BatchId { get; set; }


        [Required]
        [Display(Name = "Status")]
        public int StatusId { get; set; }

         
        [Display(Name = "Summary")]
        public string Summary { get; set; }

    }

    /* BatchSummary View Model */
    public class BatchSummaryViewModel
    {
        public Int64 RowNumber { get; set; }
        public int BatchSummaryId { get; set; }
        public int BatchId { get; set; }
        public int StatusId { get; set; }
        public string Summary { get; set; }
        public string StatusName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UserName { get; set; }
        public int TotalCount { get; set; }
    }

    /* BatchSummary View Model (Input) */
    public class BatchSummaryViewModel_Input
    {
        public int? BatchSummaryId { get; set; }
        public int? StatusId { get; set; }
        public int? BatchId { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }
}

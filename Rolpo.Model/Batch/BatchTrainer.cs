﻿/* 
Model for [dbo].[BatchTrainer] 
Created by: Prashant 
Created On: 24/06/2018 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_BatchTrainer")]
    public class BatchTrainer : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BatchTrainerId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public Guid OrganizationId { get; set; }


        [Required]
        [Display(Name = "Member")]
        public int MemberId { get; set; }


        [Required]
        [Display(Name = "Batch")]
        public int BatchId { get; set; }


        [Display(Name = "Module")]
        public int ModuleId { get; set; }

         
        [Display(Name = "Start Date")]
        public DateTime? TrainingStartDate { get; set; }

    }

    /* BatchTrainer View Model */
    public class BatchTrainerViewModel
    {
        public Int64 RowNumber { get; set; }
        public int BatchTrainerId { get; set; }
        public int MemberId { get; set; }
        public string TrainerName { get; set; }
        public int BatchId { get; set; }
        public int ModuleId { get; set; }
        public string ModuleTitle { get; set; }
        public string ProfileImageUrl { get; set; }
        public DateTime? TrainingStartDate { get; set; }
         public int TotalCount { get; set; }
    }

    /* BatchTrainer View Model (Input) */
    public class BatchTrainerViewModel_Input
    {
        public int? BatchTrainerId { get; set; }
        public int? MemberId { get; set; }
        public int? BatchId{ get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }
}

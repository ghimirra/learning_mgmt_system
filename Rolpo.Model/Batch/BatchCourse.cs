﻿/* 
Model for [dbo].[BatchCourse] 
Created by: Prashant 
Created On: 24/06/2018 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_BatchCourse")]
    public class BatchCourse : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BatchCourseId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public Guid OrganizationId { get; set; }


        [Required]
        [Display(Name = "Batch")]
        public int BatchId { get; set; }


        [Required]
        [Display(Name = "Course")]
        public int CourseId { get; set; }


        [Display(Name = "Start Date")]
        public DateTime CourseStartDate { get; set; }


        [MaxLength(-1)]
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

    }

    /* BatchCourse View Model */
    public class BatchCourseViewModel
    {
        public Int64 RowNumber { get; set; }
        public int BatchCourseId { get; set; }
        public int BatchId { get; set; }
        public int CourseId { get; set; }
        public string CourseFullName { get; set; }
        public string CourseCode { get; set; }
        public DateTime CourseStartDate { get; set; }
        public string Remarks { get; set; }

        public int TotalCount { get; set; }
    }

    /* BatchCourse View Model (Input) */
    public class BatchCourseViewModel_Input
    {
        public int? BatchCourseId { get; set; }
        public int? CourseId { get; set; }
        public int? BatchId { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }
}

﻿/* 
Model for [dbo].[Batch] 
Created by: Prashant 
Created On: 21/06/2018 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_Batch")]
    public class Batch : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BatchId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public Guid OrganizationId { get; set; }


        [Required]
        [MaxLength(100)]
        [Display(Name = "Batch Code")]
        public string BatchCode { get; set; }


        [MaxLength(500)]
        [Display(Name = "Batch Name")]
        public string BatchName { get; set; }


        [MaxLength(100)]
        [Display(Name = "Batch Level")]
        public string BatchLevel { get; set; }


        [MaxLength(100)]
        [Display(Name = "Batch Category")]
        public string BatchCategory { get; set; }

        
        [Display(Name = "Coordinator")]
        public int CoordinatorId { get; set; }

        
        [Display(Name = "ASS. Coordinator")]
        public int AssistantCoordinatorId { get; set; }


        [Required]
        [Display(Name = "Batch Start")]
        public DateTime BatchStartDate { get; set; }


        [Display(Name = "Batch End")]
        public DateTime? BatchEndDate { get; set; }


        [Display(Name = "Summary")]
        public string BatchSummary { get; set; }

        public string TrainingAddress { get; set; }

    }

    /* Batch View Model */
    public class BatchViewModel
    {
        public Int64 RowNumber { get; set; }
        public int BatchId { get; set; }
        public string BatchCode { get; set; }
        public string BatchName { get; set; }
        public string BatchLevel { get; set; }
        public string BatchCategory { get; set; }
        public string Coordinator { get; set; }
        public string ASSCoordinator { get; set; }
        public int CoordinatorId { get; set; }
        public int AssistantCoordinatorId { get; set; }

        public DateTime? BatchStartDate { get; set; }
        public DateTime? BatchEndDate { get; set; }
        public string BatchSummary { get; set; }
        public string TrainingAddress { get; set; }

        public string CourseCode { get; set; }
        public string CourseName { get; set; }
        public int Duration { get; set; }
        
        

        public int TotalCount { get; set; }
    }



    /* Batch View Model (Input) */
    public class BatchViewModel_Input
    {
        public int? BatchId { get; set; }
        public string BatchCode { get; set; }
        public string SearchText { get; set; }
        public int? CoordinatorId { get; set; }
        public string Coordinator { get; set; }
        public int? TraineeId { get; set; }
        public DateTime? BatchStartDate { get; set; }
        public DateTime? BatchEndDate { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }


    /* Batch Report View Model */
    public class BatchReportViewModel
    {
        public Int64 RowNumber { get; set; }
        public int BatchId { get; set; }
        public string BatchCode { get; set; }
        public string BatchName { get; set; }
        public string BatchLevel { get; set; }
        public string BatchCategory { get; set; }
        public string Coordinator { get; set; }
        public string ASSCoordinator { get; set; }
        public int CoordinatorId { get; set; }
        public int AssistantCoordinatorId { get; set; }

        public string CourseFullName { get; set; }
        public string LevelName { get; set; }
        public int CoursePeriod { get; set; }
        public string PeriodTypeName { get; set; }
        public string CoursePeriodTXT { get; set; }
        public DateTime? BatchStartDate { get; set; }
        public DateTime? BatchEndDate { get; set; }

        public string BatchStartDateLOCAL { get; set; }
        public string BatchEndDateLOCAL { get; set; }
        public int TotalTrainees { get; set; }
        public string BatchSummary { get; set; }
        public string TrainingAddress { get; set; }
        public string Duration { get; set; }
        public string CourseCode { get; set; }
        public string CourseName { get; set; }
         public int TotalCount { get; set; }
    }
}

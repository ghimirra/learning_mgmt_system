﻿/* 
Model for [dbo].[BatchTrainee] 
Created by: Prashant 
Created On: 24/06/2018 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_BatchTrainee")]
    public class BatchTrainee : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BatchTraineeId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public Guid OrganizationId { get; set; }


        [Required]
        [Display(Name = "Batch")]
        public int BatchId { get; set; }


        [Required]
        [Display(Name = "Trainee")]
        public int MemberId { get; set; }

        
        [Display(Name = "Joined Date")]
        public DateTime JoinedDate { get; set; }

    }

    /* BatchTrainee View Model */
    public class BatchTraineeViewModel
    {
        public Int64 RowNumber { get; set; }
        public int BatchTraineeId { get; set; }
        public int BatchId { get; set; }
        public int MemberId { get; set; }
        public DateTime? JoinedDate { get; set; }
        public string TraineeName { get; set; }
        public int TotalCount { get; set; }
        public string BatchCode { get; set; }
        public string CourseName { get; set; }
        public string Position { get; set; }
        public string TraineeLevel { get; set; }
        public string DepartmentName { get; set; }
        public DateTime BatchStartDate { get; set; }
        public DateTime BatchEndDate { get; set; }

        
    }

    /* BatchTrainee Report View Model */
    public class BatchTraineeReportViewModel
    {
        public Int64 RowNumber { get; set; }
        public int BatchTraineeId { get; set; }
        public int BatchId { get; set; }
        public string BatchCode { get; set; }
        public string BatchName { get; set; }
        public int MemberId { get; set; }
        public string ProfileImageUrl { get; set; }
        public string TraineeName { get; set; }
        public string TraineeLevelName { get; set; }
        public string Position { get; set; }
        public string CompanyName { get; set; }
        public string Department{ get; set; }
        public DateTime? BatchStartDate { get; set; }
        public DateTime? BatchEndDate { get; set; }
        public string BatchStartDateLOCAL { get; set; }
        public string BatchEndDateLOCAL { get; set; }
        public DateTime? JoinedDate { get; set; }
        public int TotalCount { get; set; }
        public string Duration{ get; set; }

    }

    /* BatchTrainee View Model (Input) */
    public class BatchTraineeViewModel_Input
    {
        public int? BatchTraineeId { get; set; }
        public int? BatchId { get; set; }
        public string SearchText { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }
}

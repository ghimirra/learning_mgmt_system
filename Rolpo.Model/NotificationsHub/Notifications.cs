﻿/* 
Model for [dbo].[Notifications] 
Created by: Prashant 
Created On: 13/10/2017 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_Notifications")]
    public class Notifications : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NotificationId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public Guid OrganizationId { get; set; }


        [Required]
        [MaxLength(50)]
        [Display(Name = "Notify Code")]
        public string NotifyCode { get; set; }


        [Required]
        [Display(Name = "ScheduledDate")]
        public DateTime ScheduledDate { get; set; }


        [Display(Name = "HasEmailQueued")]
        public string HasEmailQueued { get; set; }


        [Display(Name = "AssociatedItemId")]
        public int AssociatedItemId { get; set; }


        [Required]
        [MaxLength(200)]
        [Display(Name = "FromName")]
        public string FromName { get; set; }


        [Required]
        [MaxLength(200)]
        [Display(Name = "FromAddress")]
        public string FromAddress { get; set; }


        [Required]
        [Display(Name = "ToAddresses")]
        public string ToAddresses { get; set; }


        [Required]
        [MaxLength(200)]
        [Display(Name = "Subject")]
        public string Subject { get; set; }

    }

    /* Notifications View Model */
    public class NotificationsViewModel
    {
        public Int64 RowNumber { get; set; }
        public int NotificationId { get; set; }
        public string NotifyCode { get; set; }
        public DateTime ScheduledDate { get; set; }
        public int AssociatedItemId { get; set; }
        public string FromName { get; set; }
        public string FromAddress { get; set; }
        public string ToAddresses { get; set; }
        public string Subject { get; set; }

        public int TotalCount { get; set; }
    }

    /* Notifications View Model (Input) */
    public class NotificationsViewModel_Input
    {
        public int? NotificationId { get; set; }
        public string NotifyCode { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }
}

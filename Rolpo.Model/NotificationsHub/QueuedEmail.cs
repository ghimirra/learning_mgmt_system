﻿/* 
Model for [dbo].[QueuedEmail] 
Created by: Prashant 
Created On: 12/10/2017 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_QueuedEmail")]
    public class QueuedEmail : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QueuedEmailId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public Guid OrganizationId { get; set; }


        [Required]
        [MaxLength(200)]
        [Display(Name = "FromAddress")]
        public string FromAddress { get; set; }


        [Required]
        [MaxLength(200)]
        [Display(Name = "FromName")]
        public string FromName { get; set; }


        [Required]
        [MaxLength(200)]
        [Display(Name = "ToAddress")]
        public string ToAddress { get; set; }


        [Required]
        [MaxLength(200)]
        [Display(Name = "ToName")]
        public string ToName { get; set; }


        [MaxLength(1000)]
        [Display(Name = "Cc")]
        public string Cc { get; set; }


        [MaxLength(1000)]
        [Display(Name = "Bcc")]
        public string Bcc { get; set; }


        [Required]
        [MaxLength(100)]
        [Display(Name = "SubjectCode")]
        public string SubjectCode { get; set; }


        [Required]
        [MaxLength(200)]
        [Display(Name = "Subject")]
        public string Subject { get; set; }


        [Required]
        [MaxLength(-1)]
        [Display(Name = "Body")]
        public string Body { get; set; }


        [Display(Name = "SentDate")]
        public DateTime? SentDate { get; set; }


        [Display(Name = "SentTries")]
        public int SentTries { get; set; }


        [Display(Name = "Priority")]
        public int Priority { get; set; }

    }

    /* QueuedEmail View Model */
    public class QueuedEmailViewModel
    {
        public Int64 RowNumber { get; set; }
        public int QueuedEmailId { get; set; }
        public string FromAddress { get; set; }
        public string FromName { get; set; }
        public string ToAddress { get; set; }
        public string ToName { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public string SubjectCode { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime? SentDate { get; set; }
        public int SentTries { get; set; }
        public int Priority { get; set; }

        public int TotalCount { get; set; }
    }

    /* QueuedEmail View Model (Input) */
    public class QueuedEmailViewModel_Input
    {
        public int? QueuedEmailId { get; set; }
        public bool LoadNotSentItemsOnly { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }
}

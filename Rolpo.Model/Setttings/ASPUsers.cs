﻿/* 
Model for [dbo].[ASPNETUsers] 
Created by: Prashant 
Created On: 01/06/2016 
 */
using System;

namespace Rolpo.Model
{


    /* ASPNETUsers View Model */
    public class ASPNETUsersViewModel
    {
        public Int64 RowNumber { get; set; }
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string RolesJSON { get; set; }
        public int StaffId { get; set; }
        public int TotalCount { get; set; }
    }

    /* ASPNETUsers View Model */
    public class RolesViewModel
    {
        public string RoleGroup { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public string DisplayName{ get; set; }
        public int RolePriority { get; set; }
        public int RoleOrderId { get; set; }
        public string Value{ get; set; }
    }

    /* ASPNETUsers View Model (Input) */
    public class ASPNETUsersViewModel_Input
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string SearchText { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }

}

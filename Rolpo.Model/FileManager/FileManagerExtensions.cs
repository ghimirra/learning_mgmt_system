﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;


namespace Rolpo.UI
{


    public static class FileManagerExtensions
	{
        public static Byte[] ExportToExcel(DataTable dt,ExcelReportProperties prop)
        {

            Byte[] fileresult = null;

            try
            {
                
                IWorkbook workbook;
                workbook = new HSSFWorkbook();

                ISheet sheet1 = workbook.CreateSheet(dt.TableName);

                //font style
                var heading = workbook.CreateFont();
                heading.FontHeightInPoints = 12;
                heading.FontName = "Times New Roman";
                heading.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Normal;

                


                var font_white = workbook.CreateFont();
                font_white.FontHeightInPoints = 11;
                font_white.FontName = "Calibri";
                font_white.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                font_white.Color = IndexedColors.White.Index;

                //style for row 1 and row2 
                ICellStyle cellStyle1 = workbook.CreateCellStyle();
                cellStyle1.FillForegroundColor = IndexedColors.LightGreen.Index;
                cellStyle1.FillPattern = FillPattern.SolidForeground;
                cellStyle1.SetFont(heading);

                //style for row 3 and four
                ICellStyle cellStyle2 = workbook.CreateCellStyle();
                cellStyle2.FillForegroundColor = IndexedColors.White.Index;
                cellStyle2.FillPattern = FillPattern.SolidForeground;
                

                //style for heading row
                ICellStyle cellStyle3 = workbook.CreateCellStyle();
                cellStyle3.FillForegroundColor = IndexedColors.OliveGreen.Index;
                cellStyle3.SetFont(font_white);
                cellStyle3.FillPattern = FillPattern.SolidForeground;

                //style for data row
                ICellStyle cellStyle4 = workbook.CreateCellStyle();
                cellStyle4.FillForegroundColor = IndexedColors.Grey25Percent.Index;
                cellStyle4.FillPattern = FillPattern.SolidForeground;

                //style for data cell
                ICellStyle cellStyle5 = workbook.CreateCellStyle();
                cellStyle5.FillForegroundColor = IndexedColors.White.Index;
                cellStyle5.FillPattern = FillPattern.SolidForeground;


                //index value for row


                //first  row
                IRow row;
                ICell cell;

                row = sheet1.CreateRow(0);
                for (int i=0;i<dt.Columns.Count;i++)
                {
                    cell = row.CreateCell(i);
                    cell.CellStyle = cellStyle1;
                    cell.SetCellValue(i==0? prop.CompanyName:string.Empty);
                }
                
                row = sheet1.CreateRow(1);
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    cell = row.CreateCell(i);
                    cell.CellStyle = cellStyle1;
                    cell.SetCellValue(i==0?prop.CompanyAddressLine:string.Empty);
                }




                //third row


                row = sheet1.CreateRow(2);
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    cell = row.CreateCell(i);
                    cell.CellStyle = cellStyle2;
                    cell.SetCellValue(i == 0 ? prop.ReportTitle:(i==dt.Columns.Count-1?"Generated on :"+ DateTime.Today.Date.ToShortDateString():string.Empty));
                }

                
                row = sheet1.CreateRow(3);
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    cell = row.CreateCell(i);
                    cell.CellStyle = cellStyle2;
                    cell.SetCellValue(string.Empty);
                }


                

                //table heading row

                
                row = sheet1.CreateRow(4);
                for (int j = 0; j < dt.Columns.Count; j++)
                {

                    cell = row.CreateCell(j);
                    String columnName = dt.Columns[j].ToString();
                    cell.SetCellValue(columnName);
                    cell.CellStyle = cellStyle3;
                    sheet1.AutoSizeColumn(j);

                }

                //loops through data
                
                for (int i = 0; i <dt.Rows.Count; i++)
                {
                   
                    row = sheet1.CreateRow(5+i);
                    
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {

                        cell = row.CreateCell(j);
                        String columnName = dt.Columns[j].ToString();
                        cell.SetCellValue(dt.Rows[i][columnName].ToString());

                        cell.CellStyle = (i%2==0)?cellStyle4: cellStyle5;
                        sheet1.AutoSizeColumn(j);
                        
                    }
                }
                
                //var response = new HttpResponseMessage();
                using (var exportData = new MemoryStream())
                {
                    //Response.Clear();
                    workbook.Write(exportData);
                    fileresult = exportData.GetBuffer();


                }

                return fileresult;

            }
            catch (Exception)
            {
                //error
                return null;
            }


        }
        
        public static DataTable CopyToDataTable<T>(IEnumerable<T> arr, List<DTObject> kvpList)
        {
            DataRow rw;
            DataTable dt = new DataTable();
            

            foreach (var item in kvpList)
            {
                dt.Columns.Add(item.DisplayName);
            }

            foreach(T row in arr)
            {
                rw = dt.NewRow();
                foreach (var item in kvpList)
                {
                    switch (row.GetType().GetProperty(item.ServerObjectName).PropertyType.Name)
                    {
                        case "DateTime":
                            rw[item.DisplayName] =((DateTime)row.GetType().GetProperty(item.ServerObjectName).GetValue(row, null)).ToShortDateString();
                            break;
                        case "Decimal":
                            rw[item.DisplayName] =Convert.ToDouble(row.GetType().GetProperty(item.ServerObjectName).GetValue(row, null)).ToString("#,###.#0");
                            break;
                         default:
                            rw[item.DisplayName] = row.GetType().GetProperty(item.ServerObjectName).GetValue(row, null);
                            break;
                        
                    }
                    

                }
                dt.Rows.Add(rw);
            }
            
            return dt;
        }

        //public static Byte[] ExportToPdf(string HtmlData,string css)
        //{
        //    // variables  
        //    Byte[] bytes;

        //    // do some additional cleansing to handle some scenarios that are out of control with the html data  
        //    HtmlData = HtmlData.Replace("<br>", "<br />");

        //    // convert html to pdf  
        //    try
        //    {
        //        // create a stream that we can write to, in this case a MemoryStream  
        //        using (var stream = new MemoryStream())
        //        {
        //            // create an iTextSharp Document which is an abstraction of a PDF but **NOT** a PDF  
        //            using (var document = new iTextSharp.text.Document(PageSize.A4, 10, 10, 10, 10))
        //            {
        //                // create a writer that's bound to our PDF abstraction and our stream  
        //                using (var writer = PdfWriter.GetInstance(document, stream))
        //                {
        //                    // open the document for writing  
        //                    document.Open();
                            
        //                    using (var msCss = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(css)))
        //                    {
        //                        using (var msHtml = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(HtmlData)))
        //                        {

        //                            //Parse the HTML
        //                            iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, msHtml, msCss);
        //                        }
        //                    }

                            
        //                    document.Close();
        //                }
        //            }

        //            // get bytes from stream  
        //            bytes = stream.ToArray();

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        bytes = null;
        //    }

        //    // return  
        //    return bytes;
        //}
    }

    public class DTObject
    {
        public string ServerObjectName { get; set; }
        public string DisplayName { get; set; }
        public string DataTypeName { get; set; }
    }

    public class ExcelReportProperties
    {
        public string ReportTitle { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddressLine { get; set; }
        public string CompanyLogoUrl { get; set; }

    }
}

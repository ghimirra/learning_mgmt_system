﻿/* 
Model for [dbo].[Member] 
Created by: Prashant 
Created On: 17/06/2018 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_Member")]
    public class Member : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MemberId { get; set; }


     
        public Guid OrganizationId { get; set; }


        [Required]
        [MaxLength(150)]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }


        [Required]
        [MaxLength(150)]
        [Display(Name = "LastName")]
        public string LastName { get; set; }

        
        public int SystemRoleId { get; set; }


        [MaxLength(250)]
        [Display(Name = "Designation")]
        public string Designation { get; set; }


        [MaxLength(250)]
        [Display(Name = "Level")]
        public string Level { get; set; }

        [Display(Name = "LevelId")]
        public int? LevelId { get; set; }


        [MaxLength(250)]
        [Display(Name = "Department")]
        public string Department { get; set; }


        [MaxLength(250)]
        [Display(Name = "Address")]
        public string Address { get; set; }


        [MaxLength(150)]
        [Display(Name = "PrimaryContactEmail")]
        public string PrimaryContactEmail { get; set; }


        [MaxLength(25)]
        [Display(Name = "Phone")]
        public string Phone { get; set; }


        [MaxLength(25)]
        [Display(Name = "Mobile")]
        public string Mobile { get; set; }


        [MaxLength(500)]
        [Display(Name = "ProfileImageUrl")]
        public string ProfileImageUrl { get; set; }


        [MaxLength(200)]
        [Display(Name = "Education")]
        public string Education { get; set; }


        [MaxLength(250)]
        [Display(Name = "Notes")]
        public string Notes { get; set; }

        public string CompanyName { get; set; }
        public string Position { get; set; }

    }

    /* Member View Model */
    public class MemberViewModel
    {
        public Int64 RowNumber { get; set; }
        public int MemberId { get; set; }
        public Guid OrganizationId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int SystemRoleId { get; set; }
        public string Designation { get; set; }
        public string Level { get; set; }
        public int LevelId { get; set; }
        public string Department { get; set; }
        public string Address { get; set; }
        public string PrimaryContactEmail { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string ProfileImageUrl { get; set; }
        public string Education { get; set; }
        public string Notes { get; set; }

        public string Position { get; set; }
        public string CompanyName { get; set; }

        public int TotalCount { get; set; }
    }

    /* Member View Model (Input) */
    public class MemberViewModel_Input
    {
        public int? MemberId { get; set; }
        public string Designation { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
        public string Keyword { get; set; }
    }
}

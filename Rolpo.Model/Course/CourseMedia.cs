﻿/* 
Model for [dbo].[CourseMedia] 
Created by: Prashant 
Created On: 26/06/2018 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_CourseMedia")]
    public class CourseMedia : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CourseMediaId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public int OrganizationId { get; set; }


        [Required]
        [Display(Name = "CourseId")]
        public int CourseId { get; set; }


        [Display(Name = "TopicId")]
        public int? TopicId { get; set; }


    
        [MaxLength(500)]
        [Display(Name = "MediaURL")]
        public string MediaURL { get; set; }


        [Required]
        [Display(Name = "MediaType")]
        public int MediaType { get; set; }


        
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

    }

    /* CourseMedia View Model */
    public class CourseMediaViewModel
    {
        public Int64 RowNumber { get; set; }
        public int CourseMediaId { get; set; }
        public int CourseId { get; set; }
        public int? TopicId { get; set; }
        public string MediaURL { get; set; }
        public int MediaType { get; set; }
        public string Remarks { get; set; }
        public string Module { get; set; }
        public string MediaTypeName { get; set; }

        public int TotalCount { get; set; }
    }

    /* CourseMedia View Model (Input) */
    public class CourseMediaViewModel_Input
    {
        public int? CourseMediaId { get; set; }
        public int? CourseId { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }
}

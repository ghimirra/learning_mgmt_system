﻿/* 
Model for [dbo].[Course] 
Created by: Prashant 
Created On: 22/06/2018 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_Course")]
    public class Course : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CourseId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public Guid OrganizationId { get; set; }


        [Required]
        [MaxLength(500)]
        [Display(Name = "Course Name")]
        public string CourseFullName { get; set; }


        [Required]
        [MaxLength(100)]
        [Display(Name = "Course Code")]
        public string CourseCode { get; set; }


        [Required]
        [MaxLength(200)]
        [Display(Name = "Short Name")]
        public string CourseShortName { get; set; }


        [MaxLength(1000)]
        [Display(Name = "Description")]
        public string CourseDescription { get; set; }


        [Display(Name = "Course Category")]
        public int CourseCategoryId { get; set; }


        [Display(Name = "Sub Category")]
        public int CourseSubCategoryId { get; set; }


        [Display(Name = "Course Level")]
        public int CourseLevelId { get; set; }

         
        [Display(Name = "Course Content")]
        public string CourseContent { get; set; }


        [Display(Name = "Course Period")]
        public int CoursePeriod { get; set; }


        [Display(Name = "Period Type")]
        public int CoursePeriodTypeId { get; set; }
         
        [Display(Name = "Trainee Level")]
        public string TraineeLevelDesc { get; set; }

        [NotMapped]
        public string TraineeLevelsJSON { get; set; }

        [NotMapped]
        public string TraineeLevelsXML { get; set; }

    }

    /* Course View Model */
    public class CourseViewModel
    {
        public Int64 RowNumber { get; set; }
        public int CourseId { get; set; }
        public string CourseFullName { get; set; }
        public string CourseCode { get; set; }
        public string CourseShortName { get; set; }
        public string CourseDescription { get; set; }
        public int CourseCategoryId { get; set; }
        public string CourseCategory { get; set; }
        public string CourseSubCategory { get; set; }

        public int CourseSubCategoryId { get; set; }
        public int CourseLevelId { get; set; }
        public string LevelName { get; set; }
        public string CourseContent { get; set; }
        public int CoursePeriod { get; set; }
        public int CoursePeriodTypeId { get; set; }
        public string PeriodTypeName  { get; set; }
        public string TraineeLevelDesc { get; set; }
        public string TraineeLevelsJSON { get; set; }
        public int TotalCount { get; set; }
    }

    /* Course View Model (Input) */
    public class CourseViewModel_Input
    {
        public int? CourseId { get; set; }
        public string CourseFullName { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }
}

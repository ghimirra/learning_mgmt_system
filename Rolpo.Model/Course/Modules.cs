﻿/* 
Model for [dbo].[Modules] 
Created by: Prashant 
Created On: 22/06/2018 
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rolpo.Model
{
    [Table("TBL_Modules")]
    public class TModules : AuditableEntity<long>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ModuleId { get; set; }


        [NotMapped]
        [Display(Name = "OrganizationId")]
        public Guid OrganizationId { get; set; }


        [Required]
        [MaxLength(500)]
        [Display(Name = "Title")]
        public string ModuleTitle { get; set; }


        [MaxLength(1000)]
        [Display(Name = "Description")]
        public string ModuleDescription { get; set; }

         
        [Display(Name = "Content")]
        public string ModuleContent { get; set; }


        [Required]
        [Display(Name = "AssociatedCourseId")]
        public int AssociatedCourseId { get; set; }


        [Display(Name = "Period")]
        public int ModulePeriod { get; set; }


        [Display(Name = "Period Type")]
        public int ModulePeriodTypeId { get; set; }

    }

    /* Modules View Model */
    public class ModulesViewModel
    {
        public Int64 RowNumber { get; set; }
        public int ModuleId { get; set; }
        public string ModuleTitle { get; set; }
        public string ModuleDescription { get; set; }
        public string ModuleContent { get; set; }
        public int AssociatedCourseId { get; set; }
        public int ModulePeriod { get; set; }
        public int ModulePeriodTypeId { get; set; }
        public string PeriodTypeName { get; set; }
        public int TotalCount { get; set; }
    }

    /* Modules View Model (Input) */
    public class ModulesViewModel_Input
    {
        public int? ModuleId { get; set; }
        public string ModuleTitle { get; set; }
        public int? AssociatedCourseId { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? ShowAll { get; set; }
    }
}
